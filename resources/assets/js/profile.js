import VueMaterial from 'vue-material'

Vue.use(VueMaterial)

Vue.component('profile-form', require('./components/Profile/index.vue'));
Vue.component('company-form', require('./components/Profile/company.vue'));
Vue.component('settings-form', require('./components/Profile/settings.vue'));

new Vue({
    el: '#app'
});
