import VueMaterial from 'vue-material'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default.css'


Vue.use(VueMaterial)

Vue.component('registration-form', require('./components/Auth/register.vue'));
Vue.component('login-form', require('./components/Auth/login.vue'));
Vue.component('reset-password', require('./components/Auth/resetPassword.vue'));
Vue.component('reset-email', require('./components/Auth/resetEmail.vue'));
Vue.component('create-password', require('./components/Auth/createPassword.vue'));

new Vue({
    el: '#app'
});
