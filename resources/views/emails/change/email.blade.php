@component('mail::message')
Hello Superuser,

Agent, having contact number {{ $phone }} has requested to change email account to {{ $email }}.

@component('mail::button', ['url' => route('agents.index')])
View Agent
@endcomponent
Please verify and proceed.

Thanks,<br>
{{ config('app.name') }}
@endcomponent
