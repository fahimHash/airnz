@component('mail::message')
Hello Superuser,

Agent, {{ $name }} would like to re-activate profile.

@component('mail::button', ['url' => $route])
View Agent
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
