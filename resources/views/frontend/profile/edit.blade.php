@extends('frontend.layouts.template')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/vue-material/vue-material.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vue-material/default.css') }}">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header content-header-campaigns content-header-global">
            <div class="page-title">
                <h3>Profile</h3>
            </div>
            <ul>
                <li><a href="{{url('/profile')}}">Dashboard</a></li>
                <li><a class="current" href="{{url('/profile/edit')}}">Personal Details</a></li>
                <li><a href="{{url('/company')}}">Company Details</a></li>
                <li><a href="{{url('/activity-feed')}}">Activity Feed</a></li>
                <li><a href="{{url('/my-account')}}">Account Settings</a></li>
                <li><a href="{{ route('mybdm') }}">My BDM</a></li>
            </ul>
        </section>
        <!-- Main content -->
        <section class="content content-personal-details">
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-12">
                            @include('frontend.layouts.message')
                            <div class="box box-full">
                                <div class="box-header box-header-img text-center" style="background-image:url({{ url('assets/img/bg-profile.jpg')}});">
                                    <!-- <h2>Personal Details</h2> -->
                                </div>
                                <div class="box-body content-personal-details-edit no-padding">
                                    {!! Form::model($user, [
                                        'method' => 'PATCH',
                                        'url' => ['/profile/uploadPhoto'],
                                        'enctype'=>'multipart/form-data',
                                        'id'=>'profileEdit',
                                    ]) !!}
                                    <div class="row">
                                        <div class="col-md-8 col-md-offset-2">
                                            <ul class="users-list clearfix text-center">
                                                <div class="showimage-wrap">
                                                    <span id="showimage" class="user-profile-img-circle" style="background-image: url('{{ url('assets/img/'.$user->image) }}');"></span>
                                                </div>
                                                <li>
                                                    Upload your profile photo<br>
                                                    <input class="btn btn-sm btn-black" name="upload_photo"
                                                           id="upload_photo" type="file"
                                                           style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;">
                                                    <label for="upload_photo"
                                                           class="btn btn-sm btn-black">UPLOAD</label>
                                                    <a href="{{ url('/profile/removePhoto?_token'.csrf_token()) }}"
                                                       onclick="return confirm('Are you sure to delete this photo?');"
                                                       class="btn btn-sm btn-white">
                                                        DELETE
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    {!! Form::close() !!}
                                    <div id="app">
                                        <profile-form
                                                title="{{ $user->title }}"
                                                first-name="{{ $first_name }}"
                                                last-name="{{ $last_name }}"
                                                email="{{ $user->email }}"
                                                phone-number="{{ $user->phoneNumber }}"
                                                industry-start-date="{{ $user->startDate }}"
                                                address1="{{ $user->address1  }}"
                                                address2="{{ $user->address2  }}"
                                                address3="{{ $user->address3  }}"
                                                suburb="{{ $user->suburb }}"
                                                state="{{ $user->state->name }}"
                                                postcode="{{ $user->postcode }}"
                                        ></profile-form>
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script src="{{ mix('js/profile.js') }}"></script>
    <script>
        $(function () {
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#showimage').removeAttr("style");
                        $('#showimage').css('background-image', 'url(' + e.target.result + ')');
                        $('#showimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#upload_photo").change(function () {
                readURL(this);
            });
            $('#upload_photo').change(function() {
                $('#profileEdit').submit();
            });
        });
    </script>
@endpush
