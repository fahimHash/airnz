@extends('frontend.layouts.template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header content-header-campaigns content-header-global">
            <div class="page-title">
                <h3>Profile</h3>
            </div>
            <ul>
              <li><a href="{{url('/profile')}}">Dashboard</a></li>
              <li><a href="{{url('/profile/edit')}}">Personal Details</a></li>
              <li><a href="{{url('/company')}}">Company Details</a></li>
              <li><a href="{{url('/activity-feed')}}">Activity Feed</a></li>
              <li><a href="{{url('/my-account')}}">Account Settings</a></li>
              <li><a class="current" href="{{ route('mybdm') }}">My BDM</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content content-personal-details">

            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">

                        <div class="col-md-12">
                            @include('frontend.layouts.message')
                            <div class="box box-full text-center">
                                <div class="box-header box-header-img"
                                     style="background-image:url({{url('assets/img/bg-profile.jpg')}});">
                                    <!-- <h2>Your Business Development Manager</h2> -->
                                </div>
                                <div class="box-body no-padding">

                                    <ul class="users-list clearfix">
                                        <img src="
                                                @if(!empty($manager->image))
                                        {{url('assets/img/'.$manager->image)}}
                                        @else
                                        {{ url('assets/img/default-image.png') }}
                                        @endif
                                                " class="img-circle">
                                        <li>
                                            <h4>{{isset($manager->name) ? $manager->name : 'Agency Support'}}</h4>
                                            <h5>Air New Zealand NSW Business Development Manager</h5>
                                            <hr>
                                        </li>
                                        <li><b>Industry Experience:</b><br>4 Years</li>
                                        <li><b>Favourite Air New Zealand Destination:</b><br>New Zealand. There is so much diversity that every visit you can do something new. <br>It’s jam packed with world class vineyards, loads to do outdoors, and epic landscapes!  Plus it’s where I call home.</li>
                                        <li><b>Top Travel Tip:</b><br>Plan your trip just enough to leave some room for spontaneity</li>
                                        <li><b>Fun fact about me:</b><br>I grew up on a farm, and spent a lot of my childhood rounding up sheep.</li>
                                    </ul>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
