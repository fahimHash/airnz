@extends('frontend.layouts.template')

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header content-header-campaigns content-header-global">
          <div class="page-title">
              <h3>Profile</h3>
          </div>
          <ul>
            <li><a href="{{url('/profile')}}">Dashboard</a></li>
            <li><a href="{{url('/profile/edit')}}">Personal Details</a></li>
            <li><a href="{{url('/company')}}">Company Details</a></li>
            <li><a class="current" href="{{url('/activity-feed')}}">Activity Feed</a></li>
            <li><a href="{{url('/my-account')}}">Account Settings</a></li>
            <li><a href="{{ route('mybdm') }}">My BDM</a></li>
          </ul>
      </section>

      <!-- Main content -->
    <section class="content content-company-feed">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-10 col-md-offset-1">
          <div class="row">

            <div class="col-md-12">
              <div class="box box-full">
                <div class="box-header box-header-top box-header-top-profile">
                  <h3>Activity Feed</h3>
                  <hr>
                  <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, nesciunt.</p>
                </div>
                <!-- /.box-body -->
                <div class="row">
                  <div class="col-md-8 col-md-offset-2">
                    <ul class="anz-feed-list anz-feed-list-all clearfix" id="post-activity">
                      @include('frontend.profile.activity.single')
                    </ul>
                    <div class="content-company-feed-more text-center">
                      <a href="#" id="load-more">SCROLL FOR MORE</a>
                    </div>
                  </div>
                </div>
              </div>

            </div>

          </div>
          <!-- /.row -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

    </section>
    <!-- /.content -->

    </div>
    <!-- /.content-wrapper -->
@endsection

@push('scripts')
  <script type="text/javascript">
	var page = 1;
	$(window).scroll(function() {
	    if($(window).scrollTop() + $(window).height() >= $(document).height()) {
	        page++;
	        loadMoreData(page);
	    }
	});

	function loadMoreData(page){
	  axios.get('/activity-feed?page='+page)
    .then(function (response) {
      if(response.data.html == ""){
          $('#post-activity').append('<li><p>That&#39;s all for now!</p></li>');
          $('#load-more').hide();
          return;
      }
      $("#post-activity").append(response.data.html);
    })
    .catch(function (error) {
      console.log(error);
    });
	}
</script>
@endpush
