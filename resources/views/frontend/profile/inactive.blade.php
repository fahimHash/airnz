@extends('frontend.layouts.template')
@section('content')

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

      <div class="anz-top-menu">
        <ul>
          <li><a class="active" href="">Personal Details</a></li>
          <li><a href="">Company Details</a></li>
          <li><a href="">Activity Feed</a></li>
          <li><a href="">Account Settings</a></li>
          <li><a href="">My BDM</a></li>
        </ul>
      </div>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <div class="col-md-10 col-md-offset-1">
          <div class="row">

            <div class="owl-carousel-profile">
              <div>
                <div class="col-md-6">
                  <!-- USERS LIST -->
                  <div class="box">
                    <!-- /.box-header -->
                    <div class="anz-box-link">
                      <a href="#">Contact</a>
                    </div>
                    <h3>My BDM</h3>
                    <div class="box-body no-padding">
                      <ul class="users-list clearfix">
                        <img src="assets/img/_Image3.png" class="img-circle">
                        <li>
                          <h4>{{isset($manager->name) ? $manager->name : 'Agency Support'}}</h4>
                          <span class="users-list-date">Air New Zealand, DBM</span>
                          <hr>
                        </li>
                      </ul>
                      <div class="anz-bdm-info">
                        <div>{{isset($manager->phoneNumber) ? $manager->phoneNumber : '131 000' }}</div>
                        <div class="anz-dm-email"><a href="mailto:{{isset($manager->email) ? $manager->email : 'agencysupport@airnz.com.au'}}" target="_top">{{isset($manager->email) ? $manager->email : 'agencysupport@airnz.com.au'}}</a></div>
                        <div><a href="#" data-toggle="control-sidebar" class="btn btn-default btn-lg sidebar-right-toggle">CONTACT BDM</a></div>
                      </div>
                      <!-- /.users-list -->
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <!--/.box -->
                </div>
                <!-- /.col -->
              </div>

            </div>

          </div>
          <!-- /.row -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
