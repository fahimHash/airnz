@extends('frontend.layouts.template')

@push('styles')
    <link rel="stylesheet" href="{{ url('libs/owl.carousel/owl.carousel.css') }}">
@endpush

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header content-header-campaigns content-header-global">
            <div class="page-title">
                <h3>Profile</h3>
            </div>
            <ul>
              <li><a class="current" href="{{url('/profile')}}">Dashboard</a></li>
              <li><a href="{{url('/profile/edit')}}">Personal Details</a></li>
              <li><a href="{{url('/company')}}">Company Details</a></li>
              <li><a href="{{url('/activity-feed')}}">Activity Feed</a></li>
              <li><a href="{{url('/my-account')}}">Account Settings</a></li>
              <li><a href="{{ route('mybdm') }}">My BDM</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">

                        <div class="owl-carousel-profile">

                            <div>
                                <div class="col-md-6">
                                    <!-- USERS LIST -->
                                    <div class="box">
                                        <!-- /.box-header -->
                                        <div class="anz-box-link">
                                            <a href="{{ url('/profile/edit')}}">View</a>
                                        </div>
                                        <h3>Personal Details</h3>
                                        <div class="box-body no-padding">
                                            <ul class="users-list clearfix">
                                                <span class="user-profile-img-circle-small" style="background-image: url('{{ url('assets/img/'.$user->image) }}');"></span>
                                                <li>
                                                    <h4>{{ $user->name}}</h4>
                                                    <span class="users-list-date">Joined <strong>duo</strong> in {{ $user->joinedIn()}}</span>
                                                    <hr>
                                                </li>
                                            </ul>
                                            <table>
                                                <tr>
                                                    <td>Email:</td>
                                                    <td>
                                                        <div class="users-list-email">{{ $user->email}}</div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>Phone:</td>
                                                    <td>{{ $user->phoneNumber}}</td>
                                                </tr>
                                                <tr>
                                                    <td>State:</td>
                                                    <td>{{ $user->state->name}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Industry Experience:</td>
                                                    <td>{{ $user->tenure()}} Years</td>
                                                </tr>
                                            </table>
                                            <!-- /.users-list -->
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!--/.box -->
                                </div>
                                <!-- /.col -->
                            </div>

                            <div>
                                <div class="col-md-6">
                                    <!-- USERS LIST -->
                                    <div class="box">
                                        <!-- /.box-header -->
                                        <div class="anz-box-link">
                                            <a href="{{url('/company')}}">View</a>
                                        </div>
                                        <h3>Company Details</h3>
                                        <div class="box-body no-padding">
                                            <ul class="users-list clearfix">
                                                <img src="
                                                @if(isset($job->workplace->consortium->logo))
                                                {{url('assets/img/'.$job->workplace->consortium->logo)}}
                                                @else
                                                {{ url('assets/img/default-image.png') }}
                                                @endif
                                                        " class="img-circle">
                                                <li>
                                                    @if(isset($job->workplace->agency->name))
                                                        <h4>{{$job->workplace->agency->name}}</h4>
                                                        <span class="users-list-date">{{$job->workplace->consortium->name}}</span>
                                                    @else
                                                        <h4>Add in Company details</h4>
                                                    @endif
                                                    <hr>
                                                </li>
                                            </ul>
                                            <table>
                                                <tr>
                                                    <td>PCC:</td>
                                                    <td>{{isset($job->workplace->pcc) ? $job->workplace->pcc : 'Add in Company details'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Store Branch Name:</td>
                                                    <td>{{isset($job->workplace->name) ? $job->workplace->name : 'Add in Company details'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Position:</td>
                                                    <td>{{isset($job->position) ? $job->position : '--'}}</td>
                                                </tr>
                                                <tr>
                                                    <td>Employed since:</td>
                                                    <td>{{isset($job->startDate) ? $job->startDate : '--'}}</td>
                                                </tr>
                                            </table>
                                            <!-- /.users-list -->
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!--/.box -->
                                </div>
                                <!-- /.col -->
                            </div>

                            <div>
                                <div class="col-md-6">
                                    <!-- USERS LIST -->
                                    <div class="box">
                                        <!-- /.box-header -->
                                        <div class="anz-box-link">
                                            <a href="{{url('/activity-feed')}}">View Feed</a>
                                        </div>
                                        <h3>Activity Feed</h3>
                                        <div class="box-body no-padding">
                                            <ul class="anz-feed-list anz-feed-list-dashboard clearfix">
                                                @if(count($activities) > 0)
                                                    @foreach ($activities as $activity)
                                                        <li>
                                                            <div class="anz-feed-list-date">{{ $activity->created_at->diffForHumans() }}</div>
                                                            <i class="icon-sales"></i>
                                                            <a class="users-list-name"
                                                               href="#">{{$activity->module}}</a>
                                                            <p>{{$activity->activity_message}}</p>
                                                        </li>
                                                    @endforeach
                                                @else
                                                    <li>
                                                        <div class="anz-feed-list-date">Few minutes ago</div>
                                                        <i class="icon-sales"></i>
                                                        <a class="users-list-name" href="#">Your Activity</a>
                                                        <p>Add a new Sale or join a campaign now?</p>
                                                    </li>
                                                @endif
                                            </ul>
                                            <!-- /.users-list -->
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!--/.box -->
                                </div>
                                <!-- /.col -->
                            </div>

                            <div>
                                <div class="col-md-6">
                                    <!-- USERS LIST -->
                                    <div class="box">
                                        <!-- /.box-header -->
                                        <div class="anz-box-link">
                                            <a href="{{ route('mybdm') }}">Find more about me</a>
                                        </div>
                                        <h3>My BDM</h3>
                                        <div class="box-body no-padding">
                                            <ul class="users-list clearfix">
                                                <img src="
                                                @if(!empty($manager->image))
                                                {{url('assets/img/'.$manager->image)}}
                                                @else
                                                {{ url('assets/img/default-image.png') }}
                                                @endif
                                                        " class="img-circle">
                                                <li>
                                                    <h4>{{isset($manager->name) ? $manager->name : 'Agency Support'}}</h4>
                                                    <span class="users-list-date">Air New Zealand, Business Development Manager</span>
                                                    <hr>
                                                </li>
                                            </ul>
                                            <div class="anz-bdm-info">
                                                {{--<div>{{isset($manager->phoneNumber) ? $manager->phoneNumber : '131 000' }}</div>--}}
                                                <div>131 000</div>
                                                {{--<div class="anz-dm-email"><a--}}
                                                            {{--href="mailto:{{isset($manager->email) ? $manager->email : 'agencysupport@airnz.com.au'}}"--}}
                                                            {{--target="_top">{{isset($manager->email) ? $manager->email : 'agencysupport@airnz.com.au'}}</a>--}}
                                                {{--</div>--}}
                                                <div class="anz-dm-email"><a
                                                            href="mailto:agencysupport@airnz.com.au"
                                                            target="_top">agencysupport@airnz.com.au</a>
                                                </div>
                                                <div><a href="#" data-toggle="control-sidebar"
                                                        class="btn btn-default btn-lg sidebar-right-toggle">AGENCY
                                                        SUPPORT</a></div>
                                            </div>
                                            <!-- /.users-list -->
                                        </div>
                                        <!-- /.box-body -->
                                    </div>
                                    <!--/.box -->
                                </div>
                                <!-- /.col -->
                            </div>

                        </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('scripts')
    <script src="{{ url('libs/owl.carousel/owl.carousel.js') }}"></script>
    <script>
        $(document).ready(function () {
            if ($(window).innerWidth() < 768) {
                $(".owl-carousel-profile").addClass("owl-carousel");
                $('.owl-carousel').owlCarousel({
                    loop: true,
                    //nav: true,
                    dots: true,
                    items: 1,
                    //autoplay: 3000,
                });
            }
        });
    </script>
@endpush
