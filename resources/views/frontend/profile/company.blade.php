@extends('frontend.layouts.template')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/vue-material/vue-material.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vue-material/default.css') }}">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header content-header-campaigns content-header-global">
            <div class="page-title">
                <h3>Profile</h3>
            </div>
            <ul>
              <li><a href="{{url('/profile')}}">Dashboard</a></li>
              <li><a href="{{url('/profile/edit')}}">Personal Details</a></li>
              <li><a class="current" href="{{url('/company')}}">Company Details</a></li>
              <li><a href="{{url('/activity-feed')}}">Activity Feed</a></li>
              <li><a href="{{url('/my-account')}}">Account Settings</a></li>
              <li><a href="{{ route('mybdm') }}">My BDM</a></li>
            </ul>
        </section>

        <!-- Main content -->
        <section class="content content-company-details">

            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">

                        <div class="col-md-12">
                            @include('frontend.layouts.message')
                            <div class="box box-full">
                                <div class="box-header box-header-top box-header-top-profile">
                                    <h3>Experience</h3>
                                    <hr>
                                    <p>This is where you can keep track of your career. Add each position to help personalise your duo experience - but remember, your sales and training data is unique to you, not your company.</p>
                                    <a class="add-position" data-toggle="collapse" href="#addPosition"
                                       aria-expanded="false">
                                        Add position <span><i class="icon-ANE1227_TEP_icon_set_V1_add"></i></span>
                                    </a>
                                </div>
                                <!-- /.box-body -->
                            </div>

                            <div class="collapse" id="addPosition">
                                <div class="box box-full">
                                    <div class="box-header box-header-top clearfix">
                                        <a class="add-position" data-toggle="collapse" href="#addPosition"
                                           aria-expanded="false">
                                            X
                                        </a>
                                        <div id="app">
                                            <company-form></company-form>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>
                            </div>

                            <div class="row">
                                <section class="col-md-12 connectedSortable">

                                    @foreach ($jobs as $job)
                                        <div class="box box-full">
                                            <div class="box-header pull-right">
                                                <i class="fa fa-angle-up"></i><br>
                                                <i class="fa fa-angle-down"></i>
                                            </div>

                                            <a class="edit-position pull-right collapsed" data-toggle="collapse"
                                               href="#company-{{$loop->iteration}}" aria-expanded="false">
                                                Edit position <i class="fa fa-angle-down"></i>
                                            </a>

                                            <div class="box-body">
                                                <img src="{{url('assets/img/'.$job->workplace->consortium->logo)}}"
                                                     class="img-circle pull-left">
                                                <div class="company-details">
                                                    <h3>{{$job->workplace->agency->name }}
                                                        <span>({{ $job->workplace->consortium->name }}
                                                            )</span></h3>
                                                    <h5>{{$job->position }}</h5>
                                                    <p>{{$job->workplace->name or ''}} {{$job->workplace->state->name or ''}}
                                                        <br>
                                                        @if(isset($job->startDate))
                                                            {{ \Carbon\Carbon::createFromFormat('Y-m-d', $job->startDate)->format('F Y')}}
                                                        @endif
                                                        @if(isset($job->endDate))
                                                            - {{\Carbon\Carbon::createFromFormat('Y-m-d',$job->endDate)->format('F Y')}}
                                                        @else
                                                            - Present
                                                        @endif
                                                    </p>
                                                </div>
                                            </div>

                                            <div class="collapse collapse-company" id="company-{{$loop->iteration}}">
                                                {!! Form::model($job, [
                                                    'method' => 'PATCH',
                                                    'url' => ['/company', $job->id],
                                                    'class' => 'anz-default anz-add-company'
                                                ]) !!}
                                                <div class="row">

                                                    <div class="col-md-4 col-md-offset-2">
                                                        <div class="form-group">
                                                            <label for="">Affiliated Agency Group</label>
                                                        {!! Form::text('consortium', $job->workplace->consortium->name, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Agency Brand Name</label>
                                                            {!! Form::text('travel_agency', $job->workplace->agency->name, ['class' => 'form-control', 'disabled' => 'disabled']) !!}
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Job Title</label>
                                                            <select class="btn btn-select btn-select-light dropdown" id="position" name="position" data-width="100%" disabled="disabled">
                                                                <option selected="selected" disabled="disabled" hidden="hidden" value="">
                                                                    I am a...
                                                                </option>
                                                                <option @if($job->position == 'In-store') selected="selected" @endif>In-store</option>
                                                                <option @if($job->position == 'Office-based Travel Consultant') selected="selected" @endif>Office-based Travel Consultant</option>
                                                                <option @if($job->position == 'Home-based Travel Consultant') selected="selected" @endif>Home-based Travel Consultant</option>
                                                                <option @if($job->position == 'Mobile Travel Consultant') selected="selected" @endif>Mobile Travel Consultant</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Store Branch Name</label>
                                                            {{ Form::text('branch', $job->workplace->name, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Agency Pseudo Code (PCC)</label>
                                                            {{ Form::text('pcc', $job->workplace->pcc, ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 col-md-4-right">
                                                        <div class="form-group">
                                                            <label for="">State</label>
                                                            {{ Form::text('state', $job->workplace->state->name , ['class' => 'form-control', 'disabled' => 'disabled']) }}
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">Start date</label>
                                                            {!! Form::date('startDate', $job->startDate, ['class' => 'form-control', 'id'=>'startDate']) !!}
                                                        </div>
                                                        @if($job->active != 1)
                                                        <div class="form-group">
                                                            <label for="">End Date</label>
                                                            {!! Form::date('endDate', $job->endDate, ['class' => 'form-control','id'=>'endDate']) !!}
                                                        </div>
                                                        @endif
                                                        <div class="form-group">
                                                            <div class="checkbox">
                                                                <label>
                                                                    @if($job->active == 1)
                                                                        {{ Form::checkbox('working',1, true, ['disabled' => 'disabled']) }}
                                                                        I'm currently working here
                                                                    @endif
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="text-center">
                                                    {!! Form::button('SAVE & CLOSE', ['class' => 'btn btn-black btn-lg','type' => 'submit']) !!}
                                                    <button type="submit" class="btn btn-black btn-lg btn-white"
                                                            data-toggle="collapse" href="#company-{{$loop->iteration}}"
                                                            aria-expanded="false">CANCEL
                                                    </button>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    @endforeach
                                </section>
                            </div>
                            <!-- /.row (main row) -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('scripts')
    <script src="{{ mix('js/profile.js') }}"></script>
    <script src="{{ url('libs/jquery-ui.min.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            "use strict";

            $(".connectedSortable").sortable({
                placeholder: "sort-highlight",
                connectWith: ".connectedSortable",
                handle: ".box-header, .nav-tabs",
                forcePlaceholderSize: true,
                zIndex: 999999
            });
            $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
        });
    </script>
@endpush
