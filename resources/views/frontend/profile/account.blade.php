@extends('frontend.layouts.template')
@push('styles')
    <link rel="stylesheet" href="{{ asset('css/vue-material/vue-material.css') }}">
    <link rel="stylesheet" href="{{ asset('css/vue-material/default.css') }}">
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header content-header-campaigns content-header-global">
            <div class="page-title">
                <h3>Profile</h3>
            </div>
            <ul>
                <li><a href="{{url('/profile')}}">Dashboard</a></li>
                <li><a href="{{url('/profile/edit')}}">Personal Details</a></li>
                <li><a href="{{url('/company')}}">Company Details</a></li>
                <li><a href="{{url('/activity-feed')}}">Activity Feed</a></li>
                <li><a class="current" href="{{url('/my-account')}}">Account Settings</a></li>
                <li><a href="{{ route('mybdm') }}">My BDM</a></li>
            </ul>
        </section>
        <!-- Main content -->
        <section class="content content-account-settings">
            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">
                        <div class="col-md-12">
                            @include('frontend.layouts.message')
                            <div class="box box-full">
                                <div class="box-header box-header-top box-header-top-profile">
                                    <h3>Welcome</h3>
                                    <hr>
                                    <p>You can make all the basic changes to your account right here. If you require any
                                        further information, please contact your Business Development Manager.</p>
                                </div>
                                <!-- /.box-body -->
                            </div>
                            <div class="row">
                                <section class="col-md-12 connectedSortable">
                                    <div class="box box-full">
                                        <a class="edit-position pull-right collapsed" data-toggle="collapse"
                                           href="#editPassword" aria-expanded="false">
                                            Edit Password <i class="fa fa-angle-down"></i>
                                        </a>
                                        <div class="box-body">
                                            <div class="company-details">
                                                <h3>Update Password</h3>
                                                <!-- <hr> -->
                                                <p>Make sure you protect your personal info by updating your 8 digit
                                                    password regularly. Your password must contain at least one
                                                    number.</p>
                                            </div>
                                        </div>
                                        <div class="collapse collapse-company" id="editPassword">
                                            <div id="app">
                                                <settings-form></settings-form>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box box-full">
                                        <a class="edit-position pull-right collapsed" data-toggle="collapse"
                                           href="#privacyPolicy" aria-expanded="false">
                                            View Policy <i class="fa fa-angle-down"></i>
                                        </a>
                                        <div class="box-body">
                                            <div class="company-details">
                                                <h3>Privacy Policy</h3>
                                                <!-- <hr> -->
                                                <p>We take your privacy very seriously. You can review how we (Air New
                                                    Zealand and our subsidiary companies) collect and use your personal
                                                    information.</p>
                                            </div>
                                        </div>
                                        <div class="collapse collapse-company" id="privacyPolicy">
                                            <div class="privacy-policy-details">
                                                <div id="privacy-slimscroll">
                                                    <p>This Privacy Policy explains how we (Air New Zealand Limited and
                                                        our subsidiary companies) collect and use your personal
                                                        information. 
                                                        For the purposes of applicable privacy legislation, the “Data
                                                        Controller” in respect of your personal information is Air New
                                                        Zealand Limited. </p>

                                                    <p>Personal information is any piece of information that relates to
                                                        a living individual (natural person) who can be identified,
                                                        directly or indirectly.</p>

                                                    <p>You authorise Air New Zealand Limited and its subsidiaries to
                                                        collect, use, and disclose your personal information in
                                                        accordance with this Privacy Policy and also to the extent not
                                                        prohibited by applicable privacy legislation. We will deal with
                                                        your sensitive information only in accordance with sections
                                                        1.4 and 1.5 below.</p>

                                                    <p> This Privacy Policy should be read in conjunction with our
                                                        Cookie Policy.</p>

                                                    <p>We will collect the personal information you provide us and we
                                                        may also collect personal information online from your computer,
                                                        mobile or other device. We may collect from and share your
                                                        personal information with a range of third parties, including
                                                        the other passengers named in your booking, reservation agents,
                                                        travel service providers, other airline carriers, medical,
                                                        safety, aviation and security personnel. We may also disclose
                                                        your personal information to customs and immigration,
                                                        governmental and law enforcement agencies. For example, when we
                                                        are required to do so in accordance with customs, transportation
                                                        security and civil aviation laws.</p>

                                                    <p>If you are an Airpoints Member, or hold any account associated
                                                        with us, we may also collect from and share your personal
                                                        information with a range of third parties including our
                                                        Airpoints Partners and our other third party business partners.
                                                        We will deal with your personal information for a broad range of
                                                        purposes including: to facilitate the services you have
                                                        requested, to better understand your preferences, and to
                                                        generally conduct and develop our business.</p>

                                                    <p>If you choose not to provide us with the personal information
                                                        that we request then we may not be able to offer our services
                                                        and products to you.</p>

                                                    <p>If we collect personal information from you, it is likely that we
                                                        will store it in our central data storage facilities in New
                                                        Zealand and Australia. We may disclose your personal information
                                                        to third parties in countries worldwide to or through which you
                                                        are travelling, (including countries located outside the
                                                        European Economic Area) that do not have comparable laws
                                                        protecting the privacy of personal information. Such disclosures
                                                        will be made for the purposes of providing our services to you,
                                                        operating and developing our business, or for any of the
                                                        purposes outlined in this Privacy Policy.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="box box-full">
                                        <a class="edit-position pull-right collapsed" data-toggle="collapse"
                                           href="#closeAccount" aria-expanded="false">
                                            Close Account <i class="fa fa-angle-down"></i>
                                        </a>
                                        <div class="box-body">
                                            <div class="company-details">
                                                <h3>Close Account</h3>
                                                <!-- <hr> -->
                                                <p>If you wish to close your duo account, simply click on the button
                                                    below and your Business Development Manager will take care of the
                                                    rest.</p>
                                            </div>
                                        </div>
                                        <div class="collapse collapse-company" id="closeAccount">
                                            <form class="anz-default anz-close-account">
                                                <div class="row">
                                                    <div class="col-md-6 col-md-offset-3">
                                                        <i class="icon-rewards"></i>
                                                        <p>Your duo account will be closed, and you'll be logged out
                                                            immediately. If you wish to re-activate your account please
                                                            contact [email address] within 6 months before your account
                                                            is archived and all sales, training and reward data is
                                                            permanently deleted. </p>
                                                    </div>
                                                </div>
                                                <div class="text-center">
                                                    @role('minor')
                                                    <button type="button" data-toggle="modal"
                                                            data-target="#close-account" class="btn btn-black btn-lg">
                                                        CLOSE ACCOUNT
                                                    </button>
                                                    @endauth
                                                    @role('mover')
                                                    <button type="button" data-toggle="modal"
                                                            data-target="#close-account" class="btn btn-black btn-lg">
                                                        CLOSE ACCOUNT
                                                    </button>
                                                    @endauth

                                                    <button class="btn btn-black btn-lg btn-white" data-toggle="collapse"
                                                            href="#closeAccount" aria-expanded="false">CANCEL
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </section>
                            </div>
                            <!-- /.row (main row) -->
                        </div>
                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('page_specific_modal')
    <div id="close-account" class="modal-alert modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content text-center">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <img src="assets/img/ico-alert.png" alt="">
                </div>
                <div class="modal-body">
                    <h3>You're about to close your Duo account.</h3>
                    <ul>
                        <li>All my campaign data will be removed</li>
                        <li>All Sales data will removed</li>
                        <li>All Rewards will be removed</li>
                    </ul>
                    <div>
                        {!! Form::open([
                            'url' => ['/terminate'],
                        ]) !!}
                        {!! Form::submit('CLOSE ACCOUNT', ['class' => 'btn btn-black btn-lg']) !!}
                        {!! Form::close() !!}

                        <button class="btn btn-default btn-lg" data-dismiss="modal">CANCEL</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="{{ mix('js/profile.js') }}"></script>
@endpush
