@extends('frontend.layouts.template')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">

            <div class="anz-top-menu">
                <ul>
                    <li><a class="current" href="{{url('/profile')}}">Dashboard</a></li>
                    <li><a href="{{url('/profile/edit')}}">Personal Details</a></li>
                    <li><a href="{{url('/company')}}">Company Details</a></li>
                    <li><a href="{{url('/activity-feed')}}">Activity Feed</a></li>
                    <li><a href="{{url('/my-account')}}">Account Settings</a></li>
                    <li><a href="{{ route('mybdm') }}">My BDM</a></li>
                </ul>
            </div>
        </section>

        <!-- Main content -->
        <section class="content content-personal-details">

            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">

                        <div class="col-md-12">
                            @include('frontend.layouts.message')
                            <div class="box box-full text-center">
                                <div class="box-header box-header-img"
                                     style="background-image:url({{url('assets/img/bg-profile.jpg')}});">
                                    <h2>Personal Details</h2>
                                </div>
                                <div class="box-body no-padding">
                                    <span class="content-personal-details-edit"><a href="{{url('/profile/edit')}}">Edit profile <i
                                                    class="icon-ANE1227_TEP_icon_set_V1_edit"></i></a></span>
                                    <ul class="users-list clearfix">
                                        <span class="user-profile-img-circle" style="background-image: url('{{ url('assets/img/'.$user->image) }}');"></span>
                                        <li>
                                            <h4>{{$user->name}}</h4>
                                            <span class="users-list-date">Joined <strong>duo</strong> in {{$user->joinedIn()}}</span>
                                            <hr>
                                        </li>
                                    </ul>
                                    <p>{{$user->email}}<br>
                                        {{$user->phoneNumber}}<br>
                                        {{$user->state->name}}<br>
                                        Industry Experience: {{$user->tenure()}} Years</p>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>

                    </div>
                    <!-- /.row -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
