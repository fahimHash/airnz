@extends('frontend.auth.template')
@section('content')
<div class="login-logo-wrap">
  <img src="{{ url('assets/img/logo-airnz.svg')}}">
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-login panel-default">
              <div class="anz-title clearfix">
                <div class="anz-title-wrap">
                  <a href="/"><img src="{{ url('assets/img/logo-duo.svg')}}"></a>
                </div>
              </div>
                <div class="panel-body" id="app">
                    @include('frontend.layouts.message',['display' => 'false'])
                    <login-form></login-form>
                    {{--<form id="login" class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}"--}}
                          {{--data-parsley-excluded="input[type=button], input[type=submit], input[type=reset]"--}}
                          {{--data-parsley-trigger="keyup" data-parsley-validate>--}}
                        {{--{{ csrf_field() }}--}}

                        {{--<h6><span>Login</span> to <strong>duo</strong></h6>--}}

                        {{--<div class="cus-form-group">--}}
                            {{--<div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon email-address"--}}
                                 {{--data-mdc-auto-init="registerFields">--}}
                                {{--<input id="email" type="email" class="mdc-text-field__input"--}}
                                       {{--name="email"--}}
                                       {{--value="{{ old('email') }}"--}}
                                       {{--data-parsley-required--}}
                                       {{--data-parsley-maxlength="30"--}}
                                       {{--data-parsley-required-message="Please enter your work email"--}}
                                       {{--data-parsley-type-message="This is not a valid email address"--}}
                                       {{--data-parsley-validation-threshold="0"--}}
                                {{-->--}}
                                {{--<label class="mdc-floating-label" for="email-address">Email Address</label>--}}
                                {{--<div class="mdc-line-ripple" style="transform-origin: 99.5px center 0px;">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<span id="email-helper-text" class="helper-text">--}}
                                    {{--Enter the email address registered to your duo account--}}
                                {{--</span>--}}
                        {{--</div>--}}

                        {{--<div class="cus-form-group">--}}
                            {{--<div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon password"--}}
                                 {{--data-mdc-auto-init="registerFields">--}}
                                {{--<input id="password" type="password"--}}
                                       {{--class="mdc-text-field__input"--}}
                                       {{--name="password"--}}
                                       {{--data-parsley-number="1"--}}
                                       {{--data-parsley-required--}}
                                       {{--data-parsley-required-message="Please enter your password."--}}
                                       {{--data-parsley-minlength="8"--}}
                                       {{--data-parsley-minlength-message="Password must be at least 8 characters"--}}
                                       {{--data-parsley-maxlength="30"--}}
                                       {{--data-parsley-validation-threshold="0"--}}
                                {{-->--}}
                                {{--<label class="mdc-floating-label" for="password">Password</label>--}}
                                {{--<div class="mdc-line-ripple"--}}
                                     {{--style="transform-origin: 99.5px center 0px;">--}}
                                {{--</div>--}}
                            {{--</div>--}}
                            {{--<span id="password-helper-text" class="helper-text">--}}
                                    {{--Enter the password registered to your duo account--}}
                                {{--</span>--}}
                        {{--</div>--}}

                        {{--<div class="cus-form-group">--}}
                            {{--<div class="col-md-12 text-center">--}}
                                {{--<button type="submit" class="btn btn-black">--}}
                                    {{--Login--}}
                                {{--</button>--}}

                                {{--<div><a class="btn-link" href="{{ url('/password/reset') }}">--}}
                                        {{--Forgotten your password?--}}
                                {{--</a></div>--}}
                                {{--<div><a class="btn-link" href="{{ url('/register') }}">--}}
                                        {{--Or Register?--}}
                                    {{--</a></div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</form>--}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
