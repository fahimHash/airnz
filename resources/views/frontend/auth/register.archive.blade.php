@extends('frontend.layouts.register')
@push('styles')
@endpush
@section('content')
    {{--THIS FILE IS NO LONGER IN USE --}}
    <div class="login-logo-wrap">
        <img src="{{ url('assets/img/logo.png')}}">
    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="panel panel-register panel-default">
                    <div class="anz-title clearfix">
                        <div class="anz-title-wrap">
                            <strong>duo</strong> <span class="anz-tag"><span class="anz-tag-block"><span
                                            class="anz-tag-border">|</span> We're Better Together</span></span>
                        </div>
                    </div>
                    <div class="panel-body">
                      <h6><span>Registration Form</span></h6>
                      <p>Fields marked with an * are required to create your duo profile.</p>
                      <p> The information you provide will help us provide personalised engagement content
                          which best suits your needs.</p>

                        @include('frontend.layouts.message')
                        <form id="register" class="form-horizontal" role="form" method="POST"
                              action="{{ url('/register') }}"
                              data-parsley-excluded="input[type=button], input[type=submit], input[type=reset]"
                              data-parsley-trigger="keyup" data-parsley-validate
                        >
                            {{ csrf_field() }}
                            <div class="cus-form-group">
                                <div class="mdc-select col-md-12 title" data-mdc-auto-init="registerSelect">
                                    <select class="mdc-select__native-control mdc-text-field--with-trailing-icon"
                                            id="title" name="title"
                                            data-parsley-required
                                            data-parsley-required-message="Please select your Title"
                                    >
                                        <option value="" disabled selected></option>
                                        <option value="Mr">Mr</option>
                                        <option value="Mrs">Mrs</option>
                                        <option value="Ms">Ms</option>
                                        <option value="Miss">Miss</option>
                                        <option value="Prefer not to disclose">Prefer not to disclose</option>
                                    </select>
                                    <label class="mdc-floating-label">Title*</label>
                                    <div class="mdc-line-ripple"></div>
                                </div>
                            </div>
                            <div class="cus-form-group">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon first-name"
                                     data-mdc-auto-init="registerFields">
                                    <input id="first_name" type="text"
                                           class="mdc-text-field__input"
                                           name="first_name" value="{{ old('first_name') }}"
                                           data-parsley-required
                                           data-parsley-maxlength="30"
                                           data-parsley-minlength="3"
                                           data-parsley-required-message="Please enter your first name"
                                           data-parsley-validation-threshold="0"
                                    >
                                    <label class="mdc-floating-label" for="first-name">First Name*</label>
                                    <div class="mdc-line-ripple" style="transform-origin: 99.5px center 0px;">
                                    </div>
                                </div>
                            </div>
                            <div class="cus-form-group">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon last-name"
                                     data-mdc-auto-init="registerFields">
                                    <input id="last_name" type="text" class="mdc-text-field__input"
                                           name="last_name" value="{{ old('last_name') }}"
                                           data-parsley-required
                                           data-parsley-maxlength="30"
                                           data-parsley-minlength="3"
                                           data-parsley-required-message="Please enter your last name"
                                           data-parsley-validation-threshold="0"
                                    >
                                    <label class="mdc-floating-label" for="last-name">Last Name*</label>
                                    <div class="mdc-line-ripple" style="transform-origin: 99.5px center 0px;">
                                    </div>
                                </div>
                            </div>
                            <div class="cus-form-group" style="height: 85px">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon email-address"
                                     data-mdc-auto-init="registerFields">
                                    <input id="email" type="email" class="mdc-text-field__input"
                                           name="email"
                                           value="{{ old('email') }}"
                                           data-parsley-required
                                           data-parsley-maxlength="30"
                                           data-parsley-required-message="Please enter your work email"
                                           data-parsley-type-message="This is not a valid email address"
                                           data-parsley-validation-threshold="0"
                                           data-parsley-remote-options='{ "type": "POST" }'
                                           data-parsley-remote="{{ route('checkDomain') }}"
                                           data-parsley-remote-message="Unfortunately we dont accept users from this domain at this stage. Please contact your BDM for assistance in setting up your account."
                                    >
                                    <label class="mdc-floating-label" for="email-address">Email Address*</label>
                                    <div class="mdc-line-ripple" style="transform-origin: 99.5px center 0px;">
                                    </div>
                                </div>
                                <span id="email-helper-text" class="helper-text">
                                    Enter your work email address
                                </span>
                            </div>
                            <div class="cus-form-group" style="height: 85px">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon mobile"
                                     data-mdc-auto-init="registerFields">
                                    <input id="phoneNumber" type="tel" class="mdc-text-field__input"
                                           name="phoneNumber"
                                           maxlength="10"
                                           value="{{ old('phoneNumber') }}"
                                           data-parsley-required
                                           pattern="^(?:\+?(61))? ?(?:\((?=.*\)))?(0?[2-57-8])\)? ?(\d\d(?:[- ](?=\d{3})|(?!\d\d[- ]?\d[- ]))\d\d[- ]?\d[- ]?\d{3})$"
                                           data-parsley-required-message="Please enter your mobile number"
                                           data-parsley-pattern-message="Format your number 04xx-xxx-xxx without dash"
                                           data-parsley-validation-threshold="0"
                                    >
                                    <label class="mdc-floating-label" for="mobile">Mobile*</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;">
                                    </div>
                                </div>
                                <span id="phoneNumber-helper-text" class="helper-text">
                                    We'll only use your mobile to contact you for information relating to your duo account or reward
                                </span>
                            </div>

                            <div class="cus-form-group" style="height: 95px">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon pcc"
                                     data-mdc-auto-init="registerFields">
                                    <input id="pcc" type="text" class="mdc-text-field__input"
                                           name="pcc"
                                           value="{{ old('pcc') }}"
                                           data-parsley-required
                                           data-parsley-required-message="Please enter your agency pseudo code"
                                           data-parsley-type="alphanum"
                                           data-parsley-maxlength="9"
                                           data-parsley-minlength="3"
                                           data-parsley-validation-threshold="0"
                                           maxlength="9"
                                           minlength="3"
                                    >
                                    <label class="mdc-floating-label" for="pcc">Agency
                                        Pseudo Code (PCC)*</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;">
                                    </div>
                                </div>
                                <span id="pcc-helper-text" class="helper-text">
                                    Agency Pseudo Code (PCC) is an alpha-numeric identifier for a travel agency using the global distribution system (GDS)
                                </span>
                            </div>
                            <div class="cus-form-group" style="height: 85px">
                                <div class="col-md-12 mdc-select agency-group"
                                     data-mdc-auto-init="registerSelect">
                                    <select class="mdc-select__native-control" id="consortium" data-parsley-required="" data-parsley-required-message="Please select your Affiliated Agency Group" name="consortium">
                                        <option></option>
                                        @foreach($consortium as $key => $value)
                                            <option value="{{ $key }}">{{ $value }}</option>
                                        @endforeach
                                    </select>
                                    <label class="mdc-floating-label" for="consortium">Affiliated Agency
                                        Group*</label>
                                    <div class="mdc-line-ripple"></div>
                                </div>
                                <span id="consortium-helper-text" class="helper-text">
                                    Select the Agency Group your agency is affiliated with, or belongs to
                                </span>
                            </div>
                            <div class="cus-form-group" style="height: 85px">
                                <div class="col-md-12 mdc-select agency-brand" id="agencyDiv"
                                     data-mdc-auto-init="registerSelect">
                                    <select class="mdc-select__native-control" id="agency" name="agency"
                                            data-parsley-required
                                            data-parsley-required-message='Please select your Agency Brand'>
                                        <option selected="selected" disabled="disabled" hidden="hidden"
                                                value="">
                                        </option>
                                    </select>
                                    <label class="mdc-floating-label" for="agency">Agency Brand Name*</label>
                                    <div class="mdc-line-ripple"></div>
                                </div>
                                <span id="agency-helper-text" class="helper-text">
                                    Select your Agency brand which belongs to your Affiliated Agency Group
                                </span>
                            </div>
                            <div class="cus-form-group" id="agency-other" style="display: none">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon agency-other"
                                     data-mdc-auto-init="registerFields">
                                    <input id="agency2" type="text" class="mdc-text-field__input"
                                           name="agency2"
                                           value="{{ old('agency2') }}"
                                           data-parsley-required
                                           data-parsley-required-message='Please enter your Agency Name'>
                                    <label class="mdc-floating-label" for="agency2">Enter your Agency brand name</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;"></div>
                                </div>
                            </div>
                            <div class="cus-form-group" style="height: 130px">
                                <div class="col-md-12 mdc-select store" id="storeDiv" data-mdc-auto-init="registerSelect">
                                    <select class="mdc-select__native-control" id="store" name="store"
                                            data-parsley-required
                                            data-parsley-required-message='Please select your Store'>
                                        <option selected="selected" disabled="disabled" hidden="hidden"
                                                value="">
                                        </option>
                                    </select>
                                    <label class="mdc-floating-label" for="store">Store Name*</label>
                                    <div class="mdc-line-ripple"></div>
                                </div>
                                <span id="store-helper-text" class="helper-text">
                                    If you belong to a National Agency Brand, select your Store Branch Name - this is often where your agency is located, e.g. Circular Quay <br>
                                    If you don't have a Store Brand Name, select 'Other' and enter the Suburb of your store.
                                </span>
                            </div>
                            <div class="cus-form-group"
                                 id="store-other" style="display: none">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon store-other"
                                     data-mdc-auto-init="registerFields">
                                    <input id="store2" type="text" class="mdc-text-field__input"
                                           name="store2"
                                           value="{{ old('store2') }}"
                                           data-parsley-required
                                           data-parsley-required-message='Please enter your Store Name'>
                                    <label class="mdc-floating-label" for="store2">Enter your Store Name</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;"></div>
                                </div>
                            </div>
                            <div class="cus-form-group">
                                <div class="mdc-select col-md-12 traveller"
                                     data-mdc-auto-init="registerSelect">
                                    <select class="mdc-select__native-control" id="position" name="position"
                                            data-parsley-required
                                            data-parsley-required-message='Please select who you are'>
                                        <option selected="selected" disabled="disabled" hidden="hidden"
                                                value="">
                                        </option>
                                        <option>Yes</option>
                                        <option>No</option>
                                    </select>
                                    <label class="mdc-floating-label" for="position">I am a Mobile or Home
                                        based Travel*</label>
                                    <div class="mdc-line-ripple"></div>
                                </div>
                            </div>
                            <div class="cus-form-group" style="height: 85px">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon address1"
                                     data-mdc-auto-init="registerFields">
                                    <input id="address1" type="text" class="mdc-text-field__input"
                                           name="address1"
                                           value="{{ old('address1') }}"
                                           data-parsley-required
                                           data-parsley-required-message='Please enter your Address'
                                           data-parsley-maxlength="30"
                                           data-parsley-minlength="3"
                                           data-parsley-validation-threshold="0"
                                    >
                                    <label class="mdc-floating-label" for="address1">Address Line 1*</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;"></div>
                                </div>
                                <span id="address1-helper-text" class="helper-text">
                                    We require your postal address to deliver duo reward items you have achieved. Please ensure you provide an address which will accept couriers.
                                </span>
                            </div>
                            <div class="cus-form-group">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon address2"
                                     data-mdc-auto-init="registerFields">
                                    <input id="address2" type="text" class="mdc-text-field__input"
                                           name="address2"
                                           value="{{ old('address2') }}"
                                           data-parsley-required
                                           data-parsley-required-message='Please enter your Address'
                                           data-parsley-maxlength="30"
                                           data-parsley-minlength="3"
                                           data-parsley-validation-threshold="0"
                                    >
                                    <label class="mdc-floating-label" for="address2">Address Line 2*</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;"></div>
                                </div>
                            </div>
                            <div class="cus-form-group">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon address3"
                                     data-mdc-auto-init="registerFields">

                                    <input id="address3" type="text" class="mdc-text-field__input"
                                           name="address3"
                                           value="{{ old('address3') }}"
                                           data-parsley-required
                                           data-parsley-required-message='Please enter your Suburb'
                                           data-parsley-maxlength="20"
                                           data-parsley-minlength="3"
                                           data-parsley-validation-threshold="0"
                                    >
                                    <label class="mdc-floating-label" for="address3">Enter Suburb*</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;"></div>
                                </div>
                            </div>
                            <div class="cus-form-group">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon suburb"
                                     data-mdc-auto-init="registerFields">

                                    <input id="suburb" type="text" class="mdc-text-field__input"
                                           name="suburb"
                                           value="{{ old('suburb') }}"
                                           data-parsley-required
                                           data-parsley-required-message='Please enter your Town or City'
                                           data-parsley-maxlength="20"
                                           data-parsley-minlength="3"
                                           data-parsley-validation-threshold="0"
                                    >
                                    <label class="mdc-floating-label" for="suburb">Enter Town or City*</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;">
                                    </div>
                                </div>
                            </div>
                            <div class="cus-form-group ">
                                <div class="mdc-select col-md-12 state"
                                     data-mdc-auto-init="registerSelect">
                                    <select class="mdc-select__native-control" id="state" data-parsley-required="" data-parsley-required-message="Please select your State" name="state" ><option>
                                        </option><option value="2">New South Wales</option><option value="4">Queensland</option><option value="1">Victoria</option><option value="6">Western Australia</option><option value="7">South Australia</option><option value="8">Tasmania</option><option value="3">Australian Capital Territory</option><option value="5">Northern Territory</option></select>
                                    <label class="mdc-floating-label" for="state">State*</label>
                                    <div class="mdc-line-ripple"></div>
                                </div>
                            </div>
                            <div class="cus-form-group">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded postcode"
                                     data-mdc-auto-init="registerFields">
                                    <input id="postcode" type="number" class="mdc-text-field__input"
                                           name="postcode"
                                           value="{{ old('postcode') }}"
                                           data-parsley-required
                                           data-parsley-required-message='Please enter your Postcode'
                                           data-parsley-maxlength="4"
                                           data-parsley-minlength="3"
                                           data-parsley-validation-threshold="0"
                                    >
                                    <label class="mdc-floating-label" for="postcode">Postcode*</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;">
                                    </div>
                                </div>
                            </div>
                            <div class="cus-form-group">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded mdc-text-field--with-trailing-icon password"
                                     data-mdc-auto-init="registerFields">
                                    <input id="password" type="password"
                                           class="mdc-text-field__input"
                                           name="password"
                                           data-parsley-number="1"
                                           data-parsley-required
                                           data-parsley-required-message="Please enter your new password."
                                           data-parsley-minlength="8"
                                           data-parsley-minlength-message="Password must be at least 8 characters"
                                           data-parsley-maxlength="30"
                                           data-parsley-validation-threshold="0"
                                    >
                                    <label class="mdc-floating-label" for="password">Password*</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;">
                                    </div>
                                </div>
                                <span id="password-helper-text" class="helper-text">
                                    Enter a password. It must be at least 8 characters, and contain at least one number.
                                </span>
                            </div>
                            <div class="cus-form-group">
                                <div class="col-md-12 mdc-text-field text-field mdc-text-field--upgraded password-confirm"
                                     data-mdc-auto-init="registerFields">
                                    <input id="password-confirm" type="password"
                                           class="mdc-text-field__input"
                                           name="password_confirmation"
                                           data-parsley-minlength="8"
                                           data-parsley-required-message="Please re-enter your new password."
                                           data-parsley-equalto="#password"
                                           data-parsley-required
                                           data-parsley-equalto-message="Password should be same."
                                           data-parsley-validation-threshold="0"
                                    >
                                    <label class="mdc-floating-label" for="password-confirm">Confirm
                                        Password*</label>
                                    <div class="mdc-line-ripple"
                                         style="transform-origin: 99.5px center 0px;">
                                    </div>
                                </div>
                            </div>
                            <div class="cus-form-group tc-block">
                                <div class="mdc-form-field"
                                     data-mdc-auto-init="registerCheckbox">
                                    <div class="mdc-checkbox mdc-checkbox--upgraded mdc-ripple-upgraded mdc-ripple-upgraded--unbounded">
                                        <input type="checkbox"
                                               class="mdc-checkbox__native-control"
                                               id="tc"/>
                                        <div class="mdc-checkbox__background">
                                            <svg class="mdc-checkbox__checkmark"
                                                 viewBox="0 0 24 24">
                                                <path class="mdc-checkbox__checkmark-path"
                                                      fill="none"
                                                      d="M1.73,12.91 8.1,19.28 22.79,4.59"/>
                                            </svg>
                                            <div class="mdc-checkbox__mixedmark"></div>
                                        </div>
                                    </div>
                                    <label class="duo-tos" for="tc">I accept Duo's
                                        <a class="btn-link" data-toggle="modal"
                                           data-target="#tcModal" href="#">Terms & Conditions</a> and <a class="btn-link" data-toggle="modal"
                                                    data-target="#ppModal" href="#">Privacy Policy</a></label>
                                </div>
                            </div>

                            <div class="cus-form-group">
                                <div class="col-md-12 text-center">
                                    <button type="submit"
                                            class="btn btn-black">
                                        Register
                                    </button>
                                    <div><a class="btn-link" href="{{ url('/') }}">
                                            Already a member?
                                        </a></div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="tcModal" class="modal modal-reg fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="anz-title clearfix">
                        <div class="anz-title-wrap">
                            <strong>duo</strong> <span class="anz-tag"><span class="anz-tag-block"><span
                                            class="anz-tag-border">|</span> We're Better Together</span></span>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="panel-body-tc">{!! $page->message !!}</div>
                </div>
            </div>

        </div>
    </div>

    <!-- Modal -->
    <div id="ppModal" class="modal modal-reg fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <div class="anz-title clearfix">
                        <div class="anz-title-wrap">
                            <strong>duo</strong> <span class="anz-tag"><span class="anz-tag-block"><span
                                            class="anz-tag-border">|</span> We're Better Together</span></span>
                        </div>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="panel-body-tc">{!! $privacy->message !!}
                    </div>
                </div>
            </div>

        </div>
    </div>


@endsection
@push('scripts')
    <script>
        $(function () {
            $('#register').parsley({
                errorClass: 'parsley-error icon-wrong',
                successClass: 'parsley-success icon-correct',
                errorsWrapper: '<ul class="parsley-errors-list"></ul>',
                errorTemplate: '<li></li>'
            });

            $('[data-toggle="tooltip"]').tooltip();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $("#agency").depdrop({
                url: '{{ route('agencyList') }}',
                depends: ['consortium']
            });

            $("#store").depdrop({
                url: '{{ route('storeList') }}',
                depends: ['consortium', 'agency']
            });

            let typingTimer;
            let doneTypingInterval = 1000;  //time in ms (1 seconds)

            $("#pcc").on('change keyup paste', function () {
                clearTimeout(typingTimer);
                if ($("#pcc").val() && $("#pcc").val().length >= 3) {
                    typingTimer = setTimeout(doneTyping, doneTypingInterval);
                }else {
                    $('#pcc-helper-text').html('');
                }
            });

            function doneTyping() {
                let $form = $('body > div.container > div > div > div.panel.panel-default > div > form > div:nth-child(9)');
                $('#pcc-helper-text').html('Checking PCC..').show();
                let pcc = $("#pcc").val();
                axios.post('{{ route('checkPCC') }}', {
                    pcc: pcc
                }).then(function (response) {
                    if (response.data.result == 'success') {
                        $('.agency-group > label').addClass('mdc-floating-label--float-above');
                        $('#pcc-helper-text').html('PCC is verified, register now!');
                        $('#consortium').val(response.data.store.store_consortium_id).trigger('change');
                        $('#agency').on('depdrop:afterChange', function () {
                            $('#agency').val(response.data.store.store_agency_id).trigger('change');
                        });
                        $('#store').on('depdrop:afterChange', function () {
                            $('#store').val(response.data.store.id).trigger('change');
                            check_store();
                        });
                        $('.state > label').addClass('mdc-floating-label--float-above');
                        $('#state').val(response.data.store.store_state_id).trigger('change');
                        $('.address1 > label').addClass('mdc-floating-label--float-above');
                        $('#address1').val(response.data.store.address1);
                        $('.address2 > label').addClass('mdc-floating-label--float-above');
                        $('#address2').val(response.data.store.address2);
                        $('.address3 > label').addClass('mdc-floating-label--float-above');
                        $('#address3').val(response.data.store.address3);
                        $('.suburb > label').addClass('mdc-floating-label--float-above');
                        $('#suburb').val(response.data.store.suburb);
                        $('.postcode > label').addClass('mdc-floating-label--float-above');
                        $('#postcode').val(response.data.store.postcode);
                    } else {
                        $('#pcc-helper-text').html('Please enter your Agency details below');
                        $('#address1').val('');
                        $('.address1 > label').removeClass('mdc-floating-label--float-above');
                        $('#address2').val('');
                        $('.address2 > label').removeClass('mdc-floating-label--float-above');
                        $('#address3').val('');
                        $('.address3 > label').removeClass('mdc-floating-label--float-above');
                        $('#suburb').val('');
                        $('.suburb > label').removeClass('mdc-floating-label--float-above');
                        $('#postcode').val('');
                        $('.postcode > label').removeClass('mdc-floating-label--float-above');
                    }
                })
                    .catch(function (error) {
                        $('#pcc-helper-text').html('Something went wrong!');
                    });
            }
            $('#consortium').on('change', function (e) {
                $('#agencyDiv').removeClass('mdc-select--disabled');
                $('#agencyDiv').addClass('mdc-select');
                $('#agencyDiv > label').addClass('mdc-floating-label--float-above');
                $('#storeDiv').removeClass('mdc-select--disabled');
                $('#storeDiv').addClass('mdc-select');
                $('#storeDiv > label').addClass('mdc-floating-label--float-above');

            });
            $('#agency').on('change', function (e) {
                var data = $("#agency option:selected").text();
                if (data == 'Other') {
                    $('#agency-other').show();
                } else {
                    $('#agency-other').hide();
                }
            });

            $('#store').on('change', function (e) {
                check_store();
            });
            function check_store(){
                var data = $("#store option:selected").text();
                if (data == 'Other') {
                    $('#store-other').show();
                } else {
                    $('#store-other').hide();
                }
            }

            window.Parsley.addValidator('number', {
                requirementType: 'number',
                validateString: function (value, requirement) {
                    var numbers = value.match(/[0-9]/g) || [];
                    return numbers.length >= requirement;
                },
                messages: {
                    en: 'Your password must contain at least (%s) number.'
                }
            });

            select_validation('#title');
            select_validation('#consortium');
            select_validation('#agency');
            select_validation('#store');
            select_validation('#position');
            select_validation('#state');

            function select_validation(id) {
                $(id).on('change', function () {
                    if ($(id).parsley().isValid()) {
                        $(id).removeClass('icon-wrong');
                        $(id).addClass('icon-correct');
                    } else {
                        $(id).removeClass('icon-correct');
                        $(id).addClass('icon-wrong');
                    }
                });

            }
            window.Parsley.on('field:error', function() {
                if(this.$element.context.id === 'email' || this.$element.context.id === 'phoneNumber' || this.$element.context.id === 'pcc'
                    || this.$element.context.id === 'consortium' || this.$element.context.id === 'agency' || this.$element.context.id === 'store'
                    || this.$element.context.id === 'address1' || this.$element.context.id === 'password' ){
                    $('#'+this.$element.context.id+'-helper-text').hide();
                    $('#'+this.$element.context.id).parent().parent().css('height','95px');
                }
                $('#'+this.$element.context.id).parent().addClass('mdc-text-field--invalid');
            });
            window.Parsley.on('field:success', function() {
                if(this.$element.context.id === 'email' || this.$element.context.id === 'phoneNumber' || this.$element.context.id === 'pcc'
                    || this.$element.context.id === 'consortium' || this.$element.context.id === 'agency' || this.$element.context.id === 'store'
                    || this.$element.context.id === 'address1' || this.$element.context.id === 'password'){
                    $('#'+this.$element.context.id).parent().parent().css('height','85px');
                }
                $('#'+this.$element.context.id).parent().removeClass('mdc-text-field--invalid');
            });
        });
    </script>
@endpush
