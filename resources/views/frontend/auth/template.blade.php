<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link href="{{ url('libs/bootstrap/css/bootstrap-without-forms.min.css')}}" type="text/css" rel="stylesheet">
    <link href="{{ url('assets/duo.admin.css')}}" type="text/css" rel="stylesheet">
    <link href="{{ url('assets/register.css')}}?v=1.2" type="text/css" rel="stylesheet">
    @stack('styles')
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
<div class="register-wrap">
    @yield('content')
    <footer class="footer">
        @include('frontend.layouts.footer')
    </footer>
</div>
<script src="{{ url('libs/jquery-2.2.3.min.js') }}"></script>
<script src="{{ url('libs/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ mix('js/bootstrap.js') }}"></script>
<script src="{{ mix('js/app.js') }}"></script>
@stack('scripts')
</body>
</html>
