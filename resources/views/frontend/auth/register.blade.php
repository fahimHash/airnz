@extends('frontend.auth.template')
@section('content')
<div class="login-logo-wrap">
    <img src="{{ url('assets/img/logo-airnz.svg')}}">
</div>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="panel panel-register panel-default">
                <div class="anz-title clearfix">
                    <div class="anz-title-wrap">
                        <a href="/"><img src="{{ url('assets/img/logo-duo.svg')}}"></a>
                    </div>
                </div>
                <div class="panel-body">
                    <h6><span>Registration Form</span></h6>
                    <p>Fields marked with an * are required to create your duo profile.</p>
                    <p> The information you provide will help us provide personalised engagement content
                        which best suits your needs.</p>
                    @include('frontend.layouts.message')
                    <div id="app">
                        <registration-form></registration-form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection