@extends('frontend.auth.template')
@push('styles')
    <style>
        .toggleBlock {
            display: block;
        }
    </style>
@endpush
@section('content')
    <div class="login-logo-wrap">
        <img src="{{ url('assets/img/logo-airnz.svg')}}">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-heading text-center"></div>
                <div class="panel panel-default">
                  <div class="anz-title clearfix">
                      <div class="anz-title-wrap">
                          <a href="/"><img src="{{ url('assets/img/logo-duo.svg')}}"></a>
                      </div>
                  </div>
                    <div class="panel-body" id="app">
                        @if (session('status'))
                        @elseif(session('status2'))
                        @else
                            <div id="reset_password">
                                <reset-password></reset-password>
                            </div>
                        @endif
                        <div id="reset_email" style="display: none">
                            <reset-email></reset-email>
                        </div>
                        @if (session('status'))
                            <form>
                                <div class="col-md-12 text-center">
                                    <h6>Check your <strong>email</strong></h6>
                                    <p>
                                        You will soon receive an email with <br>your password reset link.
                                    </p>
                                    <a class="btn btn-black" onclick="window.location='{{ url('/login') }}';">
                                        Close
                                    </a>
                                </div>
                            </form>
                        @endif
                        @if (session('status2'))
                            <form>
                                <div class="col-md-12 text-center">
                                    <h6><strong>Thanks,</strong>We'll be in touch</h6>
                                    <p>
                                        We'll be in touch, an Air New Zealand <br>
                                        Representative will be in touch shortly <br>
                                        to reset your email.
                                    </p>
                                    <a class="btn btn-black" onclick="window.location='{{ url('/login') }}';">
                                        Close
                                    </a>
                                </div>
                            </form>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            $('[data-toggle="tooltip"]').tooltip();
            $('#reset-email').on('click', function () {
                $('#reset_password').hide();
                $('#reset_email').show();
            });
        });
    </script>
@endpush
