@extends('frontend.auth.template')

@section('content')
    <div class="login-logo-wrap">
        <img src="{{ url('assets/img/logo-airnz.svg')}}">
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                  <div class="anz-title clearfix">
                      <div class="anz-title-wrap">
                          <a href="/"><img src="{{ url('assets/img/logo-duo.svg')}}"></a>
                      </div>
                  </div>
                    <div class="panel-body" id="app">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        <create-password token="{{ $token }}"></create-password>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
