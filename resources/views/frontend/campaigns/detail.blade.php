@extends('frontend.layouts.template', ['class' => 'onboarding-page'])

@push('styles')
@endpush

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper content-wrapper-preloader">
        <section class="content-header content-header-campaigns">
            <div class="page-title">
                <h3><span>duo</span>campaigns</h3>
            </div>
            <ul>
                <li><a href="#home" class="current">Home</a></li>
                <li><a href="#child-1">Child 1</a></li>
                <li><a href="#child-2">Child 2</a></li>
            </ul>
        </section>
        <div class="content content-onboarding">

            <!-- Main content -->
            <div class="onboarding-body onboarding-body-sales onboarding-body-campaigns">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="onboarding-body-box box-white" style="background-image: url({{ url('assets/img/'.$campaign->image) }});">
                                <div class="onboarding-body-box-util">
                                    <span><i class="icon-sales"></i></span>
                                        <span class="approved"><i class="icon-approved"></i></span>
                                </div>
                                <h4>{{ $campaign->name }}</h4>
                                <hr>
                                <div class="onboarding-body-sales-date">{{ \Carbon\Carbon::parse($campaign->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($campaign->endDate)->formatLocalized('%d %B %Y') }}</div>
                                <p>{!! $campaign->message !!}</p>
                                <div class="onboarding-body-box-knob" style="position: initial;">
                                    <div class="salestracker-block-knob">
                                        <input type="text" class="knob" value="{{ $daysLeft }}" data-thickness="0.05" data-max="100" data-width="100" data-height="100">
                                        <span>Days Left</span>
                                    </div>
                                    <div class="salestracker-block-knob">
                                        <input type="text" class="knob" value="{{$sale}}" data-thickness="0.05" data-max="150" data-width="100" data-height="100">
                                        <span>Complete</span>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <section class="onboarding-body">

            </section>
        </div>
    </div>
    <!-- /.content-wrapper -->

@endsection

@push('scripts')
    <script src="{{ url('libs/jquery.knob.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(".knob").knob({
                'readOnly':true,
                'fgColor': '#831780',
                'bgColor': '#D9B8DA',
            });
        });
    </script>
@endpush
