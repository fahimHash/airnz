@extends('frontend.layouts.template', ['class' => 'onboarding-page'])

@push('styles')
@endpush

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper content-wrapper-preloader">
        <!-- Content Header (Page header) -->
        <div class="preloader" style="background-image: url({{ url('assets/img/loading_campaign.jpg') }});">
          <div class="status">
            <img src="{{ url('assets/img/loading.png') }}" class="img-responsive">
          </div>
        </div>
        <section class="content-header content-header-campaigns">
            <div class="page-title">
                <h3><span>duo</span>campaigns</h3>
            </div>
            <ul>
                <li><a href="#all" class="current">All Campaigns <span>({{ $allCampaigns->count() }})</span></a></li>
                <li><a href="#coming-soon">Coming Soon <span>(0)</span></a></li>
                <li><a href="#sales">Sales <span>({{ $salesCampaigns->count() }})</span></a></li>
                <li><a href="#training">Training <span>({{ $tierCampaigns->count() }})</span></a></li>
                {{-- <li><a href="#engagements">Engagements <span>({{ $engagementCampaigns->count() }})</span></a></li> --}}
                <li><a href="#completed">Completed <span>({{$agentCompletedCampaigns->count()}})</span></a></li>
            </ul>
        </section>
        <div class="content content-onboarding">

            <!-- Main content -->
            <div class="onboarding-body onboarding-body-sales onboarding-body-campaigns">
                @include('frontend.layouts.message')
                <div class="row">
                    @if(isset($allCampaigns[0]))
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-12">
                                    @if(isset($allCampaigns[0]->pivot->joined))
                                        @include('frontend.campaigns.template', ['id'=> $allCampaigns[0]->id, 'mainTitle' => $allCampaigns[0]->name, 'mainContent' => $allCampaigns[0]->message, 'date' =>  \Carbon\Carbon::parse($allCampaigns[0]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[0]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[0]->image, 'inProgress' => true, 'complete' => false, 'approved' => true])
                                    @elseif(isset($allCampaigns[0]->pivot->completed))
                                        @include('frontend.campaigns.template', ['id'=> $allCampaigns[0]->id, 'mainTitle' => $allCampaigns[0]->name, 'mainContent' => $allCampaigns[0]->message, 'date' =>  \Carbon\Carbon::parse($allCampaigns[0]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[0]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[0]->image, 'complete' => true, 'inProgress' => false, 'approved' => true])
                                    @else
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[0]->id, 'mainTitle' => $allCampaigns[0]->name, 'mainContent' => $allCampaigns[0]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[0]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[0]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[0]->image, 'inProgress' => false, 'complete' => false, 'approved' => false])
                                    @endif
                                </div>
                            </div>
                        </div>
                    @endif
                    @if(isset($allCampaigns[1]))
                        <div class="col-md-4">
                            @if(isset($allCampaigns[1]->pivot->joined))
                                @include('frontend.campaigns.template', ['id'=>$allCampaigns[1]->id, 'mainTitle' => $allCampaigns[1]->name, 'mainContent' => $allCampaigns[1]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[1]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[1]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[1]->image, 'inProgress' => true, 'complete' => false, 'approved' => true])
                            @elseif(isset($allCampaigns[1]->pivot->completed))
                                @include('frontend.campaigns.template', ['id'=>$allCampaigns[1]->id, 'mainTitle' => $allCampaigns[1]->name, 'mainContent' => $allCampaigns[1]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[1]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[1]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[1]->image, 'complete' => true, 'inProgress' => false, 'approved' => true])
                            @else
                                @include('frontend.campaigns.template', ['id'=>$allCampaigns[1]->id, 'mainTitle' => $allCampaigns[1]->name, 'mainContent' => $allCampaigns[1]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[1]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[1]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[1]->image, 'inProgress' => false, 'complete' => false, 'approved' => false])
                            @endif
                        </div>
                    @endif
                </div>
            </div>

            <section class="onboarding-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            @if(isset($allCampaigns[2]))
                                <div class="col-md-4">
                                    @if(isset($allCampaigns[2]->pivot->joined))
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[2]->id, 'mainTitle' => $allCampaigns[2]->name, 'mainContent' => $allCampaigns[2]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[2]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[2]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[2]->image, 'inProgress' => true, 'complete' => false, 'approved' => true])
                                    @elseif (isset($allCampaigns[2]->pivot->completed))
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[2]->id, 'mainTitle' => $allCampaigns[2]->name, 'mainContent' => $allCampaigns[2]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[2]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[2]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[2]->image, 'complete' => true, 'inProgress' => false, 'approved' => true])
                                    @else
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[2]->id, 'mainTitle' => $allCampaigns[2]->name, 'mainContent' => $allCampaigns[2]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[2]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[2]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[2]->image, 'inProgress' => false, 'complete' => false, 'approved' => false])
                                    @endif
                                </div>
                            @endif
                            @if(isset($allCampaigns[3]))
                                <div class="col-md-8">
                                    @if(isset($allCampaigns[3]->pivot->joined))
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[3]->id, 'mainTitle' => $allCampaigns[3]->name, 'mainContent' => $allCampaigns[3]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[3]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[3]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[3]->image, 'inProgress' => true, 'complete' => false, 'approved' => true])
                                    @elseif (isset($allCampaigns[3]->pivot->completed))
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[3]->id, 'mainTitle' => $allCampaigns[3]->name, 'mainContent' => $allCampaigns[3]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[3]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[3]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[3]->image, 'complete' => true, 'inProgress' => false, 'approved' => true])
                                    @else
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[3]->id, 'mainTitle' => $allCampaigns[3]->name, 'mainContent' => $allCampaigns[3]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[3]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[3]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[3]->image, 'inProgress' => false, 'complete' => false, 'approved' => false])
                                    @endif
                                </div>
                            @endif
                            @if(isset($allCampaigns[4]))
                                <div class="col-md-4">
                                    @if(isset($allCampaigns[4]->pivot->joined))
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[4]->id, 'mainTitle' => $allCampaigns[4]->name, 'mainContent' => $allCampaigns[4]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[4]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[4]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[4]->image, 'inProgress' => true, 'complete' => false, 'approved' => true])
                                    @elseif (isset($allCampaigns[4]->pivot->completed))
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[4]->id, 'mainTitle' => $allCampaigns[4]->name, 'mainContent' => $allCampaigns[4]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[4]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[4]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[4]->image, 'complete' => true, 'inProgress' => false, 'approved' => true])
                                    @else
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[4]->id, 'mainTitle' => $allCampaigns[4]->name, 'mainContent' => $allCampaigns[4]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[4]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[4]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[4]->image, 'inProgress' => false, 'complete' => false, 'approved' => false])
                                    @endif
                                </div>
                            @endif
                            @if($allCampaigns->count() > 4))
                                @for ($i = ($allCampaigns->count() + $agentCampaignsInProgress->count() - $allCampaigns->count()) ; $i < $allCampaigns->count(); $i++)
                                    <div class="col-md-4">
                                        @include('frontend.campaigns.template', ['id'=>$allCampaigns[$i]->id, 'mainTitle' => $allCampaigns[$i]->name, 'mainContent' => $allCampaigns[$i]->message, 'date' => \Carbon\Carbon::parse($allCampaigns[$i]->startDate)->formatLocalized('%d %B %Y').' - '.\Carbon\Carbon::parse($allCampaigns[$i]->endDate)->formatLocalized('%d %B %Y'), 'background_image' => $allCampaigns[$i]->image, 'inProgress' => false, 'complete' => false, 'approved' => false])
                                    </div>
                                @endfor
                            @endif
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /.content-wrapper -->

@endsection

@push('scripts')
    <script src="{{ url('libs/jquery.knob.js') }}"></script>
    <script>
    // makes sure the whole site is loaded
    $(window).load(function() {
    	"use strict";
            // will first fade out the loading animation
    	$(".status").fadeOut();
            // will fade out the whole DIV that covers the website.
    	$(".preloader").delay(1000).fadeOut("slow");
        $(".preloader-hide #sidebarleft, .preloader-hide-close").addClass("preloader-show");
    });
    $(document).ready(function () {
        $(".knob").knob({
            'readOnly':true,
            'fgColor': '#831780',
            'bgColor': '#D9B8DA',
        });

        @for ($i = 0; $i < $allCampaigns->count(); $i++)

            @if(isset($allCampaigns[$i]->pivot->joined))
                axios.post("campaigns/progress", {
                    id: '{!! $allCampaigns[$i]->id !!}'
                }).then(function (response) {

                    $('#{!! $allCampaigns[$i]->id !!}-days').val(response.data.daysLeft);
                    $('#{!! $allCampaigns[$i]->id !!}-sale').val(response.data.sales);
                })
            @endif
        @endfor
});
    </script>
@endpush
