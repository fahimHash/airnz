<div class="onboarding-body-box box-white" style="background-image: url({{ url('assets/img/'.$background_image) }});">
  <div class="onboarding-body-box-util">
    <span><i class="icon-sales"></i></span>
    @if($approved)
      <span class="approved"><i class="icon-approved"></i></span>
    @endif
  </div>
  <h4>{{ $mainTitle }}</h4>
  <hr>
  <div class="onboarding-body-sales-date">{{ $date }}</div>
  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
  @if ($inProgress)
    <div class="onboarding-body-box-knob">
      <div class="salestracker-block-knob">
        <input type="text" class="knob" value="0" id="{{$id}}-days" data-thickness="0.05" data-max="100" data-width="100" data-height="100">
        <span>Days Left</span>
      </div>
      <div class="salestracker-block-knob">
        <input type="text" class="knob" value="0" id="{{$id}}-sale" data-thickness="0.05" data-max="150" data-width="100" data-height="100">
        <span>Complete</span>
      </div>
    </div>
    <a href="{{ url('campaigns/detail/'.$id) }}" class="btn btn-lg btn-black">VIEW PROGRESS</a>
  @elseif($complete)
    <a href="{{ url('campaigns/detail/'.$id) }}" class="btn btn-lg btn-black">COMPLETED</a>
  @else
    {!! Form::open(['url' => '/join-campaign', 'class' => 'form-horizontal']) !!}
    {{ Form::hidden('campaign_id', $id , array('required' => 'required')) }}
    {!! Form::submit('JOIN', ['class' => 'btn btn-lg btn-black']) !!}
    {!! Form::close() !!}
  @endif
</div>

