@extends('frontend.auth.template')
@section('content')
    <div class="login-logo-wrap">
      <img src="{{ url('assets/img/logo-airnz.svg')}}">
    </div>
    <div class="container container-tc container-full-white-bg">
      <div class="panel panel-register panel-default">
        <div class="anz-title clearfix">
            <div class="anz-title-wrap">
                <a href="/"><img src="{{ url('assets/img/logo-duo.svg')}}"></a>
            </div>
        </div>
      </div>
      <div class="panel-body-tc">{!! $page->message !!}</div>
    </div>
@endsection
