@extends('frontend.layouts.template', ['class' => 'wrapper-components'])

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        <section class="content-header content-header-campaigns content-header-components">
            <div class="page-title">
                <h3><span>duo</span> {!! $post->title !!}</h3>
            </div>
        </section>
        <div id="maincontent" class="content content-components">

            <!-- Main content -->
            <section class="onboarding-body components-body">
                <div class="row">
                    <div class="col-md-12">
                        {!! $post->message !!}
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /.content-wrapper -->
@endsection
