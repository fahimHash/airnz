@extends('frontend.layouts.template', ['class' => 'training-page onboarding-page'])

@push('styles')
  <link rel="stylesheet" href="{{ url('libs/animate.min.css') }}">
@endpush

@section('content')

  <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper content-wrapper-preloader">
   <!-- Content Header (Page header) -->
   <div class="preloader" style="background-image: url(assets/img/loading_bg.jpg);">
     <div class="status">
       <img src="assets/img/loading.png" class="img-responsive">
     </div>
   </div>
   <section class="content-header content-header-campaigns">
     <div class="page-title">
       <h3><span>duo</span>campaigns</h3>
     </div>
     <ul>
       <li><a href="#" class="current">All Campaigns <span>(8)</span></a></li>
       <li><a href="#">Coming Soon <span>(3)</span></a></li>
       <li><a href="#">Sales <span>(3)</span></a></li>
       <li><a href="#">Traning <span>(4)</span></a></li>
       <li><a href="#">Completed <span>(5)</span></a></li>
     </ul>
   </section>
   <div id="maincontent" class="content content-onboarding content-training">

     <!-- Main content -->
     <section class="onboarding-body">
       <div class="row">
         <div class="col-md-12">
           <div class="row">
             <div class="col-md-12">
               <div class="onboarding-body-box onboarding-body-box-head box-white">
                 <h4>Welcome to the <span>duo</span>training</h4>
                 <hr>
                 <div class="row"><div class="col-md-8">Lorem ipsum dolor sit amet, consectetur adipisicing elit. <a href="">Esse ad maiores nulla</a>, provident quia, itaque, optio quam ipsam dolor non nam alias, architecto nihil? Minima soluta cum facere dolorum voluptas non, tempora veniam! Assumenda enim ab.</div></div>
                 {!! Form::open(['url' => 'http://blog-silverstripe.dev/Security/login', 'class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}
                  {!! Form::hidden('email', 'john@example.com') !!}
                  {!! Form::hidden('pass', '123456') !!}
                  {!! Form::submit('Go to Training ') !!}
                 {!! Form::close() !!}
               </div>
             </div>
             <div class="col-md-8">
               <div class="onboarding-body-box" style="background-image: url(assets/img/onboarding/slice4.png);">
                 <h4>Lorem ipsum dolor</h4>
                 <hr>
                 <div class="onboarding-body-date"><strong>Status:</strong> Completed (05 Feb 2017)</div>
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                 <a href="#" class="btn btn-lg btn-black">RE-SIT MODULE</a>
               </div>
             </div>
             <div class="col-md-4">
               <div class="onboarding-body-box" style="background-image: url(assets/img/training_bg_1.png);">
                 <h4>NZ All Stars</h4>
                 <hr>
                 <div class="onboarding-body-date"><strong>Status:</strong> Completed (05 Feb 2017)</div>
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                 <a href="#" class="btn btn-lg btn-black">RE-SIT MODULE</a>
               </div>
             </div>
             <div class="col-md-4">
               <div class="onboarding-body-box" style="background-image: url(assets/img/training_bg_1.png);">
                 <h4>Lorem ipsum dolor</h4>
                 <hr>
                 <div class="onboarding-body-date"><strong>Status:</strong> Completed (05 Feb 2017)</div>
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                 <a href="#" class="btn btn-lg btn-black">RE-SIT MODULE</a>
               </div>
             </div>
             <div class="col-md-4">
               <div class="onboarding-body-box" style="background-image: url(assets/img/training_bg_2.png);">
                 <h4>Lorem ipsum dolor</h4>
                 <hr>
                 <div class="onboarding-body-date"><strong>Status:</strong> Completed (05 Feb 2017)</div>
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                 <a href="#" class="btn btn-lg btn-black">RE-SIT MODULE</a>
               </div>
             </div>
             <div class="col-md-4">
               <div class="onboarding-body-box" style="background-image: url(assets/img/training_bg_3.png);">
                 <h4>Lorem ipsum dolor</h4>
                 <hr>
                 <div class="onboarding-body-date"><strong>Status:</strong> Incomplate</div>
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                 <a href="#" class="btn btn-lg btn-black">RESTART MODULE</a>
               </div>
             </div>
             <div class="col-md-4">
               <div class="onboarding-body-box" style="background-image: url(assets/img/training_bg_1.png);">
                 <h4>Lorem ipsum dolor</h4>
                 <hr>
                 <div class="onboarding-body-date"><strong>Status:</strong> Incomplate</div>
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                 <a href="#" class="btn btn-lg btn-black">RESTART MODULE</a>
               </div>
             </div>
             <div class="col-md-4">
               <div class="onboarding-body-box" style="background-image: url(assets/img/training_bg_2.png);">
                 <h4>Lorem ipsum dolor</h4>
                 <hr>
                 <div class="onboarding-body-date"><strong>Status:</strong> Incomplate</div>
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                 <a href="#" class="btn btn-lg btn-black">RESTART MODULE</a>
               </div>
             </div>
             <div class="col-md-4">
               <div class="onboarding-body-box" style="background-image: url(assets/img/training_bg_3.png);">
                 <h4>Lorem ipsum dolor</h4>
                 <hr>
                 <div class="onboarding-body-date"><strong>Status:</strong> Completed (05 Feb 2017)</div>
                 <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                 <a href="#" class="btn btn-lg btn-black">START MODULE</a>
               </div>
             </div>
           </div>
         </div>
       </div>
     </section>
   </div>
 </div>
 <!-- /.content-wrapper -->

@endsection

@push('scripts')
    <script src="{{ ('libs/jquery.knob.js') }}"></script>
    <script>
    // makes sure the whole site is loaded
    $(window).load(function() {
    	"use strict";
            // will first fade out the loading animation
    	$(".status").fadeOut();
            // will fade out the whole DIV that covers the website.
    	$(".preloader").delay(1000).fadeOut("slow");
        var width = $(window).width();
        $(window).on('resize', function(){
            if($(this).width() != width){
                width = $(this).width();
                adjustAnimate();
            }
        });
        function adjustAnimate(){
            if ( $(window).width() > 768) {
                $("#mainnav").addClass("animated bounceInDown");
                $("#sidebarleft").addClass("animated bounceInLeft");
                $("#onboarding-header-wrap, #onboarding-slider-wrap, .onboarding-body-box-1, .onboarding-body-box-2, .onboarding-body-box-3, .onboarding-body-box-4, .onboarding-body-box-5").addClass("animated bounceInUp");
                // $("#onboarding-slider-wrap").addClass("animated bounceInUp");
            } else {}
        }

        $(".preloader-hide #sidebarleft, .preloader-hide-close").addClass("preloader-show");
    });

    $(document).ready(function () {
        $(".knob").knob({
            'readOnly':true,
            'fgColor': '#831780',
            'bgColor': '#D9B8DA',
        });
    });
    </script>
    <style media="screen">
      #sidebarleft {
        list-style:none; padding-left:0; animation-delay:1s; -moz-animation-delay:1s; -webkit-animation-delay:1s;
      }
      #mainnav {
        animation-delay:1.7s; -moz-animation-delay:1.7s; -webkit-animation-delay:1.7s;
      }
      #maincontent {
        animation-delay:1.5s; -moz-animation-delay:1.5s; -webkit-animation-delay:1.5s;
      }
    </style>
@endpush
