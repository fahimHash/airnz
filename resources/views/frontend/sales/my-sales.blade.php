@extends('frontend.sales.template')

@section('sales-content')
    <!-- Main content -->
    <section class="content content-sales content-my-sales">
        <!-- Main row -->
        <div class="row">
            <!-- Left col -->
            <div class="col-md-10 col-md-offset-1">
                <div class="row">
                    <div class="col-md-12">
                    @include('frontend.layouts.message')
                    <!-- swap view -->
                        <div id="welcome-view" class="box box-full"
                             style="background-image: url( {{url('assets/img/bg-sales-header.jpg')}}); background-size: cover; background-position: center center;">
                            <div class="box-header">
                                <h2><span>virtual</span>plane</h2>
                                <hr>
                            </div>
                            <div class="box-body">
                                <div class="bg-circle">
                                    <h3>Welcome<br>to duosales</h3>
                                    <p>Every passenger you book will fill one seat on your virtual plane. For every group booking you make, nine passengers will be accepted onto the virtual plane
                                        Keep an eye out for special guests who will also be filling seats on your plane. The more seats you fill, the more duo rewards you'll earn.</p>
                                    <a href="#" class="btn btn-default next">NEXT</a>
                                </div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <div id="animation-view" class="box box-full" style="display: none">
                            <video id="plane-video">
                                <source src="{{url('assets/img/Aeroplane.mp4')}}" type="video/mp4">
                                Your browser does not support HTML5 video.
                            </video>
                        </div>
                        <div id="plane-view" class="box box-white box-full box-duo-sales box-modal"
                             style="display: none">
                            <div class="box-header">
                                <h2><span>virtual</span>plane</h2>
                                <hr>
                            </div>
                            <div class="box-body">
                                <div class="box-duo-sales-left">
                                    <p>Every passenger you book will fill one seat on your virtual plane. For every group booking you make, nine passengers will be accepted onto the virtual plane
                                        Keep an eye out for special guests who will also be filling seats on your plane. The more seats you fill, the more duo rewards you'll earn.</p>
                                    {{--<img src="{{url('assets/img/minion.gif')}}" class="minion"--}}
                                         {{--style="position:  absolute;left: 40%; z-index: 99;bottom:  62%;width:  35px;">--}}
                                    <div class="box-duo-sales-left-img">
                                        @include('frontend.sales.plane')
                                        <style media="screen">
                                            .seatsavailable {
                                                display: none;
                                            }

                                            {{ $class }}


                                            {
                                                display: inherit
                                            ;
                                            }
                                        </style>
                                    </div>
                                </div>
                                <div class="box-duo-sales-right">
                                    <div class="salestracker-block-knob">
                                        <input type="text" class="knob validatedSales" value="{{ $validatedSales }}"
                                               data-thickness="0.05" data-max="10000" data-width="150"
                                               data-height="150">
                                        <span>Total Sales</span>
                                    </div>
                                    <div class="salestracker-block-knob">
                                        <input type="text" class="knob validatedCurrentWeek"
                                               value="{{ $validatedCurrentWeek }}"
                                               data-thickness="0.05" data-max="100" data-width="150" data-height="150">
                                        <span>This Week</span>
                                    </div>
                                    <div class="box-duo-sales-info">
                                        <div class="box-duo-sales-info-left">
                                            <h4>{{ $emptySeat }}</h4>
                                            Empty Seats
                                        </div>
                                        <div class="box-duo-sales-info-right">
                                            <h3>SYD <span><img src="{{ url('assets/img/ico-plane.png') }}"
                                                               alt=""></span> AKL</h3>
                                            <div class="seats-available">
                                                <img src="{{ url('assets/img/ico-empty.png') }}"
                                                     alt=""> {{ $emptySeat }} Seats available
                                            </div>
                                            <div class="seats-full">
                                                <img src="{{ url('assets/img/ico-full.png') }}"
                                                     alt=""> {{ $validatedSales }} Seats full
                                            </div>
                                        </div>
                                        <hr>
                                        <h6>Special guests:</h6>
                                        <a class="btn-guest-one"><span>?</span></a>
                                        <a class="btn-guest-two"><span>?</span></a>
                                        <a class="btn-guest-three"><span>?</span></a>
                                        {{--<a class="btn-guest-four"><span>?</span></a>--}}
                                        {{--<a class="btn-guest-five"><span>?</span></a>--}}
                                        {{--<a class="btn-guest-six"><span>?</span></a>--}}

                                    </div>
                                    <div class="text-center"><a href="{{ route('add-sales') }}"
                                                                class="btn btn-lg btn-black">ADD SALES</a></div>
                                </div>
                            </div>
                            <!-- /.box-body -->
                            @include('frontend.sales.guest')
                        </div>
                        <!-- swap view end -->
                        <div class="box box-full box-map">
                            <div class="box-header">
                                <h2><span>destination</span>tracker</h2>
                                <hr>
                            </div>
                            <div id="networktracker" class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                                <div class="zoomBtn">
                                    <button data-zoom="+1" class="zoomin">Zoom In</button>
                                    <button data-zoom="-1" class="zoomout">Zoom Out</button>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 box-body">
                                <ul class="nav nav-tabs nav-tabs-salestracker" role="tablist">
                                    <li role="presentation"><a href="#tab-yearly-map" aria-controls="tab-yearly-map"
                                                               role="tab" data-toggle="tab" onclick="showMap('yearly')">Yearly</a>
                                    </li>
                                    <li role="presentation" class="active"><a href="#tab-monthly-map"
                                                                              aria-controls="tab-monthly-map"
                                                                              role="tab" data-toggle="tab"
                                                                              onclick="showMap('monthly')">Monthly</a>
                                    </li>
                                    <li role="presentation"><a href="#tab-weekly-map"
                                                               aria-controls="tab-weekly-map" role="tab"
                                                               data-toggle="tab" onclick="showMap('weekly')">Weekly</a>
                                    </li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <h5><b>Validated Sales</b></h5>
                                    <div role="tabpanel" class="tab-pane" id="tab-yearly-map">
                                        <canvas id="validated-yearly-sales" width="300" height="300"></canvas>
                                    </div>
                                    <div role="tabpanel" class="tab-pane active" id="tab-monthly-map">
                                        <canvas id="validated-monthly-sales" width="300" height="300"></canvas>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="tab-weekly-map">
                                        <canvas id="validated-weekly-sales" width="300" height="300"></canvas>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <!-- /.box-body -->
                        </div>

                        <div class="box box-full">
                            <div class="box-header">
                                <h2><span>sales</span>tracker</h2>
                                <hr>
                            </div>
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs nav-tabs-salestracker" role="tablist">
                                <li role="presentation"><a href="#tab-yearly" aria-controls="tab-yearly" role="tab"
                                                           data-toggle="tab">Yearly</a>
                                </li>
                                <li role="presentation"><a href="#tab-monthly" aria-controls="tab-monthly" role="tab"
                                                           data-toggle="tab">Monthly</a>
                                </li>
                                <li role="presentation" class="active"><a href="#tab-weekly" aria-controls="tab-weekly"
                                                                          role="tab" data-toggle="tab">Weekly</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content tab-content-salestracker">
                                <div role="tabpanel" class="tab-pane" id="tab-yearly">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-content-salestracker-left">
                                                <div id="chart-yearly" style="height: 300px;"></div>
                                            </div>
                                            <div class="tab-content-salestracker-right clearfix">
                                                <div class="salestracker-block-knob">
                                                    <input type="text" class="knob" id='thisYear' value="0"
                                                           data-thickness="0.05" data-max="10000" data-width="120"
                                                           data-height="120">
                                                    <span>This Year</span>
                                                </div>
                                                <div class="salestracker-block-knob">
                                                    <input type="text" class="knob" id='lastYear' value="0"
                                                           data-thickness="0.05" data-max="10000" data-width="120"
                                                           data-height="120">
                                                    <span>Last Year</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="tab-monthly">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-content-salestracker-left">
                                                <div id="chart-monthly" style="height: 300px;"></div>
                                            </div>
                                            <div class="tab-content-salestracker-right clearfix">
                                                <div class="salestracker-block-knob">
                                                    <input type="text" class="knob" id='thisMonth' value="0"
                                                           data-thickness="0.05" data-max="1000" data-width="120"
                                                           data-height="120">
                                                    <span>This Month</span>
                                                </div>
                                                <div class="salestracker-block-knob">
                                                    <input type="text" class="knob" id='lastMonth' value="0"
                                                           data-thickness="0.05" data-max="1000" data-width="120"
                                                           data-height="120">
                                                    <span>Last Month</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane active" id="tab-weekly">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="tab-content-salestracker-left">
                                                <div id="chart-weekly" style="height: 300px;"></div>
                                            </div>
                                            <div class="tab-content-salestracker-right clearfix">
                                                <div class="salestracker-block-knob">
                                                    <input type="text" class="knob" id='thisWeek' value="0"
                                                           data-thickness="0.05" data-max="100" data-width="120"
                                                           data-height="120">
                                                    <span>This Week</span>
                                                </div>
                                                <div class="salestracker-block-knob">
                                                    <input type="text" class="knob" id='lastWeek' value="0"
                                                           data-thickness="0.05" data-max="100" data-width="120"
                                                           data-height="120">
                                                    <span>Last Week</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @include('frontend.sales.table')
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@push('sales-page-js')
    <script src="{{ url('js/mysales.js') }}"></script>
    <script src="{{ url('libs/d3/d3.v3.min.js') }}"></script>
    <script src="{{ url('libs/d3/topojson.v0.min.js') }}"></script>
    <script src="{{ url('libs/chartjs/2.7.1/Chart.js') }}"></script>
    <script>
        $('.next').click(function () {
            $('.validatedSales').val({{ $validatedSales }}).trigger('change');
            $('.validatedCurrentWeek').val({{ $validatedCurrentWeek }}).trigger('change');
            // setTimeout(function () {
            //     $('.minion')
            //         .animate({now: '100'}, {
            //             step: function (now, fx) {
            //                 $(this).css('transform', 'translate(0px, ' + now + 'px)');
            //             },
            //             duration: '10000'
            //         }, 'linear')
            //         .animate({now: '270'}, {
            //             step: function (now, fx) {
            //                 $(this).css('transform', 'translate(0px,100px) rotate(' + now + 'deg)');
            //             },
            //             duration: 'slow'
            //         }, 'linear')
            //         .animate({x: '100'}, {
            //             step: function (x, fx) {
            //                 $(this).css('transform', 'translate(' + x + 'px, 100px) rotate(270deg)');
            //             },
            //             duration: '10000'
            //         }, 'linear')
            //         .slideUp();
            // }, 1000);
        });
        var destinations = {!! json_encode($pathways) !!};
        var airports = [
                @foreach($destinations as $destination)
            {
                code: "{{ $destination->code }}",
                city: "{{ $destination->name }}",
                lat: "{{ $destination->latitude }}",
                lon: "{{ $destination->longitude }}",
                weeklySale: "{{ array_key_exists($destination->code, $validatedWeeklySales) ? $validatedWeeklySales[$destination->code] : 0 }}",
                monthlySale: "{{ array_key_exists($destination->code, $validatedMonthlySales) ? $validatedMonthlySales[$destination->code] : 0 }}",
                yearlySale: "{{ array_key_exists($destination->code, $validatedYearlySales) ? $validatedYearlySales[$destination->code] : 0 }}"
            },
            @endforeach
        ];
        d3.select(window)
            .on("resize", function () {
                d3.select("g").attr("transform", "scale(1)");
                $("svg").height($("#networktracker").height());
                $("svg").width($("#networktracker").width());
            });
        var height = 500;
        var width = $("#networktracker").width();

        var projection = d3.geo.mercator()
            .center([28, 20])
            .scale(152.5)
            .rotate([-180, 0]);

        // Define 'div' for tooltips
        var tooltip = d3.select("#networktracker")
            .append("div")
            .attr("class", "tooltip hidden");

        var svg = d3.select("#networktracker").append("svg")
            .attr("width", width)
            .attr("height", height);

        var path = d3.geo.path()
            .projection(projection);
        var g = svg.append("g");


        var offsetL = 20;
        var offsetT = 0;

        var zoomfactor = 1;

        var zoomlistener = d3.behavior.zoom()
            .scaleExtent([1, 50])
            .on("zoom", redraw);

        $(".zoomin").on("click", function () {
            zoomfactor = zoomfactor + 0.2;
            zoomlistener.scale(zoomfactor).event(d3.select("#networktracker"));
        });

        $(".zoomout").on("click", function () {
            zoomfactor = zoomfactor - 0.2;
            zoomlistener.scale(zoomfactor).event(d3.select("#networktracker"));
        });

        function redraw() {
            console.log("here", d3.event.translate, d3.event.scale);
            g.attr("transform", "translate(" + d3.event.translate + ")" + " scale(" + d3.event.scale + ")");
        }

        d3.json("{{ url('libs/d3/world-110m2.json') }}", function (error, topology) {
            g.selectAll("path")
                .data(topojson.object(topology, topology.objects.countries)
                    .geometries)
                .enter()
                .append("path")
                .attr("d", path);

            g.selectAll("circle")
                .data(airports)
                .enter()
                .append("circle")
                .attr("cx", function (d) {
                    return projection([d.lon, d.lat])[0];
                })
                .attr("cy", function (d) {
                    return projection([d.lon, d.lat])[1];
                })
                .attr("r", function (d) {
                    return d.monthlySale > 5 ? d.monthlySale : 5;
                })
                .style("fill", "#682e7b")
                .style("opacity", "0.7")
                .on("mousemove", function (d) {
                    var mouse = d3.mouse(svg.node()).map(function (d) {
                        return parseInt(d);
                    });
                    tooltip.classed("hidden", false)
                    tooltip.transition()
                        .duration(500)
                        .style("opacity", 0);
                    tooltip.transition()
                        .duration(200)
                        .style("opacity", .9);
                    tooltip.attr("style", "left:" + (mouse[0] + offsetL) + "px;top:" + (mouse[1] + offsetT) + "px");
                    tooltip.html(
                        '<p>' + d.code + '<br><b>Sales ' + d.monthlySale + "</b></p>");
                })
                .on("mouseout", function (d) {
                    tooltip.classed("hidden", true);
                });

            g.selectAll("text")
                .data(airports)
                .enter()
                .append("text")
                .attr("x", function (d) {
                    return projection([d.lon - 2.5, d.lat])[0];
                })
                .attr("y", function (d) {
                    return projection([d.lon, d.lat - 2])[1];
                })
                .text(function (d) {
                    return d.monthlySale > 5 ? d.monthlySale : '';
                })
                .attr("font", "bold 16px sans-serif")
                .attr("fill", "white");

            var arcGroup = g.append('g');
            // --- Helper functions (for tweening the path)
            var lineTransition = function lineTransition(path) {
                path.transition()
                //NOTE: Change this number (in ms) to make lines draw faster or slower
                    .duration(6000)
                    .attrTween("stroke-dasharray", tweenDash)
                    .each("end", function (d, i) {
                        ////Uncomment following line to re-transition
                        //d3.select(this).call(transition);

                        //We might want to do stuff when the line reaches the target,
                        //  like start the pulsating or add a new point or tell the
                        //  NSA to listen to this guy's phone calls
                        //doStuffWhenLineFinishes(d,i);
                    });
            };
            var tweenDash = function tweenDash() {
                //This function is used to animate the dash-array property, which is a
                //  nice hack that gives us animation along some arbitrary path (in this
                //  case, makes it look like a line is being drawn from point A to B)
                var len = this.getTotalLength(),
                    interpolate = d3.interpolateString("0," + len, len + "," + len);

                return function (t) {
                    return interpolate(t);
                };
            };
            // you can build the links any way you want - e.g., if you have only
            //  certain items you want to draw paths between
            // Alterntively, it can be created automatically based on the data
            links = [];
            for (var i = 0, len = destinations.length; i < len; i++) {
                // (note: loop until length - 1 since we're getting the next
                //  item with i+1)
                links.push({
                    type: "LineString",
                    coordinates: [
                        [destinations[i].lonS, destinations[i].latS],
                        [destinations[i].lon, destinations[i].lat]
                    ]
                });
            }
            // Standard enter / update
            var pathArcs = arcGroup.selectAll(".arc")
                .data(links);
            //enter
            pathArcs.enter()
                .append("path").attr({
                'class': 'arc'
            }).style({
                fill: 'none',
            });
            //update
            pathArcs.attr({
                //d is the points attribute for this path, we'll draw
                //  an arc between the points using the arc function
                d: path
            })
                .style({
                    stroke: '#682e7b',
                    'stroke-width': '1px'
                })
                // Uncomment this line to remove the transition
                .call(lineTransition);
            //exit
            pathArcs.exit().remove();
        });
        // zoom and pan
        var zoom = d3.behavior.zoom()
            .scaleExtent([1, 50])
            .on("zoom", function () {
                var e = d3.event,
                    // now, constrain the x and y components of the translation by the
                    // dimensions of the viewport
                    tx = Math.min(0, Math.max(e.translate[0], width - width * e.scale)),
                    ty = Math.min(0, Math.max(e.translate[1], height - height * e.scale));
                // then, update the zoom behavior's internal translation, so that
                // it knows how to properly manipulate it on the next movement
                zoom.translate([tx, ty]);
                // and finally, update the <g> element's transform attribute with the
                // correct translation and scale (in reverse order)
                g.attr("transform", [
                    "translate(" + [tx, ty] + ")",
                    "scale(" + e.scale + ")"
                ].join(" "));
            });

        if ($(window).width() < 1400) {
            g.attr("transform", "translate(-240, 0)scale(1)");
            g.selectAll("circle")
                .attr("d", path.projection(projection));
            g.selectAll("path")
                .attr("d", path.projection(projection));
            svg.call(zoom);
        } else {
            svg.call(zoom);
        }

        function showMap(value) {
            this.g.selectAll("text").remove()
            this.g.selectAll("circle").remove()

            this.g.selectAll("circle")
                .data(airports)
                .enter()
                .append("circle")
                .attr("cx", function (d) {
                    return projection([d.lon, d.lat])[0];
                })
                .attr("cy", function (d) {
                    return projection([d.lon, d.lat])[1];
                })
                .attr("r", function (d) {
                    switch (value) {
                        case 'yearly':
                            return d.yearlySale > 5 ? d.yearlySale : 5;
                            break;
                        case 'monthly':
                            return d.monthlySale > 5 ? d.monthlySale : 5;
                            break;
                        case 'weekly':
                            return d.weeklySale > 5 ? d.weeklySale : 5;
                            break;
                        default:
                            return d.monthlySale > 5 ? d.monthlySale : 5;
                            break;
                    }
                })
                .style("fill", "#682e7b")
                .style("opacity", "0.7")
                .on("mousemove", function (d) {
                    var mouse = d3.mouse(svg.node()).map(function (d) {
                        return parseInt(d);
                    });
                    tooltip.classed("hidden", false)
                    tooltip.transition()
                        .duration(500)
                        .style("opacity", 0);
                    tooltip.transition()
                        .duration(200)
                        .style("opacity", .9);
                    tooltip.attr("style", "left:" + (mouse[0] + offsetL) + "px;top:" + (mouse[1] + offsetT) + "px");
                    tooltip.html(
                        '<p>' + d.code + "</b></p>");
                })
                .on("mouseout", function (d) {
                    tooltip.classed("hidden", true);
                });
            this.g.selectAll("text")
                .data(airports)
                .enter()
                .append("text")
                .attr("x", function (d) {
                    return projection([d.lon - 2.5, d.lat])[0];
                })
                .attr("y", function (d) {
                    return projection([d.lon, d.lat - 2])[1];
                })
                .text(function (d) {
                    switch (value) {
                        case 'yearly':
                            return d.yearlySale > 0 ? d.yearlySale : '';
                            break;
                        case 'monthly':
                            return d.monthlySale > 0 ? d.monthlySale : '';
                            break;
                        case 'weekly':
                            return d.weeklySale > 0 ? d.weeklySale : '';
                            break;
                        default:
                            return d.monthlySale > 0 ? d.monthlySale : '';
                            break;
                    }
                })
                .attr("font", "bold 16px sans-serif")
                .attr("fill", "white");
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            var width = $(window).width();
            if (width >= 1400) {

            }
            $("body").on("click", ".next", function () {
                $('#welcome-view').hide();
                $('#animation-view').show();
                $('#plane-video').get(0).play();
            });
            $("#plane-video").bind("ended", function() {
                $('#animation-view').hide();
                $('#plane-view').show();
            });

            $("body").on("click", ".btn-guest-one", function () {
                var quotations = [
                    'My daughter thinks were going to Disneyland for her, but it’s actually for me! I can’t wait to feel Captain America’s biceps in the flesh.',
                    'My daughter has stopped calling me ‘mum’ and now just refers to me as ‘Tired Lady’. It’s cute.',
                    'I forgot to pack Mr Cuddles, but my daughter hasn’t realised yet. Do they turn planes around for this sort of thing?',
                    'Most people hate airplane food but I love it! I love any food I didn’t have to make!',
                    'In-flight entertainment is great, but I know when we get home my daughter is going to demand her own private screen in the lounge room.',
                    'I told my daughter they eat snails in France. She replied “I do that in the backyard!”',
                    'My husband has packed tennis racquets, snorkelling gear and runners, whereas all I’ve packed is Michelle Obama’s autobiography and a sarong. ',
                    'Flying with just two kids is stressful enough, I can’t imagine the Brady Bunch ever took a holiday!',
                    'My daughter is going through this phase where she calls everyone Frank. Male. Female. Everyone is Frank. And they say kids are creative? '
                ];
                $("#guest-one-quote").html(quotations[Math.floor(Math.random() * quotations.length)]);
                $("#guest-one").modal("show");
                processModal();
            });

            $("body").on("click", ".btn-guest-two", function () {
                var quotations = [
                    'We often get mistaken for a couple. Which is odd, since I’m very much out of Bob’s league. -  Lance',
                    'We may fly Economy on the outside, but we’re First Class people on the inside. - Lance',
                    'My form of in-flight entertainment is asking Lance if he’s put on weight. – Bob',
                    'We use our real names for flights of course, but like to book hotels under our aliases - Liza and Barbra. – Bob',
                    'Call me old-fashioned, but whether cruising on the seas or cruising at altitude, I always change into my three-piece-suit for the dinner service. – Lance'
                ];
                $("#guest-two-quote").html(quotations[Math.floor(Math.random() * quotations.length)]);
                $("#guest-two").modal("show");
                processModal();
            });

            $("body").on("click", ".btn-guest-three", function () {
                var quotations = [
                    'Every time I look in the mirror, I say ‘I am the business.’ It’s empowering, you should try it.',
                    'I’m not all work-work-work. I still have fun at the office. One time I pretended my stapler was an alligator. People were talking about it for weeks',
                    'Coffee is for the weak. Tea is for the boring. Straight-up battery fluid is for those who really want to succeed in life.',
                    'Every time my assistant offers me a coffee I tell her to make it an espresso martini! She cracks up every time! But I guess I am paying her…',
                    'My eldest once made me a necklace made out of stationery she found at my desk. It was hideous of course, but I admired her work ethic.',
                    'I was rocking the pant suit way before Hillary made it cool.',
                    'My hair tie is actually a spare phone charger. It’s fashion meets function! ',
                    'One time I used my mascara as a stylus. Gosh it was a laugh. I did ruin my phone though.'
                ];
                $("#guest-three-quote").html(quotations[Math.floor(Math.random() * quotations.length)]);
                $("#guest-three").modal("show");
                processModal();
            });

            $("body").on("click", ".btn-guest-four", function () {
                var quotations = [
                    'I love backpacking. I’ve really cracked the art of getting as much in my bag as possible. I’ve dubbed myself origami-clothing king.',
                    'Fun fact: backpackers are people too! We just don’t smell as good.',
                    'I may be on a budget, but I’ve still booked the most expensive bungee jump I could find in Switzerland (use a destination we fly to). Not really keen on cheap ropes.',
                    'I actually really want to meet the Queen. She’s such a boss! Just like my grandma, but more rich and less bogan.',
                    'One time travelling through Cambodia I ate a whole spider! It was about the size of a tic-tac but I normally leave that part out.',
                    'I’ve tried heaps of cuisines from around the world but sometimes you just can’t beat a ham and butter sandwich, am I right?',
                    'My parents are a little worried about me travelling the world on my own, but to be honest I’m more worried about them trying to use Netflix on their own.',
                    'I booked a ridiculously cheap 24-bed hostel dorm in Buenos Aires. Like, really cheap. I’m not sure if it has a roof...',
                    'Mum said by the time I come home she’ll have converted my bedroom into an upstairs kitchen. Suits me though! I won’t have to go far for the fridge!'
                ];
                $("#guest-four-quote").html(quotations[Math.floor(Math.random() * quotations.length)]);
                $("#guest-four").modal("show");
                processModal();
            });

            $("body").on("click", ".btn-guest-five", function () {
                var quotations = [
                    'I can’t wait for our romantic gondola ride in Venice! Sean normally gets quite seasick but if he can hold it in for an hour it’ll be beautiful. – Sarah',
                    'Croissants are alright I guess, but I still reckon they’d be better with a snag and tomato sauce inside. – Sean',
                    'Sarah made me try sushi when we were in Tokyo. Kinda like a cold, fishy sausage roll. Not for me. - Sean ',
                    'Sean has literally been wearing the same Bintang singlet for six weeks. I’ll definitely make him take it off before we get to the Vatican. – Sarah',
                    'Watching Sean use chopsticks is like watching Bambi trying to walk for the first time. - Sarah',
                    'Sean’s a bit of a nervous flyer. But when I reminded him there’d be an open bar on board he calmed right down. – Sarah',
                    'n Paris, Sarah watched me eat a whole plate of chicken wings before she told me they were actually frogs legs. – Sean'
                ];
                $("#guest-five-quote").html(quotations[Math.floor(Math.random() * quotations.length)]);
                $("#guest-five").modal("show");
                processModal();
            });

            $("body").on("click", ".btn-guest-six", function () {
                var quotations = [
                    'I’ve decided to stop taking photos and start living in the moment. So I make Gareth take all the photos these days. He knows all my good angles. – Pauline\n',
                    'Who needs a credit card when you have your kids’ inheritance to spend? – Gareth',
                    'Last year we took a tour of the real Downton Abbey for my birthday. I was so looking forward to it, but Gareth accidentally booked the ‘Peasant Experience’ and it ended up being quite hard work! – Pauline',
                    'I love a good buffet breakfast in a fancy hotel, whereas Gareth insists on bringing his All-Bran with us on every trip. - Pauline',
                    'Everyone complains about rainy England but I love it! They give you a pint as standard over there! – Gareth',
                    'We could be in a remote desert in the middle of Morocco and Gareth will still somehow end up finding the nearest Irish pub. – Pauline',
                    'If my hotel doesn’t have a complimentary bathrobe and slippers you can guarantee I won’t be staying there again! – Gareth',
                    '’ve downloaded an app so I can learn some Spanish before we go to South America next year. I’m starting with all the food words of course! - Gareth'
                ];
                $("#guest-six-quote").html(quotations[Math.floor(Math.random() * quotations.length)]);
                $("#guest-six").modal("show");
                processModal();
            });

            function processModal() {
                $(".box-modal").addClass("after_modal_appended");
                //appending modal background inside the box-modal div
                $('.modal-backdrop').appendTo('.box-modal');
                //remove the padding right and modal-open class from the body tag which bootstrap adds when a modal is shown
                $('body').removeClass("modal-open")
                $('body').css("padding-right", "");
            }
        });
        salesChart(document.getElementById("validated-weekly-sales").getContext('2d'),
                {!! json_encode(array_values($validatedWeeklySales)) !!},
                {!! json_encode(array_keys($validatedWeeklySales)) !!}
        );
        salesChart(document.getElementById("validated-monthly-sales").getContext('2d'),
                {!! json_encode(array_values($validatedMonthlySales)) !!},
                {!! json_encode(array_keys($validatedMonthlySales)) !!}
        );
        salesChart(document.getElementById("validated-yearly-sales").getContext('2d'),
                {!! json_encode(array_values($validatedYearlySales)) !!},
                {!! json_encode(array_keys($validatedYearlySales)) !!}
        );

        function salesChart(ctx, sales, cty) {
            var myChart = new Chart(ctx, {
                type: 'horizontalBar',
                data: {
                    labels: cty,
                    datasets: [{
                        label: 'Tickets Sold',
                        data: sales,
                        backgroundColor: [
                            '#862284', '#862284', '#862284', '#862284', '#862284', '#862284', '#862284', '#862284', '#862284', '#862284', '#862284', '#862284'
                        ],
                        borderColor: [
                            '#482056', '#482056', '#482056', '#482056', '#482056', '#482056', '#482056', '#482056', '#482056', '#482056', '#482056', '#482056'
                        ],
                        borderWidth: 1,
                        hoverBorderWidth: 0
                    }]
                },
                options: {
                    tooltip: {
                        display: false
                    },
                    responsive: true,
                    legend: {
                        display: false
                    },
                    scales: {
                        yAxes: [{
                            barPercentage: 1,
                            ticks: {
                                beginAtZero: true
                            }
                        }],
                        xAxes: [{
                            display: false,
                            ticks: {
                                beginAtZero: true
                            }
                        }]
                    },
                    animation: {
                        onComplete: function (animation) {
                            var chartInstance = this.chart,
                                ctx = chartInstance.ctx;
                            ctx.font = "bold 12px  sans-serif ";
                            ctx.textAlign = 'center';
                            ctx.textBaseline = 'bottom';

                            this.data.datasets.forEach(function (dataset, i) {
                                var meta = chartInstance.controller.getDatasetMeta(i);
                                meta.data.forEach(function (bar, index) {
                                    var data = dataset.data[index];
                                    if (data > 0) {
                                        ctx.fillStyle = '#fff';
                                        ctx.fillText(data, bar._model.x - 12.5, bar._model.y + 7);
                                    }
                                });
                            });
                        }
                    }
                }
            });
        }
    </script>
@endpush
