<div class="row">

    <div class="col-md-12">
        <div class="box box-full">
            <div class="box-header">
                <a class="add-position collapsed" data-toggle="collapse" href="#uploadSales" aria-expanded="false">
                    <i class="fa fa-angle-up"></i>
                </a>
                <h2>Upload Sales</h2>
                <div class="collapse" id="uploadSales">
                    <hr>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatem, nesciunt. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut, mollitia molestias consectetur, quam nostrum sunt, earum laboriosam eum consequuntur ab animi eveniet blanditiis. Rerum, atque aspernatur velit ut cumque iure! <a href="#" type="button" data-toggle="modal" data-target="#add-sales-alert">Alert Testing</a> <a href="#" type="button" data-toggle="modal" data-target="#add-sales-success">success Testing</a></p>

                    <a class="btn btn-lg btn-black" href="{{ url('/sales-template' )}}">DOWNLOAD TEMPLATE</a>

                    <form action="{{ url('sales-upload')}}" method="POST" class="dropzone" id="uploadSales" enctype="multipart/form-data">
                        {{ csrf_field() }}
                    </form>

                    <span>Duplicate sales will be removed. Air New Zealand template files only can be uploaded. File name must contain member ID.</span>

                    <div class="add-sales">
                        {{-- <button class="btn btn-black btn-lg">ADD SALES</button> --}}
                    </div>

                </div>
            </div>
            <!-- /.box-body -->
        </div>

    </div>

</div>