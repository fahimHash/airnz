<div class="box box-white box-full box-mysales">
  <div class="box-header">
      <a class="add-position collapsed" data-toggle="collapse" href="#mySales" aria-expanded="false">
          <i class="fa fa-angle-up"></i>
      </a>
      <h2><span>my</span>sales</h2>
      <hr>
  </div>
  <div class="box-body">
    <div class="collapse" id="mySales">

      <All
        month-year="{{$month_year }}"
        all-count="{{ $all_count}}"
        valid-count="{{ $valid_count}}"
        pending-count="{{ $pending_count}}"
        cancelled-count="{{ $cancelled_count}}"
        problem-count="{{ $problem_count}}"
      ></All>

    </div>
    </div>
</div>
