@extends('frontend.layouts.template', ['class' => 'sales-page'])

@push('styles')
  <link rel="stylesheet" href="{{ url('libs/morris/morris.css') }}">
@endpush

@section('content')
<div class="content-wrapper">
    <section class="content-header content-header-campaigns content-header-global">
            <div class="page-title">
                <h3><span>duo</span>sales</h3>
            </div>
            <ul>
              <li><a class="current" href="{{ url('/my-sales')}}">My Sales</a></li>
              <li><a href="{{ url('/add-sales')}}">Add a Booking</a></li>
            </ul>
        </section>
    @yield('sales-content')
</div>
@endsection

@push('scripts')
    <script src="{{ url('libs/jquery.knob.js') }}"></script>
    <script src="{{ url('libs/raphael-min.js') }}"></script>
    <script src="{{ url('libs/morris/morris.min.js') }}"></script>
    <script src="{{ url('libs/morris/morris.min.cus.js') }}"></script>
    <script>
        $(function() {
            window.onerror = function(message, url, lineNumber) {
                return true;
            };
            $(".knob").knob({
                'readOnly':true,
                'fgColor': '#831780',
                'bgColor': '#D9B8DA',
            });
        });
        var saleschartyearly = Morris.Area({
            element: 'chart-yearly',
            resize: true,
            redraw: true,
            data: [
              {year: '2010', saleCurrent: 1 }
            ],
            xkey: 'year',
            ykeys: ['saleCurrent'],
            labels: ['Sales'],
            lineColors: ['#862284', '#72CFE4'],
            hideHover: 'auto',
            parseTime: false
        });
        var saleschartmonthly = Morris.Area({
            element: 'chart-monthly',
            resize: true,
            redraw: true,
            data: [
              {month: '2016-02', saleCurrent: 1 }
            ],
            xkey: 'month',
            ykeys: ['saleCurrent'],
            labels: ['Sales'],
            lineColors: ['#862284', '#72CFE4'],
            hideHover: 'auto',
            parseTime: false
        });
        var saleschartweekly = Morris.Area({
            element: 'chart-weekly',
            resize: true,
            redraw: true,
            data: [
              {day: 'Monday', saleCurrent: 0, saleLast: 0 }
            ],
            xkey: 'day',
            ykeys: ['saleCurrent', 'saleLast'],
            labels: ['Current Week', 'Last Week'],
            lineColors: ['#862284', '#72CFE4'],
            hideHover: 'auto',
            parseTime: false
        });
        axios.get('api/sales/salesChartYearly')
        .then(function (response) {
          saleschartyearly.setData(response.data);
          saleschartyearly.redraw();
        })
        .catch(function (error) {
          console.log(error);
        });
        axios.get('api/sales/salesChartMonthly')
        .then(function (response) {
          saleschartmonthly.setData(response.data);
          saleschartmonthly.redraw();
        })
        .catch(function (error) {
          console.log(error);
        });
        axios.get('api/sales/salesChartWeekly')
        .then(function (response) {
          saleschartweekly.setData(response.data);
          saleschartweekly.redraw();
        })
        .catch(function (error) {
          console.log(error);
        });
        axios.get('api/sales/salesCount')
        .then(function (response) {
          $('#thisYear').val(response.data.salesThisYear[0]).trigger('change');
          $('#lastYear').val(response.data.salesLastYear[0]).trigger('change');
          $('#lastMonth').val(response.data.salesLastMonth[0]).trigger('change');
          $('#thisMonth').val(response.data.salesThisMonth[0]).trigger('change');
          $('#thisWeek').val(response.data.salesThisWeek[0]).trigger('change');
          $('#lastWeek').val(response.data.salesLastWeek[0]).trigger('change');
        })
        .catch(function (error) {
          console.log(error);
        });
        $('a[data-toggle="tab"]').on('shown.bs.tab', function(e) {
          var target = $(e.target).attr("href") // activated tab
          switch (target) {
            case "#tab-yearly":
              saleschartyearly.redraw();
              $(window).trigger('resize');
              break;
            case "#tab-monthly":
              saleschartmonthly.redraw();
              $(window).trigger('resize');
              break;
            case "#tab-weekly":
              saleschartweekly.redraw();
              $(window).trigger('resize');
              break;
            case "#tab-yearly-map":
                validatedYearlySales.redraw();
              $(window).trigger('resize');
              break;
            case "#tab-monthly-map":
                validatedMonthlySales.redraw();
              $(window).trigger('resize');
              break;
            case "#tab-weekly-map":
                validatedWeeklySales.redraw();
              $(window).trigger('resize');
              break;
          }
        });
  </script>
@stack('sales-page-js')
@endpush
