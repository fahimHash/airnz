<div id="guest-one" class="modal fade guest-block" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 guest-content-img">
                  <div class="guest-block-img" style="background-image:url('{{ url("assets/img/guest-one.jpg") }}')"></div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
                  <div class="guest-content">
                    <p class="guest-quote">"<span id="guest-one-quote"></span>"</p>
                    <h3>Tina & her family </h3>
                    <p>Traveling as a family can be super stressful, but Tina and her family have it semi-sorted.
                        For Tina, the tears, the screams and the “adventures” of flying with young children are always worth it, as long as there’s a sandy and sunny light at the end of the tunnel.  (Preferably with a Mai Tai in hand.)</p>
                    <div class="btn-guest-block"><a href="#" class="btn btn-lg btn-default btn-guest">ADD TO PLANE</a></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="guest-two" class="modal fade guest-block" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 guest-content-img">
                <div class="guest-block-img" style="background-image:url('{{ url("assets/img/guest-two.jpg") }}')"></div>
              </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                  <div class="guest-content">
                    <p class="guest-quote">"<span id="guest-two-quote"></span>"</p>
                    <h3>Lance & Bob – Life-long friends </h3>
                    <p>Lance & Bob travel together, live together, and have seen every stage musical known to man together. For all their similarities and shared love of the finer things in life, they always seem to find something to bicker about.</p>
                    <div class="btn-guest-block"><a href="#" class="btn btn-lg btn-default btn-guest">ADD TO PLANE</a></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="guest-three" class="modal fade guest-block" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 guest-content-img">
                <div class="guest-block-img" style="background-image:url('{{ url("assets/img/guest-three.jpg") }}')"></div>
              </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                  <div class="guest-content">
                    <p class="guest-quote">"<span id="guest-three-quote"></span>"</p>
                    <h3>Suzy – Workaholic </h3>
                    <p>Traveling as a family can be super stressful, but Tina and her family have it semi-sorted.</p>
                    <div class="btn-guest-block"><a href="#" class="btn btn-lg btn-default btn-guest">ADD TO PLANE</a></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="guest-four" class="modal fade guest-block" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 guest-content-img">
                <div class="guest-block-img" style="background-image:url('{{ url("assets/img/guest-four.jpg") }}')"></div>
              </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                  <div class="guest-content">
                    <p class="guest-quote">"<span id="guest-four-quote"></span>"</p>
                    <h3>Shane - Backpacker </h3>
                    <p>Shane didn’t spend his entire summer working at his mum’s dog grooming business for nothing – he’s got a list of destinations to hit up – and staying well groomed in the process is the least of his priorities. Him and the beard he’s been growing are going to travel the world until they run out of money – so let the stinginess begin!</p>
                    <div class="btn-guest-block"><a href="#" class="btn btn-lg btn-default btn-guest">ADD TO PLANE</a></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="guest-five" class="modal fade guest-block" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 guest-content-img">
                <div class="guest-block-img" style="background-image:url('{{ url("assets/img/guest-five.jpg") }}')"></div>
              </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                  <div class="guest-content">
                    <p class="guest-quote">"<span id="guest-five-quote"></span>"</p>
                    <h3>Sarah & Sean - Honeymooners</h3>
                    <p>Sarah and Sean are newlyweds about to embark on an ambitious round-the-world honeymoon. Sean’s barely set foot outside his country town in thirty years, so city-girl Sarah is devilishly looking forward to seeing her hubby outside his comfort zone. What could possibly go wrong?
                    </p>
                    <div class="btn-guest-block"><a href="#" class="btn btn-lg btn-default btn-guest">ADD TO PLANE</a></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="guest-six" class="modal fade guest-block" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 guest-content-img">
                <div class="guest-block-img" style="background-image:url('{{ url("assets/img/guest-six.jpg") }}')"></div>
              </div>
                <div class="col-xs-12 col-sm-12 col-md-8 col-lg-8">
                  <div class="guest-content">
                    <p class="guest-quote">"<span id="guest-six-quote"></span>"</p>
                    <h3>Gareth & Pauline - Retirees</h3>
                    <p>Gareth and Pauline are travelling the world one Big Bus Tour at a time. Pauline is all about the people, the culture, and snapping the perfect shot for her Instagram. (Anything to make the girls at the bowling club a little jealous.) On the other hand, Gareth is happiest inside a fancy hotel. Or bar. Or better yet, the bar at the fancy hotel.
                    </p>
                    <div class="btn-guest-block"><a href="#" class="btn btn-lg btn-default btn-guest">ADD TO PLANE</a></div>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
