@extends('frontend.sales.template')

@section('sales-content')

            <!-- Main content -->
            <section class="content content-sales">

                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <div class="col-md-10 col-md-offset-1">

                        <div class="row">

                            <div class="col-md-12">
                              <div class="box box-white box-full box-duo-sales box-modal">
                                  <div class="box-header">
                                      <h2><span>duo</span>sales</h2>
                                      <hr>
                                  </div>
                                  <div class="box-body">
                                      <div class="box-duo-sales-left">
                                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur illo minus hic ea quos delectus inventore reprehenderit aperiam, explicabo officia nostrum ipsam eos ad facilis at debitis.</p>
                                        <div class="box-duo-sales-left-img">
                                          <img src="assets/img/plane.png">
                                        </div>
                                      </div>
                                      <div class="box-duo-sales-right">
                                        <div class="salestracker-block-knob">
                                        <input type="text" class="knob" value="56" data-thickness="0.05" data-max="100" data-width="150" data-height="150">
                                        <span>Total Sales</span>
                                        </div>
                                        <div class="salestracker-block-knob">
                                        <input type="text" class="knob" value="75" data-thickness="0.05" data-max="150" data-width="150" data-height="150">
                                        <span>This Week</span>
                                        </div>
                                        <div class="box-duo-sales-info">
                                          <div class="box-duo-sales-info-left">
                                            <h4>360</h4>
                                            Empty Seats
                                          </div>
                                          <div class="box-duo-sales-info-right">
                                            <h3>SYD <span><img src="assets/img/ico-plane.png" alt=""></span> AKL</h3>
                                            <div class="seats-available">
                                              <img src="assets/img/ico-empty.png" alt=""> 360 Seats available
                                            </div>
                                            <div class="seats-full">
                                              <img src="assets/img/ico-full.png" alt=""> 000 Seats full
                                            </div>
                                          </div>
                                          <hr>
                                          <h6>Special guests:</h6>
                                          <a class="btn-guest"><span><img src="assets/img/modal-img.png"></span><h5>Richie McCaw</h5></a>
                                          <a class="btn-guest"><span>?</span></a>
                                          <a class="btn-guest"><span>?</span></a>

                                        </div>
                                        <div class="text-center clearfix"><a href="add-sales.html" class="btn btn-lg btn-black">ADD SALES</a></div>
                                      </div>
                                  </div>
                                  <!-- /.box-body -->
                                    <div id="addGuest" class="modal fade" role="dialog">
                                      <div class="modal-dialog">

                                        <!-- Modal content-->
                                        <div class="modal-content">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>

                                          <div><img src="assets/img/modal-img.png"></div>
                                          <h3>Riche McCaw</h3>
                                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente optio repellendus eligendi ab ea, expedita, sed recusandae.</p>
                                          <div><a href="#" class="btn btn-lg btn-default">ADD TO PLANE</a></div>
                                        </div>

                                      </div>
                                      <style>
                                      #addGuest.modal,
                                        .modal-backdrop {
                                            position: absolute !important;
                                        }
                                      </style>
                                    </div>
                              </div>

                                <div class="box box-full box-map">
                                    <div class="box-header">
                                        <h2><span>destination</span>tracker</h2>
                                        <hr>
                                    </div>
                                    <div id="map"></div>
                                    <div class="box-body box-body-black">



                                      <!-- Tab panes -->
                                      <div class="tab-content">
                                        <h5>Vancouver Sales</h5>
                                        <div id="validated-sales" style="height: 200px;"></div>

                                      </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>

                                <div class="box box-white box-full">
                                    <div class="box-header">
                                        <h2><span>sales</span>tracker</h2>
                                        <hr>
                                    </div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs nav-tabs-salestracker" role="tablist">
                                      <li role="presentation"><a href="#tab-yearly" aria-controls="tab-yearly" role="tab" data-toggle="tab">Yearly</a>
                                      </li>
                                      <li role="presentation"><a href="#tab-monthly" aria-controls="tab-monthly" role="tab" data-toggle="tab">Monthly</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab-weekly" aria-controls="tab-weekly" role="tab" data-toggle="tab">Weekly</a>
                                      </li>
                                    </ul>


                                    <!-- Tab panes -->
                                    <div class="tab-content tab-content-salestracker">
                                      <div role="tabpanel" class="tab-pane" id="tab-yearly">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="tab-content-salestracker-left">
                                              <div id="chart-yearly" style="height: 300px;"></div>
                                            </div>
                                            <div class="tab-content-salestracker-right clearfix">
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="56" data-thickness="0.05" data-max="100" data-width="150" data-height="150">
                                                <span>This Year</span>
                                                </div>
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="75" data-thickness="0.05" data-max="150" data-width="150" data-height="150">
                                                <span>Last Year</span>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div role="tabpanel" class="tab-pane" id="tab-monthly">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="tab-content-salestracker-left">
                                              <div id="chart-monthly" style="height: 300px;"></div>
                                            </div>
                                            <div class="tab-content-salestracker-right clearfix">
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="56" data-thickness="0.05" data-max="100" data-width="150" data-height="150">
                                                <span>This Month</span>
                                                </div>
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="75" data-thickness="0.05" data-max="150" data-width="150" data-height="150">
                                                <span>Last Month</span>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div role="tabpanel" class="tab-pane active" id="tab-weekly">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="tab-content-salestracker-left">
                                              <div id="chart-weekly" style="height: 300px;"></div>
                                            </div>
                                            <div class="tab-content-salestracker-right clearfix">
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="56" data-thickness="0.05" data-max="100" data-width="150" data-height="150">
                                                <span>This Week</span>
                                                </div>
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="75" data-thickness="0.05" data-max="150" data-width="150" data-height="150">
                                                <span>Last Week</span>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>


                                    <!-- /.box-body -->
                                </div>

                                <div class="onboarding-body onboarding-body-sales">
                                  <div class="row">
                                    <div class="col-md-4">
                                      <!-- <div class="col-md-12"> -->
                                        <div class="onboarding-body-box box-white" style="background-image: url(assets/img/onboarding/slice3.png);">
                                          <div class="onboarding-body-box-util">
                                            <img src="assets/img/ico-plane-black.png" alt="">
                                            <img src="assets/img/ico-valid.png" alt="">
                                          </div>
                                          <h4>Buenos Agents</h4>
                                          <hr>
                                          <div class="onboarding-body-sales-date">2b8 Jan 2016 - 05 Mar 2017</div>
                                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</p>
                                          <div class="onboarding-body-box-knob">
                                            <div class="salestracker-block-knob">
                                            <input type="text" class="knob" value="24" data-thickness="0.05" data-max="100" data-width="100" data-height="100">
                                            <span>Days Left</span>
                                            </div>
                                            <div class="salestracker-block-knob">
                                            <input type="text" class="knob" value="85" data-thickness="0.05" data-max="100" data-width="100" data-height="100">
                                            <span>Complete</span>
                                            </div>
                                          </div>
                                          <a href="#" class="btn btn-lg btn-black">VIEW PROGRESS</a>
                                        </div>
                                      <!-- </div> -->
                                    </div>
                                    <div class="col-md-8">
                                      <div class="row">
                                        <div class="col-md-12">
                                          <div class="onboarding-body-box box-white" style="background-image: url(assets/img/bg-bird.png);">
                                            <div class="onboarding-body-box-util">
                                              <img src="assets/img/ico-plane-black.png" alt="">
                                              <img src="assets/img/ico-valid.png" alt="">
                                            </div>
                                            <h4>Do Dave's Itinerary</h4>
                                            <hr>
                                            <div class="onboarding-body-sales-date">2b8 Jan 2016 - 05 Mar 2017</div>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit</p>
                                            <div class="onboarding-body-box-knob">
                                              <div class="salestracker-block-knob">
                                              <input type="text" class="knob" value="24" data-thickness="0.05" data-max="100" data-width="100" data-height="100">
                                              <span>Days Left</span>
                                              </div>
                                              <div class="salestracker-block-knob">
                                              <input type="text" class="knob" value="85" data-thickness="0.05" data-max="150" data-width="100" data-height="100">
                                              <span>Complete</span>
                                              </div>
                                            </div>
                                            <a href="#" class="btn btn-lg btn-black">VIEW PROGRESS</a>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>

                                @include('frontend.sales.table')

                            </div>

                        </div>
                        <!-- /.row -->

                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->


            </section>
            <!-- /.content -->
      @endsection

      @section('sales-page-js')
        <script type="text/javascript">
        $(document).ready(function(){

          $("body").on("click",".btn-guest",function(){

              $("#addGuest").modal("show");

              $(".box-modal").addClass("after_modal_appended");

              //appending modal background inside the box-modal div
              $('.modal-backdrop').appendTo('.box-modal');

              //remove the padding right and modal-open class from the body tag which bootstrap adds when a modal is shown

              $('body').removeClass("modal-open")
              $('body').css("padding-right","");
          });

          });
        </script>
      @endsection
