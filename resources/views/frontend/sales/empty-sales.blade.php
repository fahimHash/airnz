@extends('frontend.sales.template')

@section('sales-content')

            <!-- Main content -->
            <section class="content content-sales">

                <!-- Main row -->
                <div class="row">
                    <!-- Left col -->
                    <div class="col-md-10 col-md-offset-1">

                        <div class="row">

                            <div class="col-md-12">
                                <div class="box box-white box-full box-duo-sales">
                                    <div class="box-header">
                                        <h2><span>duo</span>sales</h2>
                                        <hr>
                                    </div>
                                    <div class="box-body">
                                        <div class="box-duo-sales-left">
                                          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Tenetur illo minus hic ea quos delectus inventore reprehenderit aperiam, explicabo officia nostrum ipsam eos ad facilis at debitis.</p>
                                          <div class="box-duo-sales-left-img">
                                            <img src="assets/img/plane.png">
                                          </div>
                                        </div>
                                        <div class="box-duo-sales-right">
                                          <div class="salestracker-block-knob">
                                          <input type="text" class="knob" value="56" data-thickness="0.05" data-max="100" data-width="150" data-height="150">
                                          <span>Total Sales</span>
                                          </div>
                                          <div class="salestracker-block-knob">
                                          <input type="text" class="knob" value="75" data-thickness="0.05" data-max="150" data-width="150" data-height="150">
                                          <span>This Week</span>
                                          </div>
                                          <div class="box-duo-sales-info">
                                            <div class="box-duo-sales-info-left">
                                              <h4>360</h4>
                                              Empty Seats
                                            </div>
                                            <div class="box-duo-sales-info-right">
                                              <h3>SYD <span><img src="assets/img/ico-plane.png" alt=""></span> AKL</h3>
                                              <div class="seats-available">
                                                <img src="assets/img/ico-empty.png" alt=""> 360 Seats available
                                              </div>
                                              <div class="seats-full">
                                                <img src="assets/img/ico-full.png" alt=""> 000 Seats full
                                              </div>
                                            </div>
                                            <hr>
                                            <h6>Special guests:</h6>
                                            <a class="btn-guest"><span>?</span></a>
                                            <a class="btn-guest"><span>?</span></a>
                                            <a class="btn-guest"><span>?</span></a>

                                          </div>
                                          <div class="text-center"><a href="add-sales.html" class="btn btn-lg btn-black">ADD SALES</a></div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                </div>

                                <div class="box box-full box-map">
                                    <div class="box-header">
                                        <h2><span>destination</span>tracker</h2>
                                        <hr>
                                    </div>
                                    <div id="map"></div>
                                    <!-- /.box-body -->
                                </div>

                                <div class="box box-full">
                                    <div class="box-header">
                                        <h2><span>sales</span>tracker</h2>
                                        <hr>
                                    </div>

                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs nav-tabs-salestracker" role="tablist">
                                      <li role="presentation"><a href="#tab-yearly" aria-controls="tab-yearly" role="tab" data-toggle="tab">Yearly</a>
                                      </li>
                                      <li role="presentation"><a href="#tab-monthly" aria-controls="tab-monthly" role="tab" data-toggle="tab">Monthly</a>
                                      </li>
                                      <li role="presentation" class="active"><a href="#tab-weekly" aria-controls="tab-weekly" role="tab" data-toggle="tab">Weekly</a>
                                      </li>
                                    </ul>


                                    <!-- Tab panes -->
                                    <div class="tab-content tab-content-salestracker">
                                      <div role="tabpanel" class="tab-pane" id="tab-yearly">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="tab-content-salestracker-left">
                                              <div id="chart-yearly" style="height: 300px;"></div>
                                            </div>
                                            <div class="tab-content-salestracker-right clearfix">
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="56" data-thickness="0.05" data-max="100" data-width="150" data-height="150">
                                                <span>This Year</span>
                                                </div>
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="75" data-thickness="0.05" data-max="150" data-width="150" data-height="150">
                                                <span>Last Year</span>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div role="tabpanel" class="tab-pane" id="tab-monthly">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="tab-content-salestracker-left">
                                              <div id="chart-monthly" style="height: 300px;"></div>
                                            </div>
                                            <div class="tab-content-salestracker-right clearfix">
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="56" data-thickness="0.05" data-max="100" data-width="150" data-height="150">
                                                <span>This Month</span>
                                                </div>
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="75" data-thickness="0.05" data-max="150" data-width="150" data-height="150">
                                                <span>Last Month</span>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div role="tabpanel" class="tab-pane active" id="tab-weekly">
                                        <div class="row">
                                          <div class="col-md-12">
                                            <div class="tab-content-salestracker-left">
                                              <div id="chart-weekly" style="height: 300px;"></div>
                                            </div>
                                            <div class="tab-content-salestracker-right clearfix">
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="56" data-thickness="0.05" data-max="100" data-width="150" data-height="150">
                                                <span>This Week</span>
                                                </div>
                                                <div class="salestracker-block-knob">
                                                <input type="text" class="knob" value="75" data-thickness="0.05" data-max="150" data-width="150" data-height="150">
                                                <span>Last Week</span>
                                                </div>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>


                                    <!-- /.box-body -->
                                </div>

                                @include('frontend.sales.table')

                            </div>

                        </div>
                        <!-- /.row -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
@endsection
