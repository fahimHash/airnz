@extends('frontend.layouts.template', ['class' => 'sales-page'])

@push('styles')
    <link href="{{ url('css/datepicker.css') }}" rel="stylesheet" type='text/css'>
    {{--<link rel="stylesheet" href="{{ url('libs/dropzone/dropzone.css')}}">--}}
    <style>
        label {
            margin-bottom: 0;
        }
    </style>
@endpush

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header content-header-campaigns content-header-global">
                <div class="page-title">
                    <h3><span>duo</span>sales</h3>
                </div>
                <ul>
                  <li><a href="{{ url('/my-sales')}}">My Sales</a></li>
                  <li><a class="current" href="{{ url('/add-sales')}}">Add a Booking</a></li>
                </ul>
            </section>

        <!-- Main content -->
        <section class="content content-sales">

            <!-- Main row -->
            <div class="row">
                <!-- Left col -->
                <div class="col-md-10 col-md-offset-1">
                    <div class="row">

                        <div class="col-md-12">
                            @include('frontend.layouts.message')
                            <div class="box box-full">
                                <div class="box-header">
                                    <a class="add-position" data-toggle="collapse" href="#addSales"
                                       aria-expanded="false">
                                        <i class="fa fa-angle-up"></i>
                                    </a>
                                    <h2>Add a Booking</h2>
                                    <div class="collapse in" id="addSales">
                                        <hr>
                                        <p>To receive rewards, or participate in duo campaigns, enter your Air New
                                            Zealand bookings below. Each booking will be validated before rewards can be
                                            earned. You can check the status of your booking below.</p>

                                        {!! Form::open(['url' => '/add-sales', 'class' => 'add-sales-form','id'=>'add-sales',
                                        'data-parsley-excluded'=>"input[type=button], input[type=submit], input[type=reset]",
                                        'data-parsley-trigger'=>"keyup input change paste", 'data-parsley-validate'
                                        ]) !!}

                                        <div class="add-pnr-wrap">
                                            <a class="add-pnr">
                                                Add PNR <span><i class="icon-ANE1227_TEP_icon_set_V1_add"></i></span>
                                            </a>

                                            <div class="pnr-entry">
                                                <div class="form-group">
                                                    <div class="mdc-text-field text-field mdc-text-field--upgraded pnr"
                                                         data-mdc-auto-init="registerFields">
                                                        <input class="mdc-text-field__input" type="text"
                                                               name="pnr[]"
                                                               autocomplete="off"
                                                               oninput="this.value = this.value.toUpperCase()"
                                                               maxlength="6"
                                                               data-parsley-letter="H"
                                                               data-parsley-required
                                                               data-parsley-maxlength="6"
                                                               data-parsley-minlength="6"
                                                               data-parsley-maxlength-message="PNR should be 6 characters."
                                                               data-parsley-minlength-message="PNR should be 6 characters."
                                                               data-parsley-required-message="Air New Zealand PNR is required"
                                                               data-parsley-validation-threshold="0"
                                                               data-parsley-remote-options='{ "type": "POST" }'
                                                               data-parsley-remote="{{ route('checkPNR') }}"
                                                               data-parsley-remote-message="Error - This PNR has already been entered into DUO."
                                                        >
                                                        <label for="pnr" class="mdc-floating-label">Air New Zealand
                                                            PNR</label>
                                                        <div class="mdc-line-ripple"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="mdc-text-field text-field mdc-text-field--upgraded creation"
                                                         data-mdc-auto-init="registerFields">
                                                        <input class="mdc-text-field__input add-sales-datepicker"
                                                               type="text" autocomplete='off'
                                                               name="creation[]"
                                                               data-parsley-required data-parsley-required-message="PNR Creation Date is required" data-parsley-validation-threshold="0" data-parsley-trigger="input"
                                                        >
                                                        <label for="creation" class="mdc-floating-label">PNR Creation
                                                            Date</label>
                                                        <div class="mdc-line-ripple"></div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="mdc-text-field text-field mdc-text-field--upgraded groupid"
                                                         data-mdc-auto-init="registerFields">
                                                        <input class="mdc-text-field__input" type="text"
                                                               name="groupid[]"
                                                               autocomplete="off"
                                                               >
                                                        <label for="groupid" class="mdc-floating-label">Group Shell
                                                            ID</label>
                                                        <div class="mdc-line-ripple"></div>
                                                    </div>
                                                </div>
                                                <div class="pnr-util">
                                                    <a class="add-pnr-row">
                                                        <span><i class="icon-ANE1227_TEP_icon_set_V1_add"></i></span>
                                                    </a>
                                                    <a class="delete-pnr">
                                                        <i class="icon-ANE1227_TEP_icon_set_V1_trash2"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            {!! Form::button('SUBMIT SALES', ['class' => 'btn btn-black btn-lg','type' => 'submit']) !!}
                                            <button class="btn btn-default btn-lg"
                                                    onclick="window.location.href='{{url('/my-sales')}}';return false;">
                                                CANCEL
                                            </button>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                                <!-- /.box-body -->
                            </div>
                        </div>
                    </div>
                {{--@include('frontend.sales.upload')--}}
                <!-- /.row -->
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('page_specific_modal')
    <div id="add-sales-alert" class="modal-alert modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content text-center">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <img src="assets/img/ico-alert.png" alt="">
                </div>
                <h3>Oh no!</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sapiente optio repellendus eligendi ab ea,
                    expedita, sed recusandae.</p>
                <div>
                    <button type="button" class="btn btn-lg btn-black" data-dismiss="modal">GO BACK</button>
                </div>
            </div>

        </div>
    </div>
    <div id="add-sales-success" class="modal-success modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content text-center">

                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <img src="assets/img/ico-success.png" alt="">
                </div>
                <h3>Awesome!</h3>
                <p>Success, your bookings will be validated and we'll notifiy you when these are approved!</p>
                <div>
                    <button type="button" class="btn btn-lg btn-black" data-dismiss="modal">GO IT!</button>
                </div>
            </div>

        </div>
    </div>
@endsection

@push('scripts')
    {{--<script src="{{('libs/dropzone/dropzone.js')}}"></script>--}}
    <script src="{{ url('js/datepicker.js') }}"></script>
    <script src="{{ url('libs/parsley.min.js') }}"></script>
    <script type="text/javascript">
        var max_fields_pnr = 5;
        var max_fields_pnr_entry = 20;
        var count_pnr = 1;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#add-sales').parsley({
            errorClass: 'parsley-error icon-wrong',
            successClass: 'parsley-success icon-correct',
            errorsWrapper: '<ul class="parsley-errors-list"></ul>',
            errorTemplate: '<li></li>'
        });
        $(document).on("click", ".add-pnr", function () {
            if (count_pnr < max_fields_pnr) {
                var ele = '<div class="add-pnr-wrap">\
          <a class="add-pnr" id="addbtn">\
            Add PNR <span><i class="icon-ANE1227_TEP_icon_set_V1_add"></i></span>\
          </a>\
          <div class="pnr-entry">\
            <div class="form-group">\
                <div class="mdc-text-field mdc-text-field--upgraded" data-mdc-auto-init="MDCTextField">\
                    <input class="mdc-text-field__input" type="text"  name="pnr[]"\
                     autocomplete="off"\
                     data-parsley-required\
                     data-parsley-maxlength="6"\
                     data-parsley-maxlength-message="PNR should be 6 characters."\
                     data-parsley-minlength="6"\
                     data-parsley-minlength-message="PNR should be 6 characters."\
                     data-parsley-required-message="Air New Zealand PNR is required"\
                     data-parsley-validation-threshold="0"\
                     data-parsley-remote-options=\'{ "type": "POST" }\' data-parsley-remote="{{ route('checkPNR') }}"\
                     data-parsley-remote-message="Error - This PNR has already been entered into DUO."\
                     data-parsley-letter="H"\
                     oninput="this.value = this.value.toUpperCase()"\
                     maxlength="6"\
                     >\
                    <label for="pnr" class="mdc-floating-label">Air New Zealand PNR</label>\
                    <div class="mdc-line-ripple"></div>\
                </div>\
            </div>\
            <div class="form-group">\
                <div class="mdc-text-field mdc-text-field--upgraded" data-mdc-auto-init="MDCTextField">\
                    <input class="mdc-text-field__input add-sales-datepicker" type="text"  name="creation[]" autocomplete="off" data-parsley-required data-parsley-required-message="PNR Creation Date is required" data-parsley-validation-threshold="0">\
                    <label for="creation" class="mdc-floating-label">PNR Creation Date</label>\
                    <div class="mdc-line-ripple"></div>\
                </div>\
            </div>\
            <div class="form-group">\
                <div class="mdc-text-field mdc-text-field--upgraded" data-mdc-auto-init="MDCTextField">\
                    <input class="mdc-text-field__input" type="text"  name="groupid[]" autocomplete="off">\
                                        <label for="groupid" class="mdc-floating-label">Group Shell ID</label>\
                                        <div class="mdc-line-ripple"></div>\
                                   </div>\
                               </div>\
                                <div class="pnr-util">\
                                  <a class="add-pnr-row">\
                                    <span><i class="icon-ANE1227_TEP_icon_set_V1_add"></i></span>\
                                  </a>\
                                  <a class="delete-pnr">\
                                    <i class="icon-ANE1227_TEP_icon_set_V1_trash2"></i>\
                                  </a>\
                                </div>\
                              </div>\
                            </div>';
                var closestEle = $(this).closest('.add-pnr-wrap');
                if (closestEle.find("input").val() == '') {
                    closestEle.closest(".pnr-entry").remove();
                }
                $(this).closest('.add-pnr-wrap').before(ele);
                $(this).closest('.add-pnr').remove();
                count_pnr++;
            }
            initMdcDatePicker(document.querySelector('.add-sales-datepicker'));
            window.mdcAutoInit();
            $('#add-sales').parsley().refresh();
        })
        $(document).on('click', ".add-pnr-row", function () {
            var th = $(this)
            th.closest(".add-pnr-wrap").each(
                function () {
                    if (($(this).find('.pnr-entry')).length != max_fields_pnr_entry) {
                        var ele = '<div class="pnr-entry">\
                <div class="form-group">\
                <div class="mdc-text-field mdc-text-field--upgraded" data-mdc-auto-init="MDCTextField">\
                    <input class="mdc-text-field__input" type="text"  name="pnr[]"\
                    autocomplete="off"\
                    data-parsley-required\
                    data-parsley-maxlength="6"\
                    data-parsley-minlength="6"\
                    data-parsley-maxlength-message="PNR should be 6 characters."\
                    data-parsley-minlength-message="PNR should be 6 characters."\
                    data-parsley-required-message="Air New Zealand PNR is required"\
                    data-parsley-validation-threshold="0"\
                    data-parsley-remote-options=\'{ "type": "POST" }\'\
                    data-parsley-remote="{{ route('checkPNR') }}"\
                    data-parsley-remote-message="Error - This PNR has already been entered into DUO."\
                    data-parsley-letter="H"\
                        oninput="this.value = this.value.toUpperCase()"\
                        maxlength="6"\
                    >\
                    <label for="pnr" class="mdc-floating-label">Air New Zealand PNR</label>\
                    <div class="mdc-line-ripple"></div>\
                </div>\
            </div>\
            <div class="form-group">\
                <div class="mdc-text-field mdc-text-field--upgraded" data-mdc-auto-init="MDCTextField">\
                    <input class="mdc-text-field__input add-sales-datepicker" type="text"  name="creation[]" autocomplete="off" data-parsley-required data-parsley-required-message="PNR Creation Date is required" data-parsley-validation-threshold="0">\
                    <label for="creation" class="mdc-floating-label">PNR Creation Date</label>\
                    <div class="mdc-line-ripple"></div>\
                </div>\
            </div>\
            <div class="form-group">\
                <div class="mdc-text-field mdc-text-field--upgraded" data-mdc-auto-init="MDCTextField">\
                    <input class="mdc-text-field__input" type="text"  name="groupid[]" autocomplete="off">\
                    <label for="groupid" class="mdc-floating-label">Group Shell ID</label>\
                    <div class="mdc-line-ripple"></div>\
               </div>\
           </div>\
                <div class="pnr-util">\
                  <a class="add-pnr-row">\
                    <span><i class="icon-ANE1227_TEP_icon_set_V1_add"></i></span>\
                  </a>\
                  <a class="delete-pnr">\
                    <i class="icon-ANE1227_TEP_icon_set_V1_trash2"></i>\
                  </a>\
                </div>\
            </div>';
                        th.closest('.pnr-entry').before(ele);
                    }
                }
            );
            initMdcDatePicker(document.querySelector('.add-sales-datepicker'));
            window.mdcAutoInit();
            $('#add-sales').parsley().refresh();
        })
        $(document).on("click", ".delete-pnr", function () {
            var ele = $(this);
            $(this).closest(".add-pnr-wrap").each(
                function () {
                    if (($(this).find('.pnr-entry')).length >= 2) {
                        ele.closest('.pnr-entry').remove();
                    } else {
                        if (count_pnr >= 2) {
                            if (ele.parent().closest('.add-pnr-wrap').has('#addbtn').length != 1) {
                                ele.parent().closest('.add-pnr-wrap').remove();
                                count_pnr--;
                            }
                        }
                    }
                }
            );
        });
        function initMdcDatePicker(input) {
            if(input != null) {
                const picker = new window.mdcDatePicker()
                    .on('submit', (val) => {
                        if(input.nextElementSibling.classList[0] == 'mdc-floating-label'){
                            input.nextElementSibling.classList.add('mdc-floating-label--float-above');
                        }else{
                            input.nextElementSibling.nextElementSibling.classList.add('mdc-floating-label--float-above');
                        }
                        input.value = val.format("DD-MM-YYYY");
                    }).on('close',() => {
                        $('#add-sales').parsley().refresh();
                    });
                input.addEventListener('focus', () => picker.open());
            }
        }
        initMdcDatePicker(document.querySelector('.add-sales-datepicker'));

        window.Parsley.addValidator('letter', {
            requirementType: 'string',
            validateString: function (value, requirement) {
                if(value.charAt(5) == 'H'){
                    return true;
                }else{
                    return false;
                }
            },
            messages: {
                en: 'Your Air New Zealand PNR must end in the letter H.'
            }
        });
        @if (session('modal'))
            $('#add-sales-success').modal('show');
        @endif
    </script>
    {{--
    <script type="text/javascript">
        /*Dropzone.prototype.defaultOptions.dictDefaultMessage = "Drop files here to upload";
        Dropzone.prototype.defaultOptions.dictFallbackMessage = "Your browser does not support drag'n'drop file uploads.";
        Dropzone.prototype.defaultOptions.dictFallbackText = "Please use the fallback form below to upload your files like in the olden days.";
        Dropzone.prototype.defaultOptions.dictFileTooBig = "File is too big (filesizeMiB). Max filesize: maxFilesizeMiB.";
        Dropzone.prototype.defaultOptions.dictInvalidFileType = "You can't upload files of this type.";
        Dropzone.prototype.defaultOptions.dictResponseError = "Server responded with statusCode code.";
        Dropzone.prototype.defaultOptions.dictCancelUpload = "Cancel upload";
        Dropzone.prototype.defaultOptions.dictCancelUploadConfirmation = "Are you sure you want to cancel this upload?";
        Dropzone.prototype.defaultOptions.dictRemoveFile = "Remove file";
        Dropzone.prototype.defaultOptions.dictMaxFilesExceeded = "You can not upload any more files.";*/
        Dropzone.prototype.defaultOptions.dictRemoveFile = "";
        Dropzone.options.uploadSales = {
            // forceFallback: true,
            dictDefaultMessage: "Drop file here or",
            acceptedFiles: ".csv",
            addRemoveLinks: true,
            removedfile: function (file) {
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
            },
            success: function (file, response) {
                console.log(response);
                if (response.flag) {
                    $('#add-sales-success > div > div > p').text(response.msg);
                    $('#add-sales-success').modal('show');
                } else {
                    $('#add-sales-alert > div > div > p').text(response.msg);
                    $('#add-sales-alert').modal('show');
                }

            },
            error: function (file, response) {
                console.log(response);
                if (response.file[0]) {
                    $('#add-sales-alert > div > div > p').text(response.file[0]);
                } else {
                    $('#add-sales-alert > div > div > p').text(response.msg);
                }
                $('#add-sales-alert').modal('show');
            }
        };
    </script>
    --}}
@endpush
