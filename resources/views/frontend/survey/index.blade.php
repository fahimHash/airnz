@extends('frontend.layouts.template', ['class' => 'training-page onboarding-page'])

@push('styles')
@endpush

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper content-wrapper-preloader">
        <!-- Content Header (Page header) -->
        <section class="content-header content-header-campaigns">
            <div class="page-title">
                <h3><span>duo</span>surveys</h3>
            </div>
        </section>
        <div id="maincontent" class="content content-onboarding content-training">
        @include('frontend.layouts.message')
            <!-- Main content -->
            <section class="onboarding-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            @foreach($surveys as $survey)
                                <div class="col-md-4">
                                    <div class="onboarding-body-box" style="background-image: url({{ asset('storage/'.$survey->image) }});">
                                        <h4>{{ $survey->name }}</h4>
                                        <hr>
                                        <p>{{ $survey->description }}</p>
                                        <a href="{{ route('f.survey.show', $survey->slug) }}" class="btn btn-lg btn-black">START SURVEY</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /.content-wrapper -->

@endsection

@push('scripts')
@endpush
