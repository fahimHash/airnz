@extends('frontend.layouts.template', ['class' => 'training-page onboarding-page'])

@push('styles')
@endpush

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper content-wrapper-preloader">
        <!-- Content Header (Page header) -->
        <section class="content-header content-header-campaigns">
            <div class="page-title">
                <h3><span>duo</span>surveys</h3>
            </div>
        </section>
        <div id="maincontent" class="content content-training content-onboarding">
            @include('frontend.layouts.message')
            <!-- Main content -->
            <section class="onboarding-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            {!! Form::open(['url' => route('f.survey.next',$survey->slug), 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data','id'=>'surveyForm']) !!}
                            {!! Form::hidden('currentPage',$questions->currentPage()) !!}
                            {!! Form::hidden('lastPage',$questions->lastPage()) !!}
                            @foreach($questions as $question)
                                {!! Form::hidden('question',$question->id) !!}
                            <div class="col-md-8" style="background-color: rgba(255, 255, 255, 0.9)">
                                <div class="col-md-8">
                                    <div class="onboarding-body-box">
                                        <h4>{{ $question->title }}</h4>
                                        <hr>
                                        @foreach($question->answers as $answer)
                                            {!! Form::checkbox('answers[]',$answer->id) !!}
                                            {!! Form::label($answer->name) !!}
                                            <br>
                                        @endforeach
                                        @if($questions->hasMorePages())
                                            {!! Form::submit('NEXT', ['class' => 'btn btn-lg btn-black']) !!}
                                        @else
                                            {!! Form::submit('SUBMIT!', ['class' => 'btn btn-lg btn-black']) !!}
                                        @endif
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{ url('https://dummyimage.com/350x350/d8d5e0/ffffff')}} }}" class="img-responsive">
                                </div>
                            </div>
                            {!! Form::close() !!}
                            @endforeach
                            @foreach($surveys as $survey)
                                <div class="col-md-4">
                                    <div class="onboarding-body-box" style="background-image: url({{ asset('storage/'.$survey->image) }});">
                                        <h4>{{ $survey->name }}</h4>
                                        <hr>
                                        <p>{{ $survey->description }}</p>
                                        <a href="{{ route('f.survey.show', $survey->slug) }}" class="btn btn-lg btn-black">START SURVEY</a>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /.content-wrapper -->

@endsection

@push('scripts')
@endpush
