@foreach($news as $item)
    <img class="img-responsive" src="{{url('assets/img/'.$item->image)}}">
    <div class="anz-feed-list">
        <div class="anz-feed-list-date">{{ \Carbon\Carbon::parse($item->created_at)->formatLocalized('%d %B %Y') }}</div>
        <i class="icon-sales"></i>
        <a class="users-list-name" href="/news/{{ $item->slug }}">{{ $item->title }}</a>
        <p>{{ $item->summary }}</p>
    </div>
@endforeach