@include('frontend.layouts.header')
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper {{ isset($class) ? $class : '' }}">

    @include('frontend.layouts.navbar')
    @include('frontend.layouts.sidebar')
    @include('frontend.layouts.control-siderbar')

    @yield('content')


    <footer class="main-footer clearfix">
        @include('frontend.layouts.footer')
    </footer>
    @yield('page_specific_modal')
</div>
<!-- ./wrapper -->

@include('frontend.layouts.scripts')
