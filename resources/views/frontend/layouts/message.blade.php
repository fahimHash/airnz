@if (session('success'))
  <div class="alert alert-success alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{ session('success') }}
  </div>
@endif

@if (session('danger'))
  <div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      {{ session('danger') }}
  </div>
@endif

@if (session('userId'))
  <div class="alert alert-danger alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      Please verify your account. <a href="{{url('verify/resend', session('userId') )}}">Resend Confirmation Email</a>
  </div>
@endif
@if(!isset($display))
  @if ($errors->all())
  <div class="alert alert-warning alert-dismissable">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      @foreach( $errors->all() as $error )
         {{ $error }} <br>
      @endforeach
  </div>
  @endif
@endif