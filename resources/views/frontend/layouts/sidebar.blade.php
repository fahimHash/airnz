<div class="preloader-hide">
<!-- Left side column. contains the logo and sidebar -->
<aside id="sidebarleft" class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
      <!-- <li class="header">MAIN NAVIGATION</li> -->
      <li><a href="{{ url('/home') }}"><i class="icon-dashboard"></i> <span>Dashboard</span></a></li>
      <li class="treeview">
        <a href="{{ url('/my-sales') }}">
          <i class="icon-sales"></i> <span><strong>duo</strong>sales</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('/my-sales') }}"><i class="fa fa-angle-right"></i> My Sales</a></li>
          <li class="active"><a href="{{ url('/add-sales') }}"><i class="fa fa-angle-right"></i> Add a Booking</a></li>
        </ul>
      </li>
      <li><a href="{{ url('/campaigns') }}"><i class="icon-campaigns"></i> <span><strong>duo</strong>campaigns</span></a></li>
      <li><a href="{{ url('/training') }}"><i class="icon-couching"></i> <span><strong>duo</strong>training</span></a></li>
      <li><a href="{{ url('/travel-tools') }}"><i class="icon-tools"></i> <span><strong>duo</strong>tools</span></a></li>
      <li><a href="{{ url('/rewards') }}"><i class="icon-rewards"></i> <span><strong>duo</strong>rewards</span></a></li>
      <!-- <li><a href="{{ url('/profile/detail') }}"><i class="icon-profile"></i> <span>Profile</span></a></li> -->
      <li class="treeview">
        <a href="{{ url('/profile') }}">
          <i class="icon-profile"></i> <span>Profile</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="{{ url('/profile') }}"><i class="fa fa-angle-right"></i> Dashboard</a></li>
          <li><a href="{{ url('/profile/edit') }}"><i class="fa fa-angle-right"></i> Personal Details</a></li>
          <li><a href="{{ url('/company') }}"><i class="fa fa-angle-right"></i> Company Details</a></li>
          <li><a href="{{ url('/activity-feed') }}"><i class="fa fa-angle-right"></i> Activity Feed</a></li>
          <li><a href="{{ url('/my-account') }}"><i class="fa fa-angle-right"></i> Account Settings</a></li>
          <li><a href="{{ route('mybdm') }}"><i class="fa fa-angle-right"></i> My BDM</a></li>
        </ul>
      </li>
      <li><a href="{{ url('/tour') }}"><i class="icon-tour"></i> <span>Take the Tour</span></a></li>
      <li><a href="{{ url('/logout') }}"
              onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();"
        ><i class="icon-ANE1227_TEP_icon_set_V1_exit"></i> <span>Log out</span></a>
        <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
</div>
