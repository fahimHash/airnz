<!-- Control Sidebar -->
 <aside class="control-sidebar">
   <div class="control-sidebar-close">
     <a class="sidebar-right-toggle" href="#" data-toggle="control-sidebar">X</a>
   </div>
   <!-- Create the tabs -->
   <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
     <li class="anz-notification-tab"><a href="#control-sidebar-notification-tab" data-toggle="tab">Notifications</a></li>
       <!-- Tab panes -->
     <li class="anz-contact-tab active"><a href="#control-sidebar-contact-tab" data-toggle="tab">Contact</a></li>
   </ul>
   <div class="tab-content">
     <!-- Home tab content -->
     <div class="tab-pane anz-notification-tab" id="control-sidebar-notification-tab">
       @if (count(Auth::user()->unreadNotifications) > 0)
         <span class="unread" id="unreadNotification">{{ count(Auth::user()->unreadNotifications) }} Unread</span>
       @endif
       <ul class="anz-feed-list clearfix">
         @foreach(Auth::user()->unreadNotifications as $notification)
           <li>
             <a class="recent clearfix" id="newNotification" href="#">
               <span class="anz-feed-list-date">{{ $notification->created_at->diffForHumans() }}</span>
               <i class="icon-{{$notification['data']['module']}}"></i>
               <span class="users-list-name">{{$notification['data']['title']}}</span>
               <span class="users-list-text">{{$notification['data']['message']}}</span>
             </a>
           </li>
         @endforeach
         @foreach(Auth::user()->lastTenNotifications as $readNotifications)
           <li>
             <a class="clearfix" href="{{ url($readNotifications['data']['link'])}}">
               <span class="anz-feed-list-date">{{ $readNotifications->created_at->diffForHumans() }}</span>
               <i class="icon-{{$readNotifications['data']['module']}}"></i>
               <span class="users-list-name">{{$readNotifications['data']['title']}}</span>
               <span class="users-list-text">{{$readNotifications['data']['message']}}</span>
             </a>
           </li>
         @endforeach
       </ul>
     </div>
     <!-- /.tab-pane -->
     <!-- /.tab-pane -->
     <!-- Settings tab content -->
     <div class="tab-pane anz-contact-tab active" id="control-sidebar-contact-tab">
       <div class="box-sidebar">
         <!-- /.box-header -->
         <div class="box-body no-padding">
           <ul class="users-list clearfix">
             <img src="
                 @if(!empty($manager->image))
                  {{url('assets/img/'.$manager->image)}}
                 @else
                  {{ url('assets/img/default-image.png') }}
                 @endif
                 " class="img-circle">
             <li>
               <h4>{{!empty($manager->name) ? $manager->name : 'Agency Support'}}</h4>
               <span class="users-list-date">Air New Zealand, BDM</span>
             </li>
           </ul>
           <div class="anz-bdm-info">
             {{--<div>{{!empty($manager->phoneNumber) ? $manager->phoneNumber : '131 000' }}</div>--}}
             <div>131 000</div>
             {{--<div class="anz-dm-email"><a href="mailto:{{!empty($manager->email) ? $manager->email : 'agencysupport@airnz.com.au'}}" target="_top">{{ !empty($manager->email) ? $manager->email : 'agencysupport@airnz.com.au'}}</a></div>--}}
             <div class="anz-dm-email"><a href="mailto:agencysupport@airnz.com.au" target="_top">agencysupport@airnz.com.au</a></div>
             <div><a href="#" data-toggle="control-sidebar" class="btn btn-black btn-lg">AGENCY SUPPORT</a></div>
             <hr>
           </div>
           <!-- /.users-list -->
         </div>
         <!-- /.box-body -->
       </div>
     </div>
     <!-- /.tab-pane -->
   </div>
 </aside>
 <!-- /.control-sidebar -->