<div class="pull-left">
    <img class="star-logo" src="{{ url('assets/img/Star-Alliance-Full-reverse.png') }}">
</div>
<div class="pull-right">
    <a href="{{ route('about.us') }}">About <strong>duo</strong></a> <span>|</span> <a href="{{ route('terms.and.conditions') }}">Terms & Conditions</a> <span>|</span> <a href="mailto:agencysupport@airnz.com.au?Subject=Contact%20Us" target="_top">Contact Us </a>
    {{--<span class="copyright">Copyright Air New Zealand 2016</span>--}}
</div>
