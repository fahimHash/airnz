<header class="preloader-hide-close main-header">

    <!-- Sidebar toggle button-->
    <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
      <span class="sr-only">Toggle navigation</span>
    </a>

    <!-- Header Navbar: style can be found in header.less -->
    <nav id="#mainnav" class="navbar navbar-static-top">

      <div class="anz-title">
        <img src="{{ url('assets/img/logo-duo-black.svg')}}">
      </div>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li>
            <span class="anz-logo">
              <img src="{{ url('assets/img/logo-airnz.svg')}}">
            </span>
          </li>
          <!-- Notifications: style can be found in dropdown.less -->
          <li class="notifications-menu">
            <a href="#" class="sidebar-right-toggle" data-toggle="control-sidebar">
              <i class="icon-alert"></i>
              <span class="label label-default">{{ count(Auth::user()->unreadNotifications) }}</span>
            </a>
          </li>
        </ul>
      </div>

    </nav>
  </header>
