<script src="{{ url('libs/jquery-2.2.3.min.js') }}"></script>
<script src="{{ url('libs/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ mix('js/bootstrap.js') }}"></script>
<script src="{{ url('libs/app.min.js') }}"></script>
<script src="{{ url('assets/custom.js') }}"></script>
@stack('scripts')
</body>
</html>