@extends('frontend.layouts.template', ['class' => 'onboarding-page'])
@push('styles')
@endpush
@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper content-wrapper-preloader">
        <!-- Content Header (Page header) -->
        <section class="content-header content-header-campaigns">
            <div class="page-title">
                <h3><span></span>My Rewards</h3>
            </div>
        </section>
        <div class="content content-onboarding">

            <section class="onboarding-body">
                <div class="row">
                    <div class="onboarding-body-box box-white" style="background-image: url({{ url('assets/img/'.$reward->image) }});">
                        <div class="onboarding-body-box-util">
                            <span><i class="icon-sales"></i></span>
                            <span class="approved"><i class="icon-approved"></i></span>
                        </div>
                        <h4>{{ $reward->name }}</h4>
                        <hr>
                        <div class="onboarding-body-sales-date">{{ $reward->endDate }}</div>
                        {!! $reward->message !!}
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /.content-wrapper -->

@endsection

@push('scripts')
@endpush
