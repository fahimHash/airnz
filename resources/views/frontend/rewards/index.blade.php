@extends('frontend.layouts.template', ['class' => 'onboarding-page'])

@push('styles')
@endpush

@section('content')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper content-wrapper-preloader">
        <!-- Content Header (Page header) -->
        <section class="content-header content-header-campaigns content-header-global">
                <div class="page-title">
                    <h3><span>duo</span>rewards</h3>
                </div>
                <ul>
                  <li><a class="current" href="#">My Rewards</a></li>
                </ul>
            </section>
        <div class="content content-onboarding">

            <section class="onboarding-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            @foreach($rewards as $reward)
                                <div class="col-md-4">
                                <div class="onboarding-body-box box-white" style="background-image: url({{ url('assets/img/'.$reward->image) }});">
                                    <div class="onboarding-body-box-util">
                                        <a href="{{ url('rewards/detail/'.$reward->id) }}">View more</a>
                                        <span><i class="icon-sales"></i></span>
                                        <span class="approved"><i class="icon-approved"></i></span>
                                    </div>
                                    <h4>{{ $reward->name }}</h4>
                                    <hr>
                                    <div class="onboarding-body-sales-date">{{ $reward->endDate }}</div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /.content-wrapper -->

@endsection

@push('scripts')
@endpush
