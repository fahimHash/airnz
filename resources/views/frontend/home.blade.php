@extends('frontend.layouts.template', ['class' => 'onboarding-page'])

@push('styles')
    <link rel="stylesheet" href="{{ url('libs/animate.min.css') }}">
    <link rel="stylesheet" href="{{ url('libs/owl.carousel/owl.carousel.css') }}">
@endpush

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <div class="content content-onboarding content-wrapper-preloader">
            <!-- Content Header (Page header) -->
            <div class="preloader" style="background-image: url( {{url('assets/img/bg-onboarding.jpg')}} );">
                <div class="status">
                    <img id="bg-spin" src="{{url('assets/img/loading2.png')}}" class="img-responsive">
                    <img class="loading-logo img-responsive" src="{{url('assets/img/loading-logo.png')}}">
                </div>
            </div>
            <section id="onboarding-header-wrap" class="onboarding-header">
                <div class="row">
                    <!-- <div class="col-md-5"> -->
                    <div class="onboarding-users-list-wrap">
                        <div class="onboarding-users-list">
                            <img src="{{url('assets/img/_Image9.png')}}" class="img-circle">
                            <div class="users-list-info">
                                <h2>{{$user->name}}</h2>
                                <p>Profile 20% complete</p>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown"
                                            aria-haspopup="true" aria-expanded="false">
                                        Get more from <strong>duo</strong> <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="onboarding-header-block-tour">
                        <div class="onboarding-header-block">
                            <i class="icon-tour"></i>
                        </div>
                        <div class="text-center"><a href="#">Take the Tour</a></div>
                    </div>
                    <div class="onboarding-header-block-rewards">
                        <div class="onboarding-header-block rewards-icons">
                            <i class="icon-tour"></i>
                            <i class="icon-tour"></i>
                            <i class="icon-tour"></i><br>
                            <i class="icon-tour"></i>
                            <i class="icon-tour disabled"></i>
                            <i class="icon-tour disabled"></i>
                        </div>
                        <div class="text-center"><a href="#">Earn Rewards</a></div>
                    </div>
                    <div class="onboarding-header-block-sales">
                        <div class="onboarding-header-block">
                            <div class="onboarding-header-block-knob">
                                <input type="text" class="knob" value="12" data-thickness="0.06" data-max="100"
                                       data-width="100" data-height="100">
                                <span>This Week</span>
                            </div>
                            <div class="onboarding-header-block-knob">
                                <input type="text" class="knob" value="48" data-thickness="0.06" data-max="150"
                                       data-width="100" data-height="100">
                                <span>Last Week</span>
                            </div>
                            <div class="onboarding-header-block-knob">
                                <input type="text" class="knob" value="103" data-thickness="0.06" data-max="150"
                                       data-width="100" data-height="100">
                                <span>November</span>
                            </div>
                        </div>
                        <div class="text-center"><a href="#">View Sales</a></div>
                    </div>
                </div>
            </section>

            <!-- Main content -->
            <section id="onboarding-slider-wrap" class="onboarding-slider">
                <div class="owl-carousel">
                    <div>
                        <img class="img-responsive hidden-xs" src="{{ url('assets/img/onboarding/slider.jpg')}}">
                        <img class="img-responsive hidden-sm hidden-md hidden-lg"
                             src="{{url('assets/img/onboarding/slider_mini.jpg')}}">
                        <div class="owl-caption">
                            <h3>Do Dave's<br>Itinerary</h3>
                            <p>WIN a Keith Prowse Travel<br> Super Bowl package worth $20,000</p>
                            <a href="#" class="btn btn-lg btn-black">ENTER NOW</a>
                        </div>
                    </div>
                    <div>
                        <img class="img-responsive hidden-xs" src="{{ url('assets/img/onboarding/slider.jpg')}}">
                        <img class="img-responsive hidden-sm hidden-md hidden-lg"
                             src="{{ url('assets/img/onboarding/slider_mini.jpg')}}">
                        <div class="owl-caption">
                            <h3>Do Dave's<br>Itinerary</h3>
                            <p>WIN a Keith Prowse Travel<br> Super Bowl package worth $20,000</p>
                            <a href="#" class="btn btn-lg btn-black">ENTER NOW</a>
                        </div>
                    </div>
                    <div>
                        <img class="img-responsive hidden-xs" src="{{ url('assets/img/onboarding/slider.jpg')}}">
                        <img class="img-responsive hidden-sm hidden-md hidden-lg"
                             src="{{ url('assets/img/onboarding/slider_mini.jpg')}}">
                        <div class="owl-caption">
                            <h3>Do Dave's<br>Itinerary</h3>
                            <p>WIN a Keith Prowse Travel<br> Super Bowl package worth $20,000</p>
                            <a href="#" class="btn btn-lg btn-black">ENTER NOW</a>
                        </div>
                    </div>
                    <div>
                        <img class="img-responsive hidden-xs" src="{{ url('assets/img/onboarding/slider.jpg')}}">
                        <img class="img-responsive hidden-sm hidden-md hidden-lg"
                             src="{{ url('assets/img/onboarding/slider_mini.jpg')}}">
                        <div class="owl-caption">
                            <h3>Do Dave's<br>Itinerary</h3>
                            <p>WIN a Keith Prowse Travel<br> Super Bowl package worth $20,000</p>
                            <a href="#" class="btn btn-lg btn-black">ENTER NOW</a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- /.content -->

            <section class="onboarding-body">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            @foreach($pinned as $page)
                                <div
                                        @if($page->layout == 'standard')
                                        class="col-md-6"
                                        @elseif($page->layout == 'extended')
                                        class="col-md-12"
                                        @endif>
                                    <div class="onboarding-body-box onboarding-body-box-1"
                                         style="background-color: rgba(255, 255, 255, 0.9); background-image: url({{ asset('storage/'.$page->image) }});">
                                        <h4><span>duo</span>{{ $page->title }}</h4>
                                        <hr>
                                        <p>{{ $page->summary }}</p>
                                        <a href="{{ route('f.page.show', $page->slug) }}" class="btn btn-lg btn-black">LEARN MORE</a>
                                    </div>
                                </div>
                            @endforeach
                            @foreach($pages as $page)
                                    <div
                                            @if($page->layout == 'standard')
                                            class="col-md-6"
                                            @elseif($page->layout == 'extended')
                                            class="col-md-12"
                                            @endif>
                                        <div class="onboarding-body-box onboarding-body-box-1"
                                             style="background-color: rgba(255, 255, 255, 0.9); background-image: url({{ asset('storage/'.$page->image) }});">
                                            <h4><span>duo</span>{{ $page->title }}</h4>
                                            <hr>
                                            <p>{{ $page->summary }}</p>
                                            <a href="{{ route('f.page.show', $page->slug) }}" class="btn btn-lg btn-black">LEARN MORE</a>
                                        </div>
                                    </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="onboarding-body-box onboarding-body-box-3 onboarding-body-box-right">
                                    <div class="onboarding-body-box-right-title">
                                        <h4><span>duo</span>news</h4>
                                        <hr>
                                    </div>
                                    <div class="onboarding-body-box-right-scroll">
                                        @if($news)
                                            @foreach($news as $item)
                                                <img class="img-responsive" src="{{url('assets/img/'.$item->image)}}">
                                                <div class="anz-feed-list">
                                                    <div class="anz-feed-list-date">{{ \Carbon\Carbon::parse($item->created_at)->formatLocalized('%d %B %Y') }}</div>
                                                    <i class="icon-sales"></i>
                                                    <a class="users-list-name" href="/news/{{ $item->slug }}">{{ $item->title }}</a>
                                                    <p>{{ $item->summary }}</p>
                                                </div>
                                            @endforeach
                                        @endif
                                        <div id="duo-news">

                                        </div>
                                        <div class="anz-feed-list text-center">
                                            <a href="#duo-news" id="load-news">LOAD MORE</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('scripts')
    <script src="{{ url('libs/jquery.knob.js') }}"></script>
    <script src="{{ url('libs/owl.carousel/owl.carousel.js') }}"></script>
    <script>
        // makes sure the whole site is loaded
        jQuery(window).load(function () {
            "use strict";
            // will first fade out the loading animation
            jQuery(".status").fadeOut();
            // will fade out the whole DIV that covers the website.
            jQuery(".preloader").delay(1000).fadeOut("slow");
            jQuery("#mainnav").addClass("animated bounceInDown");
            jQuery("#sidebarleft").addClass("animated bounceInLeft");
            jQuery("#onboarding-header-wrap, #onboarding-slider-wrap, .onboarding-body-box-1, .onboarding-body-box-2, .onboarding-body-box-3, .onboarding-body-box-4").addClass("animated bounceInUp");
            // jQuery("#onboarding-slider-wrap").addClass("animated bounceInUp");

            var width = jQuery(window).width();
            jQuery(window).on('resize', function(){
                if(jQuery(this).width() != width){
                    width = jQuery(this).width();
                    adjustAnimate();
                }
            });

            function adjustAnimate(){
                if ( jQuery(window).width() > 768) {
                    jQuery("#mainnav").addClass("animated bounceInDown");
                    jQuery("#sidebarleft").addClass("animated bounceInLeft");
                    jQuery("#onboarding-header-wrap, #onboarding-slider-wrap, .onboarding-body-box-1, .onboarding-body-box-2, .onboarding-body-box-3, .onboarding-body-box-4, .onboarding-body-box-5").addClass("animated bounceInUp");
                    // jQuery("#onboarding-slider-wrap").addClass("animated bounceInUp");
                } else {}
            }

            jQuery(".preloader-hide #sidebarleft, .preloader-hide-close").addClass("preloader-show");
        });
        $(document).ready(function () {
            $(".knob").knob({
                'readOnly': true,
                'fgColor': '#009DC1',
                'bgColor': '#b2e1ec',
            });
            $('.owl-carousel').owlCarousel({
                loop: true,
                //nav: true,
                dots: true,
                items: 1,
                autoplay: 3000,
            });
            var page = 1;
            $('#load-news').on('click', function () {
                page++;
                loadMoreData(page);
            });
            function loadMoreData(page) {
                axios.get('/news-feed?page=' + page)
                    .then(function (response) {
                        if (response.data.html == "") {
                            $('#duo-news').append('<div class="anz-feed-list text-center"><p>That&#39;s all for now!</p></div>');
                            $('#load-news').parent().hide();
                            return;
                        }
                        $("#duo-news").append(response.data.html);
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
            }
        });
    </script>
@endpush
