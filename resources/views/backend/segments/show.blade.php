@extends('backend.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Agent Segmentation
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/segments')}}">Agent Segments</a></li>
                <li class="active">Segmentation Detail</li>
            </ol>
        </section>
        @include('backend.layouts.message')
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Detail</h3>
                            <div class="box-tools">
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/backend/segments/'.$type, $segment->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                <button type="submit" class="btn btn-default"><i class="fa fa-trash"></i></button>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    <label for="title" class="control-label">Segment Title: </label> {{ $segment->name }}
                                    <p></p>
                                </div>
                                <div class="form-group text-center">
                                    <label for="title" class="control-label">Segment Description: </label> {{ $segment->description }}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                                <table class="table table-hover table-responsive padding" id="sales-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Agent Name</th>
                                        <th>Email</th>
                                    </tr>
                                    </thead>
                                </table>
                                <div class="box-footer text-center">
                                    <a href="{{ route('segments.index') }}" class="btn btn-warning">Go Back</a>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        $('#sales-table').DataTable({
            pageLength: 10,
            lengthChange: false,
            processing: true,
            serverSide: true,
            defaultContent: '',
            ajax: {
                url: '{{ url('/') }}/backend/segments/{{ $type }}-table',
                type:'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data: function(d){
                    d.rules = JSON.stringify({!! $segment->filters !!}, null, 2)
                },
            },
            columns: [
                {data: 'id', visible: false},
                {data: 'name', defaultContent: ''},
                {data: 'email', defaultContent: ''}
            ],
            search: {
                "regex": true
            }
        });
        $('#sales-table').css({
            width: ''
        });
    </script>
@endpush
