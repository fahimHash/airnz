@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Agent Segmentation
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Agent Segmentation</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>

                            <div class="box-tools">
                                @can('manage_user')
                                    <a href="{{ url('backend/segments/create') }}" class="btn btn-success btn-sm">Add
                                        New
                                        Agent Segmentation</a>
                                @endcan
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="tabs">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#sales" role="tab" data-toggle="tab">
                                            <icon class="fa fa-pie-chart"></icon> Sales Performance
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#systems" role="tab" data-toggle="tab" onclick="systemDataTables()">
                                            <i class="fa fa-pie-chart"></i> System Performance
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#training" role="tab" data-toggle="tab" onclick="trainingDataTables()">
                                            <i class="fa fa-pie-chart"></i> Training Performance
                                        </a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade active in" id="sales">
                                        <table class="table table-hover table-responsive padding" id="sale-table">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Segment Title</th>
                                                <th>Segment Description</th>
                                                <th>No. of Agents</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="systems">
                                        <table class="table table-hover table-responsive padding" id="systemsTable">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Segment Title</th>
                                                <th>Segment Description</th>
                                                <th>No. of Agents</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="training">
                                        <table class="table table-hover table-responsive padding" id="trainingsTable">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Segment Title</th>
                                                <th>Segment Description</th>
                                                <th>No. of Agents</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#sale-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: true,
                defaultContent: '',
                ajax: '{!! route('segments.sale.table.index') !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'name', defaultContent: ''},
                    {data: 'description', defaultContent: ''},
                    {data: 'agents_count', defaultContent: ''}
                ],
                search: {
                    "regex": true
                }
            });
            $('#sale-table tbody').on('click', 'tr', function () {
            var data = table.row(this).data();
            document.location.href = '{{ url('backend/segments/sale') }}/' + data.id;
            });
        });

        function systemDataTables() {
            if (!$.fn.dataTable.isDataTable('#systemsTable')) {
                $('#systemsTable').DataTable({
                    pageLength: 10,
                    lengthChange: false,
                    processing: true,
                    serverSide: true,
                    defaultContent: '',
                    ajax: '{!! route('segments.system.table.index') !!}',
                    columns: [
                        {data: 'id', visible: false},
                        {data: 'name', defaultContent: ''},
                        {data: 'description', defaultContent: ''},
                        {data: 'agents_count', defaultContent: ''}
                    ],
                    search: {
                        "regex": true
                    }
                });
                $('#systemsTable').css({
                    width: ''
                });
            }
        }

        function trainingDataTables() {
            if (!$.fn.dataTable.isDataTable('#trainingsTable')) {
                $('#trainingsTable').DataTable({
                    pageLength: 10,
                    lengthChange: false,
                    processing: true,
                    serverSide: true,
                    defaultContent: '',
                    ajax: '{!! route('segments.training.table.index') !!}',
                    columns: [
                        {data: 'id', visible: false},
                        {data: 'name', defaultContent: ''},
                        {data: 'description', defaultContent: ''},
                        {data: 'agents_count', defaultContent: ''}
                    ],
                    search: {
                        "regex": true
                    }
                });
                $('#trainingsTable').css({
                    width: ''
                });
            }
        }
    </script>
@endpush
