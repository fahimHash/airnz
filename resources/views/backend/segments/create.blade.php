@extends('backend.layouts.app')
@push('styles')
    <link rel="stylesheet" type="text/css"
          href="{{ url('libs/jQuery-QueryBuilder-master/css/query-builder.default.min.css') }}"/>
    <link rel="stylesheet" type="text/css"
          href="{{ url('libs/jQuery-QueryBuilder-master/css/bootstrap-datepicker3.min.css') }}"/>
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Agent Segmentation
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/segments')}}">Agent Segments</a></li>
                <li class="active">Create Segmentation</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
                @include('backend.layouts.message')
                <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false">Sale Performance
                                    Segment</a>
                            </li>
                            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">System Performance
                                    Segment</a>
                            </li>
                            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Training Performance
                                    Segment</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="tab_1">
                                {!! Form::open(['url' => route('segments.sale.store'), 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data', 'id' => 'segmentForm']) !!}
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                                    {!! Form::label('name', 'Segment Title: ', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-9">
                                        {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
                                    {!! Form::label('description', 'Description: ', ['class' => 'col-md-2 control-label']) !!}
                                    <div class="col-md-9">
                                        {!! Form::text('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                        <input id="filters" name="filters" type="hidden">
                                        {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                {!! Form::close() !!}
                                <div class="form-group">
                                    <div class="col-md-offset-1 col-md-10">
                                        <div id="sale-builder"></div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-warning reset" id="btn-reset">Reset</button>
                                            <button type="button" class="btn btn-primary" id="btn-get">Generate</button>
                                            <button type="button" class="btn btn-success" id="saveBtn">Save Segment</button>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover table-responsive padding" id="sales-table">
                                            <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Agent Name</th>
                                                <th>Email</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">

                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">

                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script src="{{ url('libs/jQuery-QueryBuilder-master/js/query-builder.standalone.min.js')}}"></script>
    <script src="{{ url('libs/jQuery-QueryBuilder-master/js/bootstrap-datepicker.min.js')}}"></script>
    <script src="{{ url('libs/moment.min.js')}}"></script>
    <script>
        $(document).ready(function () {
            $('.nav-tabs a[href="#tab_1"]').tab('show');

            $('#saveBtn').on('click', function () {
                loadFilter();
                $('#segmentForm').submit();
            });

            $('#btn-reset').on('click', function () {
                $('#sale-builder').queryBuilder('reset');
            });

            $('#btn-get').on('click', function () {
                loadFilter();
                $('#sales-table').DataTable().ajax.reload();
            });
        });
        function loadFilter() {
            var result = $('#sale-builder').queryBuilder('getRules');
            $('#filters').val(JSON.stringify(result, null, 2));
        }

        var rules_basic = {
            condition: 'AND',
            rules: [{
                id: 'status',
                operator: 'equal',
                value: 'Valid'
            }]
        };
        // Fix for Bootstrap Datepicker
        $('#sale-builder').on('afterUpdateRuleValue.queryBuilder', function(e, rule) {
            if (rule.filter.plugin === 'datepicker') {
                rule.$el.find('.rule-value-container input').datepicker('update');
            }
        });
        $('#sale-builder').queryBuilder({
            plugins: ['bt-tooltip-errors'],

            filters: [
                {
                    id: 'status',
                    label: 'Sale Status',
                    type: 'string',
                    input: 'select',
                    values: {
                        Valid: 'Valid',
                        Pending: 'Pending',
                        Cancelled: 'Cancelled',
                        Problem: 'Problem'
                    },
                    operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null']
                },
                {
                    id: 'issued_date',
                    label: 'Ticket Issue Date',
                    type: 'date',
                    validation: {
                        format: 'YYYY/MM/DD'
                    },
                    plugin: 'datepicker',
                    plugin_config: {
                        format: 'yyyy/mm/dd',
                        todayBtn: 'linked',
                        todayHighlight: true,
                        autoclose: true
                    }
                },
                {
                    id: 'departure_date',
                    label: 'Departure Date',
                    type: 'date',
                    validation: {
                        format: 'YYYY/MM/DD'
                    },
                    plugin: 'datepicker',
                    plugin_config: {
                        format: 'yyyy/mm/dd',
                        todayBtn: 'linked',
                        todayHighlight: true,
                        autoclose: true
                    }
                },
                {
                    id: 'pnr',
                    label: 'PNR',
                    type: 'string'
                },
                {
                    id: 'stock',
                    label: 'Stock',
                    type: 'string'
                },
                {
                    id: 'ticket_number',
                    label: 'Ticket Number',
                    type: 'string'
                },
                {
                    id: 'origin',
                    label: 'Departing Airport',
                    type: 'string',
                    input: 'select',
                    values: {
                        @foreach($destinations as $destination)
                        {{ $destination->code }}: '{{ $destination->name }}',
                        @endforeach
                    },
                    operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null']
                },
                {
                    id: 'cty',
                    label: 'Arrival Airport',
                    type: 'string',
                    input: 'select',
                    values: {
                        @foreach($destinations as $destination)
                        {{ $destination->code }}: '{{ $destination->name }}',
                        @endforeach
                    },
                    operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null']
                },{
                    id: 'consortium_id',
                    label: 'Consortium',
                    type: 'integer',
                    input: 'select',
                    values: {
                        @foreach($consortiums as $consortium)
                        {{ $consortium->id }}: '{{ $consortium->name }}',
                        @endforeach
                    },
                    operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null']
                }, {
                    id: 'state_id',
                    label: 'State',
                    type: 'integer',
                    input: 'select',
                    values: {
                        @foreach($states as $state)
                        {{ $state->id }}: '{{ $state->name }}',
                        @endforeach
                    },
                    operators: ['equal', 'not_equal', 'in', 'not_in', 'is_null', 'is_not_null']
                }],
                rules: rules_basic
        });

        loadFilter();

        $('#sales-table').DataTable({
            pageLength: 10,
            lengthChange: false,
            processing: true,
            serverSide: true,
            defaultContent: '',
            ajax: {
                url: '{{ route('segments.sale.table')}}',
                type:'GET',
                headers: {
                    'X-CSRF-TOKEN': '{{ csrf_token() }}'
                },
                data: function(d){
                    d.rules = $('#filters').val()
                },
            },
            columns: [
                {data: 'id', visible: false},
                {data: 'name', defaultContent: ''},
                {data: 'email', defaultContent: ''}
            ],
            search: {
                "regex": true
            }
        });
        $('#sales-table').css({
            width: ''
        });
    </script>
@endpush
