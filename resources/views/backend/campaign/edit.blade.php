@extends('backend.layouts.app')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit Campaign
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url('backend/campaigns')}}"> All Campaigns</a></li>
        <li class="active">Edit Campaign</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header">
              <!-- <h3 class="box-title"></h3> -->
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class='box-body'>
              @include('backend.layouts.message')

              {!! Form::model($campaign, [
                  'method' => 'PATCH',
                  'url' => ['/backend/campaigns', $campaign->id],
                  'class' => 'form-horizontal',
                  'enctype'=>'multipart/form-data','id'=>'campaignForm'
              ]) !!}

              @include ('backend.campaign.form', ['submitButtonText' => 'Update'])

              {!! Form::close() !!}
            </div>
            </div>
            <!-- /.box -->

          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
  @endsection
