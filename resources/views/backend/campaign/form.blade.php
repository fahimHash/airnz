@push('styles')
    <link rel="stylesheet" href="{{ url('libs/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <style>
        #myNav li:not([disabled]) {
            cursor: pointer;
        }

        .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            background-color: #3c8dbc;
        }
    </style>
@endpush
<div class="container">
    <div class="row form-group">
        <div class="col-xs-12">
            <ul class="nav nav-pills nav-justified thumbnail setup-panel" id="myNav">
                <li id="navStep1" class="li-nav active" step="#step-1">
                    <a>
                        <h4 class="list-group-item-heading">Step 1</h4>
                        <p class="list-group-item-text">Campaign Title & Message</p>
                    </a>
                </li>
                <li id="navStep2" class="li-nav disabled" step="#step-2">
                    <a>
                        <h4 class="list-group-item-heading">Step 2</h4>
                        <p class="list-group-item-text">Campaign Duration & Reward</p>
                    </a>
                </li>
                <li id="navStep3" class="li-nav disabled" step="#step-3">
                    <a>
                        <h4 class="list-group-item-heading">Step 3</h4>
                        <p class="list-group-item-text">Campaign Audience</p>
                    </a>
                </li>
                <li id="navStep4" class="li-nav disabled" step="#step-4">
                    <a>
                        <h4 class="list-group-item-heading">Step 4</h4>
                        <p class="list-group-item-text">Campaign Tagging</p>
                    </a>
                </li>
                <li id="navStep5" class="li-nav disabled" step="#step-5">
                    <a>
                        <h4 class="list-group-item-heading">Step 5</h4>
                        <p class="list-group-item-text">Publish Campaign</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="container">
    <div class="row setup-content" id="step-1">
        <div class="col-xs-12">
            <div class="col-md-12 well">
                <div class="container col-xs-12">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                        {!! Form::label('name', 'Name: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('type') ? ' has-error' : ''}}">
                        <label class="col-md-4 control-label" for="type">Campaign Type</label>
                        <div class="col-md-6">
                            <select name="type" id="type" class="col-md-12">
                                <option value="">Select an option</option>
                                <option value="Tier Promotion"
                                        @if(isset($submitButtonText) && $campaign->type == 'Tier Promotion') selected="selected" @endif>
                                    Tier Promotion
                                </option>
                                <option value="Sales"
                                        @if(isset($submitButtonText) && $campaign->type == 'Sales') selected="selected" @endif>
                                    Sales
                                </option>
                                <option value="Engagement Competitions"
                                        @if(isset($submitButtonText) && $campaign->type == 'Engagement Competitions') selected="selected" @endif>
                                    Engagement Competitions
                                </option>
                            </select>
                            {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('message') ? ' has-error' : ''}}">
                        {!! Form::label('message', 'Message: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::textarea('message', null, ['class' => 'textarea', 'placeholder' => 'Place campaign message here', 'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;']) !!}
                            {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <div class="btn-group">
                        <button type="button" type="button" class="btn btn-md btn-warning" onclick="cancel()">Cancel</button>
                        <button type="button" type="button" onclick="step1Next()" class="btn btn-md btn-info">Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row setup-content" id="step-2">
        <div class="col-xs-12">
            <div class="col-md-12 well">
                <div class="container col-xs-12">
                    <div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">
                        {!! Form::label('image', 'Featured Image: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            <img src="{{ isset($campaign->image) ? url('assets/img/'.$campaign->image) : url('https://dummyimage.com/200x200/00bef3/ffffff')}}"
                                 id="showimage" class="img-circle" style="max-width: 200px; max-height: 200px;">
                            <input type="file" id="image" name="image" class="btn btn-info"
                                   style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;">
                            <br>
                            <label for="image" class="btn btn-info" style="margin-top:10px;">Choose File</label>
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('startDate') ? ' has-error' : ''}}">
                        {!! Form::label('startDate', 'Campaign Start Date: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::date('startDate', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('startDate', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('endDate') ? ' has-error' : ''}}">
                        {!! Form::label('endDate', 'Campaign End Date: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::date('endDate', null, ['class' => 'form-control', 'required' => 'required']) !!}
                            {!! $errors->first('endDate', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('sales') ? ' has-error' : ''}}">
                        {!! Form::label('sales', 'Sales Target: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::number('sales', null, ['class' => 'form-control', 'min' => 1]) !!}
                            {!! $errors->first('sales', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('destinations') ? ' has-error' : ''}}">
                        {!! Form::label('destinations', 'Destinations: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            <select id="destinations" name="destinations[]" multiple="multiple" class="col-md-12">
                                @foreach($destinations as $destination)
                                    <option value="{{ $destination->id }}"
                                            @if(isset($submitButtonText))
                                            @if(in_array($destination->id,$destination_sel))
                                            selected="true"
                                            @endif
                                            @endif
                                    >{{ $destination->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('destinations', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('reward_id') ? ' has-error' : ''}}">
                        <label class="col-md-4 control-label" for="reward_id">Reward</label>
                        <div class="col-md-6">
                            <select required name="reward_id" id="reward_id" class="col-md-12">
                                @foreach ($rewards as $reward)
                                    <option value="{{$reward->id}}"
                                    @if(isset($submitButtonText))
                                        {{ $reward->id == $campaign->reward_id ? 'selected="selected"' : '' }}
                                            @endif
                                    >{{$reward->name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <div class="btn-group">
                        <button type="button" type="button" class="btn btn-md btn-warning" onclick="cancel()">Cancel</button>
                        <button type="button" class="btn btn-md btn-info" onclick="prevStep()">Prev</button>
                        <button type="button" onclick="step2Next()" class="btn btn-md btn-info">Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row setup-content" id="step-3">
        <div class="col-xs-12">
            <div class="col-md-12 well">
                <div class="container col-xs-12">
                    <div class="form-group{{ $errors->has('territories') ? ' has-error' : ''}}">
                        <label class="col-md-4 control-label" for="territories">Territory</label>
                        <div class="col-md-6">
                            <select name="territories[]" id="territories" multiple="multiple" class="col-md-12">
                                <option value="">Select an option</option>
                                @foreach ($territories as $territory)
                                    <option value="{{$territory->id}}"
                                            @if(isset($submitButtonText))
                                            @if(in_array($territory->id,$territories_sel))
                                            selected="true"
                                            @endif
                                            @endif
                                    >{{$territory->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('territories', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('sale_segments') ? ' has-error' : ''}}">
                        {!! Form::label('sale_segments', 'Sale Performance: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            <select id="sale_segments" name="sale_segments[]" multiple="multiple" class="col-md-12">
                                @foreach($saleSegments as $saleSegment)
                                    <option value="{{ $saleSegment->id }}"
                                            @if(isset($submitButtonText))
                                            @if(in_array($saleSegment->id,$saleSegment_sel))
                                            selected="true"
                                            @endif
                                            @endif
                                    >{{ $saleSegment->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('sale_segments', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('system_segments') ? ' has-error' : ''}}">
                        {!! Form::label('system_segments', 'System Performance: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            <select id="system_segments" name="system_segments[]" multiple="multiple" class="col-md-12">
                                @foreach($systemSegments as $ystemSegment)
                                    <option value="{{ $ystemSegment->id }}"
                                            @if(isset($submitButtonText))
                                            @if(in_array($ystemSegment->id,$ystemSegment_sel))
                                            selected="true"
                                            @endif
                                            @endif
                                    >{{ $ystemSegment->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('system_segments', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('training_segments') ? ' has-error' : ''}}">
                        {!! Form::label('training_segments', 'Training Performance: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            <select id="training_segments" name="training_segments[]" multiple="multiple"
                                    class="col-md-12">
                                @foreach($trainingSegments as $trainingSegment)
                                    <option value="{{ $trainingSegment->id }}"
                                            @if(isset($submitButtonText))
                                            @if(in_array($trainingSegment->id,$trainingSegment_sel))
                                            selected="true"
                                            @endif
                                            @endif
                                    >{{ $trainingSegment->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('training_segments', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <div class="btn-group">
                        <button type="button" class="btn btn-md btn-warning" onclick="cancel()">Cancel</button>
                        <button type="button" class="btn btn-md btn-info" onclick="prevStep()">Prev</button>
                        <button type="button" onclick="step3Next()" class="btn btn-md btn-info">Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row setup-content" id="step-4">
        <div class="col-xs-12">
            <div class="col-md-12 well">
                <div class="container col-xs-12">
                    <div class="form-group{{ $errors->has('tag_type_id') ? ' has-error' : ''}}">
                        {!! Form::label('tag_type_id', 'Tag Type: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            <select required name="tag_type_id" id="tag_type_id" class="col-md-12">
                                @foreach ($tagTypes as $tagType)
                                    <option value="{{$tagType->id}}"
                                    @if(isset($submitButtonText))
                                        {{ $tagType->id == $campaign->tag_type_id ? 'selected="selected"' : '' }}
                                            @endif
                                    >{{$tagType->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('tag_type_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('tag_obj_id') ? ' has-error' : ''}}">
                        {!! Form::label('tag_obj_id', 'Objective Tags: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            <select required name="tag_obj_id" id="tag_obj_id" class="col-md-12">
                                @foreach ($tagObjs as $tagObj)
                                    <option value="{{$tagObj->id}}"
                                    @if(isset($submitButtonText))
                                        {{ $tagObj->id == $campaign->tag_obj_id ? 'selected="selected"' : '' }}
                                            @endif
                                    >{{$tagObj->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('tag_obj_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('tag_channel_id') ? ' has-error' : ''}}">
                        {!! Form::label('tag_channel_id', 'Category Channel Tags: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            <select required name="tag_channel_id" id="tag_channel_id" class="col-md-12">
                                @foreach ($tagChannels as $tagChannel)
                                    <option value="{{$tagChannel->id}}"
                                    @if(isset($submitButtonText))
                                        {{ $tagChannel->id == $campaign->tag_channel_id ? 'selected="selected"' : '' }}
                                            @endif
                                    >{{$tagChannel->name}}</option>
                                @endforeach
                            </select>
                            {!! $errors->first('tag_channel_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('additional_tags') ? ' has-error' : ''}}">
                        <label for="additional_tags" class="col-md-4 control-label">Additional Tags: </label>
                        <div class="col-md-6">
                            <select id="additional_tags" name="additional_tags[]" multiple="multiple" class="col-md-12">
                                @foreach($additionalTags as $key => $values)
                                    <optgroup label="{{ $key }}">
                                        @foreach($values as $additionalTag)
                                            <option value="{{ $additionalTag->id }}"
                                                    @if(isset($submitButtonText))
                                                    @if(in_array($additionalTag->id,$additionalTag_sel))
                                                    selected="true"
                                                    @endif
                                                    @endif
                                            >{{ $additionalTag->name}}</option>
                                        @endforeach
                                    </optgroup>
                                @endforeach
                            </select>
                            {!! $errors->first('additional_tags', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="text-center">
                    <div class="btn-group">
                        <button type="button" class="btn btn-md btn-warning" onclick="cancel()">Cancel</button>
                        <button type="button" class="btn btn-md btn-info" onclick="prevStep()">Prev</button>
                        <button type="button" onclick="step4Next()" class="btn btn-md btn-info">Next</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row setup-content" id="step-5">
        <div class="col-xs-12">
            <div class="col-md-12 well">
                <div class="container col-xs-12">
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : ''}}">
                        {!! Form::label('status', 'Campaign Status: ', ['class' => 'col-md-4 control-label']) !!}
                        <div class="col-md-6">
                            {{ Form::radio('status', '1', null,['id' => 'status']) }}
                            {{ Form::label('status', 'Publish') }}<br>
                            {{ Form::radio('status', '0', null,['id' => 'status']) }}
                            {{ Form::label('status', 'Draft/Unpublish') }}
                        </div>
                    </div>
                    <div class="form-group text-center">
                        <i>Note: Agents will be notified via email when campaign is published.</i>
                    </div>
                </div>
                <div class="text-center">
                    <div class="btn-group">
                        <button type="button" class="btn btn-md btn-warning" onclick="cancel()">Cancel</button>
                        <button type="button" class="btn btn-md btn-info" onclick="prevStep()">Prev</button>
                        <button type="button" class="btn btn-md btn-success" onclick="submitCampaign()">
                            {{ isset($submitButtonText) ? $submitButtonText : 'Create' }}
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="box-footer">
</div>
@push('scripts')
    <script src="{{ url('libs/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script>
        var currentStep = 1;
        $(function () {
            $('.li-nav').click(function () {

                var $targetStep = $($(this).attr('step'));
                currentStep = parseInt($(this).attr('id').substr(7));

                if (!$(this).hasClass('disabled')) {
                    $('.li-nav.active').removeClass('active');
                    $(this).addClass('active');
                    $('.setup-content').hide();
                    $targetStep.show();
                }
            });

            $('#navStep1').click();
            $('.textarea').wysihtml5();
            $('#type').select2({
                width: '100%',
                placeholder: 'Select a Type',
            });
            $('#destinations').select2({
                width: '100%',
                placeholder: 'Select an Option',
            });
            $('#tag_type_id').select2({
                width: '100%',
                placeholder: 'Select an Option',
            });
            $('#tag_obj_id').select2({
                width: '100%',
                placeholder: 'Select an Option',
            });
            $('#tag_channel_id').select2({
                width: '100%',
                placeholder: 'Select an Option',
            });
            $('#additional_tags').select2({
                maximumSelectionLength: 2,
                width: '100%',
                placeholder: 'Select an Option',
            });
            $('#territories').select2({
                width: '100%',
                placeholder: 'Select an Option',
            });
            $('#sale_segments').select2({
                width: '100%',
                placeholder: 'Select an Option',
            });
            $('#system_segments').select2({
                width: '100%',
                placeholder: 'Select an Option',
            });
            $('#training_segments').select2({
                width: '100%',
                placeholder: 'Select an Option'
            });
            $('#reward_id').select2({
                width: '100%',
                placeholder: 'Select an Option',
            });

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#showimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image").change(function () {
                readURL(this);
            });
        });
        function submitCampaign() {
            $('#campaignForm').submit();
        }
        function cancel() {
            document.location.href = '{{ route('campaigns.index') }}';
        }
        function step1Next() {
            if (true) {
                currentStep += 1;
                $('#navStep' + currentStep).removeClass('disabled');
                $('#navStep' + currentStep).click();
            }
        }

        function prevStep() {
            currentStep -= 1;
            $('#navStep' + currentStep).click();
        }

        function step2Next() {
            if (true) {
                $('#navStep3').removeClass('disabled');
                $('#navStep3').click();
            }
        }

        function step3Next() {
            if (true) {
                $('#navStep4').removeClass('disabled');
                $('#navStep4').click();
            }
        }

        function step4Next() {
            if (true) {
                $('#navStep5').removeClass('disabled');
                $('#navStep5').click();
            }
        }
    </script>
@endpush
