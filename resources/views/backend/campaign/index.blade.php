@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                All Campaign
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">All Campaign</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>

                            <div class="box-tools">
                                @can('manage_campaign')
                                    <a href="{{ url('backend/campaigns/create') }}" class="btn btn-success btn-sm">Add
                                        New Campaign</a>
                                @endcan
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive padding">
                            <table class="table table-hover" id="campaign-table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Type</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#campaign-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: false,
                defaultContent: '',
                ajax: '{!! route('campaigns-data') !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'name'},
                    {data: 'type', defaultContent: ''},
                    {data: 'startDate', defaultContent: ''},
                    {data: 'endDate', defaultContent: ''},
                    {data: 'status', name: 'status', searchable: false}
                ],
                search: {
                    "regex": true
                }
            });
            $('#campaign-table tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                document.location.href = '{{ url('backend/campaigns') }}/' + data.id;
            });
        });
    </script>
@endpush