@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Campaign
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/campaigns')}}">Campaign</a></li>
                <li class="active">View Campaign</li>
            </ol>
        </section>

    @include ('backend.layouts.message')

    <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>

                            <div class="box-tools">
                                @can('manage_campaign')
                                    {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/backend/campaigns', $campaign->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                    <button type="submit" class="btn btn-default"><i class="fa fa-trash"></i></button>
                                    {!! Form::close() !!}
                                @endcan
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Name: </label>
                                    <div class="col-md-6">
                                        <p>{{ $campaign->name }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Type: </label>
                                    <div class="col-md-6">
                                        <p>{{ is_null($campaign->type) ? 'No Type Available' : $campaign->type}}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Message: </label>
                                    <div class="col-md-6">
                                        <p>{!! empty($campaign->message) ? 'No Message Available' : $campaign->message !!}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Campaign
                                        Duration: </label>
                                    <div class="col-md-6">
                                        <p>{{ is_null($campaign->startDate) ? 'No Campaign Duration Available' : \Carbon\Carbon::parse($campaign->startDate)->formatLocalized('%d %B %Y')}}
                                            - {{ is_null($campaign->endDate) ? '' : \Carbon\Carbon::parse($campaign->endDate)->formatLocalized('%d %B %Y') }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Sales Target: </label>
                                    <div class="col-md-6">
                                        <p>{{ is_null($campaign->sales) ? 'No Sale Available' : $campaign->sales}}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="agents" class="col-md-4 control-label text-right">Destinations: </label>
                                    <div class="col-md-6">
                                        @if(count($campaign->destinations) > 0)
                                            @foreach ($campaign->destinations as $destination)
                                                <p>{{ $destination->name }}</p>
                                            @endforeach
                                        @else
                                            <p>No destinations selected for campaign</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Territory: </label>
                                    <div class="col-md-6">

                                        @if(count($campaign->territories) > 0)
                                            @foreach ($campaign->territories as $territory)
                                                <p>{{ $territory->name }}</p>
                                            @endforeach
                                        @else
                                            <p>No Territory Available</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Sales Performance Segment: </label>
                                    <div class="col-md-6">
                                        @if(count($campaign->saleSegments) > 0)
                                            @foreach ($campaign->saleSegments as $segment)
                                                <p>{{ $segment->name }}</p>
                                            @endforeach
                                        @else
                                            <p>No sales performance segment selected for campaign</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">System Performance Segment: </label>
                                    <div class="col-md-6">
                                        @if(count($campaign->systemSegments) > 0)
                                            @foreach ($campaign->systemSegments as $segment)
                                                <p>{{ $segment->name }}</p>
                                            @endforeach
                                        @else
                                            <p>No system performance segment selected for campaign</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Training Performance Segment: </label>
                                    <div class="col-md-6">
                                        @if(count($campaign->trainingSegments) > 0)
                                            @foreach ($campaign->trainingSegments as $segment)
                                                <p>{{ $segment->name }}</p>
                                            @endforeach
                                        @else
                                            <p>No training performance segment selected for campaign</p>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Tags Type: </label>
                                    <div class="col-md-6">
                                        <p>{!! empty($campaign->tagType->name) ? 'No Message Available' : $campaign->tagType->name !!}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Objective Tags: </label>
                                    <div class="col-md-6">
                                        <p>{!! empty($campaign->tagObjective->name) ? 'No Message Available' : $campaign->tagObjective->name !!}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Category Channel Tags: </label>
                                    <div class="col-md-6">
                                        <p>{!! empty($campaign->tagChannel->name) ? 'No Message Available' : $campaign->tagChannel->name !!}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Tags: </label>
                                    <div class="col-md-6">
                                        @foreach ($campaign->tags as $tag)
                                            <p>{{ $tag->name }}</p>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="box-footer col-md-offset-4 col-md-4">
                                    <a class="btn btn-info"
                                       href="{{ url('/backend/campaigns/' . $campaign->id . '/edit') }}">Edit</a>
                                    <a href="{{ route('campaigns.index') }}" class="btn btn-warning">Cancel</a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group{{ $errors->has('upload_photo') ? ' has-error' : ''}}">
                                    <div class="col-md-6">
                                        <img src="{{ isset($campaign->logo) ? url('assets/img/'.$campaign->image) : url('https://dummyimage.com/200x200/00bef3/ffffff')}}" id="showimages"
                                             style="max-width: 200px; max-height: 200px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>

            @if(count($campaignUsers) > 0)
                <div class="row">
                    <div class="col-xs-8">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Agents in Campaign</h3>
                                <div class="box-tools">
                                    {{--<div class="input-group input-group-sm" style="width: 150px;">--}}
                                    {{--<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">--}}

                                    {{--<div class="input-group-btn">--}}
                                    {{--<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                            <div class="box-body table-responsive padding">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Agent Name</th>
                                        <th>Campaign Status</th>
                                        <th>Started On</th>
                                        <th>Updated At</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($campaignUsers as $campaignUser)
                                        <tr>
                                            <td>{{ $campaignUser->agents->name }}</td>
                                            <td>
                                                @if($campaignUser->inprogress == 1)
                                                    <span class="label label-info">In Progress</span>
                                                @elseif($campaignUser->completed == 1)
                                                    <span class="label label-success">Completed</span>
                                                @endif
                                            </td>
                                            <td>{{ \Carbon\Carbon::parse($campaignUser->created_at)->formatLocalized('%d %B %Y') }}</td>
                                            <td>{{ \Carbon\Carbon::parse($campaignUser->updated_at)->formatLocalized('%d %B %Y')}}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $campaignUsers->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
