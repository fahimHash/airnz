@extends('backend.layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Sales
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('backend/sales')}}">Sales</a></li>
          <li class="active">View Sale</li>
        </ol>
      </section>

      @include ('backend.layouts.message')

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">List</h3>

                <div class="box-tools">
                  {!! Form::open([
                      'method'=>'DELETE',
                      'url' => ['/backend/sales', $sale->id],
                      'style' => 'display:inline'
                  ]) !!}
                  <button type="submit" class="btn btn-default"><i class="fa fa-trash"></i></button>
                  {!! Form::close() !!}
                </div>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Agent: </label>
                      <div class="col-md-6">
                        <p>{{ $sale->user->name }}</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Sale Status: </label>
                      <div class="col-md-6">
                        <p>{{ $sale->status }}</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Issued Date: </label>
                      <div class="col-md-6">
                        <p>{{ empty($sale->issued_date) ? 'No Issue Date available' : $sale->issued_date}}</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">PNR: </label>
                      <div class="col-md-6">
                        <p>{{ empty($sale->pnr) ? 'No PNR' : $sale->pnr}}</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">CTY: </label>
                      <div class="col-md-6">
                        <p>{{ empty($sale->cty) ? 'No CTY available' : $sale->cty}}</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Stock: </label>
                      <div class="col-md-6">
                        <p>{{ empty($sale->stock) ? 'No Stock available' : $sale->stock}}</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Ticket Number: </label>
                      <div class="col-md-6">
                        <p>{{ empty($sale->ticket_number) ? 'No Ticket Number available' : $sale->ticket_number}}</p>
                      </div>
                    </div>
                    {{-- <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Campaign: </label>
                      <div class="col-md-6">
                        <p>{{ is_null($sale->campaign) ? 'No Campaign available' : $sale->campaign}}</p>
                      </div>
                    </div> --}}
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Departure Date: </label>
                      <div class="col-md-6">
                        <p>{{ empty($sale->departure_date) ? 'No Departure Date available' : $sale->departure_date}}</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Class: </label>
                      <div class="col-md-6">
                        <p>{{ empty($sale->class) ? 'No Class available' : $sale->class}}</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Notes: </label>
                      <div class="col-md-6">
                        <p>{{ empty($sale->notes) ? 'No Notes available' : $sale->notes}}</p>
                      </div>
                    </div>
                    <div class="box-footer col-md-offset-4 col-md-4">

                        <a class="btn btn-info" href="{{ url('/backend/sales/' . $sale->id . '/edit') }}">Edit</a>

                      <a href="{{ route('sales.index') }}" class="btn btn-warning">Cancel</a>
                    </div>
                  </div>
                </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
  @endsection
