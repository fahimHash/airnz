@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Sales
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Sales</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>

                            <div class="box-tools">
                                <a href="{{ url('backend/sales/create') }}" class="btn btn-success btn-sm">Add New
                                    Sale</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive padding">
                            <table class="table table-hover" id="sales-table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Agent</th>
                                    <th>Entry Date</th>
                                    <th>Sale Status</th>
                                    <th>Issued Date</th>
                                    <th>PNR</th>
                                    <th>Destination</th>
                                    <th>Stock</th>
                                    <th>Ticket Number</th>
                                    <th>Departure Date</th>
                                    <th>Booking Class</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#sales-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: false,
                defaultContent: '',
                ajax: '{!! route('sales-data') !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'user.name'},
                    {data: 'created_at', defaultContent: ''},
                    {data: 'status', defaultContent: ''},
                    {data: 'issued_date', defaultContent: ''},
                    {data: 'pnr', defaultContent: ''},
                    {data: 'cty', defaultContent: ''},
                    {data: 'stock', defaultContent: ''},
                    {data: 'ticket_number', defaultContent: ''},
                    {data: 'departure_date', defaultContent: ''},
                    {data: 'class', defaultContent: ''}
                ],
                search: {
                    "regex": true
                }
            });
            $('#sales-table tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                document.location.href = '{{ url('backend/sales') }}/' + data.id;
            });
        });
    </script>
@endpush
