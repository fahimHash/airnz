<div class="col-md-8">
    @if(isset($submitButtonText))
        <div class="form-group">
            <label for="title" class="col-md-4 control-label">Agent: </label>
            <div class="col-md-6 control-label-agent">
                <p>{{ $sale->user->name }}</p>
            </div>
        </div>
    @else
        <div class="form-group">
            <label class="col-md-4 control-label" for="users">Agent:</label>
            <div class="col-md-6">
                <select required class="col-md-12" name="user_id" id="users">
                    <option value="">Select an option</option>
                    @foreach ($users as $user)
                        <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    @endif
    @if(isset($submitButtonText))
        <div class="form-group">
            <label for="status" class="col-md-4 control-label">Sales Status: </label>
            <div class="col-md-6">
                <select required name="status" id="status" class="col-md-12">
                    <option value="">Select an option</option>
                    <option value="Valid" @if($sale->status == 'Valid') selected="" @endif >Valid</option>
                    <option value="Pending" @if($sale->status == 'Pending') selected="" @endif >Pending</option>
                    <option value="Cancelled" @if($sale->status == 'Cancelled') selected="" @endif >Cancelled</option>
                    <option value="Problem" @if($sale->status == 'Problem') selected="" @endif >Problem</option>
                </select>
                <p>
            </div>
        </div>
    @else
        <div class="form-group">
            <label class="col-md-4 control-label" for="status">Sales Status:</label>
            <div class="col-md-6">
                <select required name="status" id="status" class="col-md-12">
                    <option value="">Select an option</option>
                    <option value="Valid">Valid</option>
                    <option value="Pending">Pending</option>
                    <option value="Cancelled">Cancelled</option>
                    <option value="Problem">Problem</option>
                </select>
            </div>
        </div>
    @endif
    <div class="form-group{{ $errors->has('issued_date') ? ' has-error' : ''}}">
        {!! Form::label('issued_date', 'Issued Date: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::date('issued_date', null, ['class' => 'form-control']) !!}
            {!! $errors->first('issued_date', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('pnr') ? ' has-error' : ''}}">
        {!! Form::label('pnr', 'PNR: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('pnr', null, ['class' => 'form-control']) !!}
            {!! $errors->first('pnr', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    @if(isset($submitButtonText))
        <div class="form-group{{ $errors->has('cty') ? ' has-error' : ''}}">
            {!! Form::label('cty', 'CTY: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('cty', null, ['class' => 'form-control']) !!}
                {!! $errors->first('cty', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @else
        <div class="form-group">
            <label class="col-md-4 control-label" for="city1">Departure City:</label>
            <div class="col-md-6">
                <select name="cty1" id="city1" class="col-md-12">
                    <option value="">Select an option</option>
                    @foreach ($destinations as $destination)
                        <option value="{{$destination->code}}">{{$destination->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-4 control-label" for="city2">Arrival City:</label>
            <div class="col-md-6">
                <select name="cty2" id="city2" class="col-md-12">
                    <option value="">Select an option</option>
                    @foreach ($destinations as $destination)
                        <option value="{{$destination->code}}">{{$destination->name}}</option>
                    @endforeach
                </select>
            </div>
        </div>
    @endif
    <div class="form-group{{ $errors->has('stock') ? ' has-error' : ''}}">
        {!! Form::label('stock', 'Stock: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('stock', null, ['class' => 'form-control']) !!}
            {!! $errors->first('stock', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('ticket_number') ? ' has-error' : ''}}">
        {!! Form::label('ticket_number', 'Ticket Number: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('ticket_number', null, ['class' => 'form-control']) !!}
            {!! $errors->first('ticket_number', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    {{-- <div class="form-group{{ $errors->has('campaign') ? ' has-error' : ''}}">
        {!! Form::label('campaign', 'Campaign: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('campaign', null, ['class' => 'form-control']) !!}
            {!! $errors->first('campaign', '<p class="help-block">:message</p>') !!}
        </div>
    </div> --}}
    <div class="form-group{{ $errors->has('departure_date') ? ' has-error' : ''}}">
        {!! Form::label('departure_date', 'Departure Date: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::date('departure_date', null, ['class' => 'form-control']) !!}
            {!! $errors->first('departure_date', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('travelclass') ? ' has-error' : ''}}">
        {!! Form::label('travelclass', 'Booking Class: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('class', null, ['class' => 'form-control']) !!}
            {!! $errors->first('travelclass', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('note') ? ' has-error' : ''}}">
        {!! Form::label('note', 'Notes: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('notes', null, ['class' => 'form-control']) !!}
            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

</div>
<div class="box-footer">
    <input type="hidden" name="active" value="1">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info']) !!}
        <a href="{{ route('sales.index') }}" class="btn btn-warning">Cancel</a>
    </div>
</div>

@push('scripts')
    <script>
        $(function () {
            $('#users').select2();
            $('#status').select2();
            $('#city1').select2();
            $('#city2').select2();
        });
    </script>
@endpush
