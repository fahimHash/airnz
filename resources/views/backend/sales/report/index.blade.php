@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Sales Report
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/sales')}}">Sales</a></li>
                <li class="active">Sales Report</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"></h3>

                            <div class="box-tools">

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <!-- Date -->
                            {!! Form::open(['url' => '/backend/sales-report', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}
                                <div class="form-group">
                                    <label class="col-md-3 control-label">Download Sales Report for Date:</label>
                                    <div class="col-md-6">
                                        <div class="input-group date">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::date('reportDate', \Carbon\Carbon::today()->toDateString(), ['class' => 'form-control', 'id'=>'reportDate']) !!}
                                        </div>
                                    </div>
                                    <div class="col-md-3 pull-right">
                                        {!! Form::submit('Download', ['class' => 'btn btn-success']) !!}
                                    </div>
                                    <!-- /.input group -->
                                </div>
                            {!! Form::close() !!}
                            <!-- /.form group -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
    <script src="{{ url('libs/modernizr-custom.js') }}"></script>
    <script>
        if (!Modernizr.inputtypes.date) {
            $("#reportDate").datepicker({ dateFormat: 'yy-mm-dd' }).val();
        }
    </script>
@endpush
