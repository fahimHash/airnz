@extends('backend.layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Validate Sales
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url('backend/sales')}}">Sales</a></li>
        <li class="active">Validate Sales</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @include ('backend.layouts.message')
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title"></h3>

              <div class="box-tools">

              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form role="form" name="uploadForm" method="post" action="{{ url('backend/validate-sales-upload')}}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="box-body">
                  <div class="form-group">
                    <label for="salesFile">File Upload</label>
                    <input type="file" name="file" accept=".csv,.xlsx">

                    <p class="help-block">.csv, .xlsx extension file are only accepted.</p>
                  </div>
                </div>
                <!-- /.box-body -->

                <div class="box-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
      @if ($progress)
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Status</h3>

              <div class="box-tools">
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="box-body">
                <p>
                  Progress
                </p>
                <div class="progress" style="display:none">
                  <div class="progress-bar progress-bar-striped active" role="progressbar"
                    aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:0%">
                    0%
                  </div>
                </div>
                <p id="message"></p>
            </div>
        </div>
        <!-- /.box-body -->
      </div>
      <!-- /.box -->
    </div>
  </div>
  @endif
</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@push('scripts')
  <script type="text/javascript">
  @if ($progress)
  (function($){
    'use strict'
    function statusUpdater() {
      $.ajax({
        'url': "{!! url('backend/sales-upload-status') !!}",
      }).done(function(r) {
        if(r.msg==='no-task'){
          $('#message').text( "No validation job in queue." );
          $('.progress').hide();
        }
        else if(r.msg==='done') {
          $('#message').text( "The sales validation is completed." );
          $('.progress').hide();
        } else {
          //get the total number of imported rows
          $('.progress-bar').css('width', r.percentage+'%')
          $('.progress-bar').text(r.percentage+'%');
          $('.progress').show();

          $('#message').text("Status is: " + r.msg);
        }
      })
      .fail(function() {
        $('#message').text( "An error has occurred..." );
      });
    }
    statusUpdater();
    setInterval(function(){ statusUpdater(); }, 5000);
  })(jQuery)
  @endif
  </script>
@endpush
