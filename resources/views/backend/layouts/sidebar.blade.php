<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ url('assets/img/'.Auth::user()->image) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{Auth::user()->name}}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
                <a href="{{ url('backend') }}">
                    <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i>
                    <span>My Agents</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/agents') }}"><i class="fa fa-circle-o"></i> List Agents</a></li>
                    <li><a href="{{ url('backend/agents/create') }}"><i class="fa fa-plus-circle"></i> Add Agents</a></li>
                    <li><a href="{{ url('backend/agents/archive') }}"><i class="fa fa-archive"></i> Archived Agents</a></li>
                    <li><a href="{{ route('blacklists.index') }}"><i class="fa fa-archive"></i> Blacklist Email Domain</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-dollar"></i>
                    <span>Sales</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/sales') }}"><i class="fa fa-circle-o"></i> List Sales</a></li>
                    <li><a href="{{ url('backend/sales/create') }}"><i class="fa fa-plus-circle"></i> Add Sales</a></li>
                    <li><a href="{{ url('backend/validate-sales') }}"><i class="fa fa-circle-o"></i> Validate Sales</a>
                    </li>
                    <li><a href="{{ url('backend/sales-report') }}"><i class="fa fa-flag"></i> Sales Report</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-clock-o"></i>
                    <span>Campaign</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/campaigns') }}"><i class="fa fa-clock-o"></i> List Campaigns</a></li>
                    <li><a href="{{ url('backend/campaigns/create') }}"><i class="fa fa-plus-circle"></i> Add Campaign</a></li>
                    <li><a href="{{ url('backend/segments') }}"><i class="fa fa-pie-chart"></i> Segments</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span>Reward</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/rewards') }}"><i class="fa fa-circle-o"></i> List Rewards</a></li>
                    <li><a href="{{ url('backend/rewards/create') }}"><i class="fa fa-plus-circle"></i> Add Reward</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-list"></i>
                    <span><b>DUO</b> SURVEYS</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/surveys') }}"><i class="fa fa-circle-o"></i> List Surveys</a></li>
                    <li><a href="{{ url('backend/surveys/create') }}"><i class="fa fa-plus-circle"></i> New Survey</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-industry"></i>
                    <span>Consortiums</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/consortiums') }}"><i class="fa fa-circle-o"></i> List Consortiums</a></li>
                    <li><a href="{{ url('backend/consortiums/create') }}"><i class="fa fa-plus-circle"></i> Add Consortium</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-edit"></i>
                    <span>Agency</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/agency') }}"><i class="fa fa-circle-o"></i> List Agencies</a></li>
                    <li><a href="{{ url('backend/agency/create') }}"><i class="fa fa-plus-circle"></i> Add Agency</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-edit"></i>
                    <span>Store</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/store') }}"><i class="fa fa-circle-o"></i> List Stores</a></li>
                    <li><a href="{{ url('backend/store/create') }}"><i class="fa fa-plus-circle"></i> Add Store</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-wrench"></i>
                    <span>Territory</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/territory') }}"><i class="fa fa-circle-o"></i> List Territories with BDMs</a></li>
                    {{--<li><a href="{{ url('backend/territory/create') }}"><i class="fa fa-plus-circle"></i> Add Territory</a></li>--}}
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-newspaper-o"></i>
                    <span>Pages</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/pages') }}"><i class="fa fa-circle-o"></i> List Pages</a></li>
                    <li><a href="{{ url('backend/pages/create') }}"><i class="fa fa-plus-circle"></i> Add Page</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-newspaper-o"></i>
                    <span>News Feed</span>
                    <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="{{ url('backend/news') }}"><i class="fa fa-circle-o"></i> List News Feeds</a></li>
                    <li><a href="{{ url('backend/news/create') }}"><i class="fa fa-plus-circle"></i> Add News Feed</a></li>
                </ul>
            </li>
            @can('manage_user')
                <li class="treeview">
                    <a href="#">
                        <i class="fa fa-gears"></i>
                        <span>User Control</span>
                        <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
                    </a>
                    <ul class="treeview-menu">
                        <li><a href="{{ url('backend/users') }}"><i class="fa fa-circle-o"></i> Users</a></li>
                        @role('admin')
                        <li><a href="{{ url('backend/roles') }}"><i class="fa fa-circle-o"></i> Roles</a></li>
                        <li><a href="{{ url('backend/permissions') }}"><i class="fa fa-circle-o"></i> Permissions</a>
                        </li>
                        <li><a href="{{ url('backend/give-role-permissions') }}"><i class="fa fa-circle-o"></i> Give
                                Role Permission</a></li>
                        @endauth()
                    </ul>
                </li>
            @endcan
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
