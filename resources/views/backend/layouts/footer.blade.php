<script src="{{ url('js/bootstrap.js') }}"></script>
<script src="{{ url('libs/jquery-2.2.3.min.js') }}"></script>
<script src="{{ url('libs/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ url('libs/spin.min.js') }}"></script>
<script src="{{ url('libs/jquery.spin.js') }}"></script>
<script src="{{ url('libs/multiple-select.js') }}"></script>
<script src="{{ url('libs/datatables.min.js') }}"></script>
<script src="{{ url('libs/pace.min.js') }}"></script>
<script src="{{ url('libs/select2.min.js') }}"></script>
<script src="{{ url('libs/app.min.js') }}"></script>
<script type="text/javascript">
    axios.interceptors.request.use( (request) => {
        Pace.restart();
        return request;
    }, function (error) {
        return Promise.reject(error);
    });
    // Add a response interceptor
    axios.interceptors.response.use( (response) => {
        Pace.restart();
        return response;
    }, function (error) {
        return Promise.reject(error);
    });
    $(document).ready(function () {
        $('.main-sidebar').height($('#wrapper').height() - 50);
    });
</script>
@stack('scripts')
</body>
</html>
