<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Dashboard - {{ config('app.name', 'Laravel') }}</title>
    <!-- Bootstrap 3.3.6 -->
    <link href="{{ url('libs/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ url('assets/duo.admin.css') }}">
    <!-- Theme skin blue -->
    <link rel="stylesheet" href="{{ url('assets/skin-blue.css') }}">
    <link rel="stylesheet" href="{{ url('assets/multiple-select.css') }}">
    <link rel="stylesheet" href="{{ url('libs/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ url('libs/pace.min.css') }}">
    <link rel="stylesheet" href="{{ url('libs/select2.min.css') }}"/>
    @stack('styles')
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ mix('/css/backend.css') }}">
    <!-- Font Awesome -->
    <link href="{{ url('libs/font-awesome-4.7.0/css/font-awesome.min.css') }}" rel="stylesheet">
    <!-- Ionicons -->
    <link href="{{ url('libs/ionicons-2.0.1/css/ionicons.min.css') }}" rel="stylesheet">
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
