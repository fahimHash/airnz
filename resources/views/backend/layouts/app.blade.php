@include('backend.layouts.head')
<body class="hold-transition skin-blue sidebar-mini">

<div class="wrapper">

    @include('backend.layouts.header')

    @include('backend.layouts.sidebar')

    @yield('content')

    <footer class="main-footer clearfix">
        <div class="pull-left">
            <img class="star-logo" src="{{ url('assets/img/Star-Alliance-Full-reverse.png') }}">
        </div>
        <div class="pull-right">
            <a href="">About <strong>duo</strong></a> <span>|</span> <a href="">Contact Us <span>|</span></a> <span
                    class="copyright">Copyright Air New Zealand 2016</span>
        </div>
    </footer>

    {{-- @include('backend.layouts.control-sidebar') --}}

</div>
<!-- /#wrapper -->
@include('backend.layouts.footer')
