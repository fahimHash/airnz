@extends('backend.layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Admin's & BDM's
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('backend/users')}}">Admin / BDM</a></li>
          <li class="active">View Admin / BDM</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">List</h3>

                <div class="box-tools">
                  @role('admin')
                  {!! Form::open([
                      'method'=>'DELETE',
                      'url' => ['/backend/users', $user->id],
                      'style' => 'display:inline'
                  ]) !!}
                  <button type="submit" class="btn btn-default"><i class="fa fa-trash"></i></button>
                  {!! Form::close() !!}
                  @endauth
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="col-md-9">
                  <div class="form-group">
                    <label for="title" class="col-md-4 control-label text-right">Title: </label>
                    <div class="col-md-6">
                      <p>{{ isset($user->title) ? $user->title : 'No Title' }}</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="title" class="col-md-4 control-label text-right">Name: </label>
                    <div class="col-md-6">
                      <p>{{ $user->name }}</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="title" class="col-md-4 control-label text-right">Gender: </label>
                    <div class="col-md-6">
                      <p>{{ isset($user->gender) ? $user->gender : 'Unspecified' }}</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="title" class="col-md-4 control-label text-right">Email: </label>
                    <div class="col-md-6">
                      <p>{{ $user->email }}</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="title" class="col-md-4 control-label text-right">Phone: </label>
                    <div class="col-md-6">
                      <p>{{ isset($user->phoneNumber) ? $user->phoneNumber : 'No Phone Number' }}</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="title" class="col-md-4 control-label text-right">State: </label>
                    <div class="col-md-6">
                      <p>{{ isset($user->state) ? $user->state : 'No State Information' }}</p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="title" class="col-md-4 control-label text-right">Role: </label>
                    <div class="col-md-6">
                      <p>
                        @foreach ($roles as $role)
                          {{$role->label}}
                        @endforeach
                    </p>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="title" class="col-md-4 control-label text-right">Status: </label>
                    <div class="col-md-6">
                      <p>
                        @if($user->active  == 1)
                          <span class="label label-success">Active</span>
                        @else
                          <span class="label label-danger">Inactive</span>
                        @endif</p>
                    </div>
                  </div>
                  <div class="box-footer col-md-offset-4 col-md-4">
                    @can('manage_user')
                      <a class="btn btn-info" href="{{ url('/backend/users/' . $user->id . '/edit') }}">Edit</a>
                    @endcan
                      <a href="{{ route('users.index') }}" class="btn btn-warning">Cancel</a>
                  </div>
                </div>
                <div class="col-md-3">
                  <div class="form-group{{ $errors->has('upload_photo') ? ' has-error' : ''}}">
                    <div class="col-md-6">
                      <img src="{{ url('assets/img/'.$user->image) }}" id="showimages" class="img-circle" style="max-width: 200px; max-height: 200px;">
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
  @endsection
