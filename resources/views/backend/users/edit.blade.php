@extends('backend.layouts.app')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Edit User
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url('backend/users')}}">Admin / BDM</a></li>
        <li class="active">Edit Admin / BDM</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <div class='box-body'>
              @include('backend.layouts.message')

              {!! Form::model($user, [
                  'method' => 'PATCH',
                  'url' => ['/backend/users', $user->id],
                  'class' => 'form-horizontal',
                  'enctype'=>'multipart/form-data'
              ]) !!}

              @include ('backend.users.form', ['submitButtonText' => 'Update'])

              {!! Form::close() !!}
            </div>
            </div>
            <!-- /.box -->

          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
  @endsection
