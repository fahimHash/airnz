<div class="col-md-9">
<div class="form-group {{ $errors->has('title') ? ' has-error' : ''}}">
  <label class="col-md-4 control-label" for="title">Title</label>
  <div class="col-md-6">
    <select name="title" id="title" class="col-md-12">
        <option value="">Select an option</option>
        <option value="Mr." @if(isset($submitButtonText) && $user->title == 'Mr') selected="selected" @endif>Mr</option>
        <option value="Mrs." @if(isset($submitButtonText) && $user->title == 'Mrs') selected="selected" @endif>Mrs</option>
        <option value="Ms" @if(isset($submitButtonText) && $user->title == 'Ms') selected="selected" @endif>Ms</option>
        <option value="Miss" @if(isset($submitButtonText) && $user->title == 'Miss') selected="selected" @endif>Miss</option>
    </select>
  </div>
</div>
<div class="form-group{{ $errors->has('first_name') ? ' has-error' : ''}}">
    {!! Form::label('first_name', 'First Name: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('first_name', isset($first_name) ? $first_name : null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('last_name') ? ' has-error' : ''}}">
    {!! Form::label('last_name', 'Last Name: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('last_name', isset($last_name) ? $last_name : null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
    {!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('phoneNumber') ? ' has-error' : ''}}">
    {!! Form::label('phoneNumber', 'Phone: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('phoneNumber', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('phoneNumber', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group{{ $errors->has('gender') ? ' has-error' : ''}}">
  {!! Form::label('gender', 'Gender: ', ['class' => 'col-md-4 control-label']) !!}
  <div class="col-md-6">
    {{ Form::radio('gender', 'Male', null,['id' => 'gender']) }}
    {{ Form::label('gender', 'Male') }}<br>
    {{ Form::radio('gender', 'Female', null,['id' => 'gender']) }}
    {{ Form::label('gender', 'Female') }}
  </div>
</div>
@if (!isset($submitButtonText) || auth()->id() == $user->id)
  <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
      {!! Form::label('password', 'Password: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::password('password', ['class' => 'form-control']) !!}
          @if ($errors->has('password'))
            <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
          @endif
      </div>
  </div>
  <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : ''}}">
      {!! Form::label('password-confirm', 'Confirm Password: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::password('password-confirm', ['class' => 'form-control', 'name' => 'password_confirmation']) !!}
          {!! $errors->first('password-confirm', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
@endif

@role('admin')
<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
    {!! Form::label('role', 'Role: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if (isset($submitButtonText))
          @foreach($roles as $role)
            @if(is_array($user_roles) == in_array($role->name, $user_roles))
              <input id="roles" name="roles" type="radio" value="{{$role->name}}" checked>
            @else
              <input id="roles" name="roles" type="radio" value="{{$role->name}}">
            @endif
            {{ Form::label('roles', $role->label) }}<br>
          @endforeach
        @else
          @foreach($roles as $role)
            {{ Form::radio('roles', $role->name, null,['id' => 'roles']) }}
            {{ Form::label('roles', $role->label) }}<br>
          @endforeach
        @endif
    </div>
</div>
@endauth
@role('airnz-super-user')
<div class="form-group{{ $errors->has('roles') ? ' has-error' : ''}}">
    {!! Form::label('role', 'Role: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        @if (isset($submitButtonText))
          @foreach($roles as $role)
            @if(is_array($user_roles) == in_array($role->name, $user_roles))
              <input id="roles" name="roles" type="radio" value="{{$role->name}}" checked>
            @else
              <input id="roles" name="roles" type="radio" value="{{$role->name}}">
            @endif
            {{ Form::label('roles', $role->label) }}<br>
          @endforeach
        @else
          @foreach($roles as $role)
            {{ Form::radio('roles', $role->name, null,['id' => 'roles']) }}
            {{ Form::label('roles', $role->label) }}<br>
          @endforeach
        @endif
    </div>
</div>
@endauth

@if (isset($submitButtonText))
@role('admin')
@if(Auth::user()->id != $user->id)
<div class="form-group{{ $errors->has('active') ? ' has-error' : ''}}">
    {!! Form::label('active', 'Status: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      {{ Form::radio('active', '1', null,['id' => 'active']) }}
      {{ Form::label('active', 'Active') }}<br>
      {{ Form::radio('active', '0', null,['id' => 'active']) }}
      {{ Form::label('active', 'Inactive') }}
    </div>
</div>
@endif
@endauth
@role('airnz-super-user')
@if(Auth::user()->id != $user->id)
<div class="form-group{{ $errors->has('active') ? ' has-error' : ''}}">
    {!! Form::label('active', 'Status: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
      {{ Form::radio('active', '1', null,['id' => 'active']) }}
      {{ Form::label('active', 'Active') }}<br>
      {{ Form::radio('active', '0', null,['id' => 'active']) }}
      {{ Form::label('active', 'Inactive') }}
    </div>
</div>
@endif
@endauth
@else
  <div class="form-group{{ $errors->has('active') ? ' has-error' : ''}}">
      {!! Form::label('active', 'Status: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
        {{ Form::radio('active', '1', null,['id' => 'active']) }}
        {{ Form::label('active', 'Active') }}<br>
        {{ Form::radio('active', '0', null,['id' => 'active']) }}
        {{ Form::label('active', 'Inactive') }}
      </div>
  </div>
@endif

</div>
<div class="col-md-3">
<div class="form-group{{ $errors->has('upload_photo') ? ' has-error' : ''}}">
  <div class="col-md-6">
      <img src="{{ isset($user->image) ? url('assets/img/'.$user->image) : url('assets/img/default-image.png')}}" id="showimage" class="img-circle" style="max-width: 200px; max-height: 200px;">
        <input type="file" id="upload_photo" name="upload_photo" class="btn btn-info" style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;">
        <br>
        <label for="upload_photo" class="btn btn-info" style="margin-top:10px;">Choose File</label>
  </div>
</div>
</div>
<div class="box-footer">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info']) !!}
        <a href="{{ route('users.index') }}" class="btn btn-warning">Cancel</a>
    </div>
</div>
@push('scripts')
    <script>
        $(function () {
            $('#title').select2();

            $('#state').select2();
        });
    </script>
@endpush