@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Admin's & BDM's
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Admin Users</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>

                            <div class="box-tools">
                                @role('admin')
                                <a href="{{ url('backend/users/create') }}" class="btn btn-success btn-sm">Add New Admin
                                    or BDM</a>
                                @endauth
                                @role('airnz-super-user')
                                <a href="{{ url('backend/users/create') }}" class="btn btn-success btn-sm">Add New Admin
                                    or BDM</a>
                                @endauth
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive padding">
                            <table class="table table-hover" id="user-table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Role</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#user-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: true,
                defaultContent: '',
                ajax: '{!! route('users-data') !!}',
                columns: [
                    {data: 'id', name:'users.id', visible: false},
                    {data: 'name', name:'users.name'},
                    {data: 'email', name:'users.email', defaultContent : ''},
                    {data: 'role', name:'roles.label', defaultContent : '' },
                    {data: 'status', name:'users.active', defaultContent : '', searchable: false }
                ],
                search: {
                    "regex": true
                }
            });
            $('#user-table tbody').on( 'click', 'tr', function () {
                var data = table.row( this ).data();
                document.location.href = '{{ url('backend/users') }}/'+data.id;
            } );
        });
    </script>
@endpush