@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Reward
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Reward</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>
                            <div class="box-tools">
                                <a href="{{ url('backend/rewards/create') }}" class="btn btn-success btn-sm">Add New
                                    Reward</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive padding">
                            <table class="table table-hover" id="reward-table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Reward Caps</th>
                                    <th>End Date</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#reward-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: false,
                defaultContent: '',
                ajax: '{!! route('rewards-data') !!}',
                columns: [
                    {data: 'id', name: 'rewards.id', visible: false},
                    {data: 'name', name: 'rewards.name', defaultContent: ''},
                    {data: 'limit', name: 'rewards.limit', defaultContent: ''},
                    {data: 'endDate', name: 'rewards.endDate', defaultContent: ''}
                ],
                search: {
                    "regex": true
                }
            });
            $('#reward-table tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                document.location.href = '{{ url('backend/rewards') }}/' + data.id;
            });
        });
    </script>
@endpush
