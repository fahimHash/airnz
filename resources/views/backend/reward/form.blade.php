@push('styles')
    <link rel="stylesheet" href="{{ url('libs/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
@endpush
<div class="col-md-8">
  <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
      {!! Form::label('name', 'Name: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
          {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
    <div class="form-group{{ $errors->has('message') ? ' has-error' : ''}}">
        {!! Form::label('message', 'Message: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::textarea('message', null, ['class' => 'textarea', 'placeholder' => 'Place reward message here', 'style' => 'width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;']) !!}
            {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('limit') ? ' has-error' : ''}}">
        {!! Form::label('limit', 'Reward Caps: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::number('limit', null, ['class' => 'form-control', 'required' => 'required', 'min' => 1]) !!}
            {!! $errors->first('limit', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('endDate') ? ' has-error' : ''}}">
        {!! Form::label('endDate', 'Reward End Date: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::date('endDate', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('endDate', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
  <div class="form-group{{ $errors->has('type') ? ' has-error' : ''}}">
    <label class="col-md-4 control-label" for="type">Rewards suite</label>
    <div class="col-md-6">
      <select required name="type" id="type">
        <option>Select an option</option>
        <option value="Long Haul Flight" @if(isset($submitButtonText) && $reward->type == 'Long Haul Flight') selected="selected" @endif>Long Haul Flight</option>
        <option value="Voucher" @if(isset($submitButtonText) && $reward->type == 'Voucher') selected="selected" @endif>Voucher</option>
        <option value="Business Class Upgrade" @if(isset($submitButtonText) && $reward->type == 'Business Class Upgrade') selected="selected" @endif>Business Class Upgrade</option>
      </select>
    </div>
  </div>
    <div class="form-group{{ $errors->has('label') ? ' has-error' : ''}}" id="labelDiv">
        {!! Form::label('label', 'Label: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('label', null, ['class' => 'form-control']) !!}
            {!! $errors->first('label', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    @if(!isset($submitButtonText))
        <div class="form-group{{ $errors->has('destinations') ? ' has-error' : ''}}" id="destDiv">
            {!! Form::label('destinations', 'Destinations: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                <select id="destinations" name="destinations" multiple="multiple" style="width: 100%">
                    @foreach($destinations as $destination)
                        <option value="{{ $destination->id }}">{{ $destination->name}}</option>
                    @endforeach
                </select>
                {!! $errors->first('destinations', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @endif
    <div class="form-group{{ $errors->has('value') ? ' has-error' : ''}}">
        {!! Form::label('value', 'Value: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::number('value', null, ['class' => 'form-control']) !!}
            {!! $errors->first('value', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="col-md-4">
    <div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">
        {!! Form::label('image', 'Image: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <img src="{{ isset($reward->image) ? url('assets/img/'.$reward->image) : url('https://dummyimage.com/200x200/00bef3/ffffff')}}" id="showimage" class="img-circle" style="max-width: 200px; max-height: 200px;">
            <input type="file" id="image" name="image" class="btn btn-info" style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;">
            <br>
            <label for="image" class="btn btn-info" style="margin-top:10px;">Choose File</label>
        </div>
    </div>
</div>
<div class="box-footer">
    <input type="hidden" name="active" value="1">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info']) !!}
        <a href="{{ route('rewards.index') }}" class="btn btn-warning">Cancel</a>
    </div>
</div>
@push('scripts')
    <script src="{{ url('libs/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script>
        $(function () {
            $('.textarea').wysihtml5();
            $('#destDiv').hide();
            $('#type').multipleSelect({
                placeholder: "Select an option",
                width:'100%',
                single:true,
                filter: true,
                onClick: function(data) {
                    typeChange(data.value);
                }
            });

            $('#campaigns').select2();
            $('#destinations').select2({
                width: 'resolve'
            });

            function typeChange(selected) {
                if(selected == 'Long Haul Flight'){
                    $('#labelDiv').hide();
                    $('#destDiv').show();
                }else if(selected == 'Voucher'){
                    $('#labelDiv').show();
                    $('#destDiv').hide();
                }else if(selected == 'Business Class Upgrade'){
                    $('#labelDiv').hide();
                    $('#destDiv').show();
                }
            }
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#showimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#image").change(function(){
                readURL(this);
            });
        });
    </script>
@endpush