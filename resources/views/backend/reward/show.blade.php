@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Reward
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/rewards')}}">Reward</a></li>
                <li class="active">View Reward</li>
            </ol>
        </section>

    @include ('backend.layouts.message')

    <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>

                            <div class="box-tools">
                                @role('admin')
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/backend/rewards', $reward->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                <button type="submit" class="btn btn-default"><i class="fa fa-trash"></i></button>
                                {!! Form::close() !!}
                                @endauth
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-9">
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Title: </label>
                                    <div class="col-md-6">
                                        <p>{{ $reward->name }}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Reward Caps: </label>
                                    <div class="col-md-6">
                                        <p>{{ is_null($reward->limit) ? 'No Reward Caps Available' : $reward->type}}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Reward End
                                        Date: </label>
                                    <div class="col-md-6">
                                        <p>{{ is_null($reward->endDate) ? 'No Reward End Date Available' : $reward->endDate}}</p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="title" class="col-md-4 control-label text-right">Reward Suite: </label>
                                    <div class="col-md-6">
                                        <p>@if(is_null($reward->type))
                                                No Reward Suite Available
                                            @else
                                                {{$reward->type}} - {{$reward->label}}
                                                <br>
                                        <p>Reward Value {{$reward->value}}</p>
                                        @endif
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="agents" class="col-md-4 control-label text-right">Campaign with
                                        Reward: </label>
                                    <div class="col-md-6">
                                        @if($reward->campaigns)
                                            @foreach ($reward->campaigns as $campaign)
                                                <p>{{ $campaign->name }}</p>
                                            @endforeach
                                        @endif
                                    </div>
                                </div>
                                <div class="box-footer col-md-offset-4 col-md-4">
                                    <a class="btn btn-info"
                                       href="{{ url('/backend/rewards/' . $reward->id . '/edit') }}">Edit</a>
                                    <a href="{{ route('rewards.index') }}" class="btn btn-warning">Cancel</a>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group{{ $errors->has('upload_photo') ? ' has-error' : ''}}">
                                    <div class="col-md-6">
                                        <img src="{{ isset($reward->image) ? url('assets/img/'.$reward->image) : url('https://dummyimage.com/200x200/00bef3/ffffff')}}"
                                             id="showimages" style="max-width: 200px; max-height: 200px;">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>

            @if(count($agentsEligible) > 0)
                <div class="row">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Agents Eligible for Reward</h3>
                                <div class="box-tools">
                                    {{--<div class="input-group input-group-sm" style="width: 150px;">--}}
                                    {{--<input type="text" name="table_search" class="form-control pull-right" placeholder="Search">--}}

                                    {{--<div class="input-group-btn">--}}
                                    {{--<button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>--}}
                                    {{--</div>--}}
                                    {{--</div>--}}
                                </div>
                            </div>
                            <div class="box-body table-responsive padding">
                                <table class="table table-hover">
                                    <thead>
                                    <tr>
                                        <th>Campaign Title</th>
                                        <th>Agent Name</th>
                                        <th>Reward Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($agentsEligible as $item)
                                        <tr>
                                            <td>{{ $item->campaigns->name }}</td>
                                            <td>{{ $item->agents->name }}</td>
                                            <td>Available / Claimed / Expired</td>
                                            {{--TODO--}}
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                {!! $agentsEligible->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            @endif
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
