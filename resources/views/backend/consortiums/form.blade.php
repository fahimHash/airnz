<div class="col-md-8">
  <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
      {!! Form::label('name', 'Name: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
          {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
  <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
      {!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::email('email', null, ['class' => 'form-control']) !!}
          {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
  <div class="form-group{{ $errors->has('phone') ? ' has-error' : ''}}">
      {!! Form::label('phone', 'Phone: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::text('phone', null, ['class' => 'form-control']) !!}
          {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
  <div class="form-group{{ $errors->has('address') ? ' has-error' : ''}}">
      {!! Form::label('address', 'Street Address: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::text('address', null, ['class' => 'form-control']) !!}
          {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
  <div class="form-group">
    <label class="col-md-4 control-label" for="state">State</label>
    <div class="col-md-6">
      <select class="col-md-12" required name="state" id="state">
        <option value="Australian Capital Territory" @if(isset($submitButtonText) && $consortium->state == 'Australian Capital Territory') selected="selected" @endif >Australian Capital Territory</option>
        <option value="New South Wales" @if(isset($submitButtonText) && $consortium->state == 'New South Wales') selected="selected" @endif >New South Wales</option>
        <option value="Northern Territory" @if(isset($submitButtonText) && $consortium->state == 'Northern Territory') selected="selected" @endif >Northern Territory</option>
        <option value="Queensland" @if(isset($submitButtonText) && $consortium->state == 'Queensland') selected="selected" @endif >Queensland</option>
        <option value="South Australia" @if(isset($submitButtonText) && $consortium->state == 'South Australia') selected="selected" @endif>South Australia</option>
        <option value="Tasmania" @if(isset($submitButtonText) && $consortium->state == 'Tasmania') selected="selected" @endif>Tasmania</option>
        <option value="Victoria" @if(isset($submitButtonText) && $consortium->state == 'Victoria') selected="selected" @endif>Victoria</option>
        <option value="Western Australia" @if(isset($submitButtonText) && $consortium->state == 'Western Australia') selected="selected" @endif>Western Australia</option>
      </select>
    </div>
  </div>
    <div class="form-group{{ $errors->has('suburb') ? ' has-error' : ''}}">
        {!! Form::label('suburb', 'Suburb: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('suburb', null, ['class' => 'form-control']) !!}
            {!! $errors->first('suburb', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
  <div class="form-group{{ $errors->has('postcode') ? ' has-error' : ''}}">
      {!! Form::label('postcode', 'Postcode: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
          {!! $errors->first('postcode', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
</div>
<div class="col-md-4 ">
  <div class="form-group{{ $errors->has('upload_photo') ? ' has-error' : ''}}">
    {!! Form::label('upload_photo', 'Logo: ', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        <img src="{{ isset($consortium->logo) ? url('assets/img/'.$consortium->logo) : url('https://dummyimage.com/200x200/00bef3/ffffff')}}" id="showimage" class="img-circle" style="max-width: 200px; max-height: 200px;">
        <input type="file" id="upload_photo" name="upload_photo" class="btn btn-info" style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;">
        <br>
        <label for="upload_photo" class="btn btn-info" style="margin-top:10px;">Choose File</label>
    </div>
  </div>
</div>
<div class="box-footer">
    <input type="hidden" name="active" value="1">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info']) !!}
        <a href="{{ route('consortiums.index') }}" class="btn btn-warning">Cancel</a>
    </div>
</div>

@push('scripts')
<script>
$(function () {
    $('#state').select2();
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showimage').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
    }
    $("#upload_photo").change(function(){
        readURL(this);
    });
});
</script>
@endpush
