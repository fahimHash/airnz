@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Consortiums
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Consortiums</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>

                            <div class="box-tools">
                                @can('manage_consortium')
                                    <a href="{{ url('backend/consortiums/create') }}" class="btn btn-success btn-sm">Add
                                        New Consortium</a>
                                @endcan
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive padding">
                            <table class="table table-hover" id="consortiums-table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Agencies</th>
                                    <th>Stores</th>
                                    <th>Agents</th>
                                    <th>Total Sales</th>
                                    <th>Sales % Share</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#consortiums-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: false,
                defaultContent: '',
                ajax: '{!! route('consortiums-data') !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'name', defaultContent: ''},
                    {data: 'agencies', defaultContent: ''},
                    {data: 'stores', defaultContent: ''},
                    {data: 'agents', defaultContent: ''},
                    {data: 'sales', defaultContent: ''},
                    {data: 'percentage', defaultContent: ''}
                ],
                search: {
                    "regex": true
                }
            });
            $('#consortiums-table tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                document.location.href = '{{ url('backend/consortiums') }}/' + data.id;
            });
        });
    </script>
@endpush
