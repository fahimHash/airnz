<div class="col-md-12">
    <h4><b>{{ $survey->name }}</b></h4>
    <hr>
    <h4><b>Question</b></h4>
    <p>Type your question below</p>
    <div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
        <div class="col-md-12">
            {!! Form::textarea('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

</div>
<div class="box-footer">
    <div class="pull-right">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Save', ['class' => 'btn btn-info']) !!}
    </div>
</div>
@push('scripts')
    <script>
        $(function () {

        });
    </script>
@endpush