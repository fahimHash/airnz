@extends('backend.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <b>DUO</b> SURVEYS
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/surveys')}}"><b>DUO</b> SURVEYS</a></li>
                <li class="active">Question</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class='box-body'>
                            @include('backend.layouts.message')

                            {!! Form::open(['url' =>  route('surveys.questions.store', $survey->id), 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}

                            @include ('backend.questions.form')

                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
