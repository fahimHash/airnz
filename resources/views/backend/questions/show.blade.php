@extends('backend.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <b>DUO</b> SURVEYS
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/surveys')}}"><b>DUO</b> SURVEYS</a></li>
                <li class="active">Question</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class='box-body'>
                            <div class="col-md-12">
                                <h4><b>{{ $survey->name }}</b></h4>
                                <hr>
                                <h4><b>Question</b></h4>
                                <div class="form-group">
                                    <div class="col-md-12 well">
                                        <p>{{$question->title}}</p>
                                    </div>
                                </div>
                                <h4><b>Answers</b></h4>
                                <p>Start by adding a list of possible answers</p>
                                <button type="button" class="btn btn-default pull-right" data-toggle="modal" data-target="#myModal">
                                    Add Answer
                                </button>

                                <table class="table table-hover" id="answer-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Answer</th>
                                        <th>Answer Type</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <div class="box-footer">
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('backend.answers.modal')
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            $('#type').select2({
                'width':'100%'
            });
            var table = $('#answer-table').DataTable({
                pageLength: 10,
                searching: false,
                lengthChange: false,
                serverSide: true,
                processing: true,
                defaultContent: '',
                ajax: '{!! route('answer.data', $question->id) !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'name', name:'name',width: "75%"},
                    {data: 'type', name:'type', defaultContent : '',width: "20%"},
                    {data: 'action', name:'action', orderable: false, searchable: false,width: "5%"}
                ],
                search: {
                    "regex": true
                }
            });
        });
    </script>
@endpush