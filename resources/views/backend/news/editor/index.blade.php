<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Dashboard - {{ config('app.name', 'Laravel') }}</title>
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
    <link rel="stylesheet" href="{{ url('libs/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ url('libs/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('libs/keditor/css/keditor-1.1.5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('libs/keditor/css/keditor-components-1.1.5.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/fonts.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ url('assets/editor.css') }}">
    <script type="text/javascript" src="{{ url('libs/keditor/plugins/jquery-1.11.3/jquery-1.11.3.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('libs/keditor/plugins/bootstrap-3.3.6/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript">
        var bsTooltip = $.fn.tooltip;
        var bsButton = $.fn.button;
    </script>
    <script src="{{ url('libs/keditor/plugins/jquery-ui-1.11.4/jquery-ui.min.js') }}"></script>
    <script type="text/javascript">
        $.widget.bridge('uibutton', $.ui.button);
        $.widget.bridge('uitooltip', $.ui.tooltip);
        $.fn.tooltip = bsTooltip;
        $.fn.button = bsButton;
    </script>
    <script type="text/javascript" src="{{ url('libs/keditor/plugins/jquery.nicescroll-3.6.6/jquery.nicescroll.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('libs/keditor/plugins/ckeditor-4.5.6/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ url('libs/keditor/plugins/ckeditor-4.5.6/adapters/jquery.js') }}"></script>
    <!-- Start of KEditor scripts -->
    <script type="text/javascript" src="{{ url('libs/keditor/js/keditor-1.1.5.min.js') }}"></script>
    <script type="text/javascript" src="{{ url('libs/keditor/js/keditor-components-1.1.5.min.js') }}"></script>
    <!-- End of KEditor scripts -->
</head>
<body>
<div class="row">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
                <h1>
                    <small>Editing</small> {{ $post->title }}
                    <div class="pull-right">
                        <button class="btn btn-primary" id="saveBtn">Save & Preview</button>
                        <a class="btn btn-default" id="closeBtn" href="{{ route('news.show',$post->id) }}">Close</a>
                    </div>
                </h1>
            </div>
        </div>
    </div>
</div>
<div id="content-area">
    {!! $post->message !!}
</div>
{!! Form::model($post, [
                  'method' => 'PATCH',
                  'url' => route('news.update.content', $post->slug),
                  'class' => 'form-horizontal',
                  'enctype'=>'multipart/form-data',
                  'id' => 'contentForm'
              ]) !!}
<input id="postContent" name="message" type="hidden">
{!! Form::close() !!}
<script type="text/javascript">
    $(function () {
        $('#content-area').keditor({
            snippetsUrl: 'snippets',
        });
        //$.keditor.debug = false; //todo umcomment later
        $('#saveBtn').click(function () {
            $('#postContent').val($('#content-area').keditor('getContent'));
            $('#contentForm').submit();
        });
    });
</script>
</body>
</html>