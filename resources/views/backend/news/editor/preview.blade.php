@include('frontend.layouts.header')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ url('assets/editor.css') }}">
@endpush
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper wrapper-components">
    <div>
        <section class="content-header content-header-campaigns content-header-components">
            <div class="alert alert-warning">
                {!! Form::model($post, [
                                     'method' => 'PATCH',
                                     'url' => route('news.publish',$post->id),
                                     'class' => 'form-inline',
                                     'enctype'=>'multipart/form-data'
                                 ]) !!}
                This is preview of unpublished page
                <a class="btn btn-warning" href="{{ route('news.editor',$post->slug) }}">Edit</a>
                @if($post->publish == false)
                    {!! Form::submit('Publish', ['class' => 'btn btn-success']) !!}
                @else
                    {!! Form::submit('Unpublish', ['class' => 'btn btn-warning']) !!}
                @endif
                <a class="btn btn-basic" id="closeBtn" href="{{ route('news.show',$post->id) }}">Close</a>
                {!! Form::close() !!}
            </div>
            <div class="page-title">
                <h3><span>duo</span> {!! $post->title !!}</h3>
            </div>
        </section>
        <div id="maincontent" class="content content-components">
            <!-- Main content -->
            <section class="onboarding-body components-body">
                <div class="row">
                    <div class="col-md-12">
                        {!! $post->message !!}
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
<!-- ./wrapper -->

@include('frontend.layouts.footer')

