<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
  <div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
      {!! Form::label('title', 'Title: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
          {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
    <div class="form-group{{ $errors->has('summary') ? ' has-error' : ''}}">
        {!! Form::label('summary', 'Summary: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('summary', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('summary', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('territory') ? ' has-error' : ''}}">
        {!! Form::label('territory', 'Territory: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select id="territory" name="territory[]" multiple="multiple" class="col-md-12">
                @foreach($territories as $territory)
                    <option value="{{ $territory->id }}"
                            @if(isset($submitButtonText))
                                @if(in_array($territory->id,$territory_sel))
                                    selected="true"
                                @endif
                            @endif
                    >{{ $territory->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('territory', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
    <div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">
        {!! Form::label('image', 'Featured Image: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <img src="{{ isset($news->image) ? url('assets/img/'.$news->image) : url('https://dummyimage.com/200x200/00bef3/ffffff')}}" id="showimage" style="max-width: 200px; max-height: 200px;">
            <input type="file" id="image" name="image" class="btn btn-info" style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;">
            <br>
            <label for="image" class="btn btn-info" style="margin-top:10px;">Choose File</label>
        </div>
    </div>
</div>
<div class="box-footer">
    <input type="hidden" name="active" value="1">
    <div class="col-md-offset-4 col-xs-12 col-sm-12 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create Post & Open Editor', ['class' => 'btn btn-info']) !!}
        @if(isset($submitButtonText))
            <a href="{{ route('news.editor', ['slug' => $news->slug]) }}" class="btn btn-info">Post Editor</a>
        @endif
        <a href="{{ route('news.index') }}" class="btn btn-warning">Cancel</a>
    </div>
</div>
@push('scripts')
    <script>
        $(function () {

            $('#territory').select2();

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#showimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#image").change(function(){
                readURL(this);
            });
        });
    </script>
@endpush