@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                <b>DUO</b> SURVEYS
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/surveys')}}"><b>DUO</b> SURVEYS</a></li>
                <li class="active">Survey Details</li>
            </ol>
        </section>

    @include ('backend.layouts.message')

    <!-- Main content -->
        <section class="content">

            <div class="row">
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title"></h3>

                            <div class="box-tools">
                                <a class="btn btn-default" href="{{ url('/backend/surveys/' . $survey->id . '/edit') }}"><i class="fa fa-edit"></i></a>
                                @role('admin')
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/backend/surveys', $survey->id],
                                    'style' => 'display:inline'
                                ]) !!}
                                <button type="submit" class="btn btn-default"><i class="fa fa-trash"></i></button>
                                {!! Form::close() !!}
                                @endauth
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="col-md-8">
                                    <h4><b>{{ $survey->name }}</b></h4>
                                    <p>{{ $survey->description }}</p>
                                </div>
                                <div class="col-md-4">
                                    <h4><b>Participation Rate</b></h4>
                                    <div id="canvas-holder" style="width:75%">
                                        <canvas id="pieChart"/>
                                    </div>
                                </div>
                                <h4><b>Questions</b></h4>

                                <a href="{{ route('surveys.questions.create', $survey->id) }}" type="button" class="btn btn-default">
                                    Add Question
                                </a>

                                <table class="table table-hover" id="question-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Question</th>
                                        <th></th>
                                    </tr>
                                    </thead>
                                </table>
                                <div class="box-footer">
                                </div>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts'))
<script src="{{ url('libs/chartjs/Chart.min.js')}}"></script>

<script type="text/javascript">
    //-------------
    //- PIE CHART -
    //-------------
    window.chartColors = {
        red: 'rgb(255, 99, 132)',
        orange: 'rgb(255, 159, 64)',
        yellow: 'rgb(255, 205, 86)',
        green: 'rgb(75, 192, 192)',
        blue: 'rgb(54, 162, 235)',
        purple: 'rgb(153, 102, 255)',
        grey: 'rgb(201, 203, 207)'
    };

    var pieOptions = {
        type: 'doughnut',
        data: {
            datasets: [{
                data: [
                    50,
                    10,
                    40
                ],
                backgroundColor: [
                    window.chartColors.red,
                    window.chartColors.orange,
                    window.chartColors.yellow
                ],
                label: 'Dataset 1'
            }],
            labels: [
                "Yes",
                "No",
                "Other"
            ]
        },
        options: {
            responsive: true,
            legend: {
                position: 'top',
            },
            title: {
                display: true,
                text: 'Participation Rate'
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        }
    };
    var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
    var pieChart = new Chart(pieChartCanvas, pieOptions);


    $('#question-table').DataTable({
        pageLength: 10,
        lengthChange: false,
        serverSide: true,
        processing: false,
        defaultContent: '',
        ajax: '{{ route('question.data', $survey->id) }}',
        columns: [
            {data: 'id', visible: false},
            {data: 'title', name: 'title', defaultContent: '',width: "80%"},
            {data: 'action', name:'action', orderable: false, searchable: false, width: "20%"}
        ],
        search: {
            "regex": true
        }
    });
</script>
@endpush