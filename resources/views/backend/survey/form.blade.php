<div class="col-md-10">
    <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
        {!! Form::label('name', 'Name: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('description') ? ' has-error' : ''}}">
        {!! Form::label('description', 'Description: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::textarea('description', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('description', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('type') ? ' has-error' : ''}}">
        <label class="col-md-4 control-label" for="type">Type</label>
        <div class="col-md-6">
            <select required name="type" id="type" class="col-md-12">
                <option value="Single Answer"
                        @if(isset($submitButtonText) && $survey->type == 'Single Answer') selected="selected" @endif>
                    Single Answer
                </option>
                <option value="Multiple Choice"
                        @if(isset($submitButtonText) && $survey->type == 'Multiple Choice') selected="selected" @endif>
                    Multiple Choice
                </option>
            </select>
        </div>
    </div>
    <div class="form-group {{ $errors->has('territories') ? ' has-error' : ''}}">
        <label class="col-md-4 control-label" for="territories">Territory</label>
        <div class="col-md-6">
            <select required name="territories[]" id="territories" multiple="multiple" class="col-md-12">
                @foreach ($territories as $item)
                    <option value="{{$item->id}}"
                            @if(isset($submitButtonText))
                            @if(in_array($item->id,$territories_sel))
                            selected="true"
                            @endif
                            @endif
                    >{{$item->name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group{{ $errors->has('sale_segments') ? ' has-error' : ''}}">
        {!! Form::label('sale_segments', 'Segment - Sale Performance: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select id="sale_segments" name="sale_segments[]" multiple="multiple" class="col-md-12">
                @foreach($saleSegments as $saleSegment)
                    <option value="{{ $saleSegment->id }}"
                            @if(isset($submitButtonText))
                            @if(in_array($saleSegment->id,$saleSegment_sel))
                            selected="true"
                            @endif
                            @endif
                    >{{ $saleSegment->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('sale_segments', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('system_segments') ? ' has-error' : ''}}">
        {!! Form::label('system_segments', 'Segment - System Performance: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select id="system_segments" name="system_segments[]" multiple="multiple" class="col-md-12">
                @foreach($systemSegments as $ystemSegment)
                    <option value="{{ $ystemSegment->id }}"
                            @if(isset($submitButtonText))
                            @if(in_array($ystemSegment->id,$ystemSegment_sel))
                            selected="true"
                            @endif
                            @endif
                    >{{ $ystemSegment->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('system_segments', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('training_segments') ? ' has-error' : ''}}">
        {!! Form::label('training_segments', 'Segment - Training Performance: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select id="training_segments" name="training_segments[]" multiple="multiple"
                    class="col-md-12">
                @foreach($trainingSegments as $trainingSegment)
                    <option value="{{ $trainingSegment->id }}"
                            @if(isset($submitButtonText))
                            @if(in_array($trainingSegment->id,$trainingSegment_sel))
                            selected="true"
                            @endif
                            @endif
                    >{{ $trainingSegment->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('training_segments', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('states') ? ' has-error' : ''}}">
        {!! Form::label('states', 'State: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select id="states" name="states[]" multiple="multiple" class="col-md-12">
                @foreach($states as $state)
                    <option value="{{ $state->id }}"
                            @if(isset($submitButtonText))
                            @if(in_array($state->id,$state_sel))
                            selected="true"
                            @endif
                            @endif
                    >{{ $state->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">
        {!! Form::label('image', 'Featured Image: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <img src="{{ isset($survey->image) ? asset('storage/'.$survey->image) : url('https://dummyimage.com/200x200/00bef3/ffffff')}}"
                 id="showimage" style="max-width: 200px; max-height: 200px;">
            <input type="file" id="image" name="image" class="btn btn-info"
                   style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;">
            <br>
            <label for="image" class="btn btn-info" style="margin-top:10px;">Choose File</label>
            <button type="button" onclick="removePhoto()"
               class="btn btn-default" style="margin-top:10px;">
                DELETE
            </button>
        </div>
    </div>
    <div class="form-group{{ $errors->has('force_complete') ? ' has-error' : ''}}">
        {!! Form::label('force_complete', 'Force Completion: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::checkbox('force_complete') !!}
            {!! $errors->first('force_complete', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="box-footer">
    <input type="hidden" id="sc" name="sc" value="save">
    <div class="col-md-offset-4 col-md-4">
        <a href="{{ route('surveys.index') }}" class="btn btn-default">Cancel</a>
            <button class="btn btn-info" type="button" onclick="submitSurvey()">{{ isset($submitButtonText) ? $submitButtonText : 'Save' }}</button>
        @if(!isset($submitButtonText))
            <button class="btn btn-info" type="button" onclick="submitContinue()">Save & Continue</button>
        @endif
    </div>
</div>
@push('scripts')
    <script>
        $(function () {
            $('#states').select2();
            $('#type').select2();
            $('#territories').select2();
            $('#sale_segments').select2();
            $('#system_segments').select2();
            $('#training_segments').select2();

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#showimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image").change(function () {
                readURL(this);
            });
        });
        function submitSurvey() {
            $('#sc').val('save');
            $('#surveyForm').submit();
        }
        function submitContinue() {
            $('#sc').val('continue');
            $('#surveyForm').submit();
        }
        function removePhoto() {
            var r = confirm("Are you sure to delete this photo?");
            if (r == true) {
                axios.post('{{ route('surveys.remove.photo') }}', {
                    id: '{{ isset($survey->id) ? $survey->id : '' }}'
                }).then(function (response) {
                    $('#showimage').attr('src', '');
                })
                .catch(function (error) {

                });
            }
        }
    </script>
@endpush