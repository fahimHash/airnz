@extends('backend.layouts.app')
@push('styles')
    <script type="text/javascript" src="{{ url('libs/keditor/plugins/ckeditor-4.5.6/ckeditor.js') }}"></script>
    <script type="text/javascript" src="{{ url('libs/keditor/plugins/ckeditor-4.5.6/adapters/jquery.js') }}"></script>
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Page Editor
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/pages')}}">Pages</a></li>
                <li class="active">Edit Page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">
                                <small>Editing</small> {{ $post->title }}</h3>
                            <div class="pull-right">
                                <button class="btn btn-primary" id="saveBtn">Save & Preview</button>
                                <a class="btn btn-default" id="closeBtn"
                                   href="{{ route('pages.show',$post->id) }}">Close</a>
                            </div>
                            <div class="box-tools">
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            {!! Form::model($post, [
                                  'method' => 'PATCH',
                            'url' => route('page.update.content', $post->slug),
                            'class' => 'form-horizontal',
                            'enctype'=>'multipart/form-data',
                            'id' => 'contentForm'
                            ]) !!}
                            {!! Form::textarea('message',null,['id'=>'message']) !!}
                            {!! Form::close() !!}
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script type="text/javascript">
        CKEDITOR.replace('message', {

        });
        $(function () {
            $('#saveBtn').click(function () {
                $('#contentForm').submit();
            });
        });
    </script>
@endpush