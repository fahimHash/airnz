<div class="col-xs-12 col-sm-12 col-md-12 col-lg-8">
    <div class="form-group{{ $errors->has('title') ? ' has-error' : ''}}">
        {!! Form::label('title', 'Title: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('summary') ? ' has-error' : ''}}">
        {!! Form::label('summary', 'Summary: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('summary', null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('summary', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('territory') ? ' has-error' : ''}}">
        {!! Form::label('territory', 'Territory: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select id="territory" name="territory[]" multiple="multiple" class="col-md-12">
                @foreach($territories as $territory)
                    <option value="{{ $territory->id }}"
                            @if(isset($submitButtonText))
                            @if(in_array($territory->id,$territory_sel))
                            selected="true"
                            @endif
                            @endif
                    >{{ $territory->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('territory', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('state') ? ' has-error' : ''}}">
        {!! Form::label('state', 'State: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select id="state" name="state[]" multiple="multiple" class="col-md-12">
                @foreach($states as $state)
                    <option value="{{ $state->id }}"
                            @if(isset($submitButtonText))
                            @if(in_array($state->id,$state_sel))
                            selected="true"
                            @endif
                            @endif
                    >{{ $state->name}}</option>
                @endforeach
            </select>
            {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('pinned') ? ' has-error' : ''}}">
        {!! Form::label('pinned', 'Priority Page: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::checkbox('pinned', false, null ) !!}
            {!! $errors->first('pinned', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('layout') ? ' has-error' : ''}}">
        {!! Form::label('layout', 'Layout: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::radio('layout', 'standard', null ) !!}
            {!! Form::label('layout', 'Standard') !!}
            <br>
            {!! Form::radio('layout', 'extended', null ) !!}
            {!! Form::label('layout', 'Extended') !!}
            {!! $errors->first('layout', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('type') ? ' has-error' : ''}}">
        {!! Form::label('type', 'Editor Type: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::radio('type', 'standard', null ) !!}
            {!! Form::label('type', 'Standard') !!}
            <br>
            {!! Form::radio('type', 'advance', null ) !!}
            {!! Form::label('type', 'Advance') !!}
            {!! $errors->first('type', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-4">
    <div class="form-group{{ $errors->has('image') ? ' has-error' : ''}}">
        {!! Form::label('image', 'Featured Image: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <img src="{{ isset($page->image) ? asset('storage/'.$page->image) : url('https://dummyimage.com/200x200/00bef3/ffffff')}}"
                 id="showimage" style="max-width: 200px; max-height: 200px;">
            <input type="file" id="image" name="image" class="btn btn-info"
                   style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;">
            <br>
            <label for="image" class="btn btn-info" style="margin-top:10px;">Choose File</label>
            <button type="button" onclick="removePhoto()"
                    class="btn btn-default" style="margin-top:10px;">
                DELETE
            </button>
        </div>
    </div>
</div>
<div class="box-footer">
    <input type="hidden" name="active" value="1">
    <div class="col-md-offset-4 col-xs-12 col-sm-12 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create Page & Open Editor', ['class' => 'btn btn-info']) !!}
        @if(isset($submitButtonText))
            <a href="{{ route('page.editor', ['slug' => $page->slug]) }}" class="btn btn-info">Page Editor</a>
        @endif
        <a href="{{ route('pages.index') }}" class="btn btn-warning">Cancel</a>
    </div>
</div>
@push('scripts')
    <script>
        $(function () {

            $('#state').select2();
            $('#territory').select2();

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#showimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#image").change(function () {
                readURL(this);
            });
        });
        function removePhoto() {
            var r = confirm("Are you sure to delete this photo?");
            if (r == true) {
                axios.post('{{ route('page.remove.photo') }}', {
                    id: '{{ isset($page->id) ? $page->id : '' }}'
                }).then(function (response) {
                    $('#showimage').attr('src', '');
                })
                    .catch(function (error) {

                    });
            }
        }
    </script>
@endpush