@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Pages
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/pages')}}">Pages</a></li>
                <li class="active">View Page</li>
            </ol>
        </section>

    @include ('backend.layouts.message')

    <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-10">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Detail</h3>

                            <div class="box-tools">
                                {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/backend/pages', $page->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                <button type="submit" class="btn btn-default"><i class="fa fa-trash"></i></button>
                                {!! Form::close() !!}

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @if($page->publish == false)
                                <div class="alert alert-warning">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    This Post is Unpublished.
                                </div>
                            @else
                                <div class="alert alert-success">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    This Post is Published.
                                </div>
                            @endif
                            <div class="row">
                                <div class="col-xs-12 col-sm-12 col-md-6">
                                    <div class="form-group">
                                        <label for="title" class="col-md-4 control-label text-right">Title: </label>
                                        <div class="col-md-6">
                                            <p>{{ $page->title }}</p>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="title" class="col-md-4 control-label text-right">Summary: </label>
                                        <div class="col-md-6">
                                            <p>{{ $page->summary }}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-12 col-sm-12 col-md-4">
                                    <label for="showimages" class="col-md-6 control-label">Featured Image</label>
                                    <div class="form-group{{ $errors->has('upload_photo') ? ' has-error' : ''}}">
                                        <div class="col-md-6">
                                            <img src="{{ isset($page->image) ? asset('storage/'.$page->image) : url('https://dummyimage.com/200x200/00bef3/ffffff')}}"
                                                 id="showimages" style="max-width: 200px; max-height: 200px;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="box-footer col-md-offset-4 col-xs-12 col-sm-12 col-md-8">
                                {!! Form::model($page, [
                                     'method' => 'PATCH',
                                     'url' => route('page.publish',$page->id),
                                     'class' => 'form-inline',
                                     'enctype'=>'multipart/form-data'
                                 ]) !!}
                                @if($page->publish == false)
                                    {!! Form::submit('Publish Page', ['class' => 'btn btn-primary']) !!}
                                @else
                                    {!! Form::submit('Unpublish Page', ['class' => 'btn btn-warning']) !!}
                                @endif
                                <a class="btn btn-info" href="{{ route('page.preview.content', $page->slug) }}">Preview
                                    Post</a>
                                @if($page->type === 'terms-and-conditions')
                                    @else
                                    <a class="btn btn-info"
                                       href="{{ url('/backend/pages/' . $page->id . '/edit') }}">Edit</a>
                                @endif
                                <a href="{{ route('pages.index') }}" class="btn btn-warning">Cancel</a>
                                {!! Form::close() !!}
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
