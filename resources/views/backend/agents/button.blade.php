@can('manage_user')
    {!! Form::open([
        'url' => route('agentRestore'),
        'class' => 'form-horizontal',
        'enctype'=>'multipart/form-data'
    ]) !!}
    {!! Form::hidden('id', $user->id) !!}
    <button title="Restore Agent" type="submit" class="btn btn-default"><i class="fa fa-undo"></i></button>
    {!! Form::close() !!}
@endcan