<div class="col-md-8">
    <div class="form-group {{ $errors->has('title') ? ' has-error' : ''}}">
        <label class="col-md-4 control-label" for="title">Title</label>
        <div class="col-md-6">
            <select name="title" id="title" class="titleList col-md-12">
                <option value="">Select an option</option>
                <option value="Mr" @if(isset($submitButtonText) && $user->title == 'Mr') selected="selected" @endif>
                    Mr
                </option>
                <option value="Mrs" @if(isset($submitButtonText) && $user->title == 'Mrs') selected="selected" @endif>
                    Mrs
                </option>
                <option value="Ms" @if(isset($submitButtonText) && $user->title == 'Ms') selected="selected" @endif>
                    Ms
                </option>
                <option value="Miss" @if(isset($submitButtonText) && $user->title == 'Miss') selected="selected" @endif>
                    Miss
                </option>
            </select>
        </div>
    </div>
    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : ''}}">
        {!! Form::label('first_name', 'First Name: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('first_name', isset($first_name) ? $first_name : null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('first_name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : ''}}">
        {!! Form::label('last_name', 'Last Name: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('last_name', isset($last_name) ? $last_name : null, ['class' => 'form-control', 'required' => 'required']) !!}
            {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('email') ? ' has-error' : ''}}">
        {!! Form::label('email', 'Email: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            @if (isset($submitButtonText))
                {!! Form::email('email', null, ['class' => 'form-control','disabled' => 'disabled']) !!}
            @else
                {!! Form::email('email', null, ['class' => 'form-control']) !!}
            @endif
            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    @if (isset($submitButtonText))
        <div class="form-group{{ $errors->has('email_new') ? ' has-error' : ''}}">
            {!! Form::label('email_new', 'New Email: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::email('email_new', null, ['class' => 'form-control']) !!}
                {!! $errors->first('email_new', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group text-center">
            <i>Note: Setting new email will update existing email & notification will be sent to agent about
                the change.</i>
        </div>
    @endif
    <div class="form-group{{ $errors->has('phoneNumber') ? ' has-error' : ''}}">
        {!! Form::label('phoneNumber', 'Phone: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('phoneNumber', null, ['class' => 'form-control']) !!}
            {!! $errors->first('phoneNumber', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('address1') ? ' has-error' : ''}}">
        {!! Form::label('address1', 'Address1: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('address1', null, ['class' => 'form-control']) !!}
            {!! $errors->first('address1', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('address2') ? ' has-error' : ''}}">
        {!! Form::label('address2', 'Address2: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('address2', null, ['class' => 'form-control']) !!}
            {!! $errors->first('address2', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('address3') ? ' has-error' : ''}}">
        {!! Form::label('address3', 'Address3: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('address3', null, ['class' => 'form-control']) !!}
            {!! $errors->first('address3', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('suburb') ? ' has-error' : ''}}">
        {!! Form::label('suburb', 'Suburb: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('suburb', null, ['class' => 'form-control']) !!}
            {!! $errors->first('suburb', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('postcode') ? ' has-error' : ''}}">
        {!! Form::label('postcode', 'Postcode: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
            {!! $errors->first('postcode', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('state') ? ' has-error' : ''}}">
        {!! Form::label('state', 'State: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('state', $states, $stateSelected, ['placeholder' => 'Select a State','class' =>'stateList col-md-12']); !!}
        </div>
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
    </div>
    @can('manage_adv_user_settings')
        <div class="form-group{{ $errors->has('store') ? ' has-error' : ''}}">
            {!! Form::label('store', 'Consortium > Agency > Store: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                <select class="storeList col-md-12" name="store">
                    @foreach($storeList as $consortium)
                        <optgroup label="{{ $consortium->name }}">
                            @if(count($consortium->agencies) > 0)
                                @foreach($consortium->agencies as $agency)
                                    <optgroup label="&nbsp&nbsp {{ $agency->name }}">
                                        @if(count($agency->stores) > 0)
                                            @foreach($agency->stores as $store)
                                                <option value="{{ $store->id }}"
                                                @if(isset($submitButtonText))
                                                    {{ $store->id == $user->store_id ? 'selected="selected"' : '' }}
                                                        @endif
                                                > &nbsp&nbsp&nbsp {{ $store->name }}</option>
                                            @endforeach
                                        @endif
                                    </optgroup>
                                @endforeach
                            @endif
                        </optgroup>
                    @endforeach
                </select>
            </div>
            {!! $errors->first('store', '<p class="help-block">:message</p>') !!}
        </div>
    @endcan
    <div class="form-group{{ $errors->has('sale_target') ? ' has-error' : ''}}">
        {!! Form::label('sale_target', 'Sales Target: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::number('sale_target', null, ['class' => 'form-control', 'min' => '1']) !!}
            {!! $errors->first('sale_target', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    @if (!isset($submitButtonText))
        <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
            {!! Form::label('password', 'Password: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::password('password', ['class' => 'form-control']) !!}
                @if ($errors->has('password'))
                    <span class="help-block">
              <strong>{{ $errors->first('password') }}</strong>
            </span>
                @endif
            </div>
        </div>
        <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : ''}}">
            {!! Form::label('password-confirm', 'Confirm Password: ', ['class' => 'col-md-4 control-label']) !!}
            <div class="col-md-6">
                {!! Form::password('password-confirm', ['class' => 'form-control', 'name' => 'password_confirmation']) !!}
                {!! $errors->first('password-confirm', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    @else
        @can('manage_user')
            <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}">
                {!! Form::label('password', 'Password: ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::password('password', ['class' => 'form-control']) !!}
                    @if ($errors->has('password'))
                        <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
              </span>
                    @endif
                </div>
            </div>
            <div class="form-group{{ $errors->has('password-confirm') ? ' has-error' : ''}}">
                {!! Form::label('password-confirm', 'Confirm Password: ', ['class' => 'col-md-4 control-label']) !!}
                <div class="col-md-6">
                    {!! Form::password('password-confirm', ['class' => 'form-control', 'name' => 'password_confirmation']) !!}
                    {!! $errors->first('password-confirm', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group text-center">
                <i>Note: Setting new password will update existing password & notification will be sent to agent about
                    the change.</i>
            </div>
        @endcan
    @endif
</div>
<div class="col-md-3 text-center">
    <div class="form-group{{ $errors->has('upload_photo') ? ' has-error' : ''}}">
        <!-- <div class="col-md-6"> -->
        <img src="{{ isset($user->image) ? url('assets/img/'.$user->image) : url('https://dummyimage.com/200x200/00bef3/ffffff')}}"
             id="showimage" class="img-circle" style="max-width: 200px; max-height: 200px;">
        <input type="file" id="upload_photo" name="upload_photo" class="btn btn-info"
               style="width: 0.1px;height: 0.1px;opacity: 0;overflow: hidden;position: absolute;z-index: -1;">
        <br>
        <label for="upload_photo" class="btn btn-info" style="margin-top:10px;">Choose File</label>
        <!-- </div> -->
    </div>
</div>
<div class="box-footer">
    <div class="col-md-offset-3 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info']) !!}
        <a href="{{ route('agents.index') }}" class="btn btn-warning">Cancel</a>
    </div>
</div>
@push('scripts')
    <script src="{{ url('libs/validator.min.js') }}"></script>
    <script>
        $(function () {
            $('.titleList').select2();
            $('.storeList').select2();
            $('.stateList').select2();

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#showimage').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }
            $("#upload_photo").change(function () {
                readURL(this);
            });
        });
    </script>
@endpush

