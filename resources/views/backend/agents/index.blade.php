@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                My Agents
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">My Agents</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>

                            <div class="box-tools">
                                @can('manage_user')
                                    <a href="{{ url('backend/agents/create') }}" class="btn btn-success btn-sm">Add New
                                        Agent</a>
                                @endcan
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive padding">
                            <table class="table table-hover" id="agent-table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Contact No.</th>
                                    <th>Sale Confirmed</th>
                                    <th>Sale Pending</th>
                                    <th>Sale Cancelled</th>
                                    <th>BDM</th>
                                    <th>Created At</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#agent-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: true,
                defaultContent: '',
                ajax: '{!! route('agents-data') !!}',
                columns: [
                    {data: 'id', name: 'users.id', visible: false},
                    {data: 'name', name: 'users.name'},
                    {data: 'email', name: 'users.email', defaultContent: ''},
                    {data: 'phoneNumber', name: 'users.phoneNumber', defaultContent: ''},
                    {data: 'confirmed', name: 'confirmed', defaultContent: ''},
                    {data: 'pending', name: 'pending', defaultContent: ''},
                    {data: 'cancelled', name: 'cancelled', defaultContent: ''},
                    {data: 'territory', name: 'territory.name', defaultContent: ''},
                    {data: 'created_at', name: 'users.created_at', defaultContent: ''},
                ],
                search: {
                    "regex": true
                }
            });
            $('#agent-table tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                document.location.href = '{{ url('backend/agents') }}/' + data.id;
            });
        });
    </script>
@endpush
