@extends('backend.layouts.app')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Create New Agent
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url('backend/agents')}}"> My Agents</a></li>
        <li class="active">Create Agent</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-11">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header">
              <!-- <h3 class="box-title"></h3> -->
            </div>
            <!-- /.box-header -->
            <div class='box-body'>
            <!-- form start -->
            @include('backend.layouts.message')

            {!! Form::open(['url' => '/backend/agents', 'class' => 'form-horizontal','enctype'=>'multipart/form-data']) !!}

            @include ('backend.agents.form')

            {!! Form::close() !!}
          </div>
          </div>
          <!-- /.box -->

        </div>
        <!--/.col (left) -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
