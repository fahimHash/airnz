@extends('backend.layouts.app')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ url('libs/daterangepicker.css') }}"/>
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                @if($user->trashed()) Archived @endif Agent - {{$user->name}}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                @if($user->trashed())
                    <li><a href="{{ url('backend/agents/archive')}}"> Archived Agents</a></li>
                @else
                    <li><a href="{{ url('backend/agents')}}"> My Agents</a></li>
                @endif
                <li class="active">View Agent</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-4">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <!-- <h3 class="box-title"></h3> -->
                            <div class="box-tools">
                                @can('manage_user')
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/backend/agents', $user->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                    <button title="Archive Agent" type="submit" class="btn btn-default"><i
                                                class="fa fa-archive"></i></button>
                                    {!! Form::close() !!}
                                @endcan
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" id="showimages"
                                 src="{{ isset($user->image) ? url('assets/img/'.$user->image) : url('assets/img/default-image.png') }}" alt="Agent profile picture">

                            <h3 class="profile-username text-center">{{ empty($user->title) ? '' : $user->title.' '   }} {{ $user->name }}</h3>

                            <p class="text-muted text-center">{{ $user->email }}
                                <br>{{ empty($user->phoneNumber) ? '' : $user->phoneNumber  }}
                                <br>Agent ID: {{ empty($user->agent_id) ? '' : $user->agent_id  }}</p>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>Consortium</b> <a href="{{ empty($workplace) ? '#' : route('consortiums.show',$workplace->consortium['id']) }}"
                                            class="pull-right">{{ empty($workplace) ? 'No Consortium Information' : $workplace->consortium['name'] }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Agency</b> <a href="{{ empty($workplace) ? '#' : route('agency.show',$workplace->agency['id']) }}"
                                            class="pull-right">{{empty($workplace) ? 'No Agency Information' : $workplace->agency['name'] }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Store</b> <a href="{{ empty($workplace) ? '#' : route('store.show',$workplace->id) }}"
                                                     class="pull-right">{{empty($workplace) ? 'No Store Information' : $workplace->name }}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>BDM</b>
                                    <a href="{{ empty($workplace) ? url('/backend/agents/' . $user->id . '/edit') : route('territory.show',$workplace->territory->id) }}"
                                            class="pull-right">{{empty($workplace) ? 'Assign BDM' : $workplace->bdm->name}}</a>
                                </li>
                                <li class="list-group-item">
                                    <b>Sale Target</b> <span
                                            class="pull-right">{{ empty($user->sale_target) ? 'No Sale Target' : $user->sale_target  }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Role</b> <span class="pull-right">{{$role->label}}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Status</b> <span class="pull-right">@if($user->active  == 1)
                                            <span class="label label-success">Active</span>
                                        @else
                                            <span class="label label-danger">Lapsed</span>
                                        @endif</span>
                                </li>
                            </ul>
                            @if(!$user->trashed())
                                @if($user->active  == 0)
                                <a href="#"
                                   class="btn btn-default btn-block" id="reactivate"><b>Email Reactivation Link</b></a>
                                @endif
                            @endif
                            @can('manage_user')
                                @if(!$user->trashed())
                                    <a href="{{ url('/backend/agents/' . $user->id . '/edit') }}"
                                       class="btn btn-primary btn-block"><b>Edit</b></a>
                                @endif
                            @endcan
                            @if($user->trashed())
                                {!! Form::open([
                                    'url' => route('agentRestore'),
                                    'class' => 'form-horizontal',
                                    'enctype'=>'multipart/form-data'
                                ]) !!}
                                {!! Form::hidden('id', $user->id) !!}
                                <button title="Restore Agent" type="submit" class="btn btn-primary btn-block"><b>Restore Agent</b></button>
                                {!! Form::close() !!}
                                <a href="{{ route('archive') }}"
                                   class="btn btn-warning btn-block"><b>Cancel</b></a>
                            @else
                                <a href="{{ route('agents.index') }}"
                                   class="btn btn-warning btn-block"><b>Cancel</b></a>
                            @endif
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!--/.col (left) -->
                </div>
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <!-- <h3 class="box-title"></h3> -->
                            <div class="box-tools">
                                <div id="reportrange" class="breadcrumb"
                                     style="cursor: pointer; background-color: #fff; border: 1px solid #ccc;">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    <span></span> <b class="caret"></b>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row pad">
                                <div class="col-md-2" id="canvas-holder" style="width:30%">
                                    <canvas id="pieChart"/>
                                </div>
                                <div class="col-md-2" id="canvas-holder" style="width:30%">
                                    <canvas id="campaignChart"/>
                                </div>
                                <div class="col-md-2" id="canvas-holder" style="width:30%">
                                    <canvas id="trainingChart"/>
                                </div>
                            </div>
                            <p class="pad text-center">
                                <strong><span id="sales-duration"></span></strong>
                            </p>

                            <div class="chart">
                                <!-- Sales Chart Canvas -->
                                <canvas id="salesChart" style="height: 180px;"></canvas>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!--/.col (left) -->
                </div>
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false">Sale</a>
                            </li>
                            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Campaign</a>
                            </li>
                            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Reward</a>
                            </li>
                            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="tab_1">
                                <table class="table table-hover table-responsive padding" id="sales-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Entry Date</th>
                                        <th>Sale Status</th>
                                        <th>Issued Date</th>
                                        <th>PNR</th>
                                        <th>CTY</th>
                                        <th>Stock</th>
                                        <th>Ticket Number</th>
                                        <th>Departure Date</th>
                                        <th>Class</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <table class="table table-hover table-responsive padding" id="campaigns-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Campaign</th>
                                        <th>Type</th>
                                        <th>Status</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">
                                <table class="table table-hover table-responsive padding" id="rewards-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Reward</th>
                                        <th>Type</th>
                                        <th>Value</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script src="{{ url('libs/moment.min.js')}}"></script>
    <script src="{{ url('libs/daterangepicker.js')}}"></script>
    <script src="{{ url('libs/chartjs/Chart.min.js')}}"></script>
    <script src="{{ url('libs/jquery.sparkline.min.js')}}"></script>
    <script type="text/javascript">

            //-----------------------
            //- MONTHLY SALES CHART -
            //-----------------------
            var salesChartData = {
                type: 'line',
                data: {
                    labels: [],
                    datasets: [
                        {
                            label: "Valid Sales",
                            borderColor: "rgb(210, 214, 222)",
                            backgroundColor: "rgb(210, 214, 222)",
                            fill: "start",
                            data: []
                        },
                        {
                            label: "Pending Sales",
                            backgroundColor: "rgba(60,141,188,0.9)",
                            borderColor: "rgba(60,141,188,0.8)",
                            fill: "start",
                            data: []
                        }
                    ]
                },
                options: {
                    //Boolean - If we should show the scale at all
                    showScale: true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines: false,
                    //String - Colour of the grid lines
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    //Number - Width of the grid lines
                    scaleGridLineWidth: 1,
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines: true,
                    //Boolean - Whether the line is curved between points
                    bezierCurve: true,
                    //Number - Tension of the bezier curve between points
                    bezierCurveTension: 0.3,
                    //Boolean - Whether to show a dot for each point
                    pointDot: false,
                    //Number - Radius of each point dot in pixels
                    pointDotRadius: 4,
                    //Number - Pixel width of point dot stroke
                    pointDotStrokeWidth: 1,
                    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                    pointHitDetectionRadius: 20,
                    //Boolean - Whether to show a stroke for datasets
                    datasetStroke: true,
                    //Number - Pixel width of dataset stroke
                    datasetStrokeWidth: 2,
                    //Boolean - Whether to fill the dataset with a color
                    datasetFill: true,
                    //String - A legend template
                    {{--legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",--}}
                    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                    maintainAspectRatio: true,
                    //Boolean - whether to make the chart responsive to window resizing
                    responsive: true
                }
            };
            // Get context with jQuery - using jQuery's .get() method.
            var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var myLine = new Chart(salesChartCanvas, salesChartData);

            //---------------------------
            //- END MONTHLY SALES CHART -
            //---------------------------

            //-------------
            //- Total Sales PIE CHART -
            //-------------
            window.chartColors = {
                red: 'rgb(255, 99, 132)',
                orange: 'rgb(255, 159, 64)',
                yellow: 'rgb(255, 205, 86)',
                green: 'rgb(75, 192, 192)',
                blue: 'rgb(54, 162, 235)',
                purple: 'rgb(153, 102, 255)',
                grey: 'rgb(201, 203, 207)'
            };

            var pieOptions = {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [
                            {!! $pending !!},
                            {!! $cancelled !!},
                            {!! $problem !!},
                            {!! $valid !!}
                        ],
                        backgroundColor: [
                            window.chartColors.red,
                            window.chartColors.orange,
                            window.chartColors.yellow,
                            window.chartColors.green
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        "Pending",
                        "Cancelled",
                        "Problem",
                        "Valid"
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Total Sales Chart'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
            var pieChart = new Chart(pieChartCanvas, pieOptions);

            //-------------
            //- Campaign PIE CHART -
            //-------------
            var campaignOptions = {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [
                            {!! $campaignsInProgress !!},
                            {!! $completedCampaign !!}
                        ],
                        backgroundColor: [
                            window.chartColors.yellow,
                            window.chartColors.green
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        "In Progress",
                        "Complete"
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Campaign Chart'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            var campaignChartCanvas = $('#campaignChart').get(0).getContext('2d');
            var campaignChart = new Chart(campaignChartCanvas, campaignOptions);

            //-------------
            //- Training PIE CHART -
            //-------------
            var trainingOptions = {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [
                            1,
                            2
                        ],
                        backgroundColor: [
                            window.chartColors.yellow,
                            window.chartColors.green
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        "In Progress",
                        "Complete"
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Training Chart'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            var trainingChartCanvas = $('#trainingChart').get(0).getContext('2d');
            var trainingChart = new Chart(trainingChartCanvas, trainingOptions);

            //-----------------------
            //- DATE RANGE PICKER -
            //-----------------------

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                axios.post('{{ route('agentsDateRange') }}', {
                    startDate: start.format('YYYY-MM-DD'),
                    endDate: end.format('YYYY-MM-DD'),
                    id: '{{ $user->id }}'
                }).then(function (response) {
                    myLine.data.labels = [];
                    myLine.data.datasets[0].data = [];
                    myLine.data.datasets[1].data = [];
                    response.data.labels.forEach(function (label) {
                        myLine.data.labels.push(label);
                    });
                    response.data.valid.forEach(function (responseData) {
                        myLine.data.datasets[0].data.push(responseData.sales);
                    });
                    response.data.pending.forEach(function (responseData) {
                        myLine.data.datasets[1].data.push(responseData.sales);
                    });
                    myLine.update();
                    $('#sales-duration').html('Sales Duration: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                })
                .catch(function (error) {

                });
            }

            $('#reportrange').daterangepicker({
                autoApply: true,
                startDate: start,
                endDate: end,
                alwaysShowCalendars: true,
                opens: "left",
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);
            //-----------------------
            //- End of DATE RANGE PICKER -
            //-----------------------

            $('#sales-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: false,
                defaultContent: '',
                ajax: '{!! route('agentSalesData', $user->id) !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'created_at', defaultContent: ''},
                    {data: 'status', defaultContent: ''},
                    {data: 'issued_date', defaultContent: ''},
                    {data: 'pnr', defaultContent: ''},
                    {data: 'cty', defaultContent: ''},
                    {data: 'stock', defaultContent: ''},
                    {data: 'ticket_number', defaultContent: ''},
                    {data: 'departure_date', defaultContent: ''},
                    {data: 'class', defaultContent: ''}
                ],
                search: {
                    "regex": true
                }
            });

            $('#campaigns-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: false,
                defaultContent: '',
                ajax: '{!! route('agentCampaignsData', $user->id) !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'name', defaultContent: ''},
                    {data: 'type', defaultContent: ''},
                    {data: 'status', defaultContent: ''}
                ],
                search: {
                    "regex": true
                }
            });

            $('#rewards-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: false,
                defaultContent: '',
                ajax: '{!! route('agentRewardsData', $user->id) !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'name', defaultContent: ''},
                    {data: 'type', defaultContent: ''},
                    {data: 'value', defaultContent: ''}
                ],
                search: {
                    "regex": true
                }
            });
        $(document).ready(function () {
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $('#sales-table').css({
                    width: ''
                });
                $('#campaigns-table').css({
                    width: ''
                });
                $('#rewards-table').css({
                    width: ''
                });
            });
            $('.nav-tabs a[href="#tab_1"]').tab('show');

            $('#reactivate').on('click',function(){
                axios.post('{{ route('agents.reactivate') }}', {
                    id: '{{ $user->id }}'
                }).then(function (response) {
                    $('#reactivate').hide();
                })
                .catch(function (error) {

                });
            });
        });
    </script>
@endpush