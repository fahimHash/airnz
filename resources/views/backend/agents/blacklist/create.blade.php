@extends('backend.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Create Blacklist
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ route('blacklists.index')}}">Blacklists</a></li>
                <li class="active">Create Blacklist</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-10">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class='box-body'>
                            @include('backend.layouts.message')

                            {!! Form::open(['url' => '/backend/blacklists', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}

                            <div class="col-md-8">
                                <div class="form-group{{ $errors->has('email_domain') ? ' has-error' : ''}}">
                                    {!! Form::label('email_domain', 'Email Domain: ', ['class' => 'col-md-4 control-label']) !!}
                                    <div class="col-md-6">
                                        {!! Form::text('email_domain', null, ['class' => 'form-control', 'required' => 'required']) !!}
                                        {!! $errors->first('email_domain', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                                <div class="form-group text-center">
                                    <i>Note: For email such as 'somebody@company.com.nz', please enter only 'company.com.nz'</i>
                                </div>
                            </div>
                            <div class="box-footer">
                                <div class="col-md-offset-4 col-md-4">
                                    {!! Form::submit('Create', ['class' => 'btn btn-info']) !!}
                                    <a href="{{ route('blacklists.index') }}" class="btn btn-warning">Cancel</a>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
