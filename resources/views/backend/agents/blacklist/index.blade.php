@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Blacklist
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Blacklist</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>

                            <div class="box-tools">
                                    <a href="{{ url('backend/blacklists/create')}}" class="btn btn-success btn-sm">Add New
                                        Blacklist</a>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive padding">
                            <table class="table table-hover" id="blacklist-table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Email Domain</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#blacklist-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: true,
                defaultContent: '',
                ajax: '{!! route('blacklist-data') !!}',
                columns: [
                    {data: 'id', name: 'id', visible: false},
                    {data: 'email_domain', name: 'email_domain'}
                ]
            });
        });
    </script>
@endpush
