@extends('backend.layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          All Roles
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li class="active">Roles</li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
          @include ('backend.layouts.message')

        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">List</h3>

                <div class="box-tools">
                  {{-- <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div> --}}
                  <a href="{{ url('backend/roles/create') }}" class="btn btn-success btn-sm">Add New Role</a>
                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body table-responsive padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Label</th>
                            <th>Permissions</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($roles as $item)
                            <tr>
                                <td><a href="{{ url('/backend/roles', $item->id) }}">{{ $item->name }}</a></td>
                                <td>{{ $item->label }}</td>
                                <td>
                                  @foreach ($item->permissions as $permission)
                                    {{$permission->label}}
                                    @if (!$loop->last)
                                      ,
                                    @endif
                                  @endforeach
                                </td>
                                <td>
                                    <a href="{{ url('/backend/roles/' . $item->id . '/edit') }}">
                                        <button type="submit" class="btn btn-info btn-xs">Edit</button>
                                    </a> /
                                    {!! Form::open([
                                        'method'=>'DELETE',
                                        'url' => ['/backend/roles', $item->id],
                                        'style' => 'display:inline'
                                    ]) !!}
                                        {!! Form::submit('Delete', ['class' => 'btn btn-danger btn-xs']) !!}
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <!-- paginate -->
                {!! $roles->links() !!}
              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
