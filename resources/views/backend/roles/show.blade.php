@extends('backend.layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Role User
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('backend/roles')}}">Roles</a></li>
          <li>View Role</li>
        </ol>
      </section>

      @include ('backend.layouts.message')

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-12">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">List</h3>

                <div class="box-tools">
                  {{-- <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control pull-right" placeholder="Search">

                    <div class="input-group-btn">
                      <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
                    </div>
                  </div> --}}

                </div>
              </div>
              <!-- /.box-header -->
              <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Role</th>
                            <th>User Name</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($users as $user)
                          <tr>
                            <td> {{ $role->label }} </td>
                            <td> {{ $user->name }} </td>
                          </tr>
                        @endforeach

                    </tbody>
                </table>

              </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
