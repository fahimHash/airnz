@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Edit Territory
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/territory')}}">Territories</a></li>
                <li class="active">Edit Territory</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-12">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title"></h3>
                        </div>
                        <!-- /.box-header -->
                        <!-- form start -->
                        <div class='box-body'>
                            @include('backend.layouts.message')

                            {!! Form::model($territory, [
                                'method' => 'PATCH',
                                'url' => ['/backend/territory', $territory->id],
                                'class' => 'form-horizontal',
                                'enctype'=>'multipart/form-data',
                                'onsubmit' => 'return confirm("Are you sure to update this territory ?")'
                            ]) !!}

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                                {!! Form::label('name', 'Territory Name: ', ['class' => 'col-md-4 control-label']) !!}
                                <div class="col-md-6">
                                    {!! Form::text('name', $territory->name, ['class' => 'form-control', 'required' => 'required']) !!}
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="form-group{{ $errors->has('user') ? ' has-error' : ''}}">
                                {!! Form::label('user', 'BDM: ', ['class' => 'col-md-4 control-label']) !!}
                                <div class="col-md-6">
                                    <select required class="col-md-12" id="user" name="user">
                                        <option value="">Select an option</option>
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}"
                                                    @if(isset($manager_sel->id))
                                                    @if($manager_sel->id == $user->id))
                                                    selected="true"
                                                    @endif
                                                    @endif
                                            >{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                    {!! $errors->first('user', '<p class="help-block">:message</p>') !!}
                                    </br>
                                    </br>
                                    <span class="label label-warning">Attention! If the user doesn't exist in this list, check they have been given BDM role.
                                        Choosing new BDM will replace currently assigned BDM.
                                    </span>
                                    </br>
                                </div>
                            </div>
                            {{----}}
                            {{--<div class="form-group{{ $errors->has('states') ? ' has-error' : ''}}">--}}
                            {{--{!! Form::label('states', 'States: ', ['class' => 'col-md-4 control-label']) !!}--}}
                            {{--<div class="col-md-6">--}}
                            {{--<select required id="states" name="states[]" multiple="multiple">--}}
                            {{--@foreach($states as $state)--}}
                            {{--<option value="{{ $state->id }}"--}}
                            {{--@if(in_array($state->id,$state_sel))--}}
                            {{--selected="true"--}}
                            {{--@endif--}}
                            {{-->{{ $state->name}}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                            {{--{!! $errors->first('states', '<p class="help-block">:message</p>') !!}--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group{{ $errors->has('type') ? ' has-error' : ''}}">--}}
                            {{--{!! Form::label('type', 'Type: ', ['class' => 'col-md-4 control-label']) !!}--}}
                            {{--<div class="col-md-6">--}}
                            {{--<select required name="type[]" id="type">--}}
                            {{--<option value="Wholesale" @if($territory->type == 'Wholesale') selected="selected" @endif>Wholesale</option>--}}
                            {{--<option value="Retail" @if($territory->type == 'Retail') selected="selected" @endif>Retail</option>--}}
                            {{--<option value="TMC" @if($territory->type == 'TMC') selected="selected" @endif>TMC</option>--}}
                            {{--<option value="Corporate" @if($territory->type == 'Corporate') selected="selected" @endif>Corporate</option>--}}
                            {{--<option value="Independent/Other" @if($territory->type == 'Independent/Other') selected="selected" @endif>Independent/Other</option>--}}
                            {{--</select>--}}
                            {{--{!! $errors->first('type', '<p class="help-block">:message</p>') !!}--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group{{ $errors->has('consortiums') ? ' has-error' : ''}}">--}}
                            {{--{!! Form::label('consortiums', 'Consortiums: ', ['class' => 'col-md-4 control-label']) !!}--}}
                            {{--<div class="col-md-6">--}}
                            {{--<select required id="consortiums" name="consortiums[]" multiple="multiple">--}}
                            {{--@foreach($consortiums as $consortium)--}}
                            {{--<option value="{{ $consortium->id }}"--}}
                            {{--@if(in_array($consortium->id,$consortiums_sel))--}}
                            {{--selected="true"--}}
                            {{--@endif--}}
                            {{-->{{ $consortium->name}}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                            {{--{!! $errors->first('consortiums', '<p class="help-block">:message</p>') !!}--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--<div class="form-group{{ $errors->has('agency') ? ' has-error' : ''}}">--}}
                            {{--{!! Form::label('agency', 'Agency: ', ['class' => 'col-md-4 control-label']) !!}--}}
                            {{--<div class="col-md-6">--}}
                            {{--<select required id="agency" name="agency[]" multiple="multiple">--}}
                            {{--@foreach($agencies as $agency)--}}
                            {{--<option value="{{ $agency->id }}"--}}
                            {{--@if(in_array($agency->id,$agency_sel))--}}
                            {{--selected="true"--}}
                            {{--@endif--}}
                            {{-->{{ $agency->name}}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                            {{--{!! $errors->first('agency', '<p class="help-block">:message</p>') !!}--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="form-group{{ $errors->has('sale_target') ? ' has-error' : ''}}">
                                {!! Form::label('sale_target', 'Sales Target for an Agent: ', ['class' => 'col-md-4 control-label', 'title' => 'The sales target will be applied to all agents within the territory, any specific agent target will not be overriden.']) !!}
                                <div class="col-md-6">
                                    {!! Form::number('sale_target', null, ['class' => 'form-control', 'min' => '1']) !!}
                                    {!! $errors->first('sale_target', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            {{--<div class="form-group{{ $errors->has('branch') ? ' has-error' : ''}}">--}}
                            {{--{!! Form::label('branch', 'Branch: ', ['class' => 'col-md-4 control-label']) !!}--}}
                            {{--<div class="col-md-6">--}}
                            {{--<select required id="branch" name="branch[]" multiple="multiple">--}}
                            {{--@foreach($branches as $branch)--}}
                            {{--<option value="{{ $branch->id }}"--}}
                            {{--@if(in_array($branch->id,$branch_sel))--}}
                            {{--selected="true"--}}
                            {{--@endif--}}
                            {{-->{{ $branch->name}}</option>--}}
                            {{--@endforeach--}}
                            {{--</select>--}}
                            {{--{!! $errors->first('branch', '<p class="help-block">:message</p>') !!}--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            <div class="box-footer">
                                <div class="col-md-offset-4 col-md-4">
                                    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                    <a href="{{ route('territory.index') }}" class="btn btn-warning">Cancel</a>
                                </div>
                            </div>

                            {!! Form::close() !!}
                        </div>
                    </div>
                    <!-- /.box -->

                </div>
                <!--/.col (left) -->
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        $(function () {
            $('#user').select2();
            //var globalConsortium;
//      $('#states').multipleSelect({
//          placeholder: "Select an option",
//          width: '100%',
//          filter: true
//      });
//      $('#user').multipleSelect({
//          placeholder: "Select an option",
//          width: '100%',
//          single: true,
//          filter: true
//      });
//      $('#type').multipleSelect({
//          placeholder: "Select an option",
//          width: '100%',
//          filter: true,
//          onClick: function () {
//              $("#consortiums").multipleSelect("uncheckAll");
//              $('#consortiums').empty();
//              $("#agency").multipleSelect("uncheckAll");
//              $('#agency').empty();
//              $("#branch").multipleSelect("uncheckAll");
//              $('#branch').empty();
//          }
//      });
//      $('#consortiums').multipleSelect({
//          placeholder: "Select an option",
//          width: '100%',
//          filter: true,
//          onClick: function () {
//              consortiumChange();
//          },
//          onCheckAll: function () {
//              consortiumChange();
//          }
//      });
//      $('#agency').multipleSelect({
//          placeholder: "Select an option",
//          width: '100%',
//          filter: true
//      });
//      $('#branch').multipleSelect({
//          placeholder: "Select an option",
//          width: '100%',
//          filter: true
//      });

//      function consortiumChange() {
//          $('#spinnerContainer').spin();
//          var selectedConsortium = JSON.stringify($('#consortiums').multipleSelect('getSelects'));
//          var selectedType = JSON.stringify($('#type').multipleSelect('getSelects'));
//          //globalConsortium = selectedConsortium;
//
//          axios.post('agency', {
//              consortiums: selectedConsortium,
//              type: selectedType
//          }).then(function (response) {
//              $("#agency").multipleSelect("uncheckAll");
//              $('#agency').empty();
//              $("#branch").multipleSelect("uncheckAll");
//              $('#branch').empty();
//
//              $.each(response.data, function (key, value) {
//                  $('#agency').append('<option value=' + value.id + '>' + value.name + '</option>');
//              });
//              $('#agency').multipleSelect('refresh');
//              $('#spinnerContainer').spin(false);
//          })
//              .catch(function (error) {
//                  console.log(error);
//              });
//      }

//      function agencyChange() {
//          $('#spinnerContainer').spin();
//          var selectedAgency = JSON.stringify($('#agency').multipleSelect('getSelects'));
//          axios.post('branch', {
//              consortium: globalConsortium,
//              agency: selectedAgency,
//          }).then(function (response) {
//              $("#branch").multipleSelect("uncheckAll");
//              $('#branch').empty();
//
//              $.each(response.data, function (key, value) {
//                  $('#branch').append('<option value=' + value.id + '>' + value.name + '</option>');
//              });
//              $('#branch').multipleSelect('refresh');
//              $('#spinnerContainer').spin(false);
//          })
//              .catch(function (error) {
//                  console.log(error);
//              });
//      }
        });
    </script>
@endpush
