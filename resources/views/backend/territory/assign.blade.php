@extends('backend.layouts.app')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Assign Territory to BDM
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url('backend/territory')}}">Territory</a></li>
        <li>Assign Territory to BDM</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              @if ($errors->any())
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              @endif

              {!! Form::open(['method' => 'POST', 'url' => '/backend/territory/assign', 'class' => 'form-horizontal']) !!}

              <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                  {!! Form::label('name', 'Territory: ', ['class' => 'col-md-4 control-label']) !!}
                  <div class="col-md-6">
                      <select class="states form-control" id="state" name="state">
                          @foreach($states as $state)
                          <option value="{{ $state->id }}">{{  $state->name}}</option>
                          @endforeach
                      </select>
                      {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                  </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                  {!! Form::label('name', 'Managers: ', ['class' => 'col-md-4 control-label']) !!}
                  <div class="col-md-6">
                      <select class="user form-control" id="user" name="user" >
                          @foreach($users as $user)
                          <option value="{{ $user->id }}">{{ $user->name }}</option>
                          @endforeach
                      </select>
                      {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                  </div>
              </div>

              <div class="form-group">
                  <div class="col-md-offset-4 col-md-4">
                      {!! Form::submit('Assign', ['class' => 'btn btn-primary']) !!}
                  </div>
              </div>
              {!! Form::close() !!}
            </div>
            <!-- /.box -->

          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
