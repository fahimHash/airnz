@extends('backend.layouts.app')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Territories
                <small></small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li class="active">Territories</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @include ('backend.layouts.message')
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">List</h3>

                            <div class="box-tools">
                                {{--@can('manage_state')--}}
                                    {{--<a href="{{ url('backend/territory/create') }}" class="btn btn-success btn-sm">Add--}}
                                        {{--New Territory</a>--}}
                                {{--@endcan--}}
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body table-responsive padding">
                            <table class="table table-hover" id="territory-table">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>BDM</th>
                                    <th>State</th>
                                    <th>Consortium</th>
                                    <th>Agency</th>
                                    <th>Store</th>
                                    <th>Agents</th>
                                    <th>BDM % Share of Total Agents</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#territory-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: false,
                defaultContent: '',
                ajax: '{!! route('territory-data') !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'name'},
                    {data: 'manager.name', defaultContent: ''},
                    {data: 'states_count', defaultContent: '', searchable: false},
                    {data: 'consortiums_count', defaultContent: '', searchable: false},
                    {data: 'agencies_count', defaultContent: '', searchable: false},
                    {data: 'stores_count', defaultContent: '', searchable: false},
                    {data: 'agents_count', defaultContent: '', searchable: false},
                    {data: 'percentage', defaultContent: '', searchable: false}
                ],
                search: {
                    "regex": true
                }
            });
            $('#territory-table tbody').on('click', 'tr', function () {
                var data = table.row(this).data();
                document.location.href = '{{ url('backend/territory') }}/' + data.id;
            });
        });
    </script>
@endpush
