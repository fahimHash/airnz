@extends('backend.layouts.app')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ url('libs/daterangepicker.css') }}"/>
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Territory - {{ $territory->name }}
            </h1>
            <ol class="breadcrumb">
                <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                <li><a href="{{ url('backend/territory')}}">Territories</a></li>
                <li class="active">View Territory</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="row">
                <!-- left column -->
                <div class="col-md-4">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <div class="box-tools">
                                @role('admin')
                                {!! Form::open([
                                    'method'=>'DELETE',
                                    'url' => ['/backend/territory', $territory->id],
                                    'style' => 'display:none'
                                ]) !!}
                                <button type="submit" class="btn btn-default"><i class="fa fa-archive"></i></button>
                                {!! Form::close() !!}
                                @endauth
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body box-profile">
                            <img class="profile-user-img img-responsive img-circle" id="showimages"
                                 src="{{ isset($territory->manager->image) ? url('assets/img/'.$territory->manager->image) : url('assets/img/default-image.png')}}"
                                 alt="BDM profile picture">

                            <h3 class="profile-username text-center">
                                @if(isset($territory->manager->name))
                                    {{$territory->manager->name}}
                                @else
                                    <a
                                            @can('manage_state')
                                            href="{{ url('/backend/territory/' . $territory->id . '/edit') }}
                                            @endcan
                                                    ">Assign BDM</a>
                                @endif
                            </h3>

                            <p class="text-muted text-center">{{ empty($territory->manager->email) ? 'No Email Address' : $territory->manager->email }}
                                <br>{{ empty($territory->manager->phoneNumber) ? 'No Phone Number' : $territory->manager->phoneNumber  }}
                            </p>

                            <ul class="list-group list-group-unbordered">
                                <li class="list-group-item">
                                    <b>State</b> <span
                                            class="pull-right">{{ $territory->states->count() }}</span>
                                    <ol>
                                        @foreach($territory->states as $state)
                                            <li>{{ $state->name }}</li>
                                        @endforeach
                                    </ol>
                                </li>
                                <li class="list-group-item">
                                    <b>Consortiums</b> <span
                                            class="pull-right">{{$territory->consortiums->count() }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Agency</b> <span
                                            class="pull-right">{{ $territory->agencies->count() }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Agents</b> <span
                                            class="pull-right">{{ $territory->agents->count()  }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>BDM % Share of Total Agents</b> <span
                                            class="pull-right">{{ $percentage_agent }} %</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Sale Target for each Agent</b> <span
                                            class="pull-right">{{ $territory->sale_target }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Total Sales (Incl. Pending)</b> <span
                                            class="pull-right">{{ $territory->sales->count()  }}</span>
                                </li>
                                <li class="list-group-item">
                                    <b>Territory's Sale % Share of Total Sales (Incl. Pending)</b> <span
                                            class="pull-right">{{ $percentage_sales }} %</span>
                                </li>
                            </ul>
                            @can('manage_state')
                                <a href="{{ url('/backend/territory/' . $territory->id . '/edit') }}"
                                   class="btn btn-primary btn-block"><b>Edit</b></a>
                            @endcan
                            <a href="{{ route('territory.index') }}"
                               class="btn btn-warning btn-block"><b>Cancel</b></a>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!--/.col (left) -->
                </div>
                <div class="col-md-8">
                    <!-- general form elements -->
                    <div class="box box-primary">
                        <div class="box-header">
                            <!-- <h3 class="box-title"></h3> -->
                            <div class="box-tools">
                                <div id="reportrange" class="breadcrumb"
                                     style="cursor: pointer; background-color: #fff; border: 1px solid #ccc;">
                                    <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                                    <span></span> <b class="caret"></b>
                                </div>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row pad">
                                <div class="col-md-2" id="canvas-holder" style="width:30%">
                                    <canvas id="pieChart"/>
                                </div>
                                <div class="col-md-2" id="canvas-holder" style="width:30%">
                                    <canvas id="agencyChart"/>
                                </div>
                                <div class="col-md-2" id="canvas-holder" style="width:30%">
                                    <canvas id="agentChart"/>
                                </div>
                            </div>
                            <p class="pad text-center">
                                <strong><span id="sales-duration"></span></strong>
                            </p>

                            <div class="chart">
                                <!-- Sales Chart Canvas -->
                                <canvas id="salesChart" style="height: 180px;"></canvas>
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!--/.col (left) -->
                </div>
                <div class="col-md-12">
                    <!-- Custom Tabs -->
                    <div class="nav-tabs-custom">
                        <ul class="nav nav-tabs">
                            <li class=""><a href="#tab_1" data-toggle="tab" aria-expanded="false">Sale</a>
                            </li>
                            <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Consortium</a>
                            </li>
                            <li class=""><a href="#tab_3" data-toggle="tab" aria-expanded="false">Agency</a>
                            </li>
                            <li class=""><a href="#tab_5" data-toggle="tab" aria-expanded="false">Store</a>
                            </li>
                            <li class=""><a href="#tab_4" data-toggle="tab" aria-expanded="false">Agent</a>
                            </li>
                            <li class="pull-right"><a href="#" class="text-muted"><i class="fa fa-gear"></i></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane" id="tab_1">
                                <table class="table table-hover table-responsive padding" id="sales-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Entry Date</th>
                                        <th>Sale Status</th>
                                        <th>Issued Date</th>
                                        <th>PNR</th>
                                        <th>CTY</th>
                                        <th>Stock</th>
                                        <th>Ticket Number</th>
                                        <th>Departure Date</th>
                                        <th>Class</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_2">
                                <table class="table table-hover table-responsive padding" id="consortium-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Agencies</th>
                                        <th>Agents</th>
                                        <th>Total Sales</th>
                                        <th>Sales % Share</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane" id="tab_3">
                                <table class="table table-hover table-responsive padding" id="agency-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Type</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane active" id="tab_4">
                                <table class="table table-hover table-responsive padding" id="agent-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Sale Confirmed</th>
                                        <th>Sale Pending</th>
                                        <th>Sale Cancelled</th>
                                        <th>Created At</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                            <div class="tab-pane active" id="tab_5">
                                <table class="table table-hover table-responsive padding" id="store-table">
                                    <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>Consortium</th>
                                        <th>Agency</th>
                                        <th>Store</th>
                                        <th>PCC</th>
                                        <th>Address1</th>
                                        <th>Address2</th>
                                        <th>Address3</th>
                                        <th>Suburb</th>
                                        <th>Postcode</th>
                                        <th>State</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.tab-pane -->
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- nav-tabs-custom -->
                </div>
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script src="{{ url('libs/moment.min.js')}}"></script>
    <script src="{{ url('libs/daterangepicker.js')}}"></script>
    <script src="{{ url('libs/chartjs/Chart.min.js')}}"></script>
    <script src="{{ url('libs/jquery.sparkline.min.js')}}"></script>
    <script type="text/javascript">

        //-----------------------
        //- MONTHLY SALES CHART -
        //-----------------------
        var salesChartData = {
            type: 'line',
            data: {
                labels: [],
                datasets: [
                    {
                        label: "Valid Sales",
                        borderColor: "rgb(210, 214, 222)",
                        backgroundColor: "rgb(210, 214, 222)",
                        fill: "start",
                        data: []
                    },
                    {
                        label: "Pending Sales",
                        backgroundColor: "rgba(60,141,188,0.9)",
                        borderColor: "rgba(60,141,188,0.8)",
                        fill: "start",
                        data: []
                    }
                ]
            },
            options: {
                //Boolean - If we should show the scale at all
                showScale: true,
                //Boolean - Whether grid lines are shown across the chart
                scaleShowGridLines: false,
                //String - Colour of the grid lines
                scaleGridLineColor: "rgba(0,0,0,.05)",
                //Number - Width of the grid lines
                scaleGridLineWidth: 1,
                //Boolean - Whether to show horizontal lines (except X axis)
                scaleShowHorizontalLines: true,
                //Boolean - Whether to show vertical lines (except Y axis)
                scaleShowVerticalLines: true,
                //Boolean - Whether the line is curved between points
                bezierCurve: true,
                //Number - Tension of the bezier curve between points
                bezierCurveTension: 0.3,
                //Boolean - Whether to show a dot for each point
                pointDot: false,
                //Number - Radius of each point dot in pixels
                pointDotRadius: 4,
                //Number - Pixel width of point dot stroke
                pointDotStrokeWidth: 1,
                //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                pointHitDetectionRadius: 20,
                //Boolean - Whether to show a stroke for datasets
                datasetStroke: true,
                //Number - Pixel width of dataset stroke
                datasetStrokeWidth: 2,
                //Boolean - Whether to fill the dataset with a color
                datasetFill: true,
                //String - A legend template
                //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                maintainAspectRatio: true,
                //Boolean - whether to make the chart responsive to window resizing
                responsive: true
            }
        };
        // Get context with jQuery - using jQuery's .get() method.
        var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
        // This will get the first returned node in the jQuery collection.
        var myLine = new Chart(salesChartCanvas, salesChartData);

        //---------------------------
        //- END MONTHLY SALES CHART -
        //---------------------------

        //-------------
        //- Total Sales PIE CHART -
        //-------------
        window.chartColors = {
            red: 'rgb(255, 99, 132)',
            orange: 'rgb(255, 159, 64)',
            yellow: 'rgb(255, 205, 86)',
            green: 'rgb(75, 192, 192)',
            blue: 'rgb(54, 162, 235)',
            purple: 'rgb(153, 102, 255)',
            grey: 'rgb(201, 203, 207)'
        };

        var pieOptions = {
            type: 'doughnut',
            data: {
                datasets: [{
                    data: [
                        {!! $pending !!},
                        {!! $cancelled !!},
                        {!! $problem !!},
                        {!! $valid !!}
                    ],
                    backgroundColor: [
                        window.chartColors.red,
                        window.chartColors.orange,
                        window.chartColors.yellow,
                        window.chartColors.green
                    ],
                    label: 'Dataset 1'
                }],
                labels: [
                    "Pending",
                    "Cancelled",
                    "Problem",
                    "Valid"
                ]
            },
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Total Sales Chart'
                },
                animation: {
                    animateScale: true,
                    animateRotate: true
                }
            }
        };
        var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
        var pieChart = new Chart(pieChartCanvas, pieOptions);

        //-----------------------
        //- DATE RANGE PICKER -
        //-----------------------

        var start = moment().subtract(29, 'days');
        var end = moment();

        function cb(start, end) {
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            axios.post('{{ route('territoryDateRange') }}', {
                startDate: start.format('YYYY-MM-DD'),
                endDate: end.format('YYYY-MM-DD'),
                id: '{{ $territory->id }}'
            }).then(function (response) {
                myLine.data.labels = [];
                myLine.data.datasets[0].data = [];
                myLine.data.datasets[1].data = [];
                response.data.labels.forEach(function (label) {
                    myLine.data.labels.push(label);
                });
                response.data.valid.forEach(function (responseData) {
                    myLine.data.datasets[0].data.push(responseData.sales);
                });
                response.data.pending.forEach(function (responseData) {
                    myLine.data.datasets[1].data.push(responseData.sales);
                });
                myLine.update();
                $('#sales-duration').html('Sales Duration: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            })
                .catch(function (error) {
                    console.log(error);
                });
        }

        $('#reportrange').daterangepicker({
            autoApply: true,
            startDate: start,
            endDate: end,
            alwaysShowCalendars: true,
            opens: "left",
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        }, cb);

        cb(start, end);
        //-----------------------
        //- End of DATE RANGE PICKER -
        //-----------------------

        $('#sales-table').DataTable({
            pageLength: 10,
            lengthChange: false,
            serverSide: true,
            processing: false,
            defaultContent: '',
            ajax: '{!! route('territorySalesData',  $territory->id) !!}',
            columns: [
                {data: 'id', visible: false},
                {data: 'created_at', defaultContent: ''},
                {data: 'status', defaultContent: ''},
                {data: 'issued_date', defaultContent: ''},
                {data: 'pnr', defaultContent: ''},
                {data: 'cty', defaultContent: ''},
                {data: 'stock', defaultContent: ''},
                {data: 'ticket_number', defaultContent: ''},
                {data: 'departure_date', defaultContent: ''},
                {data: 'class', defaultContent: ''}
            ],
            search: {
                "regex": true
            }
        });

        $('#agency-table').DataTable({
            pageLength: 10,
            lengthChange: false,
            serverSide: true,
            processing: false,
            defaultContent: '',
            ajax: '{!! route('territoryAgencyData', $territory->id) !!}',
            columns: [
                {data: 'id', visible: false},
                {data: 'name', defaultContent: ''},
                {data: 'type', defaultContent: ''}
            ],
            search: {
                "regex": true
            }
        });
        $('#agent-table').DataTable({
            pageLength: 10,
            lengthChange: false,
            serverSide: true,
            processing: false,
            defaultContent: '',
            ajax: '{!! route('territoryAgentData', $territory->id) !!}',
            columns: [
                {data: 'id', name: 'users.id', visible: false},
                {data: 'name', name: 'users.name'},
                {data: 'email', name: 'users.email', defaultContent: ''},
                {data: 'confirmed', name: 'confirmed', defaultContent: ''},
                {data: 'pending', name: 'pending', defaultContent: ''},
                {data: 'cancelled', name: 'cancelled', defaultContent: ''},
                {data: 'created_at', name: 'users.created_at', defaultContent: ''}
            ],
            search: {
                "regex": true
            }
        });

        $('#consortium-table').DataTable({
            pageLength: 10,
            lengthChange: false,
            serverSide: true,
            processing: false,
            defaultContent: '',
            ajax: '{!! route('territoryConsortiumData', $territory->id) !!}',
            columns: [
                {data: 'id', visible: false},
                {data: 'name', defaultContent: ''},
                {data: 'agencies', defaultContent: ''},
                {data: 'agents', defaultContent: ''},
                {data: 'sales', defaultContent: ''},
                {data: 'percentage', defaultContent: ''}
            ],
            search: {
                "regex": true
            }
        });
        $('#store-table').DataTable({
            pageLength: 10,
            lengthChange: false,
            serverSide: true,
            processing: false,
            defaultContent: '',
            ajax: '{!! route('territoryStoreData', $territory->id) !!}',
            columns: [
                {data: 'id', name: 'stores.id', visible: false},
                {data: 'consortium', name: 'consortium.name', defaultContent: ''},
                {data: 'agency', name: 'agency.name', defaultContent: ''},
                {data: 'name', name: 'stores.name'},
                {data: 'pcc', name: 'stores.pcc'},
                {data: 'address1', name: 'stores.address1'},
                {data: 'address2', name: 'stores.address2'},
                {data: 'address3', name: 'stores.address3'},
                {data: 'suburb', name: 'stores.suburb'},
                {data: 'postcode', name: 'stores.postcode'},
                {data: 'state', name: 'state.name', defaultContent: ''}
            ],
            search: {
                "regex": true
            }
        });
        $(document).ready(function () {
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                $('#sales-table').css({
                    width: ''
                });
                $('#agency-table').css({
                    width: ''
                });
                $('#agent-table').css({
                    width: ''
                });
                $('#consortium-table').css({
                    width: ''
                });
                $('#store-table').css({
                    width: ''
                });
            });
            $('.nav-tabs a[href="#tab_1"]').tab('show');
        });

    </script>
@endpush