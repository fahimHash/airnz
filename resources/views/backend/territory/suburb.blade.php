@extends('backend.layouts.app')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Assign Suburbs to Territory
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url('backend/territory')}}">Territory</a></li>
        <li>Assign Suburbs to Territory</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              @if ($errors->any())
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              @endif

              {!! Form::open(['method' => 'POST', 'url' => '/backend/territory/suburbs', 'class' => 'form-horizontal']) !!}

              <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                  {!! Form::label('states', 'Territory: ', ['class' => 'col-md-4 control-label']) !!}
                  <div class="col-md-6">
                      <select class="states form-control" id="state" name="state">
                          @foreach($states as $state)
                          <option value="{{ $state->id }}">{{  $state->name}}</option>
                          @endforeach
                      </select>
                      {!! $errors->first('states', '<p class="help-block">:message</p>') !!}
                  </div>
              </div>
              <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                  {!! Form::label('suburbs', 'Suburbs: ', ['class' => 'col-md-4 control-label']) !!}
                  <div class="col-md-6">
                      <select class="suburbs form-control" id="suburbs" name="suburbs[]" multiple="multiple">
                          @foreach($suburbs as $suburb)
                          <option value="{{ $suburb->id }}">{{ $suburb->name }}</option>
                          @endforeach()
                      </select>
                      {!! $errors->first('suburbs', '<p class="help-block">:message</p>') !!}
                  </div>
              </div>

              <div class="form-group">
                  <div class="col-md-offset-4 col-md-4">
                      {!! Form::submit('Assign', ['class' => 'btn btn-primary']) !!}
                  </div>
              </div>
              {!! Form::close() !!}
            </div>
            <!-- /.box -->

          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
