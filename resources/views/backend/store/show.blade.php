@extends('backend.layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Store
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url('backend/store')}}">Store</a></li>
        <li class="active">View Store</li>
      </ol>
    </section>

  @include ('backend.layouts.message')

  <!-- Main content -->
    <section class="content">

      <div class="row">
        <div class="col-xs-8">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List</h3>

              <div class="box-tools">
                @role('admin')
                {!! Form::open([
                    'method'=>'DELETE',
                    'url' => ['/backend/store', $store->id],
                    'style' => 'display:inline'
                ]) !!}
                <button type="submit" class="btn btn-default"><i class="fa fa-trash"></i></button>
                {!! Form::close() !!}
                @endauth
              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="col-md-9">
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">Consortium: </label>
                  <div class="col-md-6">
                    <p>{{ $store->consortium['name'] }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">Agency: </label>
                  <div class="col-md-6">
                    <p>{{ $store->agency['name'] }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">Agency Type: </label>
                  <div class="col-md-6">
                    <p>{{ $store->agency['type'] }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">Store Name: </label>
                  <div class="col-md-6">
                    <p>{{ $store->name }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">PCC Code: </label>
                  <div class="col-md-6">
                    <p>{{ $store->pcc }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">Address 1: </label>
                  <div class="col-md-6">
                    <p>{{ is_null($store->address1) ? '-' : $store->address1}}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">Address 2: </label>
                  <div class="col-md-6">
                    <p>{{ is_null($store->address2) ? '-' : $store->address2}}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">Address 3: </label>
                  <div class="col-md-6">
                    <p>{{ is_null($store->address3) ? '-' : $store->address3}}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">Suburb: </label>
                  <div class="col-md-6">
                    <p>{{ $store->suburb }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">State: </label>
                  <div class="col-md-6">
                    <p>{{ $store->state['name'] }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">Postcode: </label>
                  <div class="col-md-6">
                    <p>{{ $store->postcode }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">BDM: </label>
                  <div class="col-md-6">
                    <p>{{ $store->bdm['name'] }}</p>
                  </div>
                </div>
                <div class="form-group">
                  <label for="title" class="col-md-4 control-label text-right">BDM Email: </label>
                  <div class="col-md-6">
                    <p>{{ $store->bdm['email'] }}</p>
                  </div>
                </div>
                <div class="box-footer col-md-offset-4 col-md-4">
                  <a class="btn btn-info" href="{{ url('/backend/store/' . $store->id . '/edit') }}">Edit</a>
                  <a href="{{ route('store.index') }}" class="btn btn-warning">Cancel</a>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
