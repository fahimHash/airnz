@push('styles')
@endpush
<div class="col-md-8">
    <div class="form-group{{ $errors->has('agency') ? ' has-error' : ''}}">
        {!! Form::label('agency', 'Consortium > Agency - Type: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            <select class="consortiumList col-md-12" name="agency">
                @foreach($consortiumList as $consortium)
                    <optgroup label="{{ $consortium->name }}">
                        @if(count($consortium->agencies) > 0)
                            @foreach($consortium->agencies as $agency)
                                <option value="{{ $agency->id }}"
                                @if(isset($submitButtonText))
                                    {{ $agency->id == $store->store_agency_id ? 'selected="selected"' : '' }}
                                        @endif
                                >{{ $agency->name }} - {{ $agency->type }}</option>
                            @endforeach
                        @else
                            <option disabled>Create new Agency</option>
                        @endif
                    </optgroup>
                @endforeach
            </select>
        </div>
        {!! $errors->first('agency', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
        {!! Form::label('name', 'Store Name: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('pcc') ? ' has-error' : ''}}">
        {!! Form::label('pcc', 'PCC Code: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('pcc', null, ['class' => 'form-control']) !!}
            {!! $errors->first('pcc', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('address1') ? ' has-error' : ''}}">
        {!! Form::label('address1', 'Address 1: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('address1', null, ['class' => 'form-control']) !!}
            {!! $errors->first('address1', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('address2') ? ' has-error' : ''}}">
        {!! Form::label('address2', 'Address 2: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('address2', null, ['class' => 'form-control']) !!}
            {!! $errors->first('address2', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('address3') ? ' has-error' : ''}}">
        {!! Form::label('address3', 'Address 3: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('address3', null, ['class' => 'form-control']) !!}
            {!! $errors->first('address3', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('suburb') ? ' has-error' : ''}}">
        {!! Form::label('suburb', 'Suburb: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('suburb', null, ['class' => 'form-control']) !!}
            {!! $errors->first('suburb', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('state') ? ' has-error' : ''}}">
        {!! Form::label('state', 'State: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('state', $states, $stateSelected, ['placeholder' => 'Select a State','class' =>'stateList col-md-12']); !!}
        </div>
        {!! $errors->first('state', '<p class="help-block">:message</p>') !!}
    </div>
    <div class="form-group{{ $errors->has('postcode') ? ' has-error' : ''}}">
        {!! Form::label('postcode', 'Postcode: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('postcode', null, ['class' => 'form-control']) !!}
            {!! $errors->first('postcode', '<p class="help-block">:message</p>') !!}
        </div>
    </div>
    <div class="form-group{{ $errors->has('bdm') ? ' has-error' : ''}}">
        {!! Form::label('bdm', 'BDM: ', ['class' => 'col-md-4 control-label']) !!}
        <div class="col-md-6">
            {!! Form::select('bdm', $users, $bdmSelected, ['placeholder' => 'Select BDM', 'class' => 'bdmList col-md-12']); !!}
        </div>
        {!! $errors->first('bdm', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="box-footer">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info']) !!}
        <a href="{{ route('store.index') }}" class="btn btn-warning">Cancel</a>
    </div>
</div>
@push('scripts')
    <script>
        $(function () {
            $('.consortiumList').select2();
            $('.stateList').select2();
            $('.bdmList').select2();
        });
    </script>
@endpush