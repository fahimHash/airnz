@extends('backend.layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Store
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Store</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @include ('backend.layouts.message')
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List</h3>

              <div class="box-tools">
                <a href="{{ url('backend/store/create') }}" class="btn btn-success btn-sm">Add New Store</a>

              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive padding">
              <table class="table table-hover" id="store-table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Consortium</th>
                            <th>Agency</th>
                            <th>Name</th>
                            <th>PCC Code</th>
                            <th>State</th>
                            <th>BDM</th>
                        </tr>
                    </thead>
                </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#store-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: true,
                defaultContent: '',
                ajax: '{!! route('store-data') !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'consortium', name:'consortium.name', defaultContent : ''},
                    {data: 'agency', name:'agency.name', defaultContent : ''},
                    {data: 'name', name:'name'},
                    {data: 'pcc', name:'pcc', defaultContent : ''},
                    {data: 'state', name:'state.name', defaultContent : ''},
                    {data: 'bdm', name:'bdm.name', defaultContent : ''}
                ],
                search: {
                    "regex": true
                }
            });
            $('#store-table tbody').on( 'click', 'tr', function () {
                var data = table.row( this ).data();
                document.location.href = '{{ url('backend/store') }}/'+data.id;
            } );
        });
    </script>
@endpush
