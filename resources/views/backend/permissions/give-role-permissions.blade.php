@extends('backend.layouts.app')
@section('content')
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Give Role Permission
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="{{ url('backend/permissions')}}">Permissions</a></li>
        <li>Give Role Permission</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
              @if ($errors->any())
                  <ul class="alert alert-danger">
                      @foreach ($errors->all() as $error)
                          <li>{{ $error }}</li>
                      @endforeach
                  </ul>
              @endif

              {!! Form::open(['method' => 'POST', 'url' => '/backend/give-role-permissions', 'class' => 'form-horizontal']) !!}

              <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                  {!! Form::label('name', 'Role: ', ['class' => 'col-md-4 control-label']) !!}
                  <div class="col-md-6">
                      <select class="roles form-control" id="role" name="role">
                          @foreach($roles as $role)
                          <option value="{{ $role->name }}">{{ $role->label }}</option>
                          @endforeach()
                      </select>
                      {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                  </div>
              </div>
              <div class="form-group{{ $errors->has('label') ? ' has-error' : ''}}">
                  {!! Form::label('label', 'Permissions: ', ['class' => 'col-md-4 control-label']) !!}
                  <div class="col-md-6">
                      {{-- <select class="permissions form-control" id="permissions" name="permissions[]" multiple="multiple"> --}}
                          @foreach($permissions as $permission)
                          {{-- <option value="{{ $permission->name }}">{{ $permission->label }}</option> --}}
                          {{ Form::checkbox('permissions[]', $permission->name, null,['id' => 'permissions']) }}
                          {{ Form::label('permissions',$permission->label) }}<br>
                          @endforeach()
                      {{-- </select> --}}
                      {!! $errors->first('label', '<p class="help-block">:message</p>') !!}
                  </div>
              </div>

              <div class="form-group">
                  <div class="col-md-offset-4 col-md-4">
                      {!! Form::submit('Grant', ['class' => 'btn btn-primary']) !!}
                  </div>
              </div>
              {!! Form::close() !!}
            </div>
            <!-- /.box -->

          </div>
          <!--/.col (left) -->
        </div>
        <!-- /.row -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
