<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Answer</h4>
            </div>
            <div class="modal-body">
                {!! Form::open(['url' =>  route('questions.answers.store', $question->id), 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data']) !!}

                <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
                    <div class="col-md-12">
                        {!! Form::textarea('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group{{ $errors->has('type') ? ' has-error' : ''}}">
                    <div class="col-md-12">
                        <select required name="type" id="type" class="col-md-12">
                            <option value="Single Answer"
                                     selected="selected">
                                Single Answer
                            </option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                {!! Form::submit('Save changes', ['class' => 'btn btn-primary']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>