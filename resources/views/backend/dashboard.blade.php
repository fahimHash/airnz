@extends('backend.layouts.app')
@push('styles')
    <link rel="stylesheet" type="text/css" href="{{ url('libs/daterangepicker.css') }}"/>
@endpush
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Dashboard
                <small>Control panel</small>
            </h1>
            <div id="reportrange" class="breadcrumb"
                       style="cursor: pointer; background-color: #fff; border: 1px solid #ccc;">
                <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>&nbsp;
                <span></span> <b class="caret"></b>
            </div>
        </section>

        <!-- Main content -->
        <section class="content">
        @include ('backend.layouts.message')
        <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-{{ $percentageNewAgent['bgColor'] }}">
                        <div class="inner">
                            <h3>{{ $newAgents }}</h3><span class="description-percentage text-white"><i
                                        class="fa fa-caret-{{ $percentageNewAgent['sign'] }}"></i> {{ $percentageNewAgent['value'] }}
                                %</span>

                            <p>New Agents Registration</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="{{ url('backend/agents') }}" class="small-box-footer">More info <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-{{ $percentageTrashAgent['bgColor'] }}">
                        <div class="inner">
                            <h3>{{ $archivedAgent }}</h3><span class="description-percentage text-white"><i
                                        class="fa fa-caret-{{ $percentageTrashAgent['sign'] }}"></i> {{ $percentageTrashAgent['value'] }}
                                %</span>

                            <p>Archived Agents</p>
                        </div>
                        <div class="icon">
                            <i class="ion-archive"></i>
                        </div>
                        <a href="{{ url('backend/agents/archive') }}" class="small-box-footer">More info <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-{{ $percentageTotalAgent['bgColor'] }}">
                        <div class="inner">
                            <h3>{{ $agents }}</h3><span class="description-percentage text-white"><i
                                        class="fa fa-caret-{{ $percentageTotalAgent['sign'] }}"></i> {{ $percentageTotalAgent['value'] }}
                                %</span>

                            <p>Total Agents</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person"></i>
                        </div>
                        <a href="{{ url('backend/agents') }}" class="small-box-footer">More info <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-2 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-{{ $percentageSale['bgColor'] }}">
                        <div class="inner">
                            <h3>{{ $sales }}</h3><span class="description-percentage text-white"><i
                                        class="fa fa-caret-{{ $percentageSale['sign'] }}"></i> {{ $percentageSale['value'] }}
                                %</span>

                            <p>Sales</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="{{ url('backend/sales') }}" class="small-box-footer">More info <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-3 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-yellow">
                        <div class="inner">
                            <h3>0%</h3><span class="description-percentage text-white"><i class="fa fa-caret-left"></i> 0%</span>

                            <p>Training Module</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="{{ url('backend/') }}" class="small-box-footer">More info <i
                                    class="fa fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-md-8">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sales Report</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                                {{--<div class="btn-group">--}}
                                    {{--<button type="button" class="btn btn-box-tool dropdown-toggle"--}}
                                            {{--data-toggle="dropdown">--}}
                                        {{--<i class="fa fa-wrench"></i></button>--}}
                                    {{--<ul class="dropdown-menu" role="menu">--}}
                                        {{--<li><a href="#">Action</a></li>--}}
                                        {{--<li><a href="#">Another action</a></li>--}}
                                        {{--<li><a href="#">Something else here</a></li>--}}
                                        {{--<li class="divider"></li>--}}
                                        {{--<li><a href="#">Separated link</a></li>--}}
                                    {{--</ul>--}}
                                {{--</div>--}}
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-center">
                                        <strong><span id="sales-duration"></span></strong>
                                    </p>

                                    <div class="chart">
                                        <!-- Sales Chart Canvas -->
                                        <canvas id="salesChart" style="height: 180px;"></canvas>
                                    </div>
                                    <!-- /.chart-responsive -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- ./box-body -->
                    </div>
                    <!-- /.box -->
                </div>
                <!-- /.col -->
                <div class="col-md-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">Sales Target</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-center">
                                    </p>
                                    @foreach($sales_target as $value)
                                        <div class="progress-group">
                                            <span class="progress-text">{{ $value['territory'] }}</span>
                                            <span class="progress-number"><b>{{ $value['sales'] }}</b>/<b>{{ $value['target'] }}</b></span>
                                            <div class="progress sm">
                                                <div class="progress-bar progress-bar-aqua"
                                                     style="width: {{ $value['percentage'] }}%"></div>
                                            </div>
                                        </div>
                                    @endforeach()
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- ./box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </div>
            <!-- /.row -->
            <!-- Main row -->
            <div class="row">
                <div class="col-md-8 connectedSortable">

                    <!-- MAP & BOX PANE -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Flights Destination</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="row">
                                <div class="col-md-9 col-sm-8">
                                    <div class="pad">
                                        <!-- Map will be created here -->
                                        <div id="world-map-markers" style="height: 325px;"></div>
                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-3 col-sm-4">
                                    <div id="flight-details" class="pad box-pane-right bg-green"
                                         style="min-height: 350px; text-align: center">
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
                <div class="col-md-4">
                    <!-- Info Boxes Style 2 -->
                    <div class="info-box bg-yellow">
                        <span class="info-box-icon"><i class="fa fa-dollar"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Sales</span>
                            <span class="info-box-number">5,200</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 50%"></div>
                            </div>
                            <span class="progress-description">
                    50% Increase in 30 Days
                  </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                    <div class="info-box bg-green">
                        <span class="info-box-icon"><i class="fa fa-users"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Agents</span>
                            <span class="info-box-number">92,050</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 20%"></div>
                            </div>
                            <span class="progress-description">
                    20% Increase in 30 Days
                  </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                    <div class="info-box bg-red">
                        <span class="info-box-icon"><i class="fa fa-list"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Campaigns</span>
                            <span class="info-box-number">114,381</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 70%"></div>
                            </div>
                            <span class="progress-description">
                    70% Increase in 30 Days
                  </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                    <div class="info-box bg-aqua">
                        <span class="info-box-icon"><i class="ion-ios-chatbubble-outline"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text">Rewards</span>
                            <span class="info-box-number">163,921</span>

                            <div class="progress">
                                <div class="progress-bar" style="width: 40%"></div>
                            </div>
                            <span class="progress-description">
                    40% Increase in 30 Days
                  </span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
                <!-- right col -->
                <section class="col-lg-6 connectedSortable">

                    <!-- MAP & BOX PANE -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">Consortium Performance</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="row">
                                <div class="col-md-9 col-sm-8">
                                    <div class="pad">
                                        <div id="canvas-holder" style="width:40%; display:none">
                                            <canvas id="pieChart"/>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                </section>
                <section class="col-lg-6 connectedSortable">

                    <!-- MAP & BOX PANE -->
                    <div class="box box-success">
                        <div class="box-header with-border">
                            <h3 class="box-title">BDM Performance</h3>

                            <div class="box-tools pull-right">
                                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i
                                            class="fa fa-minus"></i>
                                </button>

                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body no-padding">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="pad">

                                    </div>
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6 col-sm-12">
                                    <div class="pad">

                                    </div>
                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.box-body -->
                    </div>
                </section>
            </div>
            <!-- /.row (main row) -->

        </section>
        <!-- /.content -->
    </div>
@endsection

@push('scripts')
    <script src="{{ url('libs/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
    <script src="{{ url('libs/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{ url('libs/moment.min.js')}}"></script>
    <script src="{{ url('libs/daterangepicker.js')}}"></script>
    <script src="{{ url('libs/chartjs/Chart.min.js')}}"></script>
    <script src="{{ url('libs/jquery.sparkline.min.js')}}"></script>
    <script type="text/javascript">
        
            //-----------------------
            //- MONTHLY SALES CHART -
            //-----------------------
            var salesChartData = {
                type: 'line',
                data: {
                    labels: [],
                    datasets: [
                        {
                            label: "Valid Sales",
                            borderColor: "rgb(210, 214, 222)",
                            backgroundColor: "rgb(210, 214, 222)",
                            fill: "start",
                            data: []
                        },
                        {
                            label: "Pending Sales",
                            backgroundColor: "rgba(60,141,188,0.9)",
                            borderColor: "rgba(60,141,188,0.8)",
                            fill: "start",
                            data: []
                        }
                    ]
                },
                options: {
                    //Boolean - If we should show the scale at all
                    showScale: true,
                    //Boolean - Whether grid lines are shown across the chart
                    scaleShowGridLines: false,
                    //String - Colour of the grid lines
                    scaleGridLineColor: "rgba(0,0,0,.05)",
                    //Number - Width of the grid lines
                    scaleGridLineWidth: 1,
                    //Boolean - Whether to show horizontal lines (except X axis)
                    scaleShowHorizontalLines: true,
                    //Boolean - Whether to show vertical lines (except Y axis)
                    scaleShowVerticalLines: true,
                    //Boolean - Whether the line is curved between points
                    bezierCurve: true,
                    //Number - Tension of the bezier curve between points
                    bezierCurveTension: 0.3,
                    //Boolean - Whether to show a dot for each point
                    pointDot: false,
                    //Number - Radius of each point dot in pixels
                    pointDotRadius: 4,
                    //Number - Pixel width of point dot stroke
                    pointDotStrokeWidth: 1,
                    //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
                    pointHitDetectionRadius: 20,
                    //Boolean - Whether to show a stroke for datasets
                    datasetStroke: true,
                    //Number - Pixel width of dataset stroke
                    datasetStrokeWidth: 2,
                    //Boolean - Whether to fill the dataset with a color
                    datasetFill: true,
                    //String - A legend template
                    {{--legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%=datasets[i].label%></li><%}%></ul>",--}}
                    //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                    maintainAspectRatio: true,
                    //Boolean - whether to make the chart responsive to window resizing
                    responsive: true
                }
            };
            // Get context with jQuery - using jQuery's .get() method.
            var salesChartCanvas = $("#salesChart").get(0).getContext("2d");
            // This will get the first returned node in the jQuery collection.
            var myLine = new Chart(salesChartCanvas, salesChartData);


            //-----------------------
            //- DATE RANGE PICKER -
            //-----------------------

            var start = moment().subtract(29, 'days');
            var end = moment();

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                axios.post('backend/date-range', {
                    startDate: start.format('YYYY-MM-DD'),
                    endDate: end.format('YYYY-MM-DD')
                }).then(function (response) {
                    myLine.data.labels = [];
                    myLine.data.datasets[0].data = [];
                    myLine.data.datasets[1].data = [];
                    response.data.labels.forEach(function (label) {
                        myLine.data.labels.push(label);
                    });
                    response.data.valid.forEach(function (responseData) {
                        myLine.data.datasets[0].data.push(responseData.sales);
                    });
                    response.data.pending.forEach(function (responseData) {
                        myLine.data.datasets[1].data.push(responseData.sales);
                    });
                    myLine.update();
                    $('#sales-duration').html('Sales Duration: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
                })
                    .catch(function (error) {
                        console.log(error);
                    });
            }

            $('#reportrange').daterangepicker({
                autoApply: true,
                startDate: start,
                endDate: end,
                alwaysShowCalendars: true,
                opens: "left",
                ranges: {
                    'Today': [moment(), moment()],
                    'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                    'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                    'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                    'This Month': [moment().startOf('month'), moment().endOf('month')],
                    'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                }
            }, cb);

            cb(start, end);

            //---------------------------
            //- END MONTHLY SALES CHART -
            //---------------------------

            /* jVector Maps
             * ------------
             * Create a world map with markers
             */
            var markers = [
                    @foreach($destinations as $destination)
                {
                    latLng: [ {{ $destination->latitude }}, {{ $destination->longitude }}],
                    name: '{{ $destination->name }}',
                    code: '{{ $destination->code }}'
                },
                @endforeach
            ];

            $('#world-map-markers').vectorMap({
                map: 'world_mill_en',
                normalizeFunction: 'polynomial',
                hoverOpacity: 0.7,
                hoverColor: true,
                backgroundColor: 'transparent',
                markersSelectable: true,
                regionStyle: {
                    initial: {
                        fill: 'rgba(210, 214, 222, 1)',
                        "fill-opacity": 1,
                        stroke: 'none',
                        "stroke-width": 0,
                        "stroke-opacity": 1
                    },
                    hover: {
                        "fill-opacity": 0.7,
                        cursor: 'pointer'
                    },
                    selected: {
                        fill: 'yellow'
                    },
                    selectedHover: {}
                },
                markerStyle: {
                    initial: {
                        fill: '#00a65a',
                        stroke: '#111'
                    }
                },
                markers: markers,
                onMarkerClick: function (event, index) {
                    axios.post('backend/flights', {
                        startDate: start.format('YYYY-MM-DD'),
                        endDate: end.format('YYYY-MM-DD'),
                        code: markers[index].code
                    }).then(function (response) {
                        $('#flight-details').html('');
                        $('#flight-details').append('<div class="description-block margin-bottom">Sales Duration: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY') + '</div>');
                        if (response.data.length == 0) {
                            $('#flight-details').append('<div class="description-block margin-bottom"><div class="sparkbar pad" data-color="#fff"></div><h5 class="description-header">' + markers[index].name + '</h5><span>No Flight Information</span></div>');
                        }
                        response.data.forEach(function (data) {
                            $('#flight-details').append('<div class="sparkbar pad"></div><h5 class="description-header">' + markers[index].name + '</h5><span>Route : ' + data.cty + '</span><br><span>Sales : ' + data.sales + '</span></div>');
                            $(".sparkbar").sparkline(randomNumber(), {
                                type: 'bar',
                                barColor: '#fff'
                            });
                        });
                    })
                    .catch(function (error) {
                        console.log(error);
                    });
                }
            });
            $('#flight-details').append('<div class="description-block margin-bottom">Sales Duration: ' + start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY') + '</div>');
            $('#flight-details').append('<div class="description-block margin-bottom"><div class="sparkbar pad" data-color="#fff"></div><h5 class="description-header"></h5><span>Click on marker for more information</span></div>');

            function randomNumber(){
                let numbers = [];
                for(let i=0; i < 7; i++){
                    numbers.push(Math.floor(Math.random() * 100) + 1);
                }
                return numbers;
            }
            //-------------
            //- PIE CHART -
            //-------------
            window.chartColors = {
                red: 'rgb(255, 99, 132)',
                orange: 'rgb(255, 159, 64)',
                yellow: 'rgb(255, 205, 86)',
                green: 'rgb(75, 192, 192)',
                blue: 'rgb(54, 162, 235)',
                purple: 'rgb(153, 102, 255)',
                grey: 'rgb(201, 203, 207)'
            };

            var pieOptions = {
                type: 'doughnut',
                data: {
                    datasets: [{
                        data: [
                            500,
                            100,
                            450,
                            250,
                            340,
                        ],
                        backgroundColor: [
                            window.chartColors.red,
                            window.chartColors.orange,
                            window.chartColors.yellow,
                            window.chartColors.green,
                            window.chartColors.blue,
                        ],
                        label: 'Dataset 1'
                    }],
                    labels: [
                        "Red",
                        "Orange",
                        "Yellow",
                        "Green",
                        "Blue"
                    ]
                },
                options: {
                    responsive: true,
                    legend: {
                        position: 'top',
                    },
                    title: {
                        display: true,
                        text: 'Chart.js Doughnut Chart'
                    },
                    animation: {
                        animateScale: true,
                        animateRotate: true
                    }
                }
            };
            var pieChartCanvas = $('#pieChart').get(0).getContext('2d');
            var pieChart = new Chart(pieChartCanvas, pieOptions);

    </script>
@endpush
