@extends('backend.layouts.app')

@section('content')
  <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Agency
          <small></small>
        </h1>
        <ol class="breadcrumb">
          <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
          <li><a href="{{ url('backend/agency')}}">Agency</a></li>
          <li class="active">View Agency</li>
        </ol>
      </section>

      @include ('backend.layouts.message')

      <!-- Main content -->
      <section class="content">

        <div class="row">
          <div class="col-xs-8">
            <div class="box">
              <div class="box-header">
                <h3 class="box-title">List</h3>

                <div class="box-tools">
                  @role('admin')
                  {!! Form::open([
                      'method'=>'DELETE',
                      'url' => ['/backend/agency', $agency->id],
                      'style' => 'display:inline'
                  ]) !!}
                  <button type="submit" class="btn btn-default"><i class="fa fa-trash"></i></button>
                  {!! Form::close() !!}
                  @endauth
                </div>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Name: </label>
                      <div class="col-md-6">
                        <p>{{ $agency->name }}</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Type: </label>
                      <div class="col-md-6">
                        <p>{{ is_null($agency->type) ? 'No Type Available' : $agency->type}}</p>
                      </div>
                    </div>
                    <div class="form-group">
                      <label for="title" class="col-md-4 control-label text-right">Consortium: </label>
                      <div class="col-md-6">
                        <p>{{ is_null($agency->consortium['name']) ? 'No Consortium' : $agency->consortium['name']}}</p>
                      </div>
                    </div>
                    <div class="box-footer col-md-offset-4 col-md-4">
                        <a class="btn btn-info" href="{{ url('/backend/agency/' . $agency->id . '/edit') }}">Edit</a>
                      <a href="{{ route('agency.index') }}" class="btn btn-warning">Cancel</a>
                    </div>
                  </div>
                </div>
              <!-- /.box-body -->
            </div>
            <!-- /.box -->
          </div>
        </div>
      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
  @endsection
