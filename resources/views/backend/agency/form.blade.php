<div class="col-md-8">
  <div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
      {!! Form::label('name', 'Name: ', ['class' => 'col-md-4 control-label']) !!}
      <div class="col-md-6">
          {!! Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) !!}
          {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
      </div>
  </div>
  <div class="form-group{{ $errors->has('type') ? ' has-error' : ''}}">
    <label class="col-md-4 control-label" for="state">Type</label>
    <div class="col-md-6">
      <select required name="type" id="type" class="col-md-12">
        <option value="Wholesale" @if(isset($submitButtonText) && $agency->type == 'Wholesale') selected="selected" @endif>Wholesale</option>
        <option value="Retail" @if(isset($submitButtonText) && $agency->type == 'Retail') selected="selected" @endif>Retail</option>
        <option value="TMC" @if(isset($submitButtonText) && $agency->type == 'TMC') selected="selected" @endif>TMC</option>
        <option value="Corporate" @if(isset($submitButtonText) && $agency->type == 'Corporate') selected="selected" @endif>Corporate</option>
        <option value="Independent/Other" @if(isset($submitButtonText) && $agency->type == 'Independent/Other') selected="selected" @endif>Independent/Other</option>
      </select>
    </div>
  </div>
  <div class="form-group {{ $errors->has('consortium') ? ' has-error' : ''}}">
    <label class="col-md-4 control-label" for="consortium">Consortium</label>
    <div class="col-md-6">
      <select required name="consortium" id="consortium" class="col-md-12">
        @foreach ($consortiums as $item)
            <option value="{{$item->id}}"
              @if(isset($submitButtonText))
              {{ $item->id == $agency->fk_id_consortiums ? 'selected="selected"' : '' }}
              @endif
              >{{$item->name}}</option>
        @endforeach
      </select>
    </div>
  </div>
</div>
<div class="box-footer">
    <input type="hidden" name="active" value="1">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-info']) !!}
        <a href="{{ route('agency.index') }}" class="btn btn-warning">Cancel</a>
    </div>
</div>
@push('scripts')
    <script>
        $(function () {
            $('#type').select2();
            $('#consortium').select2();
        });
    </script>
@endpush