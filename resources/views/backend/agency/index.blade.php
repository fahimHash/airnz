@extends('backend.layouts.app')

@section('content')
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Agency
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="{{ url('backend')}}"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active">Agency</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      @include ('backend.layouts.message')
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">List</h3>

              <div class="box-tools">
                <a href="{{ url('backend/agency/create') }}" class="btn btn-success btn-sm">Add New Agency</a>

              </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body table-responsive padding">
              <table class="table table-hover" id="agency-table">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Type</th>
                            <th>Consortium</th>
                        </tr>
                    </thead>
                </table>
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
@endsection
@push('scripts')
    <script>
        $(document).ready(function () {
            var table = $('#agency-table').DataTable({
                pageLength: 10,
                lengthChange: false,
                serverSide: true,
                processing: true,
                defaultContent: '',
                ajax: '{!! route('agency-data') !!}',
                columns: [
                    {data: 'id', visible: false},
                    {data: 'name', name:'name'},
                    {data: 'type', name:'type', defaultContent : ''},
                    {data: 'consortium', name:'consortium.name', defaultContent : ''}
                ],
                search: {
                    "regex": true
                }
            });
            $('#agency-table tbody').on( 'click', 'tr', function () {
                var data = table.row( this ).data();
                document.location.href = '{{ url('backend/agency') }}/'+data.id;
            } );
        });
    </script>
@endpush
