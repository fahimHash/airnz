<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Enter a password. It must be at least 8 characters, and contain at least one number',
    'reset' => 'Your password has been reset!',
    'sent' => 'You will soon receive an email with a link to reset your password. ',
    'token' => 'This password reset token is invalid.',
    'user' => "This is email address is not registered to duo.",

];
