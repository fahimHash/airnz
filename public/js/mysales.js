/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 120);
/******/ })
/************************************************************************/
/******/ ({

/***/ 12:
/***/ (function(module, exports) {

/**
 * Translates the list format produced by css-loader into something
 * easier to manipulate.
 */
module.exports = function listToStyles (parentId, list) {
  var styles = []
  var newStyles = {}
  for (var i = 0; i < list.length; i++) {
    var item = list[i]
    var id = item[0]
    var css = item[1]
    var media = item[2]
    var sourceMap = item[3]
    var part = {
      id: parentId + ':' + i,
      css: css,
      media: media,
      sourceMap: sourceMap
    }
    if (!newStyles[id]) {
      styles.push(newStyles[id] = { id: id, parts: [part] })
    } else {
      newStyles[id].parts.push(part)
    }
  }
  return styles
}


/***/ }),

/***/ 120:
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(121);


/***/ }),

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_MySales_Sales_vue__ = __webpack_require__(122);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_MySales_Sales_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_MySales_Sales_vue__);

/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');



var mysales = new Vue({
  el: '#mySales',
  components: { All: __WEBPACK_IMPORTED_MODULE_0__components_MySales_Sales_vue___default.a },
  data: {}
});

/***/ }),

/***/ 122:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(123)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(125)
/* template */
var __vue_template__ = __webpack_require__(134)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/MySales/Sales.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-3ed785b6", Component.options)
  } else {
    hotAPI.reload("data-v-3ed785b6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 123:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(124);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(5)("2143094b", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3ed785b6\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Sales.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-3ed785b6\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Sales.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 124:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(false);
// imports


// module
exports.push([module.i, "\nth.active .arrow {\n    opacity: 1;\n}\n.arrow {\n    display: inline-block;\n    vertical-align: middle;\n    width: 0;\n    height: 0;\n    margin-left: 5px;\n    opacity: 0.66;\n}\n.arrow.asc {\n    border-left: 4px solid transparent;\n    border-right: 4px solid transparent;\n    border-bottom: 4px solid #000;\n}\n.arrow.dsc {\n    border-left: 4px solid transparent;\n    border-right: 4px solid transparent;\n    border-top: 4px solid #000;\n}\n", ""]);

// exports


/***/ }),

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Sale_vue__ = __webpack_require__(126);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Sale_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__Sale_vue__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Pagination_vue__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Pagination_vue___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__Pagination_vue__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["default"] = ({

    components: {
        Sale: __WEBPACK_IMPORTED_MODULE_0__Sale_vue___default.a,
        Pagination: __WEBPACK_IMPORTED_MODULE_1__Pagination_vue___default.a
    },

    props: {
        allCount: 0,
        validCount: 0,
        pendingCount: 0,
        cancelledCount: 0,
        problemCount: 0,
        monthYear: ''
    },

    data: function data() {
        var _sale;

        var sortOrders = {};
        var columns = ['Status', 'PNR Creation Date', 'PNR', 'Final Destination', 'NZ Arrival Airport', 'Campaign', 'Class', 'Status Notes'];
        columns.forEach(function (key) {
            sortOrders[key] = 1;
        });
        return {
            filterKey: '',
            sales: [],
            errors: [],
            sale: (_sale = {
                status: '',
                pnr_creation: '',
                pnr: '',
                pax: ''
            }, _defineProperty(_sale, 'pax', ''), _defineProperty(_sale, 'campaign', ''), _defineProperty(_sale, 'class', ''), _defineProperty(_sale, 'notes', ''), _sale),
            sortKey: '',
            viewFilter: '',
            sortOrders: sortOrders,
            columns: columns,
            counter: 0,
            pagination: {
                total: 0,
                per_page: 2,
                from: 1,
                to: 0,
                current_page: 1
            },
            offset: 4
        };
    },
    created: function created() {
        this.fetchSales(this.pagination.current_page);
    },


    methods: {
        fetchSales: function fetchSales(page) {
            var _this = this;

            axios.get('/api/sales?page=' + page).then(function (response) {
                _this.sales = response.data.data;
                _this.pagination = response.data;
            });
        },
        deleteSale: function deleteSale(sale) {
            var _this2 = this;

            axios.delete('/api/sales/' + sale.id).then(function (response) {
                var index = _this2.sales.indexOf(sale);
                _this2.sales.splice(index, 1);
            });
        },
        sortBy: function sortBy(key) {
            this.sortKey = key;
            this.sortOrders[key] = this.sortOrders[key] * -1;
        },
        viewAll: function viewAll() {
            this.viewFilter = '';
        },
        viewValid: function viewValid() {
            this.viewFilter = 'Valid';
        },
        viewProblem: function viewProblem() {
            this.viewFilter = 'Problem';
        },
        viewPending: function viewPending() {
            this.viewFilter = 'Pending';
        },
        viewCancelled: function viewCancelled() {
            this.viewFilter = 'Cancelled';
        }
    },
    computed: {
        filteredList: function filteredList() {
            var sortKey = this.sortKey;
            var viewFilter = this.viewFilter;
            var filterKey = this.filterKey && this.filterKey.toLowerCase();
            var order = this.sortOrders[sortKey] || 1;
            var data = this.sales;
            if (filterKey) {
                data = data.filter(function (row) {
                    return Object.keys(row).some(function (key) {
                        return String(row[key]).toLowerCase().indexOf(filterKey) > -1;
                    });
                });
            }
            if (sortKey) {
                data = data.slice().sort(function (a, b) {
                    a = a[sortKey];
                    b = b[sortKey];
                    return (a === b ? 0 : a > b ? 1 : -1) * order;
                });
            }
            if (viewFilter) {
                data = data.filter(function (row) {
                    return Object.keys(row).some(function (key) {
                        return String(row['status']).indexOf(viewFilter) > -1;
                    });
                });
            }
            return data;
        }
    },
    filters: {
        capitalize: function capitalize(str) {
            return (str.charAt(0).toUpperCase() + str.slice(1)).split('_')[0];
        }
    }
});

/***/ }),

/***/ 126:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
function injectStyle (ssrContext) {
  if (disposed) return
  __webpack_require__(127)
}
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(129)
/* template */
var __vue_template__ = __webpack_require__(130)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = injectStyle
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/MySales/Sale.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-e36885e6", Component.options)
  } else {
    hotAPI.reload("data-v-e36885e6", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 127:
/***/ (function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(128);
if(typeof content === 'string') content = [[module.i, content, '']];
if(content.locals) module.exports = content.locals;
// add the styles to the DOM
var update = __webpack_require__(5)("2fc3b6e5", content, false, {});
// Hot Module Replacement
if(false) {
 // When the styles change, update the <style> tags
 if(!content.locals) {
   module.hot.accept("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e36885e6\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Sale.vue", function() {
     var newContent = require("!!../../../../../node_modules/css-loader/index.js!../../../../../node_modules/vue-loader/lib/style-compiler/index.js?{\"vue\":true,\"id\":\"data-v-e36885e6\",\"scoped\":false,\"hasInlineConfig\":true}!../../../../../node_modules/vue-loader/lib/selector.js?type=styles&index=0!./Sale.vue");
     if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
     update(newContent);
   });
 }
 // When the module is disposed, remove the <style> tags
 module.hot.dispose(function() { update(); });
}

/***/ }),

/***/ 128:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(2)(false);
// imports


// module
exports.push([module.i, "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", ""]);

// exports


/***/ }),

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
    props: ['sale'],

    data: function data() {
        var _editForm;

        return {
            edit: false,
            editForm: (_editForm = {
                pnr_creation: '',
                pnr: '',
                pax: ''
            }, _defineProperty(_editForm, 'pax', ''), _defineProperty(_editForm, 'campaign', ''), _defineProperty(_editForm, 'class', ''), _defineProperty(_editForm, 'notes', ''), _editForm)
        };
    },


    methods: {
        editSale: function editSale() {
            this.edit = true;
            this.editForm.pnr_creation = this.sale.pnr_creation;
            this.editForm.pnr = this.sale.pnr;
            this.editForm.pax = this.sale.pax;
            this.editForm.pax = this.sale.pax;
            this.editForm.campaign = this.sale.campaign;
            this.editForm.class = this.sale.class;
            this.editForm.notes = this.sale.notes;
        },
        cancelEdit: function cancelEdit() {
            this.edit = false;
            this.editForm.pnr_creation = '';
            this.editForm.pnr = '';
            this.editForm.pax = '';
            this.editForm.pax = '';
            this.editForm.campaign = '';
            this.editForm.class = '';
            this.editForm.notes = '';
        },
        updateSale: function updateSale(oldSale, newSale) {
            var _this = this;

            if (newSale.pnr == '') {
                alert('The PNR field is required.');
            } else {
                axios.patch('/api/sales/' + oldSale.id, newSale).then(function (response) {
                    _this.$emit('update-sale');
                    _this.cancelEdit();
                    console.log(response.data);
                }, function (response) {
                    console.log(response.data);
                });
            }
        },
        tooltip: function tooltip(status) {
            if (status == 'Valid') {
                return 'Your booking was validated';
            } else if (status == 'Pending') {
                return 'Your booking is pending.  When your booking is ticketed, it will be validated';
            } else if (status == 'Cancelled') {
                return 'Your booking has not been ticketed within the required ticketing time limit.';
            } else if (status == 'Problem') {
                return 'We could not match this PNR in our system, please check the PNR details and resubmit';
            }
        }
    }
});

/***/ }),

/***/ 130:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("tr", [
    _c("td", { staticClass: "status" }, [
      _c(
        "button",
        {
          attrs: {
            "data-toggle": "tooltip",
            "data-placement": "right",
            title: _vm.tooltip(_vm.sale.status)
          }
        },
        [
          _c("img", {
            attrs: {
              src: "assets/img/ico-" + _vm.sale.status + ".png",
              alt: ""
            }
          })
        ]
      )
    ]),
    _vm._v(" "),
    _c("td", [
      _vm.edit
        ? _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.editForm.pnr_creation,
                expression: "editForm.pnr_creation"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "date" },
            domProps: { value: _vm.editForm.pnr_creation },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.editForm, "pnr_creation", $event.target.value)
              }
            }
          })
        : _c("span", [_vm._v(_vm._s(_vm.sale.pnr_creation))])
    ]),
    _vm._v(" "),
    _c("td", [
      _vm.edit
        ? _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.editForm.pnr,
                expression: "editForm.pnr"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text", required: "required" },
            domProps: { value: _vm.editForm.pnr },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.editForm, "pnr", $event.target.value)
              }
            }
          })
        : _c("span", [_vm._v(_vm._s(_vm.sale.pnr))])
    ]),
    _vm._v(" "),
    _c("td", [
      _vm.edit
        ? _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.editForm.pax,
                expression: "editForm.pax"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text" },
            domProps: { value: _vm.editForm.pax },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.editForm, "pax", $event.target.value)
              }
            }
          })
        : _c(
            "span",
            {
              attrs: {
                "data-toggle": "tooltip",
                "data-placement": "right",
                title:
                  "Total count of passengers you booked to the final destination"
              }
            },
            [_vm._v(_vm._s(_vm.sale.pax))]
          )
    ]),
    _vm._v(" "),
    _c("td", [
      _vm.edit
        ? _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.editForm.pax,
                expression: "editForm.pax"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text" },
            domProps: { value: _vm.editForm.pax },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.editForm, "pax", $event.target.value)
              }
            }
          })
        : _c(
            "span",
            {
              attrs: {
                "data-toggle": "tooltip",
                "data-placement": "right",
                title:
                  "Total count of passengers you booked to the Air New Zealand arrival port"
              }
            },
            [_vm._v(_vm._s(_vm.sale.pax))]
          )
    ]),
    _vm._v(" "),
    _c("td", [
      _vm.edit
        ? _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.editForm.campaign,
                expression: "editForm.campaign"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text" },
            domProps: { value: _vm.editForm.campaign },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.editForm, "campaign", $event.target.value)
              }
            }
          })
        : _c(
            "span",
            {
              attrs: {
                "data-toggle": "tooltip",
                "data-placement": "right",
                title:
                  "This booking is eligible for the campaign you are participating in."
              }
            },
            [_vm._v(_vm._s(_vm.sale.campaign))]
          )
    ]),
    _vm._v(" "),
    _c("td", [
      _vm.edit
        ? _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.editForm.class,
                expression: "editForm.class"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "text" },
            domProps: { value: _vm.editForm.class },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.editForm, "class", $event.target.value)
              }
            }
          })
        : _c(
            "span",
            {
              attrs: {
                "data-toggle": "tooltip",
                "data-placement": "right",
                title: "Total count of cabin sectors you booked on this PNR"
              }
            },
            [_vm._v(_vm._s(_vm.sale.class))]
          )
    ]),
    _vm._v(" "),
    _c("td", { staticClass: "notes" }, [
      _vm.edit
        ? _c("input", {
            directives: [
              {
                name: "model",
                rawName: "v-model",
                value: _vm.editForm.notes,
                expression: "editForm.notes"
              }
            ],
            staticClass: "form-control",
            attrs: { type: "textarea" },
            domProps: { value: _vm.editForm.notes },
            on: {
              input: function($event) {
                if ($event.target.composing) {
                  return
                }
                _vm.$set(_vm.editForm, "notes", $event.target.value)
              }
            }
          })
        : _c("span", [_vm._v(_vm._s(_vm.sale.notes))])
    ]),
    _vm._v(" "),
    _c("td", { staticClass: "text-right" }, [
      !_vm.edit
        ? _c(
            "button",
            {
              staticClass: "btn btn-edit",
              attrs: { type: "button" },
              on: { click: _vm.editSale }
            },
            [_c("i", { staticClass: "icon-ANE1227_TEP_icon_set_V1_edit" })]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.edit
        ? _c(
            "button",
            {
              staticClass: "btn btn-save",
              attrs: { type: "button" },
              on: {
                click: function($event) {
                  _vm.updateSale(_vm.sale, _vm.editForm)
                }
              }
            },
            [_vm._v("\n            SAVE\n        ")]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.edit
        ? _c(
            "button",
            {
              staticClass: "btn btn-cancel",
              attrs: { type: "button" },
              on: { click: _vm.cancelEdit }
            },
            [_c("i", { staticClass: "icon-ANE1227_TEP_icon_set_V1_close" })]
          )
        : _vm._e(),
      _vm._v(" "),
      !_vm.edit
        ? _c(
            "button",
            {
              staticClass: "btn btn-delete",
              attrs: { type: "button" },
              on: {
                click: function($event) {
                  _vm.$emit("delete-sale", _vm.sale)
                }
              }
            },
            [_c("i", { staticClass: "icon-ANE1227_TEP_icon_set_V1_trash2" })]
          )
        : _vm._e()
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-e36885e6", module.exports)
  }
}

/***/ }),

/***/ 131:
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__(3)
/* script */
var __vue_script__ = __webpack_require__(132)
/* template */
var __vue_template__ = __webpack_require__(133)
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/assets/js/components/MySales/Pagination.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-99fc4f40", Component.options)
  } else {
    hotAPI.reload("data-v-99fc4f40", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["default"] = ({
  props: {
    pagination: {
      type: Object,
      required: true
    },
    offset: {
      type: Number,
      default: 4
    }
  },
  computed: {
    pagesNumber: function pagesNumber() {
      if (!this.pagination.to) {
        return [];
      }
      var from = this.pagination.current_page - this.offset;
      if (from < 1) {
        from = 1;
      }
      var to = from + this.offset * 2;
      if (to >= this.pagination.last_page) {
        to = this.pagination.last_page;
      }
      var pagesArray = [];
      for (from = 1; from <= to; from++) {
        pagesArray.push(from);
      }
      return pagesArray;
    }
  },
  methods: {
    changePage: function changePage(page) {
      this.pagination.current_page = page;
    }
  }
});

/***/ }),

/***/ 133:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "ul",
    { staticClass: "pagination" },
    [
      _c("li", [
        _c(
          "a",
          {
            attrs: { href: "#", "aria-label": "Previous" },
            on: {
              click: function($event) {
                $event.preventDefault()
                _vm.changePage(_vm.pagination.current_page - 1)
              }
            }
          },
          [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("«")])]
        )
      ]),
      _vm._v(" "),
      _vm._l(_vm.pagesNumber, function(page) {
        return _c(
          "li",
          { class: { active: page == _vm.pagination.current_page } },
          [
            _c(
              "a",
              {
                attrs: { href: "#" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    _vm.changePage(page)
                  }
                }
              },
              [_vm._v(_vm._s(page))]
            )
          ]
        )
      }),
      _vm._v(" "),
      _vm.pagination.current_page < _vm.pagination.last_page
        ? _c("li", [
            _c(
              "a",
              {
                attrs: { href: "#", "aria-label": "Next" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    _vm.changePage(_vm.pagination.current_page + 1)
                  }
                }
              },
              [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("»")])]
            )
          ])
        : _c("li", [
            _c(
              "a",
              {
                attrs: { href: "#", "aria-label": "Next" },
                on: {
                  click: function($event) {
                    $event.preventDefault()
                    _vm.changePage(_vm.pagination.current_page)
                  }
                }
              },
              [_c("span", { attrs: { "aria-hidden": "true" } }, [_vm._v("»")])]
            )
          ])
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-99fc4f40", module.exports)
  }
}

/***/ }),

/***/ 134:
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c("ul", { staticClass: "nav nav-tabs", attrs: { role: "tablist" } }, [
      _c("li", { staticClass: "active", attrs: { role: "presentation" } }, [
        _c(
          "a",
          {
            attrs: {
              href: "#mysales-all",
              "aria-controls": "mysales-all",
              role: "tab",
              "data-toggle": "tab"
            },
            on: {
              click: function($event) {
                _vm.viewAll()
              }
            }
          },
          [
            _vm._v("All\n            "),
            _c("span", [
              _vm._v("(" + _vm._s(_vm.allCount) + ") "),
              _c("span", [_vm._v("|")])
            ])
          ]
        )
      ]),
      _vm._v(" "),
      _c("li", { attrs: { role: "presentation" } }, [
        _c(
          "a",
          {
            attrs: {
              href: "#mysales-validated",
              "aria-controls": "mysales-validated",
              role: "tab",
              "data-toggle": "tab"
            },
            on: {
              click: function($event) {
                _vm.viewValid()
              }
            }
          },
          [
            _vm._v("Validated\n            "),
            _c("span", [
              _vm._v("(" + _vm._s(_vm.validCount) + ") "),
              _c("span", [_vm._v("|")])
            ])
          ]
        )
      ]),
      _vm._v(" "),
      _c("li", { attrs: { role: "presentation" } }, [
        _c(
          "a",
          {
            attrs: {
              href: "#mysales-pending",
              "aria-controls": "mysales-pending",
              role: "tab",
              "data-toggle": "tab"
            },
            on: {
              click: function($event) {
                _vm.viewPending()
              }
            }
          },
          [
            _vm._v("Pending\n            "),
            _c("span", [
              _vm._v("(" + _vm._s(_vm.pendingCount) + ") "),
              _c("span", [_vm._v("|")])
            ])
          ]
        )
      ]),
      _vm._v(" "),
      _c("li", { attrs: { role: "presentation" } }, [
        _c(
          "a",
          {
            attrs: {
              href: "#mysales-cancelled",
              "aria-controls": "mysales-cancelled",
              role: "tab",
              "data-toggle": "tab"
            },
            on: {
              click: function($event) {
                _vm.viewCancelled()
              }
            }
          },
          [
            _vm._v("Cancelled\n            "),
            _c("span", [
              _vm._v("(" + _vm._s(_vm.cancelledCount) + ") "),
              _c("span", [_vm._v("|")])
            ])
          ]
        )
      ]),
      _vm._v(" "),
      _c("li", { attrs: { role: "presentation" } }, [
        _c(
          "a",
          {
            attrs: {
              href: "#mysales-problem",
              "aria-controls": "mysales-problem",
              role: "tab",
              "data-toggle": "tab"
            },
            on: {
              click: function($event) {
                _vm.viewProblem()
              }
            }
          },
          [
            _vm._v("Problem "),
            _c("span", [_vm._v("(" + _vm._s(_vm.problemCount) + ")")])
          ]
        )
      ])
    ]),
    _vm._v(" "),
    _c("form", { staticClass: "anz-default form-inline" }, [
      _c("div", { staticClass: "form-group" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.filterKey,
              expression: "filterKey"
            }
          ],
          staticClass: "form-control",
          attrs: {
            type: "text",
            id: "pnr",
            name: "query",
            placeholder: "Ticket No, PNR or Dest."
          },
          domProps: { value: _vm.filterKey },
          on: {
            input: function($event) {
              if ($event.target.composing) {
                return
              }
              _vm.filterKey = $event.target.value
            }
          }
        })
      ]),
      _vm._v(" "),
      _vm._m(0)
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "tab-content" }, [
      _c(
        "div",
        {
          staticClass: "tab-pane active",
          attrs: { role: "tabpanel", id: "mysales-all" }
        },
        [
          _c("h2", [_vm._v(_vm._s(_vm.monthYear))]),
          _vm._v(" "),
          _c(
            "div",
            { attrs: { id: "sales-january" } },
            [
              _c("table", { staticClass: "table table-mysales" }, [
                _c("thead", [
                  _c(
                    "tr",
                    [
                      _vm._l(_vm.columns, function(key) {
                        return _c(
                          "th",
                          {
                            class: { active: _vm.sortKey == key },
                            on: {
                              click: function($event) {
                                _vm.sortBy(key)
                              }
                            }
                          },
                          [
                            _vm._v(
                              "\n                            " +
                                _vm._s(_vm._f("capitalize")(key)) +
                                "\n                            "
                            ),
                            _c("span", {
                              staticClass: "arrow",
                              class: _vm.sortOrders[key] > 0 ? "asc" : "dsc"
                            })
                          ]
                        )
                      }),
                      _vm._v(" "),
                      _c("th", { attrs: { width: "150" } })
                    ],
                    2
                  )
                ]),
                _vm._v(" "),
                _c(
                  "tbody",
                  _vm._l(_vm.filteredList, function(sale) {
                    return _c("Sale", {
                      key: sale.id,
                      attrs: { sale: sale },
                      on: {
                        "delete-sale": _vm.deleteSale,
                        "update-sale": _vm.fetchSales
                      }
                    })
                  })
                )
              ]),
              _vm._v(" "),
              _c("Pagination", {
                attrs: { pagination: _vm.pagination, offset: 4 },
                nativeOn: {
                  click: function($event) {
                    _vm.fetchSales(_vm.pagination.current_page)
                  }
                }
              })
            ],
            1
          )
        ]
      )
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c(
      "button",
      {
        staticClass: "btn btn-black btn-lg",
        attrs: { onclick: "return false;" }
      },
      [_c("i", { staticClass: "icon-ANE1227_TEP_icon_set_V1_search3" })]
    )
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-3ed785b6", module.exports)
  }
}

/***/ }),

/***/ 2:
/***/ (function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function(useSourceMap) {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		return this.map(function (item) {
			var content = cssWithMappingToString(item, useSourceMap);
			if(item[2]) {
				return "@media " + item[2] + "{" + content + "}";
			} else {
				return content;
			}
		}).join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};

function cssWithMappingToString(item, useSourceMap) {
	var content = item[1] || '';
	var cssMapping = item[3];
	if (!cssMapping) {
		return content;
	}

	if (useSourceMap && typeof btoa === 'function') {
		var sourceMapping = toComment(cssMapping);
		var sourceURLs = cssMapping.sources.map(function (source) {
			return '/*# sourceURL=' + cssMapping.sourceRoot + source + ' */'
		});

		return [content].concat(sourceURLs).concat([sourceMapping]).join('\n');
	}

	return [content].join('\n');
}

// Adapted from convert-source-map (MIT)
function toComment(sourceMap) {
	// eslint-disable-next-line no-undef
	var base64 = btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap))));
	var data = 'sourceMappingURL=data:application/json;charset=utf-8;base64,' + base64;

	return '/*# ' + data + ' */';
}


/***/ }),

/***/ 3:
/***/ (function(module, exports) {

/* globals __VUE_SSR_CONTEXT__ */

// IMPORTANT: Do NOT use ES2015 features in this file.
// This module is a runtime utility for cleaner component module output and will
// be included in the final webpack user bundle.

module.exports = function normalizeComponent (
  rawScriptExports,
  compiledTemplate,
  functionalTemplate,
  injectStyles,
  scopeId,
  moduleIdentifier /* server only */
) {
  var esModule
  var scriptExports = rawScriptExports = rawScriptExports || {}

  // ES6 modules interop
  var type = typeof rawScriptExports.default
  if (type === 'object' || type === 'function') {
    esModule = rawScriptExports
    scriptExports = rawScriptExports.default
  }

  // Vue.extend constructor export interop
  var options = typeof scriptExports === 'function'
    ? scriptExports.options
    : scriptExports

  // render functions
  if (compiledTemplate) {
    options.render = compiledTemplate.render
    options.staticRenderFns = compiledTemplate.staticRenderFns
    options._compiled = true
  }

  // functional template
  if (functionalTemplate) {
    options.functional = true
  }

  // scopedId
  if (scopeId) {
    options._scopeId = scopeId
  }

  var hook
  if (moduleIdentifier) { // server build
    hook = function (context) {
      // 2.3 injection
      context =
        context || // cached call
        (this.$vnode && this.$vnode.ssrContext) || // stateful
        (this.parent && this.parent.$vnode && this.parent.$vnode.ssrContext) // functional
      // 2.2 with runInNewContext: true
      if (!context && typeof __VUE_SSR_CONTEXT__ !== 'undefined') {
        context = __VUE_SSR_CONTEXT__
      }
      // inject component styles
      if (injectStyles) {
        injectStyles.call(this, context)
      }
      // register component module identifier for async chunk inferrence
      if (context && context._registeredComponents) {
        context._registeredComponents.add(moduleIdentifier)
      }
    }
    // used by ssr in case component is cached and beforeCreate
    // never gets called
    options._ssrRegister = hook
  } else if (injectStyles) {
    hook = injectStyles
  }

  if (hook) {
    var functional = options.functional
    var existing = functional
      ? options.render
      : options.beforeCreate

    if (!functional) {
      // inject component registration as beforeCreate hook
      options.beforeCreate = existing
        ? [].concat(existing, hook)
        : [hook]
    } else {
      // for template-only hot-reload because in that case the render fn doesn't
      // go through the normalizer
      options._injectStyles = hook
      // register for functioal component in vue file
      options.render = function renderWithStyleInjection (h, context) {
        hook.call(context)
        return existing(h, context)
      }
    }
  }

  return {
    esModule: esModule,
    exports: scriptExports,
    options: options
  }
}


/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

/*
  MIT License http://www.opensource.org/licenses/mit-license.php
  Author Tobias Koppers @sokra
  Modified by Evan You @yyx990803
*/

var hasDocument = typeof document !== 'undefined'

if (typeof DEBUG !== 'undefined' && DEBUG) {
  if (!hasDocument) {
    throw new Error(
    'vue-style-loader cannot be used in a non-browser environment. ' +
    "Use { target: 'node' } in your Webpack config to indicate a server-rendering environment."
  ) }
}

var listToStyles = __webpack_require__(12)

/*
type StyleObject = {
  id: number;
  parts: Array<StyleObjectPart>
}

type StyleObjectPart = {
  css: string;
  media: string;
  sourceMap: ?string
}
*/

var stylesInDom = {/*
  [id: number]: {
    id: number,
    refs: number,
    parts: Array<(obj?: StyleObjectPart) => void>
  }
*/}

var head = hasDocument && (document.head || document.getElementsByTagName('head')[0])
var singletonElement = null
var singletonCounter = 0
var isProduction = false
var noop = function () {}
var options = null
var ssrIdKey = 'data-vue-ssr-id'

// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
// tags it will allow on a page
var isOldIE = typeof navigator !== 'undefined' && /msie [6-9]\b/.test(navigator.userAgent.toLowerCase())

module.exports = function (parentId, list, _isProduction, _options) {
  isProduction = _isProduction

  options = _options || {}

  var styles = listToStyles(parentId, list)
  addStylesToDom(styles)

  return function update (newList) {
    var mayRemove = []
    for (var i = 0; i < styles.length; i++) {
      var item = styles[i]
      var domStyle = stylesInDom[item.id]
      domStyle.refs--
      mayRemove.push(domStyle)
    }
    if (newList) {
      styles = listToStyles(parentId, newList)
      addStylesToDom(styles)
    } else {
      styles = []
    }
    for (var i = 0; i < mayRemove.length; i++) {
      var domStyle = mayRemove[i]
      if (domStyle.refs === 0) {
        for (var j = 0; j < domStyle.parts.length; j++) {
          domStyle.parts[j]()
        }
        delete stylesInDom[domStyle.id]
      }
    }
  }
}

function addStylesToDom (styles /* Array<StyleObject> */) {
  for (var i = 0; i < styles.length; i++) {
    var item = styles[i]
    var domStyle = stylesInDom[item.id]
    if (domStyle) {
      domStyle.refs++
      for (var j = 0; j < domStyle.parts.length; j++) {
        domStyle.parts[j](item.parts[j])
      }
      for (; j < item.parts.length; j++) {
        domStyle.parts.push(addStyle(item.parts[j]))
      }
      if (domStyle.parts.length > item.parts.length) {
        domStyle.parts.length = item.parts.length
      }
    } else {
      var parts = []
      for (var j = 0; j < item.parts.length; j++) {
        parts.push(addStyle(item.parts[j]))
      }
      stylesInDom[item.id] = { id: item.id, refs: 1, parts: parts }
    }
  }
}

function createStyleElement () {
  var styleElement = document.createElement('style')
  styleElement.type = 'text/css'
  head.appendChild(styleElement)
  return styleElement
}

function addStyle (obj /* StyleObjectPart */) {
  var update, remove
  var styleElement = document.querySelector('style[' + ssrIdKey + '~="' + obj.id + '"]')

  if (styleElement) {
    if (isProduction) {
      // has SSR styles and in production mode.
      // simply do nothing.
      return noop
    } else {
      // has SSR styles but in dev mode.
      // for some reason Chrome can't handle source map in server-rendered
      // style tags - source maps in <style> only works if the style tag is
      // created and inserted dynamically. So we remove the server rendered
      // styles and inject new ones.
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  if (isOldIE) {
    // use singleton mode for IE9.
    var styleIndex = singletonCounter++
    styleElement = singletonElement || (singletonElement = createStyleElement())
    update = applyToSingletonTag.bind(null, styleElement, styleIndex, false)
    remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true)
  } else {
    // use multi-style-tag mode in all other cases
    styleElement = createStyleElement()
    update = applyToTag.bind(null, styleElement)
    remove = function () {
      styleElement.parentNode.removeChild(styleElement)
    }
  }

  update(obj)

  return function updateStyle (newObj /* StyleObjectPart */) {
    if (newObj) {
      if (newObj.css === obj.css &&
          newObj.media === obj.media &&
          newObj.sourceMap === obj.sourceMap) {
        return
      }
      update(obj = newObj)
    } else {
      remove()
    }
  }
}

var replaceText = (function () {
  var textStore = []

  return function (index, replacement) {
    textStore[index] = replacement
    return textStore.filter(Boolean).join('\n')
  }
})()

function applyToSingletonTag (styleElement, index, remove, obj) {
  var css = remove ? '' : obj.css

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = replaceText(index, css)
  } else {
    var cssNode = document.createTextNode(css)
    var childNodes = styleElement.childNodes
    if (childNodes[index]) styleElement.removeChild(childNodes[index])
    if (childNodes.length) {
      styleElement.insertBefore(cssNode, childNodes[index])
    } else {
      styleElement.appendChild(cssNode)
    }
  }
}

function applyToTag (styleElement, obj) {
  var css = obj.css
  var media = obj.media
  var sourceMap = obj.sourceMap

  if (media) {
    styleElement.setAttribute('media', media)
  }
  if (options.ssrId) {
    styleElement.setAttribute(ssrIdKey, obj.id)
  }

  if (sourceMap) {
    // https://developer.chrome.com/devtools/docs/javascript-debugging
    // this makes source maps inside style tags work properly in Chrome
    css += '\n/*# sourceURL=' + sourceMap.sources[0] + ' */'
    // http://stackoverflow.com/a/26603875
    css += '\n/*# sourceMappingURL=data:application/json;base64,' + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + ' */'
  }

  if (styleElement.styleSheet) {
    styleElement.styleSheet.cssText = css
  } else {
    while (styleElement.firstChild) {
      styleElement.removeChild(styleElement.firstChild)
    }
    styleElement.appendChild(document.createTextNode(css))
  }
}


/***/ })

/******/ });