function getData() {
    return [{
            points: [-37.008172, 174.785383, 25.255256, 55.359109],
            tickets: 12,
            from: "Auckland",
            to: "Dubai",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 12 },
            curvature: 0.3
        },
        {
            points: [-37.008172, 174.785383, 22.273587, 114.203379],
            tickets: 12,
            from: "Auckland",
            to: "Hong Kong",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 12 },
            curvature: 0.2
        },
        {
            points: [-37.008172, 174.785383, 50.096467, 8.681190],
            tickets: 45,
            from: "Auckland",
            to: "Frankfurt",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 45 },
            curvature: 0.5
        },
        {
            points: [-37.008172, 174.785383, 34.027009, -118.242850],
            tickets: 22,
            from: "Auckland",
            to: "Los Angeles",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 22 },
            curvature: -0.1
        },
        {
            points: [-37.008172, 174.785383, 41.974521, -87.907010],
            tickets: 5,
            from: "Auckland",
            to: "Chicago O'Hare",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 5 },
            curvature: -0.1
        },
        {
            points: [-37.008172, 174.785383, 1.320207, 103.852473],
            tickets: 8,
            from: "Auckland",
            to: "Singapore",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 8 },
            curvature: 0.2
        },
        {
            points: [-37.008172, 174.785383, 28.582462, 77.186854],
            tickets: 55,
            from: "Auckland",
            to: "New Delhi",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 55},
            curvature: 0.3
        },
        {
            points: [-37.008172, 174.785383, 19.062080, 72.872955],
            tickets: 11,
            from: "Auckland",
            to: "Bombay",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 11 },
            curvature: 0.3
        },
        {
            points: [-37.008172, 174.785383, 40.982819, 28.810421],
            tickets: 22,
            from: "Auckland",
            to: "Istanbul Atatürk",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 22},
            curvature: 0.4
        },
        {
            points: [-37.008172, 174.785383, -28.165, 153.506111],
            tickets: 5,
            from: "Auckland",
            to: "Gold Coast",
            label: {
                enabled: true,
                anchor: 'bottom'
            },
            marker:{ size: 5},
            curvature: 0.125
        },
        {
            points: [-37.008172, 174.785383, -27.383333, 153.118333],
            tickets: 5,
            from: "Auckland",
            to: "Brisbane",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 5},
            curvature: 0.125
        },
        {
            points: [-37.008172, 174.785383, -34.945, 138.530556],
            tickets: 5,
            from: "Auckland",
            to: "Adelaide",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 5},
            curvature: -0.1
        },
         {
            points: [-37.008172, 174.785383, -37.673333, 144.843333],
            tickets: 5,
            from: "Auckland",
            to: "Melbourne",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 5},
            curvature: -0.125
        },
        {
            points: [-37.008172, 174.785383, -31.940278, 115.966944],
            tickets: 5,
            from: "Auckland",
            to: "Perth",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 5},
            curvature: -0.125
        },
        {
            points: [-37.008172, 174.785383, -29.0425, 167.938056],
            tickets: 5,
            from: "Auckland",
            to: "Norfolk Island",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 5},
            curvature: 0.1
        },
        {
            points: [-27.383333, 153.118333, -29.0425, 167.938056],
            tickets: 5,
            from: "Brisbane",
            to: "Norfolk Island",
            label: {
                enabled: true,
                anchor: 'top'
            },
            marker:{ size: 5},
            curvature: 0.1
        }
    ]
}
