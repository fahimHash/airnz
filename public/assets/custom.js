jQuery(document).ready(function(){

  //toggle tabs
  $(".notifications-menu a.sidebar-right-toggle").click(function(){
    $(".anz-notification-tab").addClass("active");
    $(".anz-contact-tab").removeClass("active");
  });

  $(".anz-bdm-info a.sidebar-right-toggle").click(function(){
    $(".anz-notification-tab").removeClass("active");
    $(".anz-contact-tab").addClass("active");
  });

  $('a.sidebar-right-toggle').click(function(){
    $("body").toggleClass("sidebar-right-open");
	});

    $('#newNotification').on('click', function () {
        $(".anz-feed-list li a").removeClass("recent");
        $("#unreadNotification").text('');
        axios.get('markNotificationAsRead')
            .then(function (response) {
            });
    });

});

$(document).ready(function () {
    $(".btn-select").each(function (e) {
        var value = $(this).find("ul li.selected").html();
        if (value != undefined) {
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
    });
});

$(document).on('click', '.btn-select', function (e) {
    e.preventDefault();
    var ul = $(this).find("ul");
    if ($(this).hasClass("active")) {
        if (ul.find("li").is(e.target)) {
            var target = $(e.target);
            target.addClass("selected").siblings().removeClass("selected");
            var value = target.html();
            $(this).find(".btn-select-input").val(value);
            $(this).find(".btn-select-value").html(value);
        }
        ul.hide();
        $(this).removeClass("active");
    }
    else {
        $('.btn-select').not(this).each(function () {
            $(this).removeClass("active").find("ul").hide();
        });
        $(this).find(".btn-select-input").val('');
        $(this).find(".btn-select-value").html('Select an option');
        ul.slideDown(300);
        $(this).addClass("active");
    }
});

$(document).on('click', function (e) {
    var target = $(e.target).closest(".btn-select");
    if (!target.length) {
        $(".btn-select").removeClass("active").find("ul").hide();
    }
});

/*$(function () {
  "use strict";
  $('#privacy-slimscroll').slimScroll({
      height: '250px'
  });
});*/
