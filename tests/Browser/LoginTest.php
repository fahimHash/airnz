<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class LoginTest extends DuskTestCase
{
    /**
     * A basic browser test example.
     *
     * @return void
     */
    public function loginTest()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('/')
                ->type('email', 'imogene32@example.net')
                ->type('password', 'test123')
                ->press('Login')
                ->assertPathIs('/home');
        });
    }
}
