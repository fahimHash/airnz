<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', 'Auth\LoginController@showLoginForm');
// Authentication Routes...
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
// Registration Routes...
Route::get('register', 'Auth\RegisterController@showRegistrationForm')->name('register');
Route::post('register', 'Auth\RegisterController@register');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
//Reset email
Route::post('email/reset', 'Auth\ResetEmailController@checkCredentials')->name('email.reset');
//Request to Activate
Route::get('reactivate/{token}', 'Auth\ReactivateController@processRequest')->name('reactivate');
// Confirm User
Route::get('verify/{token}', 'Auth\ConfirmUserController@confirm');
Route::get('verify/resend/{id}', 'Auth\ConfirmUserController@resend');
// Ajax Get Agency List
Route::get('register/consortium', 'Auth\RegisterController@ajaxConsortium')->name('consortiumList');
Route::post('register/agency', 'Auth\RegisterController@ajaxAgency')->name('agencyList');
Route::post('register/store', 'Auth\RegisterController@storeAjax')->name('storeList');
Route::post('register/pcc', 'Auth\RegisterController@ajaxPcc')->name('checkPCC');
Route::post('register/domain', 'Auth\RegisterController@checkEmailDomain')->name('checkDomain');
Route::get('register/states', 'Auth\RegisterController@ajaxState');

Route::get('terms-and-conditions', 'Frontend\TermConditionController@index')->name('terms.and.conditions');
Route::get('about-us', 'Frontend\AboutUsController@index')->name('about.us');
Route::get('privacy-policy', 'Frontend\PrivacyController@index')->name('privacy.policy');

Route::group(['namespace' => 'Frontend', 'middleware' => ['revalidate', 'permissions:access_frontend', 'inactive']], function () {
    // Controllers Within The "App\Http\Controllers\Frontend" Namespace

    Route::get('/tour', 'HomeController@showTour');
    Route::get('/training', 'HomeController@showTraining');
    Route::get('/travel-tools', 'HomeController@showTools');
    Route::get('/6months-sales', 'HomeController@showSixMonthSales');
    Route::get('/12months-sales', 'HomeController@showTwelveMonthSales');
    Route::get('/auckland-sales', 'HomeController@showAucklandSales');
    Route::get('/vancouver-sales', 'HomeController@showVancouverSales');
    Route::get('/empty-sales', 'HomeController@showEmptySales');
    Route::get('/content-page', 'HomeController@showContentPage');

    Route::get('/home', 'HomeController@index')->name('frontend');
    Route::get('/my-account', 'ProfileController@showMyAccount');
    Route::get('/markNotificationAsRead', 'HomeController@markNotificationAsRead');
    Route::get('/contactus', 'HomeController@inactiveAccount'); //User Inactive page showing contact BDM
    Route::post('/password', 'ProfileController@updatePassword');
    Route::post('/terminate', 'ProfileController@closeAccount');
    Route::get('/activity-feed', 'ActivityFeedController@index');
    Route::get('/profile', 'ProfileController@index'); /* Profile detail screen archived and replaced by Profile dashboard screen */
    Route::get('/profile/detail', 'ProfileController@index');
    Route::get('/profile/edit', 'ProfileController@edit');
    Route::patch('/profile/update', 'ProfileController@update');
    Route::patch('/profile/uploadPhoto', 'ProfileController@uploadPhoto');
    Route::get('/profile/removePhoto', 'ProfileController@remove');
    Route::get('/profile/mybdm', 'ProfileController@bdm')->name('mybdm');
    Route::get('/company', 'CompanyController@index');
    Route::post('/company', 'CompanyController@store');
    Route::patch('/company/{id}', 'CompanyController@update');
    Route::get('/my-sales', 'SalesController@index');
    Route::get('/add-sales', 'SalesController@create')->name('add-sales');
    Route::post('/add-sales', 'SalesController@store');
    Route::post('/add-sales-pnr', 'SalesController@checkPNR')->name('checkPNR');
    Route::post('/sales-upload', 'SalesController@uploadSales');
    Route::get('api/sales', 'API\MySalesController@index');
    Route::get('api/sales/valid', 'API\MySalesController@valid');
    Route::get('api/sales/pending', 'API\MySalesController@pending');
    Route::get('api/sales/cancelled', 'API\MySalesController@cancelled');
    Route::get('api/sales/problem', 'API\MySalesController@problem');
    Route::patch('api/sales/{id}', 'API\MySalesController@update');
    Route::delete('api/sales/{id}', 'API\MySalesController@destroy');
    Route::get('api/sales/salesChartYearly', 'API\MySalesController@salesChartYearly');
    Route::get('api/sales/salesChartMonthly', 'API\MySalesController@salesChartMonthly');
    Route::get('api/sales/salesChartWeekly', 'API\MySalesController@salesChartWeekly');
    Route::get('api/sales/salesCount', 'API\MySalesController@salesCount');
    Route::get('/campaigns', 'CampaignController@index');
    Route::get('/campaigns/detail/{id}', 'CampaignController@show');
    Route::post('/join-campaign', 'CampaignController@store');
    Route::get('/company/consortium', 'CompanyController@ajaxConsortium');
    Route::post('/company/agency', 'CompanyController@agencyAjax')->name('agencyAjax');
    Route::post('/company/store', 'CompanyController@storeAjax')->name('storeAjax');
    Route::post('/company/state', 'CompanyController@stateAjax')->name('stateAjax');
    Route::get('/rewards', 'RewardController@index');
    Route::get('/rewards/detail/{id}', 'RewardController@show');
    Route::post('/campaigns/progress', 'CampaignController@progressAjax');
    Route::get('/news-feed', 'HomeController@newsAjax');
    Route::get('news', 'NewsController@index');
    Route::get('news/{slug}','NewsController@show')->where('slug', '[A-Za-z0-9-_]+');
    Route::get('page', 'PageController@index');
    Route::get('page/{slug}','PageController@show')->where('slug', '[A-Za-z0-9-_]+')->name('f.page.show');
    Route::get('surveys', 'SurveyController@index')->name('f.survey.index');
    Route::get('surveys/{slug}','SurveyController@show')->where('slug', '[A-Za-z0-9-_]+')->name('f.survey.show');
    Route::post('surveys/{slug}/next','SurveyController@next')->where('slug', '[A-Za-z0-9-_]+')->name('f.survey.next');
});
Route::group(['namespace' => 'Frontend', 'middleware' => ['permissions:access_frontend', 'inactive']], function () {
    Route::get('/sales-template', 'SalesController@downloadSampleCSV');
});

Route::group(['namespace' => 'Backend', 'middleware' => ['auth', 'revalidate', 'permissions:access_backend', 'inactive'], 'prefix' => 'backend'], function () {
    // Controllers Within The "App\Http\Controllers\Backend" Namespace
    Route::get('/', 'DashboardController@index')->name('backend');
    Route::post('/date-range', 'DashboardController@dateRange')->name('dateRange');
    Route::post('/flights', 'DashboardController@flights')->name('flights');

    Route::get('/validate-sales', 'ValidateSalesController@index');
    Route::post('/validate-sales-upload', 'ValidateSalesController@uploadSales');
    Route::get('/sales-upload-status', 'ValidateSalesController@status');

    Route::get('/users/users-data', 'UsersController@usersTable')->name('users-data');
    Route::get('/users/create', 'UsersController@create');
    Route::get('/users', 'UsersController@index')->name('users.index');
    Route::patch('/users/{id}', 'UsersController@update');
    Route::get('/users/{id}', 'UsersController@show');
    Route::get('/users/{id}/edit', 'UsersController@edit');

    Route::get('/agents/agents-data', 'AgentsController@agentsTable')->name('agents-data');
    Route::get('/agents/agents-archived-data', 'ArchivedAgentsController@agentsArchivedTable')->name('agentsArchivedData');
    Route::get('/agents/archive', 'ArchivedAgentsController@index')->name('archive');
    Route::post('/agents/agency', 'AgentsController@agencyAjax');
    Route::post('/agents/{id}/agency', 'AgentsController@agencyAjax');
    Route::post('/agents/restore', 'ArchivedAgentsController@restore')->name('agentRestore');
    Route::post('/agents/date-range', 'AgentsController@dateRange')->name('agentsDateRange');
    Route::get('/agents/{id}/sales-data', 'AgentsController@salesTable')->name('agentSalesData');
    Route::get('/agents/{id}/campaigns-data', 'AgentsController@campaignsTable')->name('agentCampaignsData');
    Route::get('/agents/{id}/rewards-data', 'AgentsController@rewardsTable')->name('agentRewardsData');
    Route::post('/agents/reactivate', 'AgentsController@reactivate')->name('agents.reactivate');

    Route::get('/consortiums/consortiums-data', 'ConsortiumsController@consortiumsTable')->name('consortiums-data');
    Route::post('/consortiums/date-range', 'ConsortiumsController@dateRange')->name('consortiumDateRange');
    Route::get('/consortiums/{id}/sales-data', 'ConsortiumsController@salesTable')->name('consortiumSalesData');
    Route::get('/consortiums/{id}/agency-data', 'ConsortiumsController@agencyTable')->name('consortiumAgencyData');
    Route::get('/consortiums/{id}/agent-data', 'ConsortiumsController@agentTable')->name('consortiumAgentData');
    Route::get('/consortiums/{id}/territory-data', 'ConsortiumsController@territoryTable')->name('consortiumTerritoryData');
    Route::get('/consortiums/{id}/store-data', 'ConsortiumsController@storeTable')->name('consortiumStoreData');

    Route::get('/territory/territory-data', 'TerritoryController@territoryTable')->name('territory-data');
    Route::post('/territory/{id}/ajax', 'TerritoryController@formAjax');
    Route::post('/territory/agency', 'TerritoryController@agencyAjax');
    Route::post('/territory/{id}/agency', 'TerritoryController@agencyAjax');
    Route::post('/territory/date-range', 'TerritoryController@dateRange')->name('territoryDateRange');
    Route::get('/territory/{id}/sales-data', 'TerritoryController@salesTable')->name('territorySalesData');
    Route::get('/territory/{id}/consortium-data', 'TerritoryController@consortiumsTable')->name('territoryConsortiumData');
    Route::get('/territory/{id}/agency-data', 'TerritoryController@agencyTable')->name('territoryAgencyData');
    Route::get('/territory/{id}/agent-data', 'TerritoryController@agentTable')->name('territoryAgentData');
    Route::get('/territory/{id}/store-data', 'TerritoryController@storeTable')->name('territoryStoreData');

    Route::get('/sales/sales-data', 'SalesController@salesTable')->name('sales-data');
    Route::get('/sales-report', 'SalesReportController@index');
    Route::post('/sales-report', 'SalesReportController@store');

    Route::get('/agency/agency-data', 'AgencyController@agencyTable')->name('agency-data');

    Route::get('/campaigns/campaigns-data', 'CampaignController@campaignsTable')->name('campaigns-data');

    Route::get('/rewards/rewards-data', 'RewardController@rewardsTable')->name('rewards-data');

    Route::get('/news/news-data', 'NewsController@newsTable')->name('news.data');
    Route::get('/news/{slug}/editor', 'NewsController@editor')->name('news.editor');
    Route::patch('/news/{slug}/editor', 'NewsController@updateContent')->name('news.update.content');
    Route::get('/news/{slug}/snippets', 'NewsController@editorStaticContent');
    Route::get('/news/{slug}/preview', 'NewsController@previewContent')->name('news.preview.content');
    Route::patch('/news/{id}/publish', 'NewsController@publish')->name('news.publish');

    Route::get('/pages/pages-data', 'PagesController@pageTable')->name('page.data');
    Route::get('/pages/{slug}/editor', 'PagesController@editor')->name('page.editor');
    Route::patch('/pages/{slug}/editor', 'PagesController@updateContent')->name('page.update.content');
    Route::get('/pages/{slug}/snippets', 'PagesController@editorStaticContent');
    Route::get('/pages/{slug}/preview', 'PagesController@previewContent')->name('page.preview.content');
    Route::patch('/pages/{id}/publish', 'PagesController@publish')->name('page.publish');
    Route::post('/pages/remove-photo', 'PagesController@removePhoto')->name('page.remove.photo');

    Route::get('/store/store-data', 'StoreController@storeTable')->name('store-data');

    Route::get('/blacklists/blacklist-data', 'BlacklistController@blacklistTable')->name('blacklist-data');

    Route::post('/segments/sale','SaleSegmentController@store')->name('segments.sale.store');
    Route::get('/segments/sale/{id}','SaleSegmentController@show')->name('segments.sale.show');
    Route::delete('/segments/sale/{id}','SaleSegmentController@destroy')->name('segments.sale.destroy');

    Route::post('/segments/system','SystemSegmentController@store')->name('segments.system.store');
    Route::get('/segments/system/{id}','SystemSegmentController@show')->name('segments.system.show');
    Route::delete('/segments/system/{id}','SystemSegmentController@destroy')->name('segments.system.destroy');

    Route::post('/segments/training','TrainingSegmentController@store')->name('segments.training.store');
    Route::get('/segments/training/{id}','TrainingSegmentController@show')->name('segments.training.show');
    Route::delete('/segments/training/{id}','TrainingSegmentController@destroy')->name('segments.training.destroy');

    Route::get('/segments/sale-table','SegmentsController@displayUserDatatable')->name('segments.sale.table');
    Route::get('/segments/system-table','SegmentsController@displayUserDatatable')->name('segments.system.table'); //TODO change method for system
    Route::get('/segments/training-table','SegmentsController@displayUserDatatable')->name('segments.training.table'); //TODO change method for training

    Route::get('/segments/sale-table-index','SaleSegmentController@segmentTable')->name('segments.sale.table.index');
    Route::get('/segments/system-table-index','SystemSegmentController@segmentTable')->name('segments.system.table.index');
    Route::get('/segments/training-table-index','TrainingSegmentController@segmentTable')->name('segments.training.table.index');

    Route::get('/surveys/surveys-data', 'SurveysController@surveyTable')->name('surveys.data');
    Route::post('/surveys/remove-photo', 'SurveysController@removePhoto')->name('surveys.remove.photo');
    Route::get('/surveys/{id}/question-data', 'SurveysController@questionTable')->name('question.data');
    Route::get('/questions/{id}/answer-data', 'QuestionsController@answerTable')->name('answer.data');

    Route::resource('territory', 'TerritoryController');
    Route::resource('agents', 'AgentsController');
    Route::resource('consortiums', 'ConsortiumsController');
    Route::resource('sales', 'SalesController');
    Route::resource('agency', 'AgencyController');
    Route::resource('campaigns', 'CampaignController');
    Route::resource('rewards', 'RewardController');
    Route::resource('news', 'NewsController');
    Route::resource('pages', 'PagesController');
    Route::resource('store', 'StoreController');
    Route::resource('surveys', 'SurveysController');
    Route::resource('surveys.questions', 'QuestionsController');
    Route::resource('questions.answers', 'AnswersController');
    Route::resource('blacklists', 'BlacklistController', ['only' => ['index', 'create', 'store', 'destroy']]);
    Route::resource('segments', 'SegmentsController', ['only' => ['index', 'create']]);

});
Route::group(['namespace' => 'Backend', 'middleware' => ['auth', 'revalidate', 'permissions:access_backend', 'permissions:role_permission'], 'prefix' => 'backend'], function () {

    Route::get('/give-role-permissions', 'RolesController@getGivenRolePermission');
    Route::post('/give-role-permissions', 'RolesController@postGivenRolePermission');

    Route::post('/users', 'UsersController@store');
    Route::delete('/users/{id}', 'UsersController@destroy');

    Route::resource('roles', 'RolesController');
    Route::resource('permissions', 'PermissionsController');
});
