<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->integer('tag_type_id')->unsigned()->index();
            $table->foreign('tag_type_id')->references('id')->on('tag_types')->onDelete('cascade');
            $table->integer('tag_obj_id')->unsigned()->index();
            $table->foreign('tag_obj_id')->references('id')->on('tag_objectives')->onDelete('cascade');
            $table->integer('tag_channel_id')->unsigned()->index();
            $table->foreign('tag_channel_id')->references('id')->on('tag_channels')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropForeign('campaigns_tag_type_id_foreign');
            $table->dropColumn('tag_type_id');
            $table->dropForeign('campaigns_tag_obj_id_foreign');
            $table->dropColumn('tag_obj_id');
            $table->dropForeign('campaigns_tag_channel_id_foreign');
            $table->dropColumn('tag_channel_id');
        });
    }
}
