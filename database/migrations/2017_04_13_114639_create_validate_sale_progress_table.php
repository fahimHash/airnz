<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateValidateSaleProgressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('validate_sale_progress', function (Blueprint $table) {
            $table->increments('id');
            $table->string('filename');
            $table->string('filepath');
            $table->string('message')->nullable();
            $table->boolean('imported')->default(false);
            $table->integer('rows_imported')->default(0);
            $table->integer('rows_validated')->default(0);
            $table->integer('rows_skipped')->default(0);
            $table->integer('total_rows')->default(0);
            $table->integer('user_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('validate_sale_progress');
    }
}
