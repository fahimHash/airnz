<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBlogField extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('blog', function (Blueprint $table) {
            $table->dropColumn('message');
        });
        Schema::table('blog', function (Blueprint $table) {
            $table->mediumText('message')->nullable();
            $table->string('slug')->unique();
            $table->boolean('publish')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('blog', function (Blueprint $table) {
            $table->dropColumn('message');
            $table->dropColumn('slug');
            $table->dropColumn('publish');
        });
        Schema::table('blog', function (Blueprint $table) {
            $table->text('message',10000)->nullable();
        });
    }
}
