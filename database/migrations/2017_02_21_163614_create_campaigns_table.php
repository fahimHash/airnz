<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCampaignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campaigns', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('type')->nullable();
            $table->text('message', 10000)->nullable();
            $table->string('tag')->nullable();
            $table->date('startDate')->nullable();
            $table->date('endDate')->nullable();
            $table->string('image')->nullable();
            $table->string('sales')->nullable();
            $table->boolean('status')->default(false);
            $table->integer('territory_id')->unsigned()->nullable();
            $table->integer('reward_id')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('campaigns', function (Blueprint $table) {
            $table->foreign('territory_id')
                ->references('id')
                ->on('territories')
                ->onDelete('set null');
            $table->foreign('reward_id')
                ->references('id')
                ->on('rewards')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campaigns');
    }
}
