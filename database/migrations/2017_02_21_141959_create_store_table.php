<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('pcc')->unique();
            $table->string('address1')->nullable();
            $table->string('address2')->nullable();
            $table->string('address3')->nullable();
            $table->string('suburb')->nullable();
            $table->string('postcode')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('stores', function (Blueprint $table) {
            $table->integer('store_state_id')->unsigned()->nullable();
            $table->foreign('store_state_id')
                ->references('id')
                ->on('states')
                ->onDelete('set null');

            $table->integer('store_consortium_id')->unsigned()->nullable();
            $table->foreign('store_consortium_id')
                ->references('id')
                ->on('consortiums')
                ->onDelete('set null');

            $table->integer('store_agency_id')->unsigned()->nullable();
            $table->foreign('store_agency_id')
                ->references('id')
                ->on('agencies')
                ->onDelete('set null');

            $table->integer('store_territory_id')->unsigned()->nullable();
            $table->foreign('store_territory_id')
                ->references('id')
                ->on('territories')
                ->onDelete('set null');

            $table->integer('store_bdm_id')->unsigned()->nullable();
            $table->foreign('store_bdm_id')
                ->references('id')
                ->on('users')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stores', function (Blueprint $table) {
            $table->dropForeign(['store_bdm_id']);
            $table->dropForeign(['store_territory_id']);
            $table->dropForeign(['store_agency_id']);
            $table->dropForeign(['store_consortium_id']);
            $table->dropForeign(['store_state_id']);
        });
        Schema::drop('stores');
    }
}
