<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {
            $table->increments('id');
            /*
            * Follow fields are add as per my sales table
            */
            $table->enum('status',['Valid','Pending','Cancelled','Problem'])->default('Pending');
            $table->date('issued_date')->nullable();
            $table->string('pnr'); //pnr indentifier
            $table->string('cty')->nullable(); //airport pair code
            $table->string('stock');
            $table->string('ticket_number');
            $table->date('departure_date')->nullable();
            $table->string('class')->nullable();
            $table->string('notes')->nullable();
            /*
            * Follow fields are add from OSI Report
            */
            /*
            $table->string('osi_text')->nullable();
            $table->string('trade_name')->nullable();
            $table->string('parent_trade_name')->nullable();
            $table->string('top_parent_trade_name')->nullable();
            $table->string('region')->nullable();
            $table->string('orig_record_owner_pnr')->nullable();
            $table->string('pnr_creation_plate_no')->nullable();
            $table->string('state_name')->nullable();
            $table->boolean('trans_tasman_flag')->default(false);
            */
            /************************************/

            /*
            * Sales belongs to a user
            */
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('cascade');

            $table->integer('consortium_id')->unsigned()->nullable();
            $table->foreign('consortium_id')
                ->references('id')
                ->on('consortiums')
                ->onDelete('set null');
            /*
            * Sales belongs to a store
            */
            $table->integer('store_id')->unsigned()->nullable();
            $table->foreign('store_id')
            ->references('id')
            ->on('stores')
            ->onDelete('set null');

            /*
            * Sales belongs to a campaign
            */
            $table->integer('campaign_id')->unsigned()->nullable();
            $table->foreign('campaign_id')
                ->references('id')
                ->on('campaigns')
                ->onDelete('set null');

            $table->integer('territory_id')->unsigned()->nullable();
            $table->foreign('territory_id')
                ->references('id')
                ->on('territories')
                ->onDelete('set null');

            $table->integer('state_id')->unsigned()->nullable();
            $table->foreign('state_id')
                ->references('id')
                ->on('states')
                ->onDelete('set null');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales');
    }
}
