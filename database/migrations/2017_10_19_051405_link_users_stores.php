<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LinkUsersStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table){
            $table->integer('consortium_id')->unsigned()->nullable()->after('active');
            $table->foreign('consortium_id')
                ->references('id')
                ->on('consortiums')
                ->onDelete('set null');
            $table->integer('territory_id')->unsigned()->nullable()->after('active');
            $table->foreign('territory_id')
                ->references('id')
                ->on('territories')
                ->onDelete('set null');
            $table->integer('store_id')->unsigned()->nullable()->after('active');
            $table->foreign('store_id')
                ->references('id')
                ->on('stores')
                ->onDelete('set null');
            $table->integer('state_id')->unsigned()->nullable()->after('active');
            $table->foreign('state_id')
                ->references('id')
                ->on('states')
                ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            //
        });
    }
}
