<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyingCampaignTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->dropColumn('tag');
            $table->dropForeign('campaigns_territory_id_foreign');
            $table->dropColumn('territory_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('campaigns', function (Blueprint $table) {
            $table->string('tag')->nullable();
            $table->integer('territory_id')->unsigned()->nullable();
            $table->foreign('territory_id')
                ->references('id')
                ->on('territories')
                ->onDelete('set null');
        });
    }
}
