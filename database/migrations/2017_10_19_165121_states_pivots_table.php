<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class StatesPivotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agency_state', function (Blueprint $table) {
            $table->integer('agency_id')->unsigned();
            $table->integer('state_id')->unsigned();

            $table->foreign('agency_id')
                ->references('id')
                ->on('agencies')
                ->onDelete('cascade');

            $table->foreign('state_id')
                ->references('id')
                ->on('states')
                ->onDelete('cascade');

            $table->primary(['agency_id', 'state_id']);
        });

        Schema::create('consortium_state', function (Blueprint $table) {
            $table->integer('consortium_id')->unsigned();
            $table->integer('state_id')->unsigned();

            $table->foreign('consortium_id')
                ->references('id')
                ->on('consortiums')
                ->onDelete('cascade');

            $table->foreign('state_id')
                ->references('id')
                ->on('states')
                ->onDelete('cascade');

            $table->primary(['consortium_id', 'state_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agency_state');
        Schema::dropIfExists('consortium_state');
    }
}
