<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TerritoriesPivotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('consortium_territory', function (Blueprint $table) {
            $table->integer('consortium_id')->unsigned();
            $table->integer('territory_id')->unsigned();

            $table->foreign('consortium_id')
                ->references('id')
                ->on('consortiums')
                ->onDelete('cascade');

            $table->foreign('territory_id')
                ->references('id')
                ->on('territories')
                ->onDelete('cascade');
            $table->primary(['consortium_id', 'territory_id']);
        });

        Schema::create('state_territory', function (Blueprint $table) {
            $table->integer('state_id')->unsigned();
            $table->integer('territory_id')->unsigned();

            $table->foreign('state_id')
                ->references('id')
                ->on('states')
                ->onDelete('cascade');

            $table->foreign('territory_id')
                ->references('id')
                ->on('territories')
                ->onDelete('cascade');
            $table->primary(['state_id', 'territory_id']);
        });

        Schema::create('agency_territory', function (Blueprint $table) {
            $table->integer('agency_id')->unsigned();
            $table->integer('territory_id')->unsigned();

            $table->foreign('agency_id')
                ->references('id')
                ->on('agencies')
                ->onDelete('cascade');

            $table->foreign('territory_id')
                ->references('id')
                ->on('territories')
                ->onDelete('cascade');
            $table->primary(['agency_id', 'territory_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('consortium_territory');
        Schema::dropIfExists('state_territory');
        Schema::dropIfExists('agency_territory');
    }
}
