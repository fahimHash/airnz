<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAgenciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agencies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('label')->nullable();
            $table->string('type')->nullable();
            $table->boolean('active')->default(true);
            $table->integer('fk_id_consortiums')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

      Schema::table('agencies', function (Blueprint $table) {
            $table->foreign('fk_id_consortiums')
            ->references('id')
            ->on('consortiums')
            ->onDelete('set null');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agencies');
    }
}
