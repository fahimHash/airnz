<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmploymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employments',function(Blueprint $table){
          $table->increments('id');
          $table->string('position')->nullable();
          $table->date('startDate')->nullable();
          $table->date('endDate')->nullable();
          $table->boolean('active')->default(false);
          $table->integer('user_id')->unsigned()->nullable();
          $table->timestamps();
          $table->softDeletes();
        });

        Schema::table('employments', function (Blueprint $table){
            $table->foreign('user_id')
                ->references('id')
                ->on('users')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employments');
    }
}
