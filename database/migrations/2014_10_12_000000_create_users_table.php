<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('agent_id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('phoneNumber')->unique();
            $table->string('title')->nullable();
            $table->enum('gender',['Male','Female'])->default('Male');
            $table->date('startDate')->nullable();
            $table->string('image')->default('default-image.png');
            $table->integer('sale_target')->nullable();
            $table->string('confirmation_code')->nullable();
            $table->boolean('isConfirmed')->default(false);
            $table->boolean('active')->default(false);
            $table->boolean('default')->default(false);
            $table->date('logged_in_at')->nullable();
            $table->string('lapsed_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
            $table->index('agent_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
