<?php

use App\Agency;
use App\Consortium;
use App\State;
use Illuminate\Database\Seeder;

class RegistrationChanges extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        State::whereCode('NSW')->update(['sort_order'=>1]);
        State::whereCode('QLD')->update(['sort_order'=>2]);
        State::whereCode('VIC')->update(['sort_order'=>3]);
        State::whereCode('WA')->update(['sort_order'=>4]);
        State::whereCode('SA')->update(['sort_order'=>5]);
        State::whereCode('TAS')->update(['sort_order'=>6]);
        State::whereCode('NT')->update(['sort_order'=>8]);
        State::whereCode('ACT')->update(['sort_order'=>7]);

        Consortium::whereName('Flight Centre Travel Group')->update(['sort_order'=>1]);
        $consortiums = Consortium::all(['id','name']);
        foreach ($consortiums as $consortium){
            Consortium::whereId($consortium->id)->update(['label'=>$consortium->name]);
        }

        $agencies = Agency::all(['id', 'name']);
        foreach ($agencies as $agency){
            Agency::whereId($agency->id)->update(['label' => $agency->name]);
        }

        Consortium::whereName('(AUS) HELLOWORLD CONSORTIUM')->update(['label'=>'Helloworld Travel Group']);
        Agency::whereName('(AUS) HELLOWORLD BRANDED GROUP')->update(['label'=>'Helloworld Brand', 'sort_order' => 1]);
        Agency::whereName('(AUS) HELLOWORLD ASSOCIATE GROUP')->update(['label'=>'Helloworld Associate Brand','sort_order' => 2]);
        Agency::whereName('(AUS) HELLOWORLD FOR BUSINESS GROUP')->update(['label'=>'Helloworld Business Travel','sort_order' => 3]);
        Agency::whereName('(AUS) MY TRAVEL GROUP')->update(['label'=>'My Travel Group']);
        Agency::whereName('QBT')->update(['sort_order'=>4]);
        Agency::whereName('American Express')->update(['sort_order'=>5]);
        Agency::whereName('Viva Holidays')->update(['sort_order'=>7]);

    }
}
