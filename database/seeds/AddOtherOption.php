<?php

use App\Agency;
use App\Store;
use Illuminate\Database\Seeder;

class AddOtherOption extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Agency::create([
            'name' => 'Other',
            'active' => '1'
        ]);
        Store::create([
           'name' => 'Other',
            'pcc' => 'Other'
        ]);
    }
}
