<?php

use App\Agency;
use App\Consortium;
use App\Job;
use App\State;
use App\Store;
use App\Territory;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $faker->addProvider(new Faker\Provider\en_AU\PhoneNumber($faker));
        /*
                $user = App\User::create([
                    'title' => 'Mr',
                    'agent_id' => str_random(10),
                    'name' => 'Tahmid Hassan',
                    'email' => 'tahmid@realdigi.com.au',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Male',
                    'active' => true,
                    'isConfirmed' => true,
                    'default' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('admin');

                $user = App\User::create([
                    'title' => 'Mr',
                    'name' => 'Aaron Meehan',
                    'agent_id' => str_random(10),
                    'email' => 'aaron@realdigi.com.au',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Male',
                    'active' => true,
                    'isConfirmed' => true,
                    'default' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('admin');

                $user = App\User::create([
                    'title' => 'Mr',
                    'agent_id' => str_random(10),
                    'name' => 'Nasim Ahmed',
                    'email' => 'nasim@realdigi.com.au',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Male',
                    'active' => true,
                    'isConfirmed' => true,
                    'default' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('state-manager');
        */
        /*
                $user = App\User::create([
                    'title' => 'Mr',
                    'name' => 'Air Nz',
                    'agent_id' => str_random(10),
                    'email' => 'airnz@example.com',
                    'password' => bcrypt('flyinghigh2017!'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Male',
                    'active' => true,
                    'isConfirmed' => true,
                    'default' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('admin');

                $user = App\User::create([
                    'title' => 'Mr',
                    'name' => 'Corey Numa',
                    'agent_id' => str_random(10),
                    'email' => 'corey@pontoonplatform.com.au',
                    'password' => bcrypt('flyinghigh2017!'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Male',
                    'active' => true,
                    'isConfirmed' => true,
                    'default' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('admin');

                $user = App\User::create([
                    'title' => 'Mr',
                    'name' => 'Duo Dev Team',
                    'agent_id' => str_random(10),
                    'email' => 'duodevteam@pontoonplatform.com.au',
                    'password' => bcrypt('flyinghigh2017!'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Male',
                    'active' => true,
                    'isConfirmed' => true,
                    'default' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('admin');

                $user = App\User::create([
                    'title' => 'Mr',
                    'agent_id' => str_random(10),
                    'name' => 'Chris Fenton',
                    'email' => 'chrisfenton@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Male',
                    'active' => true,
                    'isConfirmed' => true,
                    'default' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('airnz-super-user');

                $user = App\User::create([
                    'title' => 'Mr',
                    'agent_id' => str_random(10),
                    'name' => 'Sharee Burgess',
                    'email' => 'sharee@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Male',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('national-account-manager');

                $user = App\User::create([
                    'title' => 'Mr',
                    'agent_id' => 'AGT_Agency_Support',
                    'name' => 'Agency Support',
                    'email' => 'agencysupport@airnz.com.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => '131000',
                    'gender' => 'Male',
                    'active' => true,
                    'isConfirmed' => true,
                    'default' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => 'Ms',
                    'agent_id' => str_random(10),
                    'name' => 'Stella Hritis',
                    'email' => 'Stella.Hritis@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Female',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => 'Ms',
                    'agent_id' => str_random(10),
                    'name' => 'Gail Whanau',
                    'email' => 'Gail.Whanau@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Female',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => 'Ms',
                    'agent_id' => str_random(10),
                    'name' => 'Camryn Rathbone',
                    'email' => 'Camryn.Rathbone@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Female',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => 'Ms',
                    'agent_id' => str_random(10),
                    'name' => 'Diana Devlin',
                    'email' => 'Diana.Devlin@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Female',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => 'Ms',
                    'agent_id' => str_random(10),
                    'name' => 'Elizabeth Hutchison',
                    'email' => 'elizabeth.hutchison@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Female',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => 'Ms',
                    'agent_id' => str_random(10),
                    'name' => 'Jacqui Tan',
                    'email' => 'Jacqueline.Tan@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Female',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => 'Ms',
                    'agent_id' => str_random(10),
                    'name' => 'Janelle Philpott',
                    'email' => 'Janelle.Philpott@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Female',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => 'Mr',
                    'agent_id' => str_random(10),
                    'name' => 'Nicholas Lewis',
                    'email' => 'Nicholas.Lewis@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Female',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => 'Ms',
                    'agent_id' => str_random(10),
                    'name' => 'Karen Koval',
                    'email' => 'Karen.Koval@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Female',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => 'Ms',
                    'agent_id' => str_random(10),
                    'name' => 'Melissa Van Twest',
                    'email' => 'Mel.VanTwest@airnz.co.nz1',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => 'Female',
                    'active' => true,
                    'isConfirmed' => true,
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);
                $user->assignRole('territory');

                $user = App\User::create([
                    'title' => $faker->randomElement($array = array ('Mr','Mrs','Ms','Miss')),
                    'agent_id' => str_random(10),
                    'name' => $faker->firstName.' '.$faker->lastName,
                    'email' => 'imogene32@example.net',
                    'password' => bcrypt('test123'),
                    'remember_token' => str_random(10),
                    'phoneNumber' => $faker->phoneNumber,
                    'gender' => $faker->randomElement($array = array ('Male','Female')),
                    'active' => $faker->boolean(true),
                    'isConfirmed' => $faker->boolean(true),
                    'startDate' => \Carbon\Carbon::today()->toDateString()
                ]);

                $user->assignRole('minor');

                for ($i = 0; $i < 49; $i++) {
                    $user = App\User::create([
                        'title' => $faker->randomElement($array = array ('Mr','Mrs','Ms','Miss')),
                        'agent_id' => str_random(10),
                        'name' => $faker->firstName.' '.$faker->lastName,
                        'email' => $faker->unique()->safeEmail,
                        'password' => bcrypt('test123'),
                        'remember_token' => str_random(10),
                        'phoneNumber' => $faker->phoneNumber,
                        'gender' => $faker->randomElement($array = array ('Male','Female')),
                        'active' => $faker->boolean(true),
                        'isConfirmed' => $faker->boolean(true),
                        'startDate' => \Carbon\Carbon::today()->toDateString()
                    ]);

                    $user->assignRole('minor');

                }
                */
/*
        $bdm = User::whereName('Stella Hritis')->first();
        $territory = Territory::find($bdm->manageTerritory->id);
        $store = Store::firstOrCreate([
            'name' => 'Playford Franchise',
            'pcc'  => 'BL9I',
        ]);
        $store->agency()->associate(Agency::whereLabel('Travel Associates')->first());
        $store->consortium()->associate(Consortium::whereLabel('Flight Centre Travel Group')->first());
        $store->bdm()->associate($bdm);
        $store->state()->associate(State::whereCode('QLD')->first());
        $store->territory()->associate($territory);
        $store->save();
        $this->command->info('Saving store... '.$store->name);

        $store = Store::firstOrCreate([
            'name' => 'Mascot',
            'pcc'  => 'SYDJW2101',
        ]);
        $store->agency()->associate(Agency::whereLabel('Viva Holidays')->first());
        $store->consortium()->associate(Consortium::whereLabel('Helloworld Travel Group')->first());
        $store->bdm()->associate($bdm);
        $store->state()->associate(State::whereCode('NSW')->first());
        $store->territory()->associate($territory);
        $store->save();
        $this->command->info('Saving store... '.$store->name);
*/

        $data = [
            [
                'title' => 'Mr',
                'name'  => 'Michael Jon',
                'state' => 'TAS',
                'pcc'   => 'AI1I'
            ],
            [
                'title' => 'Mr',
                'name'  => 'Amanda Scott',
                'state' => 'QLD',
                'pcc'   => 'BC9I'
            ],
            [
                'title' => 'Mr',
                'name'  => 'Paul Smith',
                'state' => 'QLD',
                'pcc'   => 'BL9I'
            ],
            [
                'title' => 'Mr',
                'name'  => 'Jo Tucker',
                'state' => 'QLD',
                'pcc'   => 'CO6I'
            ],
            [
                'title' => 'Mr',
                'name'  => 'Sam Thornton',
                'state' => 'TAS',
                'pcc'   => 'DH0I'
            ],
            [
                'title' => 'Mr',
                'name'  => 'Ashleigh Andrew',
                'state' => 'ACT',
                'pcc'   => 'DL9I'
            ],
            [
                'title' => 'Mr',
                'name'  => 'Imogen Tin',
                'state' => 'QLD',
                'pcc'   => 'EP4I'
            ],
            [
                'title' => 'Mr',
                'name'  => 'Tegan Stoke',
                'state' => 'TAS',
                'pcc'   => 'HP5I'
            ],
            [
                'title' => 'Ms',
                'name'  => 'Sarah Pin',
                'state' => 'QLD',
                'pcc'   => 'GM0I'
            ],
            [
                'title' => 'Ms',
                'name'  => 'Charlene Taller',
                'state' => 'QLD',
                'pcc'   => 'NV1'
            ],
            [
                'title' => 'Ms',
                'name'  => 'Stacy Paulie',
                'state' => 'NSW',
                'pcc'   => 'SYDJW2101'
            ],
            [
                'title' => 'Mr',
                'name'  => 'James Ball',
                'state' => 'QLD',
                'pcc'   => 'X42I'
            ]
        ];
        $i = 0;
        foreach ($data as $user) {
            $i++;
            $store = Store::wherePcc($user['pcc'])->first();
            $agent = App\User::create([
                'title'          => $user['title'],
                'agent_id'       => str_random(10),
                'name'           => $user['name'],
                'email'          => str_replace(' ', '', strtolower($user['name'])) . '@example.net',
                'password'       => bcrypt('test12345'),
                'remember_token' => str_random(10),
                'phoneNumber'    => $faker->phoneNumber,
                'active'         => $faker->boolean(true),
                'isConfirmed'    => $faker->boolean(true),
                'startDate'      => \Carbon\Carbon::today()->toDateString(),
                'address1'       => $i . ' ABC Street',
                'address2'       => 'Test Suburb ' . $i,
                'state_id'       => State::whereCode('TAS')->first()->id,
                'sale_target'    => 0,
                'consortium_id'  => $store->store_consortium_id,
                'territory_id'   => $store->store_territory_id,
                'store_id'       => $store->id
            ]);
            $agent->assignRole('minor');

            Job::create([
                'startDate' => Carbon::today()->toDateString(),
                'position'  => 'Consultant',
                'active'    => 1,
                'user_id'   => $agent->id,
                'store_id'  => $store->id
            ]);
            $this->command->info('Saving agent... '.$agent->name);
        }
    }

}
