<?php

use App\User;
use Illuminate\Database\Seeder;

class SalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array(
              'user' => 'Paul Smith',
              'pnr' => 'HEG2FH',
              'date' => '5-02-2018'
            ),
            array(
                'user' => 'Paul Smith',
                'pnr' => '4M4PWH',
                'date' => '	5-02-2018'
            ),
            array(
                'user' => 'Paul Smith',
                'pnr' => 'IZSNDH	',
                'date' => '5-02-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => '24R44H	',
                'date' => '1-02-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => 'AY6Q4H	',
                'date' => '1-02-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => 'CJHAAH	',
                'date' => '7-02-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => 'GETWFH	',
                'date' => '29-01-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => 'JMAKQH	',
                'date' => '6-02-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => 'TN3WHH	',
                'date' => '30-01-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => 'VF2FWH	',
                'date' => '6-02-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => 'WWE36H	',
                'date' => '5-02-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => '75AXQH	',
                'date' => '1-02-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => 'XDGILH	',
                'date' => '1-02-2018'
            ),
            array(
                'user' => 'Amanda Scott',
                'pnr' => 'VAI54H	',
                'date' => '2-02-2018'
            ),
            array(
                'user' => 'Amanda Scott',
                'pnr' => '7MFPQH',
                'date' => '4-02-2018'
            ),
            array(
                'user' => 'Amanda Scott',
                'pnr' => 'YP7PQH',
                'date' => '3-02-2018'
            ),
            array(
                'user' => 'Stacy Paulie',
                'pnr' => '3VZE4H',
                'date' => '31-01-2018'
            ),
            array(
                'user' => 'Jo Tucker',
                'pnr' => 'A646CH',
                'date' => '7-02-2018'
            ),array(
                'user' => 'Sam Thornton',
                'pnr' => '6NW44H',
                'date' => '31-01-2018'
            ),
            array(
                'user' => 'Michael Jon',
                'pnr' => 'ZQVYNH',
                'date' => '4-02-2018'
            ),
            array(
                'user' => 'Ashleigh Andrew',
                'pnr' => '3QI7QH',
                'date' => '8-02-2018'
            ),
            array(
                'user' => 'Tegan Stoke',
                'pnr' => '6842LH',
                'date' => '31-01-2018'
            ),
            array(
                'user' => 'Charlene Taller',
                'pnr' => 'BRDK7H',
                'date' => '2-02-2018'
            ),
            array(
                'user' => 'Sarah Pin',
                'pnr' => '477RLH',
                'date' => '5-02-2018'
            ),
            array(
                'user' => 'Sarah Pin',
                'pnr' => 'GGPVNH',
                'date' => '2-02-2018'
            ),
            array(
                'user' => 'Imogen Tin',
                'pnr' => 'IE6VHH',
                'date' => '5-02-2018'
            ),array(
                'user' => 'James Ball',
                'pnr' => 'ZXTBBH',
                'date' => '9-02-2018'
            ),
            array(
                'user' => 'Charlene Taller',
                'pnr' => 'TPB6YH',
                'date' => '7-02-2018'
            ),
            array(
                'user' => 'Paul Smith',
                'pnr' => 'BFMWJH',
                'date' => '9-02-2018'
            ),
            array(
                'user' => 'Paul Smith',
                'pnr' => 'WWJZYH',
                'date' => '9-02-2018'
            ),
            array(
                'user' => 'Paul Smith',
                'pnr' => '34NGRH',
                'date' => '5-04-2018'
            ),
            array(
                'user' => 'Paul Smith',
                'pnr' => 'STB6YH',
                'date' => '5-07-2018'
            )
        );

        foreach ($data as $item){
            $user = User::whereName($item['user'])->first();

            $sale = App\Sale::create([
                'status'        => 'Pending',
                'pnr'           => $item['pnr'],
                'pnr_creation'  => Carbon\Carbon::parse($item['date']),
                'created_at'    => Carbon\Carbon::now(),
                'store_id'      => $user->store_id,
                'user_id'       => $user->id,
                'territory_id'  => $user->territory_id,
                'state_id'      => $user->state_id,
                'consortium_id' => $user->consortium_id
            ]);

            $activity = App\ActivityFeed::create([
                'module'           => 'Sales',
                'activity_type'    => 'new-sale',
                'activity_message' => 'Sale has been successfully added!',
                'user_id'          => $user->id
            ]);
            $this->command->info('Saving sale... '.$sale->pnr);
        }

    }

    public function generateRandomSales()
    {
        $faker = Faker\Factory::create();


        for ($user_id = 17; $user_id < 67; $user_id++) {

            $now = Carbon\Carbon::now();
            $lastYear = $now->subYear();
            $user = App\User::find($user_id);
            $destination = App\Destination::inRandomOrder()->first();
            for ($i = 0; $i < 10; $i++) {

                $setMonth = $lastYear->addWeek(4);

                for ($j = 0; $j < 5; $j++) {

                    $sale = App\Sale::create([
                        'status'        => 'Valid',
                        'cty'           => $destination->code,
                        'pax'           => rand(1, 9),
                        'pnr'           => $j . 'TDAY' . $i,
                        'stock'         => $i . '35' . $j . '90',
                        'ticket_number' => $i . '869224732095/' . $j,
                        'pnr_creation'  => $setMonth->addDay(1),
                        'created_at'    => $setMonth->addDay(1),
                        'store_id'      => $user->store_id,
                        'user_id'       => $user->id,
                        'territory_id'  => $user->territory_id,
                        'state_id'      => $user->state_id,
                        'consortium_id' => $user->consortium_id
                    ]);

                    $activity = App\ActivityFeed::create([
                        'module'           => 'Sales',
                        'activity_type'    => 'new-sale',
                        'activity_message' => 'Sale has been successfully added!',
                        'user_id'          => $user_id
                    ]);
                }
            }

            $now = Carbon\Carbon::now();
            $lastMonth = $now->subMonth();

            for ($i = 0; $i < 10; $i++) {
                $sale = App\Sale::create([
                    'status'        => 'Valid',
                    'pnr'           => 'DPCYH' . $i,
                    'stock'         => $i . '68239',
                    'ticket_number' => '862161756427/' . $i,
                    'pnr_creation'  => $lastMonth->subDay(1),
                    'pax'           => rand(1, 9),
                    'cty'           => $destination->code,
                    'created_at'    => $lastMonth->subDay(1),
                    'store_id'      => $user->store_id,
                    'user_id'       => $user->id,
                    'territory_id'  => $user->territory_id,
                    'state_id'      => $user->state_id,
                    'consortium_id' => $user->consortium_id
                ]);

                $activity = App\ActivityFeed::create([
                    'module'           => 'Sales',
                    'activity_type'    => 'new-sale',
                    'activity_message' => 'Sale has been successfully added!',
                    'user_id'          => $user_id
                ]);

            }

            $now = Carbon\Carbon::now();

            for ($i = 0; $i < 10; $i++) {

                $sale = App\Sale::create([
                    'status'        => 'Valid',
                    'pnr'           => 'JU6' . $i . 'LH',
                    'stock'         => $i . '234759',
                    'ticket_number' => '0869321450325/' . $i,
                    'pnr_creation'  => $now->subDay(1),
                    'pax'           => rand(1, 9),
                    'cty'           => $destination->code,
                    'created_at'    => $now->subDay(1),
                    'store_id'      => $user->store_id,
                    'user_id'       => $user->id,
                    'territory_id'  => $user->territory_id,
                    'state_id'      => $user->state_id,
                    'consortium_id' => $user->consortium_id
                ]);

                $activity = App\ActivityFeed::create([
                    'module'           => 'Sales',
                    'activity_type'    => 'new-sale',
                    'activity_message' => 'Sale has been successfully added!',
                    'user_id'          => $user_id
                ]);

            }
        }
    }
}

