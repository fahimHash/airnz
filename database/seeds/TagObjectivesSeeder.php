<?php

use App\TagObjective;
use Illuminate\Database\Seeder;

class TagObjectivesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = [
            'Conversion',
            'Training',
            'Engagement',
            'Profile',
        ];
        foreach ($type as $tag) {
            TagObjective::create([
                'name' => $tag
            ]);
        }
    }
}
