<?php
use App\Page;
use Illuminate\Database\Seeder;

class CreateAboutUsPage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = Page::create([
            'title' => 'About Us',
            'summary' => 'About Us',
            'slug' => 'about-us',
            'type' => 'about-us',
        ]);
    }
}
