<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //$this->call(RolesTableSeeder::class);
        //$this->call(PermissionsTableSeeder::class);
        //$this->call(PermissionsRolesTableSeeder::class);
        //$this->call(StatesTableSeeder::class);
        //$this->call(DestinationSeeder::class);
        //$this->call(ConsortiumTableSeeder::class);
        //$this->call(AgencyTableSeeder::class);
        //$this->call(TerritoryTableSeeder::class);
        //$this->call(StoreTableSeeder::class);

        //$this->call(TagsSeeder::class);
        //$this->call(TagTypesSeeder::class);
        //$this->call(TagObjectivesSeeder::class);
        //$this->call(TagChannelsSeeder::class);
        //$this->call(TagTrainingsSeeder::class);
        //$this->call(CreateTcPage::class);
        //$this->call(RegistrationChanges::class);
        //$this->call(CreatePrivacyPage::class);
        //$this->call(UpdateTcPage::class);
        //$this->call(AddOtherOption::class);

        //$this->call(AustraliaDestinations::class);
        //$this->call(CreateAboutUsPage::class);
        $this->call(UsersTableSeeder::class);
        $this->call(SalesTableSeeder::class);
    }
}
