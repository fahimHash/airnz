<?php

use App\Agency;
use App\Consortium;
use App\Job;
use App\State;
use App\Store;
use App\Territory;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StoreTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bdm = User::whereName('Stella Hritis')->first();
        $territory = Territory::find($bdm->manageTerritory->id);

        $consortium = Consortium::whereName('Flight Centre Travel Group')->first();
        $date = \Carbon\Carbon::now();

        Agency::firstOrCreate([
            'name' => 'My Adventure Travel',
            'label' => '',
            'type' => 'Retail',
            'fk_id_consortiums' => $consortium->id,
            'created_at' => $date, 'updated_at' => $date
        ]);
        Agency::firstOrCreate([
            'name' => 'Flight Centre - Sports & Events',
            'label' => '',
            'type' => 'Wholesale',
            'fk_id_consortiums' => $consortium->id,
            'created_at' => $date, 'updated_at' => $date
        ]);
        Agency::firstOrCreate([
            'name' => 'FCBT',
            'label' => '',
            'type' => 'Corporate',
            'fk_id_consortiums' => $consortium->id,
            'created_at' => $date, 'updated_at' => $date
        ]);
        $this->command->info('Importing Excel..');

        Excel::selectSheets('Full List')->load('storage/app/app/Flight Centre.xlsx', function ($reader) use (&$consortium, &$bdm, &$territory) {
            $this->command->info('Reading Excel..');
            $reader->each(function ($row) use (&$consortium, &$bdm, &$territory) {
                $this->command->info('Reading Row..');
                if ($row->store_name) {
                    $this->command->info('Checking store...'.$row->store_name);
                    $this->command->info('Checking PCC...'.$row->sabre_pseudo_code);
                    $validate = Validator::make($row->toArray(), [
                        'sabre_pseudo_code' => 'required|unique:stores,pcc'
                    ]);
                    if (!$validate->fails()) {
                        $this->command->info('Creating store... '.$row->store_name);
                        $this->command->info('Creating PCC... '.$row->sabre_pseudo_code);
                        $store = Store::firstOrCreate([
                            'name'     => $row->store_name,
                            'pcc'      => $row->sabre_pseudo_code,
                            'address1' => $row->address_1,
                            'address2' => $row->address_2,
                            'address3' => $row->address_3,
                            'suburb'   => $row->suburb,
                            'postcode' => $row->postcode
                        ]);
                        $agency = Agency::whereName($row->brand_name)->first();
                        $state = State::whereCode($row->state)->first();
                        $store->consortium()->associate($consortium);
                        $store->agency()->associate($agency);
                        $store->state()->associate($state);
                        $store->save();
                        $this->command->info('Saving store... '.$store->name);
                        /*$bdm = User::whereName($row->bdm_first_name . ' ' . $row->bdm_last_name)->first();
                        if ($bdm->manageTerritory) {
                            $territory = Territory::find($bdm->manageTerritory->id);
                        } else {
                            $territory = new Territory([
                                'name'        => $bdm->name . '\'s Territory',
                                'sale_target' => 300
                            ]);
                            $territory->save();
                            $territory->manager()->associate($bdm)->save();
                        }
                        $territory = Territory::find($bdm->manageTerritory->id);*/
                        $store->bdm()->associate($bdm);
                        $store->territory()->associate($territory);
                        $store->save();
                        $this->command->info('Sync Consortium '.$consortium->id);
                        $territory->consortiums()->syncWithoutDetaching($consortium->id);
                        $this->command->info('Sync agencies '.$agency->id);
                        $territory->agencies()->syncWithoutDetaching($agency->id);
                        $territory->states()->syncWithoutDetaching($state->id);
                        $territory->save();
                        $this->command->info('Saving store... '.$territory->name);
                        /*
                        if ($count != 67) {
                            $user = User::find($count);
                            $user->workplace()->associate($store);
                            $user->consortium()->associate($store->consortium()->first());
                            $user->territory()->associate($store->territory()->first());
                            $user->state()->associate($store->state()->first());
                            $user->sale_target = $store->territory()->first()->sale_target;
                            $user->save();

                            Job::create([
                                'startDate' => Carbon::today()->toDateString(),
                                'active'    => 1,
                                'position'  => 'Travel Consultant',
                                'user_id'   => $user->id,
                                'store_id'  => $store->id
                            ]);
                            $count++;
                        }
                        */
                    }else{
                        $this->command->info('Validation Failed '.$row->store_name);
                    }
                }
            });
        });
    }
}
