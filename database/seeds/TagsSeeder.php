<?php

use App\Tag;
use Illuminate\Database\Seeder;

class TagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $core_product = [
            'Core Product',
            'Skycouch',
            'Premium Economy',
            'Business Premier',
            'Economy',
            'Premium Experience',
            'Airband',
            'Coffee App',
            'Lounges',
            'Entertainment',
            'Food & Wine',
            'Airports',
            'Check-In',
            'Baggage',
            'Corporate Traveller',
            'Travelling Famillies'
        ];
        $trans_tasman = [
            'Trans Tasman',
            'Virgin Australia Alliance',
            'Seats to Suit TT',
        ];
        $domestic_nz = [
            'Domestic New Zealand',
            'Auckland',
            'Wellington',
            'Christchurch',
            'Queenstown',
            'Dunedin',
            'Northland',
            'Tauranga',
        ];
        $north_america = [
            'USA',
            'Los Angeles',
            'San Francisco',
            'Houston',
            'Canada',
            'Vancouver',
            'Deeper USA',
        ];
        $south_america = [
            'South America',
            'Buenos Aires',
            'Brazil',
            'Peru',
            'Deeper SAM',
        ];
        $pacific_islands = [
            'Pacific Islands',
            'Seats to Suit PI',
            'Hawaii',
            'Cook Islands',
            'Norfolk Island',
        ];
        $network = [
            'Network/Booking',
            'Fares',
            'Seasonal Services',
            'GDS',
            'Ancillaries',
            'Long Haul Fares',
            'Star Alliance',
        ];
        $brand = [
            'Brand',
            'Business Update',
            'Sustainability',
            'Sponsorship',
            'Sale',
            'Event',
        ];
        $loyalty = [
            'Loyalty',
            'Airpoints',
        ];
        $support = [
            'Support',
            'Agency Support',
            'BDM Support',
            'Marketing Support',
            'Agent Discount',
            'Policies',
            'Transit',
        ];
        $ancillaries = [
            'Ancillaries',
            'OneUp',
        ];
        foreach ($core_product as $tag) {
            Tag::create([
                'parent_group' => 'Core Product',
                'name'         => $tag
            ]);
        }
        foreach ($trans_tasman as $tag) {
            Tag::create([
                'parent_group' => 'Trans Tasman',
                'name'         => $tag
            ]);
        }
        foreach ($domestic_nz as $tag) {
            Tag::create([
                'parent_group' => 'Domestic New Zealand',
                'name'         => $tag
            ]);
        }
        foreach ($north_america as $tag) {
            Tag::create([
                'parent_group' => 'North America',
                'name'         => $tag
            ]);
        }
        foreach ($south_america as $tag) {
            Tag::create([
                'parent_group' => 'South America',
                'name'         => $tag
            ]);
        }
        foreach ($pacific_islands as $tag) {
            Tag::create([
                'parent_group' => 'Pacific Islands',
                'name'         => $tag
            ]);
        }
        foreach ($network as $tag) {
            Tag::create([
                'parent_group' => 'Network',
                'name'         => $tag
            ]);
        }
        foreach ($brand as $tag) {
            Tag::create([
                'parent_group' => 'Brand',
                'name'         => $tag
            ]);
        }
        foreach ($loyalty as $tag) {
            Tag::create([
                'parent_group' => 'Loyalty',
                'name'         => $tag
            ]);
        }
        foreach ($support as $tag) {
            Tag::create([
                'parent_group' => 'Support',
                'name'         => $tag
            ]);
        }
        foreach ($ancillaries as $tag) {
            Tag::create([
                'parent_group' => 'Ancillaries',
                'name'         => $tag
            ]);
        }
    }
}
