<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = \Carbon\Carbon::now();
        $permissions = [
            [
                'name'  => 'access_backend',
                'label' => 'Access Backend',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'manage_user',
                'label' => 'Manage Agent',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'manage_company',
                'label' => 'Manage Company',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'manage_state',
                'label' => 'Manage Territory',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'access_frontend',
                'label' => 'Access Frontend',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'role_permission',
                'label' => 'Manage Role & Permission',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'manage_adv_user_settings',
                'label' => 'Update Agent Status, Consortium & Territory',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'manage_consortium',
                'label' => 'Manage Consortium',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'manage_campaign',
                'label' => 'Manage Campaign',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ]
        ];
        DB::table('permissions')->insert($permissions);
    }
}
