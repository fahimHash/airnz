<?php

use Illuminate\Database\Seeder;

class DestinationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $destinations = [
            ['name' => 'Houston', 'code' => 'IAH', 'latitude' => '29.704067', 'longitude' => '-95.620361'],
            ['name' => 'San Francisco', 'code' => 'SFO', 'latitude' => '37.615223', 'longitude' => '-122.389977'],
            ['name' => 'Los Angeles LAX', 'code' => 'LAX', 'latitude' => '33.942791', 'longitude' => '-118.410042'],
            ['name' => 'Vancouver', 'code' => 'YVR', 'latitude' => '49.246292', 'longitude' => '-123.116226'],
            ['name' => 'Buenos Aires', 'code' => 'EZE', 'latitude' => '-34.814908', 'longitude' => '-58.534675'],
            ['name' => 'Auckland', 'code' => 'AKL', 'latitude' => '-36.999261', 'longitude' => '174.787905'],
            ['name' => 'Wellington', 'code' => 'WLG', 'latitude' => '-41.327594', 'longitude' => '174.807598'],
            ['name' => 'Christchurch', 'code' => 'CHC', 'latitude' => '-43.486398', 'longitude' => '172.53687'],
            ['name' => 'Dunedin', 'code' => 'DUD', 'latitude' => '-45.925862', 'longitude' => '170.202168'],
            ['name' => 'Queenstown', 'code' => 'ZQN', 'latitude' => '-45.020972', 'longitude' => '168.739923'],
            ['name' => 'Cook Islands', 'code' => 'RAR', 'latitude' => '-21.200124', 'longitude' => '-159.797244'],
            ['name' => 'Norfolk Island', 'code' => 'NLK', 'latitude' => '-29.041673', 'longitude' => '167.939374']
        ];
        DB::table('destinations')->insert($destinations);
    }
}
