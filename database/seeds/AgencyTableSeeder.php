<?php

use Illuminate\Database\Seeder;

class AgencyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = \Carbon\Carbon::now();
        $data = [
            [
                'name' => 'Corporate Traveller',
                'label' => '',
                'type' => 'Corporate',
                'fk_id_consortiums' => 4,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name' => 'Cruise About',
                'label' => 'Retail',
                'type' => 'Corporate',
                'fk_id_consortiums' => 4,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name' => 'Escape Travel',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 4,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'FCM',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 4,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'Flight Centre',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 4,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'Student Flights',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 4,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'Travel Associates',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 4,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'Infinity',
                'label' => '',
                'type' => 'Wholesale',
                'fk_id_consortiums' => 4,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => '(AUS) HELLOWORLD BRANDED GROUP',
                'label' => 'Helloworld',
                'type' => 'Retail',
                'fk_id_consortiums' => 5,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name' => '(AUS) HELLOWORLD ASSOCIATE GROUP',
                'label' => 'Helloworld',
                'type' => 'Retail',
                'fk_id_consortiums' => 5,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name' => '(AUS) HELLOWORLD FOR BUSINESS GROUP',
                'label' => 'Helloworld For Business',
                'type' => 'TMC',
                'fk_id_consortiums' => 5,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'Viva Holidays',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 5,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'QBT',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 5,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => '(AUS) TRAVELLERS CHOICE AGENTS',
                'label' => 'Travellers Choice Agents',
                'type' => 'Retail',
                'fk_id_consortiums' => 5,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'Travelscene American Express Franchise',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 5,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'Magellan',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 7,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'MTA',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 8,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'STA',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 12,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'American Express',
                'label' => '',
                'type' => 'Corporate',
                'fk_id_consortiums' => 14,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'ATPI',
                'label' => '',
                'type' => 'Corporate',
                'fk_id_consortiums' => 15,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'Carlson Wagonlit Travel',
                'label' => '',
                'type' => 'Corporate',
                'fk_id_consortiums' => 17,
                'created_at' => $date, 'updated_at' => $date
            ],[
                'name' => 'HRG',
                'label' => '',
                'type' => 'Retail',
                'fk_id_consortiums' => 18,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name' => '(AUS) MY TRAVEL GROUP',
                'label' => 'My Travel Group',
                'type' => 'Retail',
                'fk_id_consortiums' => 5,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name' => 'Independent/Other',
                'label' => 'Independent/Other',
                'type' => 'Retail',
                'fk_id_consortiums' => 6,
                'created_at' => $date, 'updated_at' => $date
            ]
        ];
        DB::table('agencies')->insert($data);
    }
}
