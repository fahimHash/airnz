<?php

use App\Page;
use Illuminate\Database\Seeder;

class CreateTcPage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = Page::create([
            'title' => 'Terms and conditions',
            'summary' => 'Terms and conditions',
            'slug' => 'terms-and-conditions',
            'type' => 'terms-and-conditions',
        ]);
    }
}
