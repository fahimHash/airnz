<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = \Carbon\Carbon::now();
        $states = [
            [
                'name'       => 'Victoria',
                'code'       => 'VIC',
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'       => 'New South Wales',
                'code'       => 'NSW',
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'       => 'Australian Capital Territory',
                'code'       => 'ACT',
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'       => 'Queensland',
                'code'       => 'QLD',
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'       => 'Northern Territory',
                'code'       => 'NT',
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'       => 'Western Australia',
                'code'       => 'WA',
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'       => 'South Australia',
                'code'       => 'SA',
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'       => 'Tasmania',
                'code'       => 'TAS',
                'created_at' => $date, 'updated_at' => $date
            ]
        ];
        DB::table('states')->insert($states);
    }
}
