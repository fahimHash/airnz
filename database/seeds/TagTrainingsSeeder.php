<?php

use App\TagTraining;
use Illuminate\Database\Seeder;

class TagTrainingsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $training = [
            'Interactive_Active',
            'Interactive_Passive',
            'Static_Active',
            'Static_Passive',
        ];
        foreach ($training as $tag) {
            TagTraining::create([
                'name' => $tag
            ]);
        }
    }
}
