<?php

use Illuminate\Database\Seeder;

class TerritoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $territory = App\Territory::create([
            'name' => 'Agency Support\'s Territory',
            'sale_target' => 300,
            'default' => true
        ]);
        $territory->manager()->associate(App\User::whereAgentId('AGT_Agency_Support')->first())->save();

    }
}
