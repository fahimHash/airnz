<?php

use App\TagChannel;
use Illuminate\Database\Seeder;

class TagChannelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = [
            'Famil',
            'Module',
            'Information',
            'Survey',
            'Famil',
            'Download',
            'Podcast',
            'Recall Quiz',
            'AirNZ Event',
            'Partner Event',
            'Webinar',
            'Incentive',
            'Pop Quiz',
            'Inspection',
            'Competition',
        ];
        foreach ($type as $tag) {
            TagChannel::create([
                'name' => $tag
            ]);
        }
    }
}
