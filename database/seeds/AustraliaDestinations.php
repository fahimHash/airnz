<?php

use Illuminate\Database\Seeder;

class AustraliaDestinations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $destinations = [
            ['name' => 'Brisbane', 'code' => 'BNE', 'latitude' => '-27.394015', 'longitude' => '153.122308'],
            ['name' => 'Sunshine Coast', 'code' => 'MCY', 'latitude' => '-26.603976', 'longitude' => '153.089948'],
            ['name' => 'Gold Coast', 'code' => 'OOlL', 'latitude' => '-26.603976', 'longitude' => '153.089948'],
            ['name' => 'Sydney', 'code' => 'SYD', 'latitude' => '-33.940368', 'longitude' => '151.175352'],
            ['name' => 'Melbourne', 'code' => 'MEL', 'latitude' => '-37.669352', 'longitude' => '144.840888'],
            ['name' => 'Adelaide', 'code' => 'ADL', 'latitude' => '-34.948371', 'longitude' => '138.530035'],
            ['name' => 'Cairns', 'code' => 'CNS', 'latitude' => '-16.878307', 'longitude' => '145.749989'],
            ['name' => 'Brasilia International Airport', 'code' => 'BSB', 'latitude' => '-15.869716', 'longitude' => '-47.918533'],
            ['name' => 'Jorge Chavez International Airport', 'code' => 'LIM', 'latitude' => '-12.025543', 'longitude' => '-77.110341'],
        ];
        DB::table('destinations')->insert($destinations);
    }
}
