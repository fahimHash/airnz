<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Role;
use App\Permission;

class PermissionsRolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::first()->permissions()->attach([1, 2, 3, 4, 6, 7, 8, 9]); //admin
        Role::find(2)->permissions()->attach([1, 2, 3, 4, 7, 8, 9]);    //airnz-super-user
        Role::find(3)->permissions()->attach([1, 2, 3, 4, 7, 8, 9]);    //national-account-manager
        Role::find(4)->permissions()->attach([1, 2, 3, 4, 7, 8, 9]);    //state-manager
        Role::find(5)->permissions()->attach([1, 2, 3, 7, 8, 9]);       //bdm
        Role::find(6)->permissions()->attach(5);                        //mover
        Role::find(7)->permissions()->attach(5);                        //minor
    }
}
