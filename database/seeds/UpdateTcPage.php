<?php

use App\Page;
use Illuminate\Database\Seeder;

class UpdateTcPage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Page::where('type','=','terms-and-conditions')->update(['message' => '<h1>Air New Zealand - Duo Programme - Terms and Conditions</h1>

<h3>1 - Summary</h3>

<p>The Duo Programme (Programme) is a membership programme offered by Air&nbsp;New&nbsp;Zealand Limited<br />
(Air NZ,&nbsp;we) to Australian registered Travel Agents (Member,&nbsp;you).</p>

<p>By registering for membership, you agree to these terms and conditions (Terms), which govern your membership in the Programme.</p>

<p>We draw your attention to the following important provisions:</p>

<ul>
	<li>We will collect, use and disclose your personal information (paragraph 3);</li>
	<li>You have certain obligations as a duo Member (paragraph 2);</li>
	<li>We and our commercial partners may send you emails and other communications that relate to rewards, benefits or other offers tin connection with the Programme (paragraph 4); &nbsp;</li>
	<li>We may change the benefits and rewards offered under the Programme from time to time (paragraph 9);</li>
	<li>We may vary these Terms from time to time (paragraph 28);</li>
	<li>We limit our liability to you in respect of your duo Membership (paragraph 30);</li>
	<li>In certain limited circumstances we may terminate your duo Membership (paragraph 29);</li>
</ul>

<h3>2 - Your Obligations</h3>

<p>In order that we can administer and provide the Duo Rewards under this Programme, You will need to update your address or contact details if they change.</p>

<p>Please keep your user name, log in, and password details for your online access to the Duo Programme secure.</p>

<p>The Duo Programme is not linked to any airline booking system. &nbsp;You will need to log in to enter your Flight Bookings or log any Training so that we can validate your accrual of Duo Rewards.</p>

<p>We will monitor your account to ensure that You are logging Flight Bookings and Training undertaken by You. &nbsp;We may suspend your Membership if we have a reasonable basis to believe that you are logging Flight Bookings or Training undertaken by any other person.</p>

<p>You may log Flight Bookings and Training from [Fixed date TBC] up to 6 months following the date of that Flight Booking being issued or Training being completed. &nbsp;Any Flight Bookings or Training logged after that point may not be eligible for Duo Rewards.</p>

<h3>3 - Privacy Policy</h3>

<p>By registering for Membership, you authorise Air NZ to collect, use and disclose your personal information in accordance with these Terms.</p>

<p>We will collect personal information about your Membership and participation in the Programme. This includes the information you provide on registration; and any other information that you provide during the course of Membership, including any Flight Bookings and Training that you log. &nbsp;We may use your personal information to verify those Flight Bookings and Training so that we can correctly allocate your Duo Rewards.</p>

<p>We may also collect personal information about you from our Programme Partners, including those partners who participate in the offer of Duo Rewards, or who otherwise offer goods or services related to the Programme, including information about your transaction and purchase history.</p>

<p>We may collect personal information about you from our third party business partners to assist in identifying suitable offers and promotions from us and our third party business partners that you might be interested in.</p>

<p>We may collect, hold, use and disclose your personal information (including the personal information that you provide to us and that we obtain from third parties) to administer the Programme</p>

<p>We may disclose your personal information &nbsp;to our third party business partners including marketing and advertising companies, data processing companies and data analysis companies to conduct sales, marketing and advertising research and analysis on our behalf so that we can: (i) better understand your preferences, (ii) carry out business improvement analysis, (iii) present you with suitable offers that you might be interested in, and for any of the other purpose directly related to the Programme.</p>

<h3>4 - Email Notifications</h3>

<p>You acknowledge and agree that by providing us with your email address we may send you commercial electronic messages including but not limited to Programme communications and &nbsp;Airnotes. You also consent to us sending you communications or offers from our Duo Partners.</p>

<p>You may unsubscribe to our commercial emails at any time by clicking on the &lsquo;unsubscribe link&rsquo; in our emails. You can also update your email communication preferences on the &lsquo;My Profile&rsquo; page of our website, after signing into your online Air NZ Account.</p>

<h3>5 - Application for Membership</h3>

<p>We reserve the right to accept or decline any application for Membership. If your application is declined, we will inform you in writing.</p>

<h3>6 - Duo Member Benefits</h3>

<p>Duo Members are able to earn Duo Rewards and enjoy a range of Member benefits.</p>

<p>Duo Rewards include online Badges which may include a value item. &nbsp;These value items will be outlined in specific achievement and/or campaign Terms and Conditions. &nbsp;Specific Terms and Conditions will apply when a flight prize is awarded and will be outlined within each specific campaign.</p>

<h3>7 - Qualifying Flight Booking / Training</h3>

<p>You will earn Duo Rewards on personal achievements where we are able to validate your Flight Bookings and Training or as set out within specific campaign Terms and Conditions.</p>

<p>The allocation of Duo Rewards for validated Flight Bookings and Training may vary &nbsp;depending on:</p>

<ul>
	<li>Whether the Flight is an Air NZ operated Flight or a Partner Airline Flight, the Airpoints&nbsp;Dollars and Status Points earning rates for the relevant Partner Airline, the booking class, the product type of the particular booking and the date of travel;</li>
	<li>The particular training module or other online education programme that you complete.</li>
</ul>

<h3>8 - Duo Rewards Expiry</h3>

<p>The expiry date in which to redeem your Duo Rewards may vary. &nbsp;AirNZ will provide will reasonable time to redeem your duo Reward from the date in which it is allocated to you. &nbsp;Expiry dates will be notified to you.</p>

<h3>9 - Tax</h3>

<p>Air NZ makes no representation about your tax liability, including any FBT, as a result of your participation in the Programme, including your use or redemption of any Duo Reward.</p>

<h3>10 - Changes to these Terms</h3>

<p>We may make changes to these Terms from time to time. When we make changes, we will update these Terms on our website. We will also specify the date of the last update.</p>

<p>The changes we may make to these Terms include but are not limited to changing: the types of Memberships available; the requirements relating to earning Duo Rewards, the nature of Duo Benefits and third party Duo Partners. &nbsp;</p>

<h3>11 - Termination</h3>

<p>You may terminate your Membership at any time by logging into your Duo account and selecting &ldquo;close your account&rdquo; under Account settings.</p>

<p>When you terminate your Membership (or where we terminate your Membership) &nbsp;any Duo Rewards that have been earned but not redeemed prior to termination will be forfeited at the date of that termination. &nbsp;</p>

<p>Air NZ may terminate or suspend your Membership or cancel or suspend your right to access to your Duo Membership account by notice in writing if we reasonably believe that you have:</p>

<ul>
	<li>Abused, misused or obtained by any misrepresentation any Duo Rewards or Duo Benefits ;</li>
	<li>Behaved in a manner which Air NZ reasonably considers to be unacceptable while using any Duo Rewards or other facilities, services, or arrangements provided by or in connection with the Programme;</li>
	<li>Acted in any way that is detrimental to the interests of Air NZ or the Programme; or</li>
	<li>Provided incorrect or misleading information to Air NZ in connection with your Membership.</li>
</ul>

<p>If we decide to discontinue the Programme we may terminate your duo Membership by giving you three month&rsquo;s written notice.</p>

<p>If we cease to operate as an airline, we may terminate duo Membership any time without notice.</p>

<h3>12 - Limited Liability</h3>

<p>Subject to applicable consumer protection laws and to the fullest extent permitted by law, you agree that Air NZ will have no liability (in negligence, tort, breach of contract, or otherwise) for any loss or damage (including direct or indirect loss, or special or consequential loss or damage) arising out of or in connection with:</p>

<ul>
	<li>our variation to these Terms from time to time;</li>
	<li>our variation of Duo Rewards from time to time;</li>
	<li>our termination of your Membership in accordance with these Terms;</li>
	<li>the use of (or inability to use), the availability of, and or the lack of suitability of any Duo Reward for you or anyone else;</li>
	<li>the Duo Rewards that you forfeit upon termination of your duo Membership;</li>
	<li>the availability of Duo Rewards, or Duo Benefits;</li>
	<li>our failure or delay to provide Duo Rewards due to force majeure or other events beyond our control;</li>
	<li>third party websites, applications, products and services;</li>
</ul>

<p>Nothing in these Terms shall exclude or limit our liability for death or personal injury caused by negligence or for fraud and fraudulent misrepresentation.</p>

<h3>13 - Laws of New South Wales, Australia</h3>

<p>These Terms shall be governed by and construed in accordance with the laws of New South Wales, Australia. You and Air NZ consent to the exclusive jurisdiction of the courts of New South Wales in connection with any legal action that may arise in relation to these Terms, or the Programme .</p>

<h3>14 - Miscellaneous</h3>

<p>If any term or provision of these Terms is held to be illegal, invalid or unenforceable it will be severed from the Terms without affecting the legality, validity or enforceability of the remaining provisions.</p>

<p>Additional information about the Programme may be found on duo.airnzagent.com.au &nbsp;To the extent of any inconsistency, these Terms, as amended from time to time, shall prevail.</p>

<p>We reserve the right to audit each Duo Programme account. We reserve the right to amend and correct the Flight Booking and Training data (and any Duo Rewards accrued to your Duo Programme account in relation to the same) if we identify any inaccuracy (due to our mistake or otherwise)</p>

<h3>15 - Definitions</h3>

<p>In these Terms:</p>

<p>The terms used in these Terms and Conditions have the following meanings:</p>

<p>Duo Rewards means Rewards associated with your participation in the duo Programme that we will change and update from time to time and may include:</p>

<ul>
	<li>Accept AirNZ conditions of carriage</li>
	<li>Upgrades</li>
	<li>Other AirNZ products</li>
	<li>Koru Membership</li>
	<li>Airpoints Status / Dollar Credits</li>
</ul>

<p>Duo Partner&nbsp;means any company, business or organisation that offers products, services or other benefits that that a Member may redeem using their Duo Rewards. &nbsp;</p>

<p>Partner Airline&nbsp;means an airline (excluding Air&nbsp;New&nbsp;Zealand) that sells Qualifying Flights and or offers Reward Travel to Airpoints Members.</p>

<p>Terms&nbsp;means these terms and conditions between Air&nbsp;New&nbsp;Zealand Limited and you, which govern membership to the Programme, as may be updated from time to time.</p>
']);
    }
}
