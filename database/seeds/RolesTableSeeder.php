<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $date = \Carbon\Carbon::now();
        $roles = [
            [
                'name'  => 'admin',
                'label' => 'Super Admin',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'airnz-super-user',
                'label' => 'AirNZ Super User',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'national-account-manager',
                'label' => 'National Account Manager',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'state-manager',
                'label' => 'State Manager',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'territory',
                'label' => 'BDM',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'mover',
                'label' => 'Mover',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ],
            [
                'name'  => 'minor',
                'label' => 'Minor',
                'default' => true,
                'created_at' => $date, 'updated_at' => $date
            ]
        ];
        DB::table('roles')->insert($roles);
    }
}
