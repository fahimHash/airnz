<?php

use App\TagType;
use Illuminate\Database\Seeder;

class TagTypesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $type = [
            'Campaign',
            'Personal Challenge',
            'General Info',
        ];
        foreach ($type as $tag) {
            TagType::create([
                'name' => $tag
            ]);
        }
    }
}
