<?php

use App\Page;
use Illuminate\Database\Seeder;

class CreatePrivacyPage extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = Page::create([
            'title' => 'Privacy Policy',
            'summary' => 'Privacy Policy',
            'slug' => 'privacy-policy',
            'type' => 'privacy-policy',
            'publish' => 1,
            'message' => '<h1>Air New Zealand - Duo Programme - Terms and Conditions</h1><h3>Lorem ipsum dolor sit amet.</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex reprehenderit, officiis obcaecati officia, totam qui.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex reprehenderit, officiis obcaecati officia, totam qui.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex reprehenderit, officiis obcaecati officia, totam qui.</p><h3>Lorem ipsum dolor sit amet.</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex reprehenderit, officiis obcaecati officia, totam qui.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex reprehenderit, officiis obcaecati officia, totam qui.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex reprehenderit, officiis obcaecati officia, totam qui.</p><h3>Lorem ipsum dolor sit amet.</h3><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex reprehenderit, officiis obcaecati officia, totam qui.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex reprehenderit, officiis obcaecati officia, totam qui.</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex reprehenderit, officiis obcaecati officia, totam qui.</p>'
        ]);

    }
}
