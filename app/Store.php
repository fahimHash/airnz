<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Store extends Model
{
    use SoftDeletes;

    protected $table = 'stores';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'pcc', 'address1', 'address2' , 'address3', 'postcode', 'suburb'
    ];

    //store belongsTo to a consortium
    public function consortium()
    {
        return $this->belongsTo('App\Consortium','store_consortium_id');
    }

    //store belongsTo to a agency
    public function agency()
    {
        return $this->belongsTo('App\Agency','store_agency_id');
    }

    public function agencyOnlyLabel()
    {
        return $this->agency()->select('id','label');
    }

    //store belongsTo to a state
    public function state()
    {
        return $this->belongsTo('App\State','store_state_id');
    }

    //store belongsTo to a territory
    public function territory()
    {
        return $this->belongsTo('App\Territory','store_territory_id');
    }

    //store belongsTo to a BDM
    public function bdm()
    {
        return $this->belongsTo('App\User','store_bdm_id');
    }
}
