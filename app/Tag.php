<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * Get all of the campaigns that are assigned this tag.
     */
    public function campaigns()
    {
        return $this->morphedByMany(Campaign::class, 'taggable');
    }
}
