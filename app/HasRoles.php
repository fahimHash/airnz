<?php

namespace App;

use App\Role;
use App\Permission;

trait HasRoles
{
    // A user may have many roles.

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'role_user', 'user_id', 'role_id');
    }

    //Assign a given role to the user.
    public function assignRole($role)
    {
        return $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }

    // Determine if user has role
    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }
        return !!$role->intersect($this->roles)->count();
    }

    // Determine if user has given Permission
    public function hasPermission(Permission $permission)
    {
        return $this->hasRole($permission->roles);
    }

    public function isAdmin()
    {
        return $this->hasRole(Role::ROLE_ADMIN);
    }

    public function isSuperUser()
    {
        return $this->hasRole(Role::ROLE_SUPER_USER);
    }

    public function isStateManager()
    {
        return $this->hasRole(Role::ROLE_STATE_MANAGER);
    }

    public function isBDM()
    {
        return $this->hasRole(Role::ROLE_BDM);
    }

    public function isNationalManager()
    {
        return $this->hasRole(Role::ROLE_NATIONAL_MANAGER);
    }


}
