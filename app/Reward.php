<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Reward extends Model
{
    use SoftDeletes;
    // LINK THIS MODEL TO OUR DATABASE TABLE
    protected $table = 'rewards';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'limit', 'endDate','type','label','value'
    ];

    public function campaigns(){
        return $this->hasMany('App\Campaign','reward_id');
    }
    public function agentsEligibleForReward(){
        return $this->hasManyThrough('App\CampaignUser', 'App\Campaign',
            'reward_id', 'campaign_id', 'id')->where('joined', '=', true);
    }
}
