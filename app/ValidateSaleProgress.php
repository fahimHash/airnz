<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ValidateSaleProgress extends Model
{
  //Model keeps track of uploaded file for validating sales
  protected $table = 'validate_sale_progress';
  protected $guarded = []; //this will give us the ability to mass assign properties to the model
}
