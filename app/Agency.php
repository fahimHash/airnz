<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Agency extends Model
{
    /** MASS ASSIGNMENT
     *  define which attributes are mass assignable (for security)
     *  we only want these 8 attributes able to be filled
     */

    use SoftDeletes;

    // LINK THIS MODEL TO OUR DATABASE TABLE
    protected $table = 'agencies';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'type', 'active'
    ];

    //agency belongsTo to a consortium
    public function consortium()
    {
        return $this->belongsTo('App\Consortium', 'fk_id_consortiums');
    }

    /**
     * A agency may have many agent across all branch who are working or worked with the agency
     */
    public function agents()
    {
        return $this->hasMany('App\Job', 'agency_id');
    }

    /**
     * A agency may have many agent across all branch who are actively working here
     */
    public function activeAgents()
    {
        return $this->hasMany('App\Job', 'agency_id')->where('active', '=', 1);
    }

    public function territory()
    {
        return $this->belongsToMany(Territory::class, 'agency_territory', 'agency_id', 'territory_id');
    }

    public function state()
    {
        return $this->belongsToMany(State::class, 'agency_state', 'agency_id', 'state_id');
    }

    public function scopeByTerritoryId($query, $territory){
        return $query->whereHas('territory', function ($query) use ($territory){
            $query->where('territories.id', $territory);
        });
    }

    public function stores()
    {
        return $this->hasMany(Store::class, 'store_agency_id');
    }
}
