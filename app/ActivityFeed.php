<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivityFeed extends Model
{
    public $table = "activity_feed";

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'module',
        'activity_type',
        'activity_message',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
