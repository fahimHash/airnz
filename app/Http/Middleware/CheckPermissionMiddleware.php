<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Redirect;

class CheckPermissionMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next, $permission)
    {
        if (!$request->user()->can($permission)) {
            // withDanger refers to error message layout in view(frontend.layout.message)
            if (auth()->user()->can('access_backend')) {
                return redirect('/backend')->withDanger('You are not authorized to access this resource');
            }else{
                return redirect('/home')->withDanger('You are not authorized to access this resource');
            }
        }
        return $next($request);
    }
}
