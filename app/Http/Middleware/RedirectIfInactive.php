<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\Exceptions\GeneralException;

class RedirectIfInactive
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $user = Auth::guard('web')->user() ) {
            if ($user->active == 0){
              Auth::logout();
              return redirect('/login')->withSuccess('Thanks for signing up! Please check your email.');
            }
        }
        return $next($request);
    }
}
