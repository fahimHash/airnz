<?php

namespace App\Http\Controllers\Auth;

use App\Mail\ConfirmationEmail;
use App\Mail\ResetEmailRequest;
use App\Notifications\ResetEmail;
use App\Role;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mail;
use function str_random;
use Validator;

class ResetEmailController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function checkCredentials(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phoneNumber' => 'required|numeric|exists:users,phoneNumber',
            'email'       => 'required|email|unique:users,email'
        ]);
        if ($validator->fails()) {
            $validator->addRules([
                'phoneNumber' => 'unique:users,phoneNumber'
            ]);
        }
        //contact number matched
        $user = User::wherePhonenumber($request->input('phoneNumber'))->first();
        if ($user) {
            $user->update([
                'confirmation_code' => str_random(20),
                'active' => false,
                'email' => $request->input('email')
            ]);
            $super = User::byRole(Role::ROLE_SUPER_USER)->first();
            $super->notify(new ResetEmail($request->input('phoneNumber'), $request->input('email')));
            Mail::to($user)->send(new ConfirmationEmail($user));
            return back()->with('status2',['user exist']);
        } else {
            $super = User::byRole(Role::ROLE_SUPER_USER)->first();
            $super->notify(new ResetEmail($request->input('phoneNumber'), $request->input('email')));
            Mail::to($super)
                ->send(new ResetEmailRequest($request->input('phoneNumber'),  $request->input('email')));
            return back()->with('status2',['user does not exist']);
        }
    }
}
