<?php

namespace App\Http\Controllers\Auth;

use App\Exceptions\GeneralException;
use App\User;
use App\Http\Controllers\Controller;
use function redirect;
use Validator;

class ReactivateController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function processRequest($token)
    {
        $v = Validator::make(['token' => $token],[
            'token' => 'required|alpha_num'
        ]);
        $v->validate();
        if(! $token)
        {
            throw new GeneralException('Your token is invalid.');
        }

        $user = User::whereLapsedToken($token)->first();

        if( ! $user)
        {
            throw new GeneralException('Your token is invalid.');
        }

        $user->active = true;
        $user->lapsed_token = null;
        $user->save();

        return redirect('/login')->withSuccess('We have successfully reactivated your account.');
    }
}
