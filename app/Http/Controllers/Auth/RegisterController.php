<?php

namespace App\Http\Controllers\Auth;

use App\Agency;
use App\Blacklist;
use App\Consortium;
use App\Http\Controllers\Controller;
use App\Job;
use App\Mail\ConfirmationEmail;
use App\State;
use App\Store;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Mail;
use Validator;
use function array_push;
use function explode;
use function redirect;
use function response;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $email_domain = explode('@', $request->input('email'));
        $hasDomain = Blacklist::whereEmailDomain($email_domain[1])->count();

        if ($hasDomain == 0) {
            event(new Registered($user = $this->create($request->all())));

            $this->guard()->login($user);

            return $this->registered($request, $user)
                ?: redirect($this->redirectPath());
        } else {
            return redirect('register')->withErrors(['message' => 'Unfortunately we dont accept users from this domain at this stage. Please contact your BDM for assistance in setting up your account.']);
        }

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validator = Validator::make($data, [
            'first_name'  => 'required|alpha_spaces|alpha_num|max:30',
            'last_name'   => 'required|alpha_spaces|alpha_num|max:30',
            'email'       => 'required|email|max:30|unique:users,email',
            'password'    => 'required|min:8|confirmed',
            'phoneNumber' => 'required|numeric|unique:users,phoneNumber',
            'position'    => 'required|alpha_spaces',
            'address1'    => 'sometimes|nullable|string|max:30',
            'address2'    => 'sometimes|nullable|string|max:30',
            'address3'    => 'sometimes|nullable|string|max:30',
            'suburb'      => 'required|alpha_spaces|max:30',
            'state'       => 'required|string|max:30',
            'postcode'    => 'required|numeric'
        ]);

        if (array_key_exists('pcc', $data)) {
            $validator->addRules([
                'pcc' => 'required|alpha_num|max:9',
            ]);
            if (!Store::wherePcc($data['pcc'])->exists()) {
                $validator->addRules([
                    'consortium'  => 'required|string|max:30',
                    'agency'      => 'required|string|max:30',
                    'agencyOther' => 'sometimes|nullable|string|max:30',
                    'store'       => 'required|string|max:30',
                    'storeOther'  => 'sometimes|nullable|string|max:30',
                ]);
            }
        } else {
            $validator->addRules([
                'consortium'  => 'required|string|max:30',
                'agency'      => 'required|string|max:30',
                'agencyOther' => 'sometimes|nullable|string|max:30',
                'store'       => 'required|string|max:30',
                'storeOther'  => 'sometimes|nullable|string|max:30',
            ]);
        }
        return $validator;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = $this->createUser($data);

        if (array_key_exists('pcc', $data)) {
            $store = Store::wherePcc($data['pcc'])->first();
            if ($store->exists()) { //when PCC is matched
                $user->update([
                    'agent_id'      => 'AGT' . $user->id . '-' . $store->pcc,
                    'sale_target'   => $store->territory()->first()->sale_target,
                    'consortium_id' => $store->store_consortium_id,
                    'territory_id'  => $store->store_territory_id,
                    'store_id'      => $store->id
                ]);
                $this->createJob($data, $user, $store);
            }
        } else {
            $agency = Agency::whereLabel($data['agency'])->first();
            $store = Store::whereName($data['store'])->first();

            if ($agency->name == 'Other') { //save agency name when other is selected

            } elseif ($store->name == 'Other') { //save store name when other is selected

            } else {
                // when combination of consortium, agency, store is used
                // and store already exist in system
                $user->update([
                    'agent_id'      => 'AGT' . $user->id . '-' . $store->pcc,
                    'consortium_id' => $store->store_consortium_id,
                    'territory_id'  => $store->store_territory_id,
                    'store_id'      => $store->id
                ]);
                $this->createJob($data, $user, $store);
            }
        }
        Mail::to($data['email'])->send(new ConfirmationEmail($user));
        return $user;
    }

    protected function createUser(array $data)
    {
        if ($data['title'] == '') {
            $data['title'] = 'Mr';
        }
        if ($data['title'] == 'Mr') {
            $data['gender'] = 'Male';
        } elseif ($data['title'] == 'Prefer not to disclose') {
            $data['gender'] = 'I prefer not to disclose';
        } else {
            $data['gender'] = 'Female';
        }
        $state = State::whereName($data['state'])->first();
        $user = User::create([
            'title'             => $data['title'],
            'name'              => $data['first_name'] . ' ' . $data['last_name'],
            'email'             => $data['email'],
            'password'          => bcrypt($data['password']),
            'phoneNumber'       => $data['phoneNumber'],
            'gender'            => $data['gender'],
            'agent_id'          => str_random(10),
            'isConfirmed'       => false,
            'active'            => false,
            'confirmation_code' => str_random(20),
            'startDate'         => Carbon::today()->toDateString(),
            'sale_target'       => 0,
            'address1'          => $data['address1'],
            'address2'          => $data['address2'],
            'address3'          => $data['address3'],
            'suburb'            => $data['suburb'],
            'state_id'          => $state->id,
            'postcode'          => $data['postcode']
        ]);
        $user->assignRole('minor');

        return $user;
    }

    protected function createJob(array $data, $user, $store)
    {
        Job::create([
            'startDate' => Carbon::today()->toDateString(),
            'position'  => $data['position'],
            'active'    => 1,
            'user_id'   => $user->id,
            'store_id'  => $store->id
        ]);
    }

    public function showRegistrationForm()
    {
        return view('frontend.auth.register');
    }

    public function ajaxConsortium()
    {
        $consortium = Consortium::orderByRaw('ISNULL(sort_order),sort_order ASC')->select('id', 'label')->get();
        return response()->json($consortium);
    }

    public function ajaxState()
    {
        $states = State::orderByRaw('sort_order ASC')->select('id', 'name')->get();
        return response()->json($states);
    }

    /**
     * Return list of agency based on consortium
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function ajaxAgency(Request $request)
    {
        $this->validate($request, [
            'consortium' => 'required|exists:consortiums,id',
        ]);

        $other = Agency::whereName('Other')->select(['id', 'name'])->first();
        $agency = Agency::select('id', 'label AS name')
            ->where('fk_id_consortiums', '=', $request->consortium)
            ->orderByRaw('ISNULL(sort_order),sort_order ASC')
            ->get();
        if ($agency->count() > 0) {
            $agency_array = $agency->toArray();
            array_push($agency_array, ["id" => $other->id, "name" => $other->name]);
            return response()->json($agency_array);
        }
        return response()->json([
            ["id" => $other->id, "name" => $other->name]
        ]);
    }

    public function storeAjax(Request $request)
    {
        $this->validate($request, [
            'consortium' => 'required|exists:consortiums,id',
            'agency'     => 'required|exists:agencies,id'
        ]);

        $other = Store::whereName('Other')->select(['id', 'name'])->first();
        $store = Store::select('id', 'name')
            ->where('store_consortium_id', '=', $request->consortium)
            ->where('store_agency_id', '=', $request->agency)
            ->orderBy('name', 'asc')
            ->get();
        if ($store->count() > 0) {
            $store_array = $store->toArray();
            array_push($store_array, ["id" => $other->id, "name" => $other->name]);
            return response()->json($store_array);
        }
        return response()->json([
            ["id" => $other->id, "name" => $other->name]
        ]);
    }

    public function ajaxPcc(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pcc' => 'required|alpha_num|exists:stores,pcc'
        ]);
        if ($validator->fails()) {
            return response()->json(false);
        }
//        $store = Store::wherePcc($request->input('pcc'))->select(
//            'id','name','pcc','address1','address2','address3','suburb','postcode','store_state_id','store_consortium_id','store_agency_id')->first();
        return response()->json(true);
    }

    /**
     * Response 200 sent as domain does not match with blacklist
     * Response 404 sent as domain match with blacklist
     */
    public function checkEmailDomain(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return response()->json(true);
        }

        $email_domain = explode('@', $request->input('email'));
        $hasDomain = Blacklist::whereEmailDomain($email_domain[1])->count();

        return $hasDomain == 0 ? response()->json(true) : response()->json(false);
    }

    protected function createStore(array $data, $agency, $consortium, $state)
    {
        $store = Store::create([
            'name'     => $data['store'],
            'pcc'      => $data['pcc'],
            'address1' => $data['address1'],
            'address2' => $data['address2'],
            'address3' => $data['address3'],
            'suburb'   => $data['suburb'],
            'postcode' => $data['postcode']
        ]);
        $store->agency()->associate($agency);
        $store->consortium()->associate($consortium);
        $store->state()->associate($state);
        $store->save();
        return $store;
    }
}
