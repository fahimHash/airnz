<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Exceptions\GeneralException;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use function redirect;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    public function showLoginForm()
    {
        return view('frontend.auth.login');
    }

    public function showWelcomeView()
    {
        return view('frontend.welcome');
    }

    /**
  	 * Where to redirect users after login
  	 * @return string
  	 */
    public function redirectPath()
    {
      if (auth()->user()->can('access_backend')) {
          return redirect('/backend');
      }
      return redirect($this->redirectTo);
    }

  /**
	 * @param Request $request
	 * @param $user
	 * @return \Illuminate\Http\RedirectResponse
	 * @throws GeneralException
	 */
	protected function authenticated(Request $request, $user)
	{
		/**
		 * Check to see if the users account is active
		 */
        if( $user->isConfirmed == 0){
            Auth::logout();
            return redirect('/login')->with('userId', $user->id);
        }elseif ($user->active == 0){
            Auth::logout();
            throw new GeneralException('Your account has been deactivated.');
        }
		return $this->redirectPath();
	}
}
