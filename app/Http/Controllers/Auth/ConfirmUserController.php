<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Exceptions\GeneralException;
use Mail;
use App\Mail\ConfirmationEmail;
use App\Mail\WelcomeEmail;
use Validator;

class ConfirmUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function confirm($token)
    {
        $v = Validator::make(['token' => $token], [
            'token' => 'required|alpha_num'
        ]);
        $v->validate();
        if (!$token) {
            throw new GeneralException('Your confirmation code is invalid.');
        }

        $user = User::whereConfirmation_code($token)->first();

        if (!$user) {
            throw new GeneralException('Your confirmation code is invalid.');
        }

        $user->active = true;
        $user->isConfirmed = true;
        $user->confirmation_code = null;
        $user->save();

        Mail::to($user->email)
            ->send(new WelcomeEmail($user));

        return redirect('/login')->withSuccess('We have successfully verified your email.');
    }

    public function resend($id)
    {
        $v = Validator::make(['id' => $id],[
            'id' => 'required|numeric'
        ]);
        $v->validate();
        $user = User::find($id);
        $user->confirmation_code = str_random(20);
        $user->save();

        Mail::to($user->email)
            ->send(new ConfirmationEmail($user));

        return redirect('/login')->withSuccess('You will soon receive an email with a link to verify your account.');
    }
}
