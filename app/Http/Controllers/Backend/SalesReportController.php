<?php

namespace App\Http\Controllers\Backend;

use App\Sale;
use DB;
use function functionCallback;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Excel;

class SalesReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.sales.report.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
           'reportDate' => 'required|date'
        ]);

        $activeUser = auth()->user();
        if($activeUser->isAdmin() == 1 || $activeUser->isSuperUser() == 1){

            $sales = Sale::with('store', 'store.consortium', 'store.agency', 'store.state', 'store.bdm', 'user')
                ->where(DB::raw('date(created_at)'), $request->input('reportDate'))
                ->get();
            return Excel::create('SalesReportOn-'.$request->input('reportDate'), function ($excel) use ($sales){
                $excel->sheet('Report', function ($sheet) use ($sales){
                    $sheet->prependRow(array(
//                        'Agent Name',
//                        'Agent Email',
                        'Agent ID',
//                        'TOP PARENT TRADE NAME',
//                        'PARENT TRADE NAME',
//                        'Agency Type',
//                        'PCC',
//                        'State',
//                        'BDM Name',
//                        'BDM Email',
//                        'Sale Status',
                        'Pnr Identifier',
                        'Pnr Creation Date',
//                        'Stock',
//                        'Airport Pair Code',
//                        'Ticket Number',
//                        'Sale Entered At'
                    ));
                    $sales->each(function($sale, $key) use ($sheet){
                        $sheet->appendRow([
//                            $sale->user->name,
//                            $sale->user->email,
                            $sale->user->agent_id,
//                            $sale->store->consortium->name,
//                            $sale->store->agency->name,
//                            $sale->store->agency->type,
//                            $sale->store->pcc,
//                            $sale->state->name,
//                            $sale->store->bdm->name,
//                            $sale->store->bdm->email,
//                            $sale->status,
                            $sale->pnr,
                            $sale->pnr_creation,
//                            $sale->stock,
//                            $sale->cty,
//                            $sale->ticket_number,
//                            $sale->created_at->format('Y-m-d')
                        ]);
                    });
                });
            })->download('csv');

        }else if($activeUser->isBDM() == 1){

            $territory_id = $activeUser->manageTerritory()->value('id');

            $sales = Sale::with('store', 'store.consortium', 'store.agency', 'store.state', 'store.bdm', 'user')
                ->where(DB::raw('date(created_at)'), $request->input('reportDate'))
                ->whereStateId($territory_id)
                ->get();

            return Excel::create('SalesReportOn-'.$request->input('reportDate'), function ($excel) use ($sales){
                $excel->sheet('Report', function ($sheet) use ($sales){
                    $sheet->prependRow([
//                        'Agent Name',
//                        'Agent Email',
                        'Agent ID',
//                        'TOP PARENT TRADE NAME',
//                        'PARENT TRADE NAME',
//                        'Agency Type',
//                        'PCC',
//                        'State',
//                        'BDM Name',
//                        'BDM Email',
//                        'Sale Status',
                        'Pnr Identifier',
                        'Pnr Creation Date',
//                        'Stock',
//                        'Airport Pair Code',
//                        'Ticket Number',
//                        'Sale Entered At'
                    ]);
                    $sales->each(function($sale, $key) use ($sheet){
                        $sheet->appendRow([
//                            $sale->user->name,
//                            $sale->user->email,
                            $sale->user->agent_id,
//                            $sale->store->consortium->name,
//                            $sale->store->agency->name,
//                            $sale->store->agency->type,
//                            $sale->store->pcc,
//                            $sale->state->name,
//                            $sale->store->bdm->name,
//                            $sale->store->bdm->email,
//                            $sale->status,
                            $sale->pnr,
                            $sale->pnr_creation,
//                            $sale->stock,
//                            $sale->cty,
//                            $sale->ticket_number,
//                            $sale->created_at->format('Y-m-d')
                        ]);
                    });
                });
            })->download('csv');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
