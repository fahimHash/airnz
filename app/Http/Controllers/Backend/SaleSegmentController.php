<?php

namespace App\Http\Controllers\Backend;

use App\SaleSegment;
use App\User;
use function compact;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use function redirect;
use timgws\QueryBuilderParser;
use Yajra\Datatables\Facades\Datatables;

class SaleSegmentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'name' => 'required|string',
           'description' => 'required|string',
           'filters' => 'required|json'
        ]);

        $table = DB::table('sales');
        $qbp = new QueryBuilderParser(
            array( 'status', 'issued_date', 'departure_date', 'pnr', 'stock', 'ticket_number', 'origin', 'cty', 'consortium_id', 'state_id')
        );
        try{
            $query = $qbp->parse($request->input('filters'), $table);
            $agents = $query->groupBy('user_id')->pluck('user_id');

            SaleSegment::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'filters' => $request->input('filters'),
            ])->agents()->saveMany(User::findMany($agents->toArray()));

        }catch (Exception $exception){
            return response()->back()->withInput(Input::all())->withErrors(['error', 'Try saving segment again!']);
        }
        return redirect()->route('segments.index')->withSuccess('Segment Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $segment = SaleSegment::find($id);
        $type = 'sale';
        return view('backend.segments.show', compact('segment','type'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        SaleSegment::destroy($id);
        return redirect('backend/segments')->withSuccess('Segment deleted!');
    }

    public function segmentTable()
    {
        $segments = SaleSegment::select(['id','name','description']);
        return Datatables::eloquent($segments)
            ->addColumn('agents_count', function (SaleSegment $segment) {
                return $segment->agents->count();
            })
            ->make(true);
    }
}
