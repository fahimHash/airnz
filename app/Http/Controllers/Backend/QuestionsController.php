<?php

namespace App\Http\Controllers\Backend;

use App\Answer;
use App\Question;
use App\Survey;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;


class QuestionsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($survey_id)
    {
        $survey = Survey::findOrFail($survey_id);
        return view('backend.questions.create',compact('survey'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $survey_id)
    {
        $this->validate($request,[
            'title' => 'required'
        ]);
        $data = $request->all();
        $data['survey_id'] = $survey_id;
        $question = Question::create($data);
        return redirect()->route('surveys.questions.show',[$survey_id,$question->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($survey_id, $question_id)
    {
        $survey = Survey::findOrFail($survey_id);
        $question = Question::findOrFail($question_id);
        return view('backend.questions.show', compact('survey','question'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($survey_id, $question_id)
    {
        $survey = Survey::findOrFail($survey_id);
        $question = Question::findOrFail($question_id);
        return view('backend.questions.edit', compact('survey','question'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $survey_id, $question_id)
    {
        $this->validate($request,[
            'title' => 'required'
        ]);
        $question = Question::findOrFail($question_id);
        $question->fill($request->all());
        $question->save();
        return redirect()->route('surveys.questions.show',[$survey_id,$question->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function answerTable(Request $request)
    {
        $answers = Answer::whereQuestionId($request->id)->select(['id', 'name', 'type', 'question_id']);
        return Datatables::eloquent($answers)
            ->addColumn('action',function($answer) {
                return view('backend.answers.action', compact('answer'))->render();
            })
            ->make(true);
    }
}
