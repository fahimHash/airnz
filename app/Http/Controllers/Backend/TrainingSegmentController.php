<?php

namespace App\Http\Controllers\Backend;

use App\TrainingSegment;
use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use timgws\QueryBuilderParser;
use Yajra\Datatables\Facades\Datatables;

class TrainingSegmentController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required|string',
            'description' => 'required|string',
            'filters' => 'required|json'
        ]);

        $table = DB::table('sales'); //TODO Change table
        $qbp = new QueryBuilderParser(
            array( 'status', 'issued_date', 'departure_date', 'pnr', 'stock', 'ticket_number', 'origin', 'cty', 'consortium_id', 'state_id') //TODO Change fields
        );
        try{
            $query = $qbp->parse($request->input('filters'), $table);
            $agents = $query->groupBy('user_id')->pluck('user_id');

            TrainingSegment::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'filters' => $request->input('filters'),
            ])->agents()->saveMany(User::findMany($agents->toArray()));

        }catch (Exception $exception){
            return response()->back()->withInput(Input::all())->withErrors(['error', 'Try saving segment again!']);
        }
        return redirect()->route('segments.index')->withSuccess('Segment Created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $segment = TrainingSegment::find($id);
        $type = 'training';
        return view('backend.segments.show', compact('segment','type'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        TrainingSegment::destroy($id);
        return redirect('backend/segments')->withSuccess('Segment deleted!');
    }

    public function segmentTable()
    {
        $segments = TrainingSegment::select(['id','name','description']);
        return Datatables::eloquent($segments)
            ->addColumn('agents_count', function (TrainingSegment $segment) {
                return $segment->agents->count();
            })
            ->make(true);
    }
}
