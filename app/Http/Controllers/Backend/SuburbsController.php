<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Territory;
use App\Suburb;
use Session;
use Response;

class SuburbsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // $this->validate($request, ['state' => 'string']);
      // $suburbs = Suburb::select('id','name')->where('state_name',$request->state)->get();
      // return Response::json($suburbs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $suburbs = Suburb::orderBy('id', 'asc')->get();
      $states = Territory::all();
      return view('backend.territory.suburb',compact('suburbs','states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $this->validate($request, ['state' => 'required','suburbs' => 'required']);

        foreach ($request->suburbs as $id) {
          $suburb = Suburb::findOrFail($id);
          $suburb->state_id = $request->state;
          $suburb->save();
        }

        Session::flash('flash_message','Suburbs assigned!');
        return redirect('backend/territory/assign');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
