<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\File;
use App\Sale;
use Maatwebsite\Excel\Facades\Excel;
use Config;

use App\ValidateSaleProgress;

use App\Jobs\ImportValidateSales;
use Carbon\Carbon;

class ValidateSalesController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
    public function index()
    {
        $id = Auth::id();
        $progress = ValidateSaleProgress::whereUser_id($id)->orderBy('created_at', 'desc')->first();
        if(empty($progress)) {
           $progress = false;
       }else{
           $progress = true;
       }
        return view('backend.sales.validate.index', compact('progress'));
    }

    public function status(Request $request)
    {
        $id = Auth::id();
        $progress = ValidateSaleProgress::whereUser_id($id)->orderBy('created_at', 'desc')->first();

        if(empty($progress)) {
           return response()->json(['msg' => 'no-task']); //nothing to do
        }
        if($progress->imported === 1) {
           return response()->json(['msg' => 'done']);
        } else {
           $status = $progress->rows_imported . ' rows have been imported out of a total of ' . $progress->total_rows.' where '.$progress->rows_validated
           .' sales are validated and '.$progress->rows_skipped.' cannot be matched with sales data.';
           $percentage = round(($progress->rows_imported / $progress->total_rows) * 100);
           return response()->json(['msg' => $status, 'percentage' => $percentage]);
        }
    }

    /**
    * Upload Sales csv
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function uploadSales(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|file|mimes:xlsx,csv,txt|max:10000'
        ]);

        if(Input::hasFile('file'))
        {
            if(Input::file('file')->isValid())
            {
                $fileName = time() .'-'. Input::file('file')->getClientOriginalName();
                $filepath = Storage::putFileAs('uploadSales', new File(Input::file('file')), $fileName);

                $path =  Config::get('filesystems.disks.local.root') . '/' .$filepath;

                $user_id = Auth::id();

                $progressTrack = new ValidateSaleProgress;

                // let's first count the total number of rows
                Excel::load($path, function($reader) use($progressTrack,$user_id,$fileName, $path) {
                   $objWorksheet = $reader->getActiveSheet();
                   $progressTrack->filename = $fileName;
                   $progressTrack->filepath = $path;
                   $progressTrack->total_rows = $objWorksheet->getHighestRow() - 1; //exclude the heading
                   $progressTrack->user_id = $user_id; //user who is uploading the file
                   $progressTrack->save();
                });

                $progressTrackId = $progressTrack->id;
                $job = new ImportValidateSales($progressTrack);
                dispatch($job);

                $message = 'Your uploaded file is processing.';
            }else{
                $message = 'Your uploaded file is invalid.';
            }
            return Redirect::back()->withSuccess($message);
        }
    }
}
