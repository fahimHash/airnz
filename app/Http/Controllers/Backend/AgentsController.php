<?php

namespace App\Http\Controllers\Backend;

use App\Agency;
use App\Consortium;
use App\Http\Controllers\Controller;
use App\Job;
use App\Mail\ChangePassword;
use App\Mail\ConfirmationEmail;
use App\Mail\NotifyLapsedProfile;
use App\Mail\SendAccountDetails;
use App\Reward;
use App\Sale;
use App\State;
use App\Store;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Mail;
use Validator;
use Yajra\Datatables\Facades\Datatables;
use function redirect;
use function str_random;

class AgentsController extends Controller
{
    /**
     * Instantiate a new AgentsController instance.
     */
    public function __construct()
    {
        $this->middleware('can:manage_user', ['only' => ['create', 'store', 'edit', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.agents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $storeList = Consortium::with('agencies.stores')->get();
        return view('backend.agents.create', compact('storeList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required',
            'first_name'  => 'required|max:255',
            'email'       => ['required', 'email', 'max:255', Rule::unique('users')],
            'password'    => 'required|min:6|confirmed',
            'address1'    => 'required|max:30',
            'address2'    => 'sometimes|max:30',
            'address3'    => 'required|max:30',
            'suburb'      => 'required|max:30',
            'postcode'    => 'required|max:4',
            'state'       => 'required|exists:states,id',
            'store'       => 'required|exists:stores,id',
            'phoneNumber' => 'required|numeric|unique:users,phoneNumber',
            'sale_target' => 'numeric|nullable'
        ]);

        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);
        $data['name'] = $request->input('first_name') . ' ' . $request->input('last_name');
        $data['isConfirmed'] = false;
        $data['active'] = false;
        $data['agent_id'] = str_random(10);
        $data['startDate'] = Carbon::today()->toDateString();
        $data['confirmation_code'] = str_random(20);
        $data['address1'] = $request->input('address1');
        $data['address2'] = $request->input('address2');
        $data['address3'] = $request->input('address3');
        $data['suburb'] = $request->input('suburb');
        $data['postcode'] = $request->input('postcode');
        $data['state_id'] = $request->input('state');
        $user = User::create($data);

        if ($request->hasFile('upload_photo')) {
            $file = $request->file('upload_photo');
            $fileName = str_random(10);
            $name = $fileName . '-' . $file->getClientOriginalName();
            $user->update(['image' => $name]);
            $file->move(public_path() . '/assets/img/', $name);
        }

        $user->assignRole('minor');

        $store = Store::find($request->input('store'));
        $user->update(['agent_id' => 'AGT' . $user->id . '-' . $store->pcc]);
        $user->workplace()->associate($store);
        $user->consortium()->associate($store->consortium()->first());
        $user->territory()->associate($store->territory()->first());
        $user->save();

        $job = Job::create([
            'startDate' => Carbon::today()->toDateString(),
            'active'    => 1,
            'user_id'   => $user->id,
            'store_id'  => $store->id
        ]);
        $territory = $store->territory()->first();
        if ($request->has('sale_target')) {
            if ($territory->sale_target > 0) {
                if ($territory->sale_target > $request->input('sale_target')) {
                    $user->update(['sale_target' => $territory->sale_target]);
                } else {
                    $user->update(['sale_target' => $territory->sale_target]);
                }
            } else {
                $user->update(['sale_target' => $territory->sale_target]);
            }
        } else {
            if ($territory->sale_target > 0) {
                $user->update(['sale_target' => $territory->sale_target]);
            }
        }

        Mail::to($data['email'])->send(new ConfirmationEmail($user));
        Mail::to($data['email'])->send(new SendAccountDetails($user, $request->input('password')));

        return redirect('backend/agents')->withSuccess('Agent Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::withTrashed()->with('roles', 'job.workplace')->find($id);
        $role = $user->roles()->first();
        $workplace = $user->job()->first() ? $user->job()->first()->workplace : null;

        $valid = Sale::whereUserId($id)->whereStatus('Valid')->count();
        $pending = Sale::whereUserId($id)->whereStatus('Pending')->count();
        $problem = Sale::whereUserId($id)->whereStatus('Problem')->count();
        $cancelled = Sale::whereUserId($id)->whereStatus('Cancelled')->count();
        $campaignsInProgress = User::withTrashed()->find($id)->campaignsInProgress()->count();
        $completedCampaign = User::withTrashed()->find($id)->completedCampaigns()->count();

        return view('backend.agents.show', compact('user', 'role', 'workplace', 'valid', 'pending', 'problem', 'cancelled', 'campaignsInProgress', 'completedCampaign'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('job.workplace')->find($id);
        $workplace = $user->job()->first() ? $user->job()->first()->workplace->id : 0;
        $splitName = explode(' ', $user->name, 2);
        $first_name = $splitName[0];
        $last_name = !empty($splitName[1]) ? $splitName[1] : '';
        $storeList = Consortium::with('agencies.stores')->get();
        $states = State::pluck('name', 'id')->all();
        $stateSelected = $user->state_id;
        return view('backend.agents.edit', compact('user', 'first_name', 'last_name', 'storeList', 'workplace','stateSelected','states'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), $rules = [
            'title'       => 'required',
            'first_name'  => 'required|max:255',
            //'email'       => ['required', 'email', 'max:255', 'unique:users,email,' . $id],
            'email_new'   => 'sometimes|nullable|email|unique:users,email',
            'store'       => 'required|exists:stores,id',
            'phoneNumber' => 'required|numeric|unique:users,phoneNumber,' . $id,
            'sale_target' => 'numeric|nullable',
            'address1'    => 'required|max:30',
            'address2'    => 'sometimes|max:30',
            'address3'    => 'required|max:30',
            'suburb'      => 'required|max:30',
            'postcode'    => 'required|max:4',
            'state'       => 'required|exists:states,id',
        ]);
        $validator->sometimes('password', 'min:6|confirmed', function ($input) {
            return (strlen($input->password) > 0);
        });
        $validator->validate();
        $user = User::find($id);

        $user->update([
            'name'        => $request->input('first_name') . ' ' . $request->input('last_name'),
            //'email'       => $request->input('email'),
            'title'       => $request->input('title'),
            'sale_target' => $request->input('sale_target'),
            'phoneNumber' => $request->input('phoneNumber'),
            'password'    => bcrypt($request->input('password')),
            'address1'    => $request->input('address1'),
            'address2'    => $request->input('address2'),
            'address3'    => $request->input('address3'),
            'suburb'      => $request->input('suburb'),
            'postcode'    => $request->input('postcode'),
            'state_id'    => $request->input('state'),
        ]);

        if ($request->has('email_new')) {
            $user->update([
                'email'             => $request->input('email_new'),
                'confirmation_code' => str_random(20),
                'isConfirmed'       => false
            ]);
        }
        if ($request->hasFile('upload_photo')) {
            $file = $request->file('upload_photo');
            $fileName = str_random(10);
            $name = $fileName . '-' . $file->getClientOriginalName();
            $user->update(['image' => $name]);
            $file->move(public_path() . '/assets/img/', $name);
        }

        if ($user->workplace()->first() == null) {
            /**
             * Create new Job
             * Associate workplace
             */
            $store = Store::find($request->input('store'));
            $user->workplace()->dissociate();
            $user->workplace()->associate($store);
            $user->consortium()->associate($store->consortium()->first());
            $user->territory()->associate($store->territory()->first());
            $user->save();

            Job::create([
                'startDate' => Carbon::today()->toDateString(),
                'active'    => 1,
                'user_id'   => $user->id,
                'store_id'  => $store->id
            ]);
        } elseif ($user->workplace()->first()->id != $request->input('store')) {
            /**
             * End Previous Job
             * Create new Job
             * Associate workplace
             */
            $store = Store::find($request->input('store'));
            $user->workplace()->dissociate();
            $user->workplace()->associate($store);
            $user->consortium()->associate($store->consortium()->first());
            $user->territory()->associate($store->territory()->first());
            $user->save();

            $user->job()->first()->update([
                'endDate' => Carbon::today()->toDateString(),
                'active'  => 0
            ]);

            Job::create([
                'startDate' => Carbon::today()->toDateString(),
                'active'    => 1,
                'user_id'   => $user->id,
                'store_id'  => $store->id
            ]);
        }

        $territory = $user->territory()->first();
        if ($request->has('sale_target')) {
            if ($territory) {
                if ($territory->sale_target > $request->input('sale_target')) {
                    $user->update(['sale_target' => $territory->sale_target]);
                } else {
                    $user->update(['sale_target' => $territory->sale_target]);
                }
            } else {
                $user->update(['sale_target' => $territory->sale_target]);
            }
        } else {
            if ($territory) {
                $user->update(['sale_target' => $territory->sale_target]);
            }
        }

        if ($request->has('password')) {
            Mail::to($user)->send(new ChangePassword($user, $request->input('password')));
        }
        if ($request->has('email_new')) {
            Mail::to($user)->send(new ConfirmationEmail($user));
        }

        return redirect('backend/agents')->withSuccess('Agent updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->update(['active' => 0]);
        $user->delete();
        return redirect('backend/agents')->withSuccess('Agent archived!');
    }

    /**
     * Return list of agency based on consortium
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agencyAjax(Request $request)
    {
        $agency = Agency::select('id', 'name')->where('fk_id_consortiums', '=', $request->consortium)->orderBy('id', 'asc')->get();
        return response()->json($agency);
    }

    /**
     * Process dataTable ajax response.
     *
     * @param \Yajra\Datatables\Datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function agentsTable()
    {
        $activeUser = auth()->user();
        if ($activeUser->isBDM() == 1) {
            $territory_id = $activeUser->manageTerritory()->value('id');
            $users = User::where('territory_id', '=', $territory_id)
                ->whereHas('roles', function ($q) {
                    $q->whereIn('name', ['minor', 'mover']);
                })
                ->with('sales', 'territory', 'roles')
                ->select(['users.id', 'users.name', 'users.email', 'users.phoneNumber', 'users.territory_id', 'users.created_at']);
        } else {
            $users = User::with('sales', 'territory', 'roles')->whereHas('roles', function ($q) {
                $q->whereIn('name', ['minor', 'mover']);
            })->select(['users.id', 'users.name', 'users.email', 'users.phoneNumber', 'users.territory_id', 'users.created_at']);
        }

        return Datatables::eloquent($users)
            ->addColumn('confirmed', function (User $user) {
                if ($user->sales) {
                    return $user->sales->where('status', 'Valid')->count();
                } else {
                    return '';
                }
            })
            ->addColumn('pending', function (User $user) {
                if ($user->sales) {
                    return $user->sales->where('status', 'Pending')->count();
                } else {
                    return '';
                }
            })
            ->addColumn('cancelled', function (User $user) {
                if ($user->sales) {
                    return $user->sales->where('status', 'Cancelled')->count();
                } else {
                    return '';
                }
            })
            ->addColumn('territory', function (User $user) {
                if ($user->territory) {
                    return $user->territory->name;
                } else {
                    return '';
                }
            })
            ->addColumn('created_at', function (User $user) {
                return Carbon::parse($user->created_at)->format('d/m/Y');
            })
            ->escapeColumns(['confirmed'])
            ->escapeColumns(['pending'])
            ->escapeColumns(['cancelled'])
            ->escapeColumns(['territory'])
            ->escapeColumns(['created_at'])
            ->make(true);
    }

    public function dateRange(Request $request)
    {
        $this->validate($request, [
            'startDate' => 'required|date_format:"Y-m-d"',
            'endDate'   => 'required|date_format:"Y-m-d"',
            'id'        => 'required|numeric'
        ]);
//        DB::enableQueryLog();
        $startDate = Carbon::parse($request->input('startDate'));
        $endDate = Carbon::parse($request->input('endDate'));
        $difference = $startDate->diffInDays($endDate);
//        dd(DB::getQueryLog());

        if ($difference == 0) {
            //today or yesterday or any single date
            $validSales = Sale::whereUserId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereUserId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            return response()->json([
                'labels'  => [$startDate->format('d-m-Y')],
                'valid'   => $validSales,
                'pending' => $pendingSales
            ]);
        } elseif ($difference == 6) {
            //a week
            $validSales = Sale::whereUserId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%W') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereUserId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%W') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $validFormatted = [
                ['time' => 'Monday', 'sales' => 0],
                ['time' => 'Tuesday', 'sales' => 0],
                ['time' => 'Wednesday', 'sales' => 0],
                ['time' => 'Thursday', 'sales' => 0],
                ['time' => 'Friday', 'sales' => 0],
                ['time' => 'Saturday', 'sales' => 0],
                ['time' => 'Sunday', 'sales' => 0]
            ];

            $pendingFormatted = $validFormatted;

            for ($i = 0; $i < count($validSales); $i++) {
                for ($j = 0; $j < count($validFormatted); $j++) {
                    if ($validFormatted[$j]['time'] == $validSales[$i]['time']) {
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for ($i = 0; $i < count($pendingSales); $i++) {
                for ($j = 0; $j < count($pendingFormatted); $j++) {
                    if ($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']) {
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels'  => ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                'valid'   => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        } elseif ($difference == 29 || $difference == 30 || $difference >= 7 && $difference < 31) {
            //day by day view

            $dates = [];
            $validFormatted = [];
            $pendingFormatted = [];

            for ($date = Carbon::parse($request->input('startDate')); $date->lte(Carbon::parse($request->input('endDate'))); $date->addDay()) {
                $dates[] = $date->format('d-m-Y');
                $validFormatted [] = ['time' => $date->format('d-m-Y'), 'sales' => 0];
                $pendingFormatted [] = ['time' => $date->format('d-m-Y'), 'sales' => 0];
            }

            $validSales = Sale::whereUserId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereUserId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            for ($i = 0; $i < count($validSales); $i++) {
                for ($j = 0; $j < count($validFormatted); $j++) {
                    if ($validFormatted[$j]['time'] == $validSales[$i]['time']) {
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for ($i = 0; $i < count($pendingSales); $i++) {
                for ($j = 0; $j < count($pendingFormatted); $j++) {
                    if ($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']) {
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels'  => $dates,
                'valid'   => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        } else {
            //custom range greater than 30 days show month wise
            $dates = [];
            $validFormatted = [];
            $pendingFormatted = [];

            for ($date = Carbon::parse($request->input('startDate')); $date->lte(Carbon::parse($request->input('endDate'))); $date->addMonth()) {
                $dates[] = $date->format('F-Y');
                $validFormatted [] = ['time' => $date->format('F-Y'), 'sales' => 0];
                $pendingFormatted [] = ['time' => $date->format('F-Y'), 'sales' => 0];
            }

            $validSales = Sale::whereUserId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereUserId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            for ($i = 0; $i < count($validSales); $i++) {
                for ($j = 0; $j < count($validFormatted); $j++) {
                    if ($validFormatted[$j]['time'] == $validSales[$i]['time']) {
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for ($i = 0; $i < count($pendingSales); $i++) {
                for ($j = 0; $j < count($pendingFormatted); $j++) {
                    if ($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']) {
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels'  => $dates,
                'valid'   => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        }
    }

    /**
     * Process dataTable ajax response.
     *
     * @param \Yajra\Datatables\Datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function salesTable(Request $request)
    {
        $sales = Sale::whereUserId($request->id)->select(['sales.id', 'sales.status', 'sales.issued_date', 'sales.pnr', 'sales.cty', 'sales.stock', 'sales.ticket_number', 'sales.departure_date', 'sales.class', 'sales.created_at']);
        return Datatables::eloquent($sales)
            ->make(true);
    }

    public function campaignsTable(Request $request)
    {
        $campaigns = User::withTrashed()->find($request->id)->campaigns()->select(['campaigns.id', 'campaigns.name', 'campaigns.type', 'campaign_user.joined', 'campaign_user.completed']);
        return Datatables::eloquent($campaigns)
            ->addColumn('status', function ($campaign) {
                if ($campaign->joined == 1 && $campaign->completed == 0) {
                    return 'In Progress';
                } elseif ($campaign->joined == 0 && $campaign->completed == 1) {
                    return 'Complete';
                } else {
                    return 'Unavailable';
                }
            })
            ->escapeColumns(['status'])
            ->make(true);
    }

    public function rewardsTable(Request $request)
    {
        $agentCompletedCampaigns = User::withTrashed()->find($request->id)->completedCampaigns()->with('reward')->pluck('reward_id');
        $rewards = Reward::whereIn('id', $agentCompletedCampaigns)->select(['id', 'name', 'type', 'value']);
        return Datatables::eloquent($rewards)
            ->make(true);
    }

    public function reactivate(Request $request)
    {
        $user = User::find($request->id);
        if ($user->lapsed_token) {
            Mail::to($user)->send(new NotifyLapsedProfile($user));
        } else {
            $user->update([
                'lapsed_token' => str_random(10)
            ]);
            Mail::to($user)->send(new NotifyLapsedProfile($user));
        }
        return response()->json([
            'result'  => 'success',
            'message' => 'Email Sent!'
        ]);
    }
}
