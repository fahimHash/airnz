<?php

namespace App\Http\Controllers\Backend;

use App\Question;
use App\SaleSegment;
use App\State;
use App\Survey;
use App\SystemSegment;
use App\Territory;
use App\TrainingSegment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use function response;
use Storage;
use Validator;
use Yajra\Datatables\Facades\Datatables;

class SurveysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.survey.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $territories = Territory::all('id','name');
        $states = State::all('id','name');
        $saleSegments = SaleSegment::all('id', 'name');
        $systemSegments = SystemSegment::all('id', 'name');
        $trainingSegments = TrainingSegment::all('id', 'name');
        return view('backend.survey.create', compact('territories','states','saleSegments','systemSegments','trainingSegments'));
    }

    /**
     * Store a newly created resource in storage.
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->input('name'));

        Validator::make($data,[
            'name' => 'required',
            'type' => 'required',
            'description' => 'required',
            'image' => 'required',
            'slug' => Rule::unique('surveys','slug')
        ])->validate();
        $data['image'] = $request->file('image')->store('surveys','public');
        $survey = Survey::create($data);
        $survey->save();

        $survey->territories()->sync($request->territories);
        $survey->states()->sync($request->states);
        $survey->saleSegments()->sync($request->sale_segments);
        $survey->systemSegments()->sync($request->system_segments);
        $survey->trainingSegments()->sync($request->training_segments);

        if($request->sc == 'continue'){
            return redirect()->route('surveys.questions.create',$survey->id);
        }
        return redirect('backend/surveys')->withSuccess('Survey Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $survey = Survey::findOrFail($id);
        return view('backend.survey.show',compact('survey'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $survey = Survey::findOrFail($id);
        $territories = Territory::all('id','name');
        $states = State::all('id','name');
        $saleSegments = SaleSegment::all('id', 'name');
        $systemSegments = SystemSegment::all('id', 'name');
        $trainingSegments = TrainingSegment::all('id', 'name');
        $territories_sel = $survey->territories()->get()->pluck('id')->toArray();
        $saleSegment_sel = $survey->saleSegments()->get()->pluck('id')->toArray();
        $systemSegment_sel = $survey->systemSegments()->get()->pluck('id')->toArray();
        $trainingSegment_sel = $survey->trainingSegments()->get()->pluck('id')->toArray();
        $state_sel = $survey->states()->get()->pluck('id')->toArray();

        return view('backend.survey.edit', compact(
            'survey',
            'territories',
            'states',
            'saleSegments',
            'systemSegments',
            'trainingSegments',
            'territories_sel',
            'state_sel',
            'saleSegment_sel',
            'systemSegment_sel',
            'trainingSegment_sel'
        ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name' => 'required',
            'type' => 'required',
            'description' => 'required'
        ]);
        $survey = Survey::findOrFail($id);
        $survey->fill($request->all());
        $survey->save();

        if ($request->hasFile('image')) {
            Storage::disk('public')->delete($survey->image);
            $survey->image = $request->file('image')->store('surveys', 'public');
            $survey->save();
        }

        $survey->territories()->sync($request->territories);
        $survey->states()->sync($request->states);
        $survey->saleSegments()->sync($request->sale_segments);
        $survey->systemSegments()->sync($request->system_segments);
        $survey->trainingSegments()->sync($request->training_segments);

        return redirect('backend/surveys')->withSuccess('Survey Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Survey::destroy($id);
        return redirect('backend/surveys')->withSuccess('Survey deleted!');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function surveyTable(){
        $surveys = Survey::select(['id', 'name', 'type']);
        return Datatables::eloquent($surveys)
            ->addColumn('participation', function ($survey) {
                return '50%';
            })
            ->escapeColumns(['participation'])
            ->make(true);
    }

    public function questionTable(Request $request)
    {
        $questions = Question::whereSurveyId($request->id)->select(['id', 'title','survey_id']);
        return Datatables::eloquent($questions)
            ->addColumn('action',function($question) {
                return view('backend.questions.action', compact('question'))->render();
            })
            ->make(true);
    }

    public function removePhoto(Request $request)
    {
        $survey = Survey::findOrFail($request->id);
        Storage::disk('public')->delete($survey->image);
        $survey->image = null;
        $survey->save();

        return response()->json();
    }
}
