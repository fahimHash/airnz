<?php

namespace App\Http\Controllers\Backend;

use function auth;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Role;
use App\Job;
use App\Territory;
use App\Consortium;
use function response;
use Session;
use Yajra\Datatables\Facades\Datatables;

class UsersController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.users.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (auth()->user()->isAdmin() == 1) {
            $roles = DB::table('roles')->whereNotIn('name', [ROLE::ROLE_MINOR, ROLE::ROLE_MOVER])->get();
        } else if (auth()->user()->isSuperUser() == 1) {
            $roles = DB::table('roles')->whereNotIn('name', [ROLE::ROLE_ADMIN, ROLE::ROLE_SUPER_USER, ROLE::ROLE_MINOR, ROLE::ROLE_MOVER])->get();
        }
        return view('backend.users.create', compact('roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'title'       => 'required',
            'first_name'  => 'required|max:255',
            'email'       => ['required', 'email', 'max:255', Rule::unique('users')],
            'password'    => 'required|min:6|confirmed',
            'roles'       => 'required',
            'phoneNumber' => 'required|numeric|unique:users,phoneNumber',
        ]);

        $data = $request->except('password');
        $data['password'] = bcrypt($request->password);
        $data['name'] = $request->input('first_name') . ' ' . $request->input('last_name');
        $data['isConfirmed'] = true;
        $data['agent_id'] = str_random(10);
        $data['phoneNumber'] = $request->input('phoneNumber');
        $user = User::create($data);

        if ($request->hasFile('upload_photo')) {
            $file = $request->file('upload_photo');
            $fileName = str_random(10);
            $name = $fileName . '-' . $file->getClientOriginalName();
            $user->image = $name;
            $file->move(public_path() . '/assets/img/', $name);
            $user->save();
        }

        $user->assignRole($request->roles);

        Session::flash('flash_message', 'User Added!');
        return redirect('backend/users');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        if (auth()->id() != $id) // auth user is trying to edit another user, admin can edit all below them, airnz-super-user can edit all below them, bdms only themselves
        {
            if (auth()->user()->isBDM() == 1) // user is BDM
            {
                return redirect('backend/users')->withDanger('You are not authorized to access this resource');
            } else if (auth()->user()->isSuperUser() == 1) {
                if ($user->isAdmin() == 1 || $user->isSuperUser() == 1) {
                    return redirect('backend/users')->withDanger('You are not authorized to access this resource');
                }
            }
        }

        $roles = $user->roles;
        return view('backend.users.show', compact('user', 'roles'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('roles')->findOrFail($id);

        if (auth()->user()->id != $id) // auth user is trying to edit another user, admin can edit all below them, airnz-super-user can edit all below them, bdms only themselves
        {
            if (auth()->user()->isBDM() == 1) // user is BDM
            {
                return redirect('backend/users')->withDanger('You are not authorized to access this resource');
            } else if (auth()->user()->isSuperUser() == 1) {
                if ($user->isAdmin() == 1 || $user->isSuperUser() == 1) {
                    return redirect('backend/users')->withDanger('You are not authorized to access this resource');
                }
            }
        }

        if (auth()->user()->isAdmin() == 1) {
            $roles = DB::table('roles')->whereNotIn('name', ['minor', 'mover'])->get();
        } else if (auth()->user()->isSuperUser() == 1) {
            $roles = DB::table('roles')->whereNotIn('name', ['admin', 'airnz-super-user', 'minor', 'mover'])->get();
        }


        $user_roles = [];
        foreach ($user->roles as $role) {
            $user_roles[] = $role->name;
        }

        $splitName = explode(' ', $user->name, 2);
        $first_name = $splitName[0];
        $last_name = !empty($splitName[1]) ? $splitName[1] : '';

        return view('backend.users.edit', compact('user', 'roles', 'user_roles', 'first_name', 'last_name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (auth()->id() == $id) {
            $this->validate($request, [
                'first_name' => 'required|max:255',
                'email'      => 'required',
                'password'   => 'required|min:6|confirmed'
            ]);
            $user = User::findOrFail($id);
            $user->password = bcrypt($request->password);
        } else {
            $this->validate($request, ['first_name' => 'required', 'email' => 'required']);
            $user = User::findOrFail($id);
        }
        $user->name = $request->input('first_name') . ' ' . $request->input('last_name');
        $user->email = $request->input('email');
        $user->title = $request->input('title');
        $user->phoneNumber = $request->input('phoneNumber');

        if ($request->has('active')) {
            $user->active = $request->input('active');
        }

        if ($request->hasFile('upload_photo')) {
            $file = $request->file('upload_photo');
            $fileName = str_random(10);
            $name = $fileName . '-' . $file->getClientOriginalName();
            $user->image = $name;
            $file->move(public_path() . '/assets/img/', $name);
        }

        $user->save();

        if ($request->has('roles')) {
            if (!$user->hasRole($request->roles)) {
                $user->roles()->detach();
                $user->assignRole($request->roles);
            }
        }

        Session::flash('flash_message', 'User updated!');

        return redirect('backend/users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if(!$user->default){
            User::destroy($id);
            Session::flash('flash_message', 'User deleted!');
        }else{
            Session::flash('flash_message', 'For system default user delete operation is denied!');
        }
        return redirect('backend/users');
    }

    public function usersTable()
    {

        if (auth()->user()->isAdmin() == 1) {
            $users = User::with('roles')->whereHas('roles', function ($q) {
                $q->where('name', '=', ROLE::ROLE_ADMIN)
                    ->orWhere('name', '=', ROLE::ROLE_SUPER_USER)
                    ->orWhere('name', '=', ROLE::ROLE_STATE_MANAGER)
                    ->orWhere('name', '=', ROLE::ROLE_BDM)
                    ->orWhere('name', '=', ROLE::ROLE_NATIONAL_MANAGER);
            })->select(['users.id', 'users.name', 'users.email', 'users.active']);

        } else if (auth()->user()->isSuperUser() == 1 || auth()->user()->isStateManager() == 1 || auth()->user()->isNationalManager() == 1) {
            $users = User::with('roles')->whereHas('roles', function ($query) {
                $query->where('name', '=', ROLE::ROLE_SUPER_USER)
                    ->orWhere('name', '=', ROLE::ROLE_STATE_MANAGER)
                    ->orWhere('name', '=', ROLE::ROLE_BDM)
                    ->orWhere('name', '=', ROLE::ROLE_NATIONAL_MANAGER);
            })->select(['users.id', 'users.name', 'users.email', 'users.active']);

        } else if (auth()->user()->isBDM() == 1) {
            $users = User::with('roles')->whereId(auth()->id())->select(['users.id', 'users.name', 'users.email', 'users.active']);
        }

        return Datatables::eloquent($users)
            ->addColumn('role', function (User $user) {
                return $user->roles ? str_limit($user->roles->first()->label, 30, '...') : '';
            })
            ->addColumn('status', function (User $user) {
                return $user->active == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';
            })
            ->escapeColumns(['role'])
            ->rawColumns(['status'])
            ->make(true);
    }
}
