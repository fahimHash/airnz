<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Page;
use App\State;
use App\Territory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Storage;
use Validator;
use Yajra\Datatables\Facades\Datatables;
use function compact;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.pages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $territories = Territory::all('id', 'name');
        $states = State::all(['id', 'name']);
        return view('backend.pages.create', compact('territories', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->input('title'));

        Validator::make($data, [
            'title'     => 'required',
            'summary'   => 'required',
            'territory' => 'required|array|min:1',
            'state'     => 'required|array|min:1',
            'slug'      => Rule::unique('pages', 'slug')
        ])->validate();

        if ($request->hasFile('image')) {
            $data['image'] = $request->file('image')->store('pages', 'public');
        }
        $page = Page::create($data);
        $page->territories()->attach($request->territory);
        $page->states()->attach($request->state);

        return redirect()->route('page.editor', ['slug' => $page->slug])->withSuccess('Create your page content now!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page = Page::findOrFail($id);
        return view('backend.pages.show', compact('page'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Page::findOrFail($id);
        $territories = Territory::all('id', 'name');
        $states = State::all(['id', 'name']);
        $territory_sel = $page->territories()->get()->pluck('id')->toArray();
        $state_sel = $page->states()->get()->pluck('id')->toArray();
        return view('backend.pages.edit', compact('page', 'territories', 'territory_sel', 'states', 'state_sel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title'   => 'required',
            'summary' => 'required',
        ]);

        $page = Page::findOrFail($id);
        $page->fill($request->except('image'));
        $page->save();

        if ($request->hasFile('image')) {
            Storage::disk('public')->delete($page->image);
            $page->image = $request->file('image')->store('pages', 'public');
            $page->save();
        }

        $page->territories()->sync($request->territory);
        $page->states()->sync($request->state);

        return redirect('backend/pages')->withSuccess('Page Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::findOrFail($id);
        if ($page->type === 'terms-and-conditions') {
            return redirect('backend/pages')->withDanger('Page cannot be deleted!');
        } elseif ($page->type === 'privacy-policy') {
            return redirect('backend/pages')->withDanger('Page cannot be deleted!');
        } elseif ($page->type === 'about-us') {
            return redirect('backend/pages')->withDanger('Page cannot be deleted!');
        } else {
            Page::destroy($id);
            return redirect('backend/pages')->withSuccess('Page Deleted!');
        }
    }

    public function pageTable()
    {
        $page = Page::select(['id', 'title', 'summary']);
        return Datatables::eloquent($page)
            ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editor($slug)
    {
        $post = Page::where('slug', '=', $slug)->first();
        switch ($post->type) {
            case 'terms-and-conditions':
                return view('backend.pages.editor.simple', compact('post'));
                break;
            case 'privacy-policy':
                return view('backend.pages.editor.simple', compact('post'));
                break;
            case 'standard':
                return view('backend.pages.editor.simple', compact('post'));
                break;
            case 'advance':
                return view('backend.pages.editor.index', compact('post'));
                break;
            default:
                return view('backend.pages.editor.simple', compact('post'));
        }
    }

    public function editorStaticContent()
    {
        return view('backend.pages.editor.snippets');
    }

    public function updateContent(Request $request, $slug)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);
        $post = Page::whereSlug($slug)->firstOrFail();
        $post->message = Input::get('message');
        $post->save();

        return redirect()->route('page.preview.content', ['slug' => $post->slug]);
    }

    public function previewContent($slug)
    {
        $post = Page::whereSlug($slug)->firstOrFail();
        return view('backend.pages.editor.preview', compact('post'));
    }

    public function publish($id)
    {
        $post = Page::find($id);
        if ($post->publish == false)
            $post->publish = true;
        else {
            $post->publish = false;
        }
        $post->save();

        return redirect()->route('pages.show', [$id]);
    }

    public function removePhoto(Request $request)
    {
        $page = Page::findOrFail($request->id);
        Storage::disk('public')->delete($page->image);
        $page->image = null;
        $page->save();

        return response()->json();
    }
}
