<?php

namespace App\Http\Controllers\Backend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Agency;
use App\Consortium;
use Session;
use Yajra\Datatables\Facades\Datatables;

class AgencyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.agency.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $consortiums = Consortium::select('id','name')->get();
        return view('backend.agency.create', compact('consortiums'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required|max:255|unique:agencies',
        'consortium' => 'required'
      ]);

      $agency = Agency::create([
        'name' => $request->input('name'),
        'type' => $request->input('type')
      ]);
      $consortium = Consortium::find($request->input('consortium'));
      $agency->consortium()->associate($consortium)->save();

      Session::flash('success', 'Agency Added!');
      return redirect('backend/agency');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agency = Agency::whereId($id)->with('consortium')->first();
        return view('backend.agency.show', compact('agency'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $agency = Agency::find($id);
      $consortiums = Consortium::select('id','name')->get();
      return view('backend.agency.edit', compact('agency','consortiums'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request, [
         'name' => ['required','max:255'],
         'consortium' => 'required'
       ]);

       $agency = Agency::find($id);
       $agency->name = $request->input('name');
       $agency->type = $request->input('type');
       $agency->save();

       if($agency->fk_id_consortiums != $request->input('consortium')){
         $consortium = Consortium::find($request->input('consortium'));
         $agency->consortium()->dissociate()->save();
         $agency->consortium()->associate($consortium)->save();
       }
       Session::flash('success', 'Agency Updated!');
       return redirect('backend/agency');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Agency::destroy($id);

        Session::flash('success', 'Agency deleted!');

        return redirect('backend/agency');
    }

    public function agencyTable(){
        $agency = Agency::with('consortium')->where('name','!=','Other')->whereNotNull('fk_id_consortiums')->select(['id', 'name', 'type', 'fk_id_consortiums']);
        return Datatables::eloquent($agency)
            ->addColumn('consortium', function ($agency) {
                return $agency->consortium->name ? str_limit($agency->consortium->name, 30, '...') : '';
            })
            ->escapeColumns(['consortium'])
            ->make(true);
    }
}
