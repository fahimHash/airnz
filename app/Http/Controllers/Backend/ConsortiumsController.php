<?php

namespace App\Http\Controllers\Backend;

use App\Agency;
use App\Sale;
use App\Store;
use App\User;
use Carbon\Carbon;
use function dd;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;
use Session;
use App\Consortium;
use App\Territory;
use Yajra\Datatables\Facades\Datatables;

class ConsortiumsController extends Controller
{
    /**
     * Instantiate a new ConsortiumController instance.
     */
    public function __construct()
    {
        $this->middleware('can:manage_consortium', ['only' => ['create', 'store', 'edit', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.consortiums.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.consortiums.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'         => ['required', 'max:255', Rule::unique('consortiums')],
            'email'        => ['email', 'max:255'],
            'upload_photo' => 'mimes:png,jpeg,jpg',
        ]);

        $consortium = Consortium::create($request->all());

        if ($request->hasFile('upload_photo')) {
            $file = $request->file('upload_photo');
            $fileName = str_random(10);
            $name = $fileName . '-' . $file->getClientOriginalName();
            $consortium->logo = $name;
            $file->move(public_path() . '/assets/img/', $name);
            $consortium->save();
        }
        Session::flash('success', 'Consortium Added!');
        return redirect('backend/consortiums');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $consortiums = Consortium::whereId($id)->with('agencies', 'users')->get();
        $valid = Sale::whereConsortiumId($id)->whereStatus('Valid')->count();
        $pending = Sale::whereConsortiumId($id)->whereStatus('Pending')->count();
        $problem = Sale::whereConsortiumId($id)->whereStatus('Problem')->count();
        $cancelled = Sale::whereConsortiumId($id)->whereStatus('Cancelled')->count();

        $consortium = $consortiums->first();
        $agencies = $consortiums->first()->agencies->count();
        $agents = $consortiums->first()->users->count();
        return view('backend.consortiums.show', compact(
            'consortium',
            'valid',
            'pending',
            'problem',
            'cancelled',
            'agencies',
            'agents'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $consortium = Consortium::find($id);
        return view('backend.consortiums.edit', compact('consortium'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'         => ['required', 'max:255'],
            'email'        => ['email', 'max:255'],
            'upload_photo' => 'mimes:png,jpeg,jpg',
        ]);

        $consortium = Consortium::find($id);
        $consortium->name = $request->input('name');
        $consortium->address = $request->input('address');
        $consortium->email = $request->input('email');
        $consortium->phone = $request->input('phone');
        $consortium->active = $request->input('active');
        $consortium->suburb = $request->input('suburb');
        $consortium->state = $request->input('state');
        $consortium->postcode = $request->input('postcode');
        $consortium->save();

        if ($request->hasFile('upload_photo')) {
            $file = $request->file('upload_photo');
            $fileName = str_random(10);
            $name = $fileName . '-' . $file->getClientOriginalName();
            $consortium->logo = $name;
            $file->move(public_path() . '/assets/img/', $name);
            $consortium->save();
        }
        Session::flash('success', 'Consortium Updated!');
        return redirect('backend/consortiums');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Consortium::destroy($id);

        Session::flash('success', 'Consortium deleted!');

        return redirect('backend/consortiums');
    }

    public function consortiumsTable()
    {
        $activeUser = auth()->user();

        if ($activeUser->isAdmin() == 1 || $activeUser->isSuperUser() == 1) {
            $consortiums = Consortium::with('agencies','stores', 'sales', 'users')->select(['id', 'name']);
        } else if ($activeUser->hasRole('territory') == 1) {
            $id = $activeUser->manageTerritory()->value('id');
            Consortium::ByTerritory($id)
                ->with('agencies','stores', 'sales', 'users')
                ->select(['id', 'name']);
        }

        return Datatables::eloquent($consortiums)
            ->addColumn('agencies', function (Consortium $consortium) {
                if ($consortium->agencies) {
                    return $consortium->agencies->count();
                } else {
                    return '';
                }
            })
            ->addColumn('stores', function (Consortium $consortium) {
                if ($consortium->stores) {
                    return $consortium->stores->count();
                } else {
                    return '';
                }
            })
            ->addColumn('agents', function (Consortium $consortium) {
                if ($consortium->users) {
                    return $consortium->users->count();
                } else {
                    return '';
                }
            })
            /*
            ->addColumn('agents', function (Consortium $consortium) {
                if ($consortium->agencies) {
                    $quantity = 0;
                    $consortium->agencies->each(function ($p, $k) use (&$quantity) {
                        $quantity += $p->activeAgents->count();
                    });
                    return $quantity;
                } else {
                    return '';
                }
            })
            */
            ->addColumn('sales', function (Consortium $consortium) {
                if ($consortium->sales) {
                    return $consortium->sales->count();
                } else {
                    return '';
                }
            })
            ->addColumn('percentage', function (Consortium $consortium) {
                $total_sale = Sale::count();
                if ($consortium->sales) {
                    $count = $consortium->sales->count();
                    if ($count > 0) {
                        return round(($count / $total_sale) * 100) . '%';
                    } else {
                        return '0%';
                    }
                } else {
                    return '0%';
                }
            })
            ->escapeColumns(['agencies'])
            ->escapeColumns(['stores'])
            ->escapeColumns(['agents'])
            ->escapeColumns(['sales'])
            ->escapeColumns(['percentage'])
            ->make(true);
    }

    public function dateRange(Request $request)
    {
        $this->validate($request, [
            'startDate' => 'required|date_format:"Y-m-d"',
            'endDate'   => 'required|date_format:"Y-m-d"',
            'id'        => 'required|numeric'
        ]);

        $startDate = Carbon::parse($request->input('startDate'));
        $endDate = Carbon::parse($request->input('endDate'));
        $difference = $startDate->diffInDays($endDate);

        if ($difference == 0) {
            //today or yesterday or any single date
            $validSales = Sale::whereConsortiumId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereConsortiumId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            return response()->json([
                'labels'  => [$startDate->format('d-m-Y')],
                'valid'   => $validSales,
                'pending' => $pendingSales
            ]);
        } elseif ($difference == 6) {
            //a week
            $validSales = Sale::whereConsortiumId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%W') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereConsortiumId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%W') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $validFormatted = [
                ['time' => 'Monday', 'sales' => 0],
                ['time' => 'Tuesday', 'sales' => 0],
                ['time' => 'Wednesday', 'sales' => 0],
                ['time' => 'Thursday', 'sales' => 0],
                ['time' => 'Friday', 'sales' => 0],
                ['time' => 'Saturday', 'sales' => 0],
                ['time' => 'Sunday', 'sales' => 0]
            ];

            $pendingFormatted = $validFormatted;

            for ($i = 0; $i < count($validSales); $i++) {
                for ($j = 0; $j < count($validFormatted); $j++) {
                    if ($validFormatted[$j]['time'] == $validSales[$i]['time']) {
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for ($i = 0; $i < count($pendingSales); $i++) {
                for ($j = 0; $j < count($pendingFormatted); $j++) {
                    if ($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']) {
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels'  => ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                'valid'   => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        } elseif ($difference == 29 || $difference == 30 || $difference >= 7 && $difference < 31) {
            //day by day view

            $dates = [];
            $validFormatted = [];
            $pendingFormatted = [];

            for ($date = Carbon::parse($request->input('startDate')); $date->lte(Carbon::parse($request->input('endDate'))); $date->addDay()) {
                $dates[] = $date->format('d-m-Y');
                $validFormatted [] = ['time' => $date->format('d-m-Y'), 'sales' => 0];
                $pendingFormatted [] = ['time' => $date->format('d-m-Y'), 'sales' => 0];
            }

            $validSales = Sale::whereConsortiumId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereConsortiumId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            for ($i = 0; $i < count($validSales); $i++) {
                for ($j = 0; $j < count($validFormatted); $j++) {
                    if ($validFormatted[$j]['time'] == $validSales[$i]['time']) {
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for ($i = 0; $i < count($pendingSales); $i++) {
                for ($j = 0; $j < count($pendingFormatted); $j++) {
                    if ($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']) {
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels'  => $dates,
                'valid'   => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        } else {
            //custom range greater than 30 days show month wise
            $dates = [];
            $validFormatted = [];
            $pendingFormatted = [];

            for ($date = Carbon::parse($request->input('startDate')); $date->lte(Carbon::parse($request->input('endDate'))); $date->addMonth()) {
                $dates[] = $date->format('F-Y');
                $validFormatted [] = ['time' => $date->format('F-Y'), 'sales' => 0];
                $pendingFormatted [] = ['time' => $date->format('F-Y'), 'sales' => 0];
            }

            $validSales = Sale::whereConsortiumId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereConsortiumId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            for ($i = 0; $i < count($validSales); $i++) {
                for ($j = 0; $j < count($validFormatted); $j++) {
                    if ($validFormatted[$j]['time'] == $validSales[$i]['time']) {
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for ($i = 0; $i < count($pendingSales); $i++) {
                for ($j = 0; $j < count($pendingFormatted); $j++) {
                    if ($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']) {
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels'  => $dates,
                'valid'   => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        }
    }

    /**
     * Process dataTable ajax response.
     *
     * @param \Yajra\Datatables\Datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function salesTable(Request $request)
    {
        $sales = Sale::whereConsortiumId($request->id)->select(['sales.id', 'sales.status', 'sales.issued_date', 'sales.pnr', 'sales.cty', 'sales.stock', 'sales.ticket_number', 'sales.departure_date', 'sales.class', 'sales.created_at']);
        return Datatables::eloquent($sales)
            ->make(true);
    }

    public function agentTable(Request $request)
    {
        $users = User::whereConsortiumId($request->id)->with('territory')->select(['users.id', 'users.name', 'users.email', 'users.territory_id', 'users.created_at']);
        return Datatables::eloquent($users)
            ->addColumn('territory', function (User $user) {
                return $user->territory ? str_limit($user->territory->name, 30, '...') : '';
            })
            ->escapeColumns(['territory'])
            ->make(true);
    }

    public function agencyTable(Request $request)
    {
        $agency = Agency::whereFkIdConsortiums($request->id)->select(['id', 'name', 'type']);
        return Datatables::eloquent($agency)
            ->make(true);
    }

    public function territoryTable(Request $request)
    {
        $territory = Territory::ByConsortium(Consortium::find($request->id))->with('manager')->select(['territories.id', 'territories.name', 'territories.user_id']);
        return Datatables::eloquent($territory)
            ->addColumn('user', function (Territory $territory) {
                return $territory->manager ? str_limit($territory->manager->name, 30, '...') : '';
            })
            ->escapeColumns(['user'])
            ->make(true);
    }
    public function storeTable(Request $request)
    {
        $stores = Store::whereStoreConsortiumId($request->id)
            ->with('agency', 'state', 'bdm')
            ->select([
                'stores.id',
                'stores.name',
                'stores.pcc',
                'stores.address1',
                'stores.address2',
                'stores.address3',
                'stores.suburb',
                'stores.postcode',
                'stores.store_agency_id',
                'stores.store_state_id',
                'stores.store_bdm_id'
            ]);
        return Datatables::eloquent($stores)
            ->addColumn('agency', function (Store $store) {
                return $store->agency ? str_limit($store->agency->name, 30, '...') : '';
            })
            ->addColumn('state', function (Store $store) {
                return $store->state ? str_limit($store->state->name, 30, '...') : '';
            })
            ->addColumn('bdm', function (Store$store) {
                return $store->bdm ? str_limit($store->bdm->name, 30, '...') : '';
            })
            ->escapeColumns(['agency'])
            ->escapeColumns(['state'])
            ->escapeColumns(['bdm'])
            ->make(true);
    }
}
