<?php

namespace App\Http\Controllers\Backend;

use App\Campaign;
use App\Destination;
use App\Reward;
use function compact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use const null;
use function redirect;
use Yajra\Datatables\Facades\Datatables;

class RewardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.reward.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $destinations = Destination::select('id', 'name')->get();
        return view('backend.reward.create', compact('destinations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255',
            'limit' => 'required',
            'endDate' => 'required',
            'type' => 'required'
        ]);
        $reward = Reward::create([
           'name' => $request->input('name'),
            'limit' => $request->input('limit'),
            'endDate' => $request->input('endDate'),
            'type' => $request->input('type'),
            'message' => $request->input('message'),
        ]);

        if($request->input('destinations')){
            $reward->label = Destination::find($request->input('destinations'))->name;
            $reward->save();
        }else{
            $reward->label = $request->input('label');
            $reward->save();
        }
        if($request->input('value')){
            $reward->value = $request->input('value');
            $reward->save();
        }
        if($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = str_random(10);
            $name = $fileName. '-' .$file->getClientOriginalName();
            $reward->image = $name;
            $file->move(public_path().'/assets/img/', $name);
            $reward->save();
        }

        return redirect('backend/rewards')->withSuccess('Reward Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agentsEligible = Reward::find($id)->agentsEligibleForReward()->with('campaigns', 'agents')->paginate(10);
        $reward = Reward::with('campaigns')->find($id);
        return view('backend.reward.show', compact('reward', 'agentsEligible'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reward = Reward::find($id);
        return view('backend.reward.edit', compact('reward'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name' => 'required|max:255'
        ]);

        $reward = Reward::find($id);
        $reward->name = $request->input('name');
        $reward->limit = $request->input('limit');
        $reward->endDate = $request->input('endDate');
        $reward->type = $request->input('type');
        $reward->message = $request->input('message');
        $reward->save();

        if($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = str_random(10);
            $name = $fileName. '-' .$file->getClientOriginalName();
            $reward->image = $name;
            $file->move(public_path().'/assets/img/', $name);
            $reward->save();
        }

        return redirect('backend/rewards')->withSuccess('Reward Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Reward::destroy($id);

        return redirect('backend/rewards')->withSuccess('Reward deleted!');
    }

    public function rewardsTable(){
        $rewards = Reward::with('campaigns')->select(['rewards.id', 'rewards.name', 'rewards.limit', 'rewards.endDate']);
        return Datatables::eloquent($rewards)
            ->make(true);
    }
}
