<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\User;
use Datatables;
use Illuminate\Http\Request;

class ArchivedAgentsController extends Controller
{
    public function index()
    {
        return view('backend.agents.archive');
    }

    public function agentsArchivedTable()
    {
        $activeUser = auth()->user();
        if ($activeUser->isBDM() == 1) {
            $territory_id = $activeUser->manageTerritory()->value('id');
            $users = User::onlyTrashed()
                ->where('territory_id', '=', $territory_id)
                ->whereHas('roles', function ($q) {
                    $q->whereIn('name', ['minor', 'mover']);
                })
                ->with('job.workplace.consortium', 'job.workplace.agency', 'territory.manager', 'roles')
                ->select(['users.id', 'users.name', 'users.email', 'users.phoneNumber','users.deleted_at']);
        } else {
            $users = User::onlyTrashed()
                ->with('job.workplace.consortium', 'job.workplace.agency', 'territory.manager', 'roles')
                ->whereHas('roles', function ($q) {
                    $q->whereIn('name', ['minor', 'mover']);
                })
                ->select(['users.id', 'users.name', 'users.email', 'users.phoneNumber','users.deleted_at']);
        }

        return Datatables::eloquent($users)
            ->addColumn('consortium', function (User $user) {
                if ($user->job) {
                    return $user->job->workplace->consortium ? str_limit($user->job->workplace->consortium->name, 30, '...') : '';
                } else {
                    return '';
                }
            })
            ->addColumn('agency', function (User $user) {
                if ($user->job) {
                    return $user->job->workplace->agency ? str_limit($user->job->workplace->agency->name, 30, '...') : '';
                } else {
                    return '';
                }
            })
            ->addColumn('territory', function (User $user) {
                if ($user->territory) {
                    return $user->territory->manager ? str_limit($user->territory->manager->name, 30, '...') : '';
                } else {
                    return '';
                }
            })
            ->addColumn('role', function (User $user) {
                return $user->roles ? str_limit($user->roles->first()->label, 30, '...') : '';
            })
            ->addColumn('action', function (User $user) {
                return view('backend.agents.button', compact('user'))->render();
            })
            ->escapeColumns(['consortium'])
            ->escapeColumns(['agency'])
            ->escapeColumns(['territory'])
            ->escapeColumns(['role'])
            ->escapeColumns(['action'])
            ->make(true);
    }

    public function restore(Request $request)
    {
        $user = User::onlyTrashed()->find($request->id);
        $user->active = 1;
        $user->save();

        User::onlyTrashed()->find($request->id)->restore();
        return redirect('backend/agents')->withSuccess('Agent Restored!');
    }
}
