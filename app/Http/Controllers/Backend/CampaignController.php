<?php

namespace App\Http\Controllers\Backend;

use App\Campaign;
use App\CampaignUser;
use App\Destination;
use App\Http\Controllers\Controller;
use App\Jobs\EmailCampaignAgents;
use App\Reward;
use App\SaleSegment;
use App\SystemSegment;
use App\Tag;
use App\TagChannel;
use App\TagObjective;
use App\TagType;
use App\Territory;
use App\TrainingSegment;
use Illuminate\Http\Request;
use Validator;
use Yajra\Datatables\Facades\Datatables;
use function dispatch;

class CampaignController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.campaign.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $territories = Territory::all('id', 'name');
        $destinations = Destination::all('id', 'name');
        $tagTypes = TagType::all();
        $tagObjs = TagObjective::all();
        $tagChannels = TagChannel::all();
        $additionalTags = Tag::all()->groupBy('parent_group');
        $saleSegments = SaleSegment::all('id', 'name');
        $systemSegments = SystemSegment::all('id', 'name');
        $trainingSegments = TrainingSegment::all('id', 'name');
        $rewards = Reward::all('id', 'name');
        return view('backend.campaign.create', compact('territories', 'destinations', 'tagTypes', 'tagObjs', 'tagChannels', 'additionalTags', 'saleSegments', 'systemSegments', 'trainingSegments', 'rewards'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'name'              => 'required|max:255|unique:campaigns',
            'type'              => 'required',
            'territories'       => 'array|min:0',
            'destinations'      => 'required|array|min:1',
            'startDate'         => 'required|date',
            'endDate'           => 'required|date',
            'image'             => 'required|image',
            'additional_tags'   => 'required|array|min:2',
            'sale_segments'     => 'array|min:0',
            'system_segments'   => 'array|min:0',
            'training_segments' => 'array|min:0',
            'reward_id'         => 'required|integer'
        ];
        $this->validate($request,$rules);
        $campaign = Campaign::create($request->all());

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = str_random(10);
            $name = $fileName . '-' . $file->getClientOriginalName();
            $campaign->image = $name;
            $file->move(public_path() . '/assets/img/', $name);
            $campaign->save();
        }

        $campaign->territories()->saveMany(Territory::findMany($request->territories));
        $campaign->tags()->saveMany(Tag::findMany($request->additional_tags));
        $campaign->saleSegments()->saveMany(SaleSegment::findMany($request->sale_segments));
        $campaign->systemSegments()->saveMany(SystemSegment::findMany($request->system_segments));
        $campaign->trainingSegments()->saveMany(TrainingSegment::findMany($request->training_segments));
        $campaign->destinations()->saveMany(Destination::findMany($request->destinations));

        if($request->input('status') == true){
            dispatch(new EmailCampaignAgents($campaign));
        }
        return redirect('backend/campaigns')->withSuccess('Campaign Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campaign = Campaign::with('tagType', 'tagObjective', 'tagChannel', 'destinations', 'territories','saleSegments','systemSegments','trainingSegments')->find($id);
        $campaignUsers = CampaignUser::where('campaign_id', '=', $id)->with('agents')->paginate(10);
        return view('backend.campaign.show', compact('campaign', 'campaignUsers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $territories = Territory::all('id', 'name');
        $destinations = Destination::all('id', 'name');
        $tagTypes = TagType::all();
        $tagObjs = TagObjective::all();
        $tagChannels = TagChannel::all();
        $additionalTags = Tag::all()->groupBy('parent_group');
        $saleSegments = SaleSegment::all('id', 'name');
        $systemSegments = SystemSegment::all('id', 'name');
        $trainingSegments = TrainingSegment::all('id', 'name');
        $campaign = Campaign::find($id);
        $destination_sel = $campaign->destinations()->get()->pluck('id')->toArray();
        $additionalTag_sel = $campaign->tags()->get()->pluck('id')->toArray();
        $territories_sel = $campaign->territories()->get()->pluck('id')->toArray();
        $saleSegment_sel = $campaign->saleSegments()->get()->pluck('id')->toArray();
        $systemSegment_sel = $campaign->systemSegments()->get()->pluck('id')->toArray();
        $trainingSegment_sel = $campaign->trainingSegments()->get()->pluck('id')->toArray();
        $rewards = Reward::all('id', 'name');
        return view('backend.campaign.edit', compact('campaign', 'territories', 'destinations', 'destination_sel', 'tagChannels', 'trainingSegment_sel', 'tagObjs',
            'tagTypes', 'additionalTags', 'additionalTag_sel', 'saleSegment_sel', 'saleSegments', 'systemSegments', 'trainingSegments', 'territories_sel', 'systemSegment_sel', 'rewards'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = [
            'name'              => 'required',
            'type'              => 'required',
            'territories'       => 'array|min:0',
            'destinations'      => 'required|array|min:1',
            'startDate'         => 'required',
            'endDate'           => 'required',
            'additional_tags'   => 'required|array|min:2',
            'sale_segments'     => 'array|min:0',
            'system_segments'   => 'array|min:0',
            'training_segments' => 'array|min:0',
            'reward_id'         => 'required|integer'
        ];

        Validator::make($request->input(), $rules);

        $campaign = Campaign::find($id);
        $campaign->fill($request->all());
        $campaign->save();

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = str_random(10);
            $name = $fileName . '-' . $file->getClientOriginalName();
            $campaign->image = $name;
            $file->move(public_path() . '/assets/img/', $name);
            $campaign->save();
        }

        $campaign->territories()->sync($request->territories);
        $campaign->saleSegments()->sync($request->sale_segments);
        $campaign->systemSegments()->sync($request->system_segments);
        $campaign->trainingSegments()->sync($request->training_segments);
        $campaign->tags()->sync($request->additional_tags);
        $campaign->destinations()->sync($request->destinations);

        return redirect('backend/campaigns')->withSuccess('Campaign Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Campaign::destroy($id);

        return redirect('backend/campaigns')->withSuccess('Campaign deleted!');
    }

    public function campaignsTable()
    {
        $campaigns = Campaign::with('territories')->select(['id', 'name', 'type', 'startDate', 'endDate', 'status']);
        return Datatables::eloquent($campaigns)
            ->addColumn('status', function ($campaign) {
                return $campaign->status == 1 ? '<span class="label label-success">Active</span>' : '<span class="label label-danger">Inactive</span>';
            })
            ->rawColumns(['status'])
            ->make(true);
    }
}
