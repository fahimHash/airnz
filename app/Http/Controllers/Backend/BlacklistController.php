<?php

namespace App\Http\Controllers\Backend;

use App\Blacklist;
use Datatables;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BlacklistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.agents.blacklist.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.agents.blacklist.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
           'email_domain' => 'required|string'
        ]);
        Blacklist::create($request->all());
        return redirect('backend/blacklists')->withSuccess('Email Domain Added!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Blacklist::destroy($id);
        return redirect('backend/blacklists')->withSuccess('Email Domain Deleted!');
    }

    public function blacklistTable()
    {
        $blacklist = Blacklist::select(['id', 'email_domain']);
        return Datatables::eloquent($blacklist)
            ->make(true);
    }
}
