<?php

namespace App\Http\Controllers\Backend;

use App\Destination;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActivityFeed;
use App\Sale;
use App\User;
use Session;
use Yajra\Datatables\Facades\Datatables;

class SalesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.sales.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::whereHas('roles', function ($q) {
            $q->where('name', '=', 'minor');
        })->orWhereHas('roles', function ($query) {
            $query->where('name', '=', 'mover');
        })->get();
        $destinations = Destination::all();
        return view('backend.sales.create', compact('users','destinations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'user_id' => 'required',
            'pnr' => 'required',
            'ticket_number' => 'required',
            'stock' => 'required',
            'issued_date' => 'required',
            'departure_date' => 'required',
            'cty1' => 'required',
            'cty2' => 'required'
            ]);

        $user = User::find($request->user_id);

        $sale = new Sale;
        $sale->status = $request->status;
        $sale->issued_date = $request->issued_date;
        $sale->pnr = $request->pnr;
        $sale->origin = $request->cty1;
        $sale->cty = $request->cty2;
        $sale->stock = $request->stock;
        $sale->ticket_number = $request->ticket_number;
        $sale->departure_date = $request->departure_date;
        $sale->class = $request->class;
        $sale->notes = $request->notes;
        $sale->store_id = $user->store_id;
        $sale->user_id = $user->id;
        $sale->territory_id = $user->territory_id;
        $sale->state_id = $user->state_id;
        $sale->consortium_id = $user->consortium_id;
        $sale->save();

        $activity = new ActivityFeed;
        $activity->module = 'Sales';
        $activity->activity_type = 'new-sale';
        $activity->activity_message = 'Sale has been successfully added!';
        $activity->user_id = $request->user_id;
        $activity->save();


        Session::flash('flash_message', 'Sale Added!');
        return redirect('backend/sales');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sale = Sale::with('user')->find($id);
        return view('backend.sales.show', compact('sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $sale = Sale::whereId($id)->with('user')->first();
        return view('backend.sales.edit', compact('sale'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, ['pnr' => 'required', 'ticket_number' => 'required', 'stock' => 'required', 'issued_date' => 'required', 'departure_date' => 'required']);

        $sale = Sale::find($id);
        $sale->status = $request->status;
        $sale->issued_date = $request->issued_date;
        $sale->pnr = $request->pnr;
        $sale->cty = $request->cty;
        $sale->stock = $request->stock;
        $sale->departure_date = $request->departure_date;
        $sale->class = $request->class;
        $sale->notes = $request->notes;
        $sale->save();

        Session::flash('flash_message', 'Sale Updated!');
        return redirect('backend/sales');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $sale = Sale::find($id);
        Sale::destroy($id);

        $activity = new ActivityFeed;
        $activity->module = 'Sales';
        $activity->activity_type = 'delete-sale';
        $activity->activity_message = 'Sale has been deleted successfully!';
        $activity->user_id = $sale->user_id;
        $activity->save();

        Session::flash('flash_message', 'Sale deleted!');
        return redirect('backend/sales');
    }

    /**
     * Process dataTable ajax response.
     *
     * @param \Yajra\Datatables\Datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function salesTable()
    {
        $activeUser = auth()->user();
        if ($activeUser->hasRole('admin') == 1 || $activeUser->hasRole('airnz-super-user') == 1) {

            $sales = Sale::with('user')->select(['sales.id', 'sales.user_id', 'sales.status', 'sales.issued_date', 'sales.pnr', 'sales.cty', 'sales.stock', 'sales.ticket_number', 'sales.departure_date', 'sales.class', 'sales.created_at']);

        } else if ($activeUser->hasRole('territory') == 1) {

            $territory_id = $activeUser->manageTerritory()->value('id');

            $sales = Sale::whereStateId($territory_id)->with('user')->select(['sales.id', 'sales.user_id', 'sales.status', 'sales.issued_date', 'sales.pnr', 'sales.cty', 'sales.stock', 'sales.ticket_number', 'sales.departure_date', 'sales.class', 'sales.created_at']);
        }

        return Datatables::eloquent($sales)
            ->make(true);
    }
}
