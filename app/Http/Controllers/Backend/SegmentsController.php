<?php

namespace App\Http\Controllers\Backend;

use App\Consortium;
use App\Destination;
use App\State;
use App\User;
use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use function response;
use timgws\QueryBuilderParser;
use Validator;
use Yajra\Datatables\Facades\Datatables;

class SegmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.segments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $consortiums = Consortium::all();
        $states = State::all();
        $destinations = Destination::all();
        return view('backend.segments.create', compact('consortiums', 'destinations', 'states'));
    }

    public function displayUserDatatable(Request $request)
    {
        Validator::make($request->all(), [
            'rules' => 'required|json'
        ])->validate();

        $table = DB::table('sales');
        $qbp = new QueryBuilderParser(
            array( 'status', 'issued_date', 'departure_date', 'pnr', 'stock', 'ticket_number', 'origin', 'cty', 'consortium_id', 'state_id')
        );
        try{
            $query = $qbp->parse($request->rules, $table);
            $results = $query->groupBy('user_id')->pluck('user_id');
            $agents = User::whereIn('id', $results->toArray())->select(['id','name','email']);
        }catch (Exception $exception){
            return response()->json(['result' => $exception->getMessage()]);
        }
        return Datatables::eloquent($agents)
            ->make(true);
    }
}
