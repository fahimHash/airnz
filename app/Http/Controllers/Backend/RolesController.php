<?php

namespace App\Http\Controllers\Backend;

use App\Permission;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Role;
use Session;

class RolesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $roles = Role::with('permissions')->paginate(20);
          return view('backend.roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('backend.roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,['name' => 'required']);
        Role::create( $request->all());
        Session::flash('flash_message','Role Added!');
        return redirect('backend/roles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::findOrFail($id);
        $users = Role::where('name',$role->name)->first()->users()->get();
        return view('backend.roles.show', compact('role','users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $role = Role::findOrFail($id);
      return view('backend.roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,['name' => 'required']);
        $role = Role::findOrFail($id);
        $role->update($request->all());
        Session::flash('flash_message','Role Updated!');
        return redirect('backend/roles');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        if(!$role->default){
            Role::destroy($id);
            Session::flash('flash_message','Role Deleted!');
        }else{
            Session::flash('flash_message','System default role cannot be deleted!');
        }
        return redirect('backend/roles');
    }

    /**
     * Display given permission to role.
     * @return void
     */

    public function getGivenRolePermission()
    {
        $roles = Role::select('id', 'name','label')->get();
        $permissions = Permission::select('id','name','label')->get();
        return view('backend.permissions.give-role-permissions',compact('roles','permissions'));
    }

    /**
     * Store given Permissions to Role
     *   @param Request $request
     */
    public function postGivenRolePermission(Request $request)
    {
        $this->validate($request, [
            'role' => 'required',
            'permissions' => 'required'
        ]);
        $role = Role::with('permissions')->whereName($request->role)->first();
        $role->permissions()->detach();

        foreach ($request->permissions as $permission_name) {
            $permission = Permission::whereName($permission_name)->first();
            $role->givePermissionTo($permission);
        }
        Session::flash('flash_message','Permission granted!');
        return redirect('backend/give-role-permissions');
    }
}
