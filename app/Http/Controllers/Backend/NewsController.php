<?php

namespace App\Http\Controllers\Backend;

use App\News;
use App\Territory;
use function back;
use function compact;
use File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Rule;
use Mews\Purifier\Facades\Purifier;
use function public_path;
use function redirect;
use function route;
use Validator;
use function view;
use Yajra\Datatables\Facades\Datatables;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.news.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $territories = Territory::all(['id', 'name']);
        return view('backend.news.create', compact('territories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['slug'] = str_slug($request->input('title'));

        Validator::make($data,[
            'title' => 'required',
            'summary' => 'required',
            'image' => 'required',
            'territory' => 'required|array|min:1',
            'slug' => Rule::unique('blog','slug')
        ])->validate();

        $news = News::create([
            'title' => $data['title'],
            'summary' => $data['summary'],
            'slug' => $data['slug']
        ]);

        if($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = str_random(10);
            $name = $fileName. '-' .$file->getClientOriginalName();
            $news->image = $name;
            $file->move(public_path().'/assets/img/', $name);
            $news->save();
        }
        $news->territories()->attach($request->territory);

        return redirect()->route('news.editor',['slug' => $news->slug])->withSuccess('Create your post content now!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $news = News::whereId($id)->with('territories')->first();
        return view('backend.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $territories = Territory::all(['id', 'name']);
        $news = News::find($id);
        $territory_sel = $news->territories()->get()->pluck('id')->toArray();
        return view('backend.news.edit', compact('news', 'territories', 'territory_sel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
            'summary' => 'required',
            'territory' => 'required|array|min:1'
        ]);

        $news = News::find($id);
        $news->fill($request->except('territory'));
        $news->save();

        if($request->hasFile('image')) {
            $file = $request->file('image');
            $fileName = str_random(10);
            $name = $fileName. '-' .$file->getClientOriginalName();
            $news->image = $name;
            $file->move(public_path().'/assets/img/', $name);
            $news->save();
        }

        $news->territories()->detach();
        $news->territories()->attach($request->territory);

        return redirect('backend/news')->withSuccess('News Post Updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        News::destroy($id);
        return redirect('backend/news')->withSuccess('News Post Deleted!');
    }

    public function newsTable(){
        $news = News::with('territories')->select(['id','title','summary']);
        return Datatables::eloquent($news)
            ->addColumn('territories_count', function (News $news) {
                return $news->territories->count();
            })
            ->escapeColumns(['territories_count'])
            ->make(true);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function editor($slug)
    {
        $post = News::whereSlug($slug)->firstOrFail();
        return view('backend.news.editor.index',compact('post'));
    }

    public function editorStaticContent()
    {
        return view('backend.news.editor.snippets');
    }

    public function updateContent(Request $request, $slug)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);
        $post = News::whereSlug($slug)->firstOrFail();
        //$post->message = Purifier::clean(Input::get('message'));
        $post->message = Input::get('message');
        $post->save();

        return redirect()->route('news.preview.content',['slug' => $post->slug]);
    }

    public function previewContent($slug)
    {
        $post = News::whereSlug($slug)->firstOrFail();
        return view('backend.news.editor.preview', compact('post'));
    }

    public function publish($id)
    {
        $post = News::find($id);
        if($post->publish == false)
            $post->publish = true;
        else{
            $post->publish = false;
        }
        $post->save();

        return redirect()->route('news.show', [$id]);
    }
}
