<?php

namespace App\Http\Controllers\Backend;

use App\Agency;
use App\Consortium;
use App\Http\Requests\Backend\StoreRequest;
use App\State;
use App\Store;
use App\Territory;
use App\User;
use App\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Facades\Datatables;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.store.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $consortiumList = Consortium::with('agencies')->get();
        $states = State::pluck('name', 'id')->all();
        $users = User::byRole(Role::ROLE_BDM)->pluck('name', 'id')->all();
        $bdmSelected = $stateSelected = 0;
        return view('backend.store.create',
            compact(
                'consortiumList',
                'states',
                'stateSelected',
                'users',
                'bdmSelected'
            ));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRequest|Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request)
    {
        $agency = Agency::find($request->agency);
        $consortium = Consortium::find($agency->fk_id_consortiums);
        $bdm = User::with('manageTerritory')->find($request->bdm);
        $state = State::find($request->state);

        /*
         * If BDM does not belong to any territory
         * We create new territory
         * Associate Store to it
         */

        if ($bdm->manageTerritory) {
            $territory = Territory::find($bdm->manageTerritory->id);
        } else {
            $territory = new Territory([
                'name'        => $bdm->name . '\'s Territory',
                'sale_target' => 300
            ]);
            $territory->save();
            $territory->manager()->associate($bdm)->save();
        }

        $store = Store::create($request->only([
            'name',
            'pcc',
            'address1',
            'address2',
            'address3',
            'suburb',
            'postcode'
        ]));

        $store->agency()->associate($agency);
        $store->consortium()->associate($consortium);
        $store->bdm()->associate($bdm);
        $store->state()->associate($state);
        $store->territory()->associate($territory);
        $store->save();
        $territory->consortiums()->syncWithoutDetaching([$consortium->id]);
        $territory->agencies()->syncWithoutDetaching([$agency->id]);
        $territory->states()->syncWithoutDetaching([$state->id]);
        $territory->save();

        if ($store->exists) {
            return redirect('backend/store')->withSuccess('Store Added!');
        } else {
            return redirect('backend/store')->withError('Error! Couldn\'t create store. ');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param Store $store
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function show(Store $store)
    {
        $store->with('consortium', 'agency', 'state','bdm')->get();
        return view('backend.store.show', compact('store'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Store $store
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function edit(Store $store)
    {
        $consortiumList = Consortium::with('agencies')->get();
        $states = State::pluck('name', 'id')->all();
        $users = User::byRole(ROLE::ROLE_BDM)->pluck('name', 'id')->all();
        $bdmSelected = $store->store_bdm_id;
        $stateSelected = $store->store_state_id;
        return view('backend.store.edit',
            compact(
                'store',
                'consortiumList',
                'states',
                'stateSelected',
                'users',
                'bdmSelected'
            ));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StoreRequest|Request $request
     * @param Store $store
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(StoreRequest $request, Store $store)
    {
        $store->update($request->only([
            'name',
            'pcc',
            'address1',
            'address2',
            'address3',
            'suburb',
            'postcode'
        ]));

        $agency = Agency::find($request->agency);
        $consortium = Consortium::find($agency->fk_id_consortiums);
        $bdm = User::with('manageTerritory')->find($request->bdm);
        $state = State::find($request->state);
        /*
         * If BDM does not belong to any territory
         * We create new territory
         * Associate Store to it
         */
        if ($bdm->manageTerritory->id) {
            $territory = Territory::find($bdm->manageTerritory->id);
        } else {
            $territory = new Territory([
                'name'        => $bdm->name . '\'s Territory',
                'sale_target' => 300,
                'type'        => $agency->type
            ]);
            $territory->save();
            $territory->manager()->associate($bdm)->save();
        }
        if($store->consortium->id != $consortium->id){
            $territory->consortiums()->detach([$store->consortium->id]);
        }
        if($store->agency->id != $request->agency){
            $territory->agencies()->detach([$store->agency->id]);
        }
        if($store->state->id != $request->state){
            $territory->states()->detach([$store->state->id]);
        }
        $store->agency()->associate($agency);
        $store->consortium()->associate($consortium);
        $store->bdm()->associate($bdm);
        $store->state()->associate($state);
        $store->territory()->associate($territory);
        $store->save();

        $territory->consortiums()->syncWithoutDetaching([$consortium->id]);
        $territory->agencies()->syncWithoutDetaching([$agency->id]);
        $territory->states()->syncWithoutDetaching([$state->id]);
        $territory->save();

        if ($store->exists) {
            return redirect('backend/store')->withSuccess('Store Updated!');
        } else {
            return redirect('backend/store')->withError('Error! Couldn\'t update store. ');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Store $store
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function destroy(Store $store)
    {
        $store->delete();
        return redirect('backend/store')->withSuccess('Store Deleted!');
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function storeTable()
    {
        $stores = Store::with('consortium', 'agency', 'state', 'bdm')->where('name','!=','Other')->select(['id', 'name', 'pcc', 'store_consortium_id', 'store_agency_id', 'store_state_id', 'store_bdm_id']);
        return Datatables::eloquent($stores)
            ->addColumn('consortium', function ($store) {
                if ($store->consortium->name) {
                    return $store->consortium->name ? str_limit($store->consortium->name, 40, '...') : '';
                } else {
                    return '';
                }
            })
            ->addColumn('agency', function ($store) {
                if ($store->agency->name) {
                    return $store->agency->name ? str_limit($store->agency->name, 40, '...') : '';
                } else {
                    return '';
                }
            })
            ->addColumn('state', function ($store) {
                if ($store->state->name) {
                    return $store->state->name ? str_limit($store->state->name, 30, '...') : '';
                } else {
                    return '';
                }
            })
            ->addColumn('bdm', function ($store) {
                if ($store->bdm->name) {
                    return $store->bdm->name ? str_limit($store->bdm->name, 30, '...') : '';
                } else {
                    return '';
                }
            })
            ->escapeColumns(['consortium'])
            ->escapeColumns(['agency'])
            ->escapeColumns(['state'])
            ->escapeColumns(['bdm'])
            ->make(true);
    }
}
