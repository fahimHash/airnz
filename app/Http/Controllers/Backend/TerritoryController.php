<?php

namespace App\Http\Controllers\Backend;

use App\Agency;
use App\Sale;
use App\State;
use App\Role;
use App\Store;
use function array_add;
use function array_push;
use function back;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Territory;
use App\Suburb;
use App\Branch;
use App\Consortium;
use Illuminate\Support\Facades\DB;
use Redirect;
use Session;
use Validator;
use Yajra\Datatables\Facades\Datatables;

class TerritoryController extends Controller
{
    // StateController = TerritoryController

    /**
     * Instantiate a new StateController instance.
     */
    public function __construct()
    {
        $this->middleware('can:manage_state', ['only' => ['create', 'store', 'edit', 'update', 'destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('backend.territory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::byRole(Role::ROLE_BDM)->get();
        $states = State::select('id', 'name')->get();
        $consortiums = Consortium::select('id', 'name')->get();
        return view('backend.territory.create', compact('users', 'consortiums', 'states'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'unique' => 'The BDM has already been assigned to another territory.',
            'name'   => 'Territory name already exist, please change the name.'
        ];

        Validator::make($request->all(), [
            'name'        => 'required|unique:territories,name',
            'user'        => 'unique:territories,user_id',
            'type'        => 'required',
            'states'      => 'required',
            'consortiums' => 'required',
            'agency'      => 'required'
        ], $messages)->validate();

        $territory = new Territory();
        $territory->name = $request->name;
        $territory->sale_target = $request->sale_target;

        if ($request->has('user')) {
            $territory->user_id = $request->user;
        }

        $territory->save();

        /*
         * Determining if a Agency - State Relation Already Exists
         */
        $removeAgency = [];

        foreach ($request->agency as $agency) {
            $flag = 0;
            $agencyRecord = Agency::find($agency);
            foreach ($request->states as $state) {
                $stateRecord = State::find($state);
                {
                    if ($agencyRecord->state->contains($stateRecord)) {
                        $removeAgency = array_add($removeAgency, $agencyRecord->id, 'Duplicacy Check : ' . $agencyRecord->name . ' from ' . $stateRecord->name . ' belongs to another territory. Change State to add this agency to this territory.');
                        $flag++;
                    } else {
                        $agencyRecord->state()->attach($stateRecord, ['territory_id' => $territory->id]);
                    }
                }
            }
            if ($flag != count($request->states)) {
                $territory->agencies()->attach($agencyRecord);
            }
        }

        $territory->states()->attach($request->states);
        $territory->consortiums()->attach($request->consortiums);

        User::whereStateId($territory->id)
            ->where('sale_target', '<', $request->sale_target)
            ->update(['sale_target' => $request->sale_target]);

        if (count($removeAgency) == 0) {
            return redirect('backend/territory/' . $territory->id)->withSuccess('Territory Added!');
        } else {
            $removeAgency = array_add($removeAgency, 'info', 'An agency can belong to any 8 states of Australia. unfortunately it cannot be on same state twice!');
            return redirect('backend/territory/' . $territory->id)->withErrors($removeAgency);
        }
    }

    /**
     * Display the form for assigning resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $territory = Territory::whereId($id)->with('manager', 'consortiums', 'agencies', 'states', 'agents', 'sales')->first();
        $percentage_agent = round( $this->division($territory->agents()->count(), User::ByAgents()->count()) * 100);
        $percentage_sales = round($this->division($territory->sales()->count() , (Sale::whereStatus('Valid')->count() + Sale::whereStatus('Pending')->count())) * 100);
        $valid = Sale::whereTerritoryId($id)->whereStatus('Valid')->count();
        $pending = Sale::whereTerritoryId($id)->whereStatus('Pending')->count();
        $problem = Sale::whereTerritoryId($id)->whereStatus('Problem')->count();
        $cancelled = Sale::whereTerritoryId($id)->whereStatus('Cancelled')->count();
        return view('backend.territory.show', compact(
            'territory',
            'percentage_agent',
            'percentage_sales',
            'valid',
            'pending',
            'problem',
            'cancelled'
        ));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $territory = Territory::find($id);
        $manager_sel = $territory->manager;
/*
        $consortiums_sel = $territory->consortiums->pluck('id')->toArray();
        $agency_sel = $territory->agencies->pluck('id')->toArray();
        $state_sel = $territory->states->pluck('id')->toArray();

        $agencies = Agency::select('id', 'name')->whereIn('fk_id_consortiums', $consortiums_sel)->get();
        $consortiums = Consortium::select('id', 'name')->get();
        $states = State::select('id', 'name')->get();
 */
        $users = User::byRole(Role::ROLE_BDM)->get();
        return view('backend.territory.edit', compact('territory', 'users', 'manager_sel'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $territory = Territory::find($id);

        if ($territory->user_id == $request->user) {
            $rules = [
                'name'        => 'required',
                'user'        => 'required',
            ];
        } else {
            $rules = [
                'name'        => 'required',
                'user'        => 'required|unique:territories,user_id',
            ];
        }

        $messages = [
            'unique' => 'The BDM has already been assigned to another territory.',
        ];

        Validator::make($request->all(), $rules, $messages)->validate();

        $territory->name = $request->name;
        $territory->sale_target = $request->sale_target;
        $territory->save();

        if ($territory->user_id != $request->user) {
            $ex_bdm = $territory->user_id;
            $territory->user_id = $request->user;
            $territory->save();
            Store::whereStoreBdmId($ex_bdm)->update(['store_bdm_id' => $request->user]);
        }

/*
        $territory->consortiums()->detach();
        $territory->agencies()->detach();
        $territory->states()->detach();

        DB::table('agency_state')->where('territory_id', '=', $territory->id)->delete();
*/
        /*
         * Determining if a Agency - State Relation Already Exists
         */
/*        $removeAgency = [];

        foreach ($request->agency as $agency) {
            $flag = 0;
            $agencyRecord = Agency::find($agency);
            foreach ($request->states as $state) {
                $stateRecord = State::find($state);
                {
                    if ($agencyRecord->state->contains($stateRecord)) {
                        $removeAgency = array_add($removeAgency, $agencyRecord->id, 'Duplicacy Check : ' . $agencyRecord->name . ' from ' . $stateRecord->name . ' belongs to another territory. Change State to add this agency to this territory.');
                        $flag++;
                    } else {
                        $agencyRecord->state()->attach($stateRecord, ['territory_id' => $territory->id]);
                    }
                }
            }
            if ($flag != count($request->states)) {
                $territory->agencies()->attach($agencyRecord);
            }
        }

        $territory->states()->attach($request->states);
        $territory->consortiums()->attach($request->consortiums);
*/
        User::whereTerritoryId($territory->id)->where('sale_target', '<', $request->sale_target)->update(['sale_target' => $request->sale_target]);
        User::whereTerritoryId($territory->id)->whereNull('sale_target')->update(['sale_target' => $request->sale_target]);

        /*if (count($removeAgency) == 0) {*/
            return redirect('backend/territory/' . $territory->id)->withSuccess('Territory Updated!');
        /*} else {
            $removeAgency = array_add($removeAgency, 'info', 'An agency can belong to any 8 states of Australia. unfortunately it cannot be on same state twice!');
            return redirect('backend/territory/' . $territory->id)->withErrors($removeAgency);
        }*/
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $territory = Territory::find($id);
        if(!$territory->default){
            Territory::destroy($id);
            Session::flash('flash_message', 'Territory deleted!');
        }else{
            Session::flash('flash_message', 'System default territory cannot be deleted!');
        }
        return redirect('backend/territory');
    }

    public function assign()
    {
        $states = Territory::select('id', 'name')->get();
        $users = User::byRole('territory')->get();
        return view('backend.territory.assign', compact('states', 'users'));
    }

    public function agencyAjax(Request $request)
    {
        $agency = Agency::select('id', 'name')->whereIn('fk_id_consortiums', json_decode($request->consortiums))->whereIn('type', json_decode($request->type))->orderBy('name', 'asc')->get();
        return response()->json($agency);
    }

//    public function branchAjax(Request $request){
//
//        $branch = Branch::select('id','name')->whereIn('consortiums_id', json_decode($request->consortium))->whereIn('agencies_id', json_decode($request->agency))->orderBy('name','asc')->get();
//        return response()->json($branch);
//    }

    public function territoryTable()
    {
        $activeUser = auth()->user();
        if ($activeUser->isBDM() == 1) {
            $territory_id = $activeUser->manageTerritory()->value('id');
            $territories = Territory::where('id', '=', $territory_id)->with('manager','agents','consortiums','agencies','stores','states')
                ->select(['territories.id', 'territories.name', 'territories.user_id']);
        } else {
            $territories = Territory::with('manager','agents','consortiums','agencies','stores','states')
                ->select(['territories.id', 'territories.name', 'territories.user_id']);
        }
        return Datatables::eloquent($territories)
            ->addColumn('consortiums_count', function (Territory $territory) {
                return $territory->consortiums->count();
            })
            ->addColumn('agencies_count', function (Territory $territory) {
                return $territory->agencies->count();
            })
            ->addColumn('stores_count', function (Territory $territory) {
                return $territory->stores->count();
            })
            ->addColumn('agents_count', function (Territory $territory) {
                return $territory->agents->count();
            })
            ->addColumn('states_count', function (Territory $territory) {
                return $territory->states->count();
            })
            ->addColumn('percentage', function (Territory $territory) {
                $total_agents = User::ByAgents()->count();
                if ($territory->agents) {
                    $count = $territory->agents->count();
                    if ($count > 0) {
                        return round(($count / $total_agents) * 100) . '%';
                    } else {
                        return '0%';
                    }
                } else {
                    return '0%';
                }
            })
            ->escapeColumns(['consortiums_count'])
            ->escapeColumns(['agencies_count'])
            ->escapeColumns(['stores_count'])
            ->escapeColumns(['agents_count'])
            ->escapeColumns(['states_count'])
            ->escapeColumns(['percentage'])
            ->make(true);
    }

    public function dateRange(Request $request)
    {
        $this->validate($request, [
            'startDate' => 'required|date_format:"Y-m-d"',
            'endDate'   => 'required|date_format:"Y-m-d"',
            'id'        => 'required|numeric'
        ]);

        $startDate = Carbon::parse($request->input('startDate'));
        $endDate = Carbon::parse($request->input('endDate'));
        $difference = $startDate->diffInDays($endDate);

        if ($difference == 0) {
            //today or yesterday or any single date
            $validSales = Sale::whereStateId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereStateId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            return response()->json([
                'labels'  => [$startDate->format('d-m-Y')],
                'valid'   => $validSales,
                'pending' => $pendingSales
            ]);
        } elseif ($difference == 6) {
            //a week
            $validSales = Sale::whereStateId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%W') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereStateId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%W') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $validFormatted = [
                ['time' => 'Monday', 'sales' => 0],
                ['time' => 'Tuesday', 'sales' => 0],
                ['time' => 'Wednesday', 'sales' => 0],
                ['time' => 'Thursday', 'sales' => 0],
                ['time' => 'Friday', 'sales' => 0],
                ['time' => 'Saturday', 'sales' => 0],
                ['time' => 'Sunday', 'sales' => 0]
            ];

            $pendingFormatted = $validFormatted;

            for ($i = 0; $i < count($validSales); $i++) {
                for ($j = 0; $j < count($validFormatted); $j++) {
                    if ($validFormatted[$j]['time'] == $validSales[$i]['time']) {
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for ($i = 0; $i < count($pendingSales); $i++) {
                for ($j = 0; $j < count($pendingFormatted); $j++) {
                    if ($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']) {
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels'  => ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                'valid'   => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        } elseif ($difference == 29 || $difference == 30 || $difference >= 7 && $difference < 31) {
            //day by day view

            $dates = [];
            $validFormatted = [];
            $pendingFormatted = [];

            for ($date = Carbon::parse($request->input('startDate')); $date->lte(Carbon::parse($request->input('endDate'))); $date->addDay()) {
                $dates[] = $date->format('d-m-Y');
                $validFormatted [] = ['time' => $date->format('d-m-Y'), 'sales' => 0];
                $pendingFormatted [] = ['time' => $date->format('d-m-Y'), 'sales' => 0];
            }

            $validSales = Sale::whereStateId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereStateId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            for ($i = 0; $i < count($validSales); $i++) {
                for ($j = 0; $j < count($validFormatted); $j++) {
                    if ($validFormatted[$j]['time'] == $validSales[$i]['time']) {
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for ($i = 0; $i < count($pendingSales); $i++) {
                for ($j = 0; $j < count($pendingFormatted); $j++) {
                    if ($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']) {
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels'  => $dates,
                'valid'   => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        } else {
            //custom range greater than 30 days show month wise
            $dates = [];
            $validFormatted = [];
            $pendingFormatted = [];

            for ($date = Carbon::parse($request->input('startDate')); $date->lte(Carbon::parse($request->input('endDate'))); $date->addMonth()) {
                $dates[] = $date->format('F-Y');
                $validFormatted [] = ['time' => $date->format('F-Y'), 'sales' => 0];
                $pendingFormatted [] = ['time' => $date->format('F-Y'), 'sales' => 0];
            }

            $validSales = Sale::whereStateId($request->input('id'))
                ->whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereStateId($request->input('id'))
                ->whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            for ($i = 0; $i < count($validSales); $i++) {
                for ($j = 0; $j < count($validFormatted); $j++) {
                    if ($validFormatted[$j]['time'] == $validSales[$i]['time']) {
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for ($i = 0; $i < count($pendingSales); $i++) {
                for ($j = 0; $j < count($pendingFormatted); $j++) {
                    if ($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']) {
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels'  => $dates,
                'valid'   => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        }
    }

    /**
     * Process dataTable ajax response.
     *
     * @param \Yajra\Datatables\Datatables
     * @return \Illuminate\Http\JsonResponse
     */
    public function salesTable(Request $request)
    {
        $sales = Sale::whereStateId($request->id)->select(['sales.id', 'sales.status', 'sales.issued_date', 'sales.pnr', 'sales.cty', 'sales.stock', 'sales.ticket_number', 'sales.departure_date', 'sales.class', 'sales.created_at']);
        return Datatables::eloquent($sales)
            ->make(true);
    }

    public function agentTable(Request $request)
    {
        $users = User::whereStateId($request->id)->with('sales', 'territory')->select(['users.id', 'users.name', 'users.email', 'users.state_id', 'users.created_at']);
        return Datatables::eloquent($users)
            ->addColumn('confirmed', function (User $user) {
                if ($user->sales) {
                    return $user->sales->where('status', 'Valid')->count();
                } else {
                    return '';
                }
            })
            ->addColumn('pending', function (User $user) {
                if ($user->sales) {
                    return $user->sales->where('status', 'Pending')->count();
                } else {
                    return '';
                }
            })
            ->addColumn('cancelled', function (User $user) {
                if ($user->sales) {
                    return $user->sales->where('status', 'Cancelled')->count();
                } else {
                    return '';
                }
            })
            ->escapeColumns(['confirmed'])
            ->escapeColumns(['pending'])
            ->escapeColumns(['cancelled'])
            ->make(true);
    }

    public function agencyTable(Request $request)
    {
        $agency = Agency::ByTerritoryId($request->id)->select(['id', 'name', 'type']);
        return Datatables::eloquent($agency)
            ->make(true);
    }

    public function consortiumsTable(Request $request)
    {
        $consortiums = Consortium::ByTerritory($request->id)
            ->with('agencies', 'sales', 'users')
            ->select(['id', 'name']);

        return Datatables::eloquent($consortiums)
            ->addColumn('agencies', function (Consortium $consortium) {
                if ($consortium->agencies) {
                    return $consortium->agencies->count();
                } else {
                    return '';
                }
            })
            ->addColumn('agents', function (Consortium $consortium) {
                if ($consortium->users) {
                    return $consortium->users->count();
                } else {
                    return '';
                }
            })
            ->addColumn('sales', function (Consortium $consortium) {
                if ($consortium->sales) {
                    return $consortium->sales->count();
                } else {
                    return '';
                }
            })
            ->addColumn('percentage', function (Consortium $consortium) {
                $total_sale = Sale::count();
                if ($consortium->sales) {
                    $count = $consortium->sales->count();
                    if ($count > 0) {
                        return round(($count / $total_sale) * 100) . '%';
                    } else {
                        return '0%';
                    }
                } else {
                    return '0%';
                }
            })
            ->escapeColumns(['agencies'])
            ->escapeColumns(['agents'])
            ->escapeColumns(['sales'])
            ->escapeColumns(['percentage'])
            ->make(true);
    }

    public function flights(Request $request)
    {
        $flight = Sale::select('cty', DB::raw("(COUNT(id)) as sales"))
            ->whereStatus('Valid')
            ->where('cty', 'like', $request->input('code') . '-%')
            ->whereBetween(DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')])
            ->orWhere('cty', 'like', '%-' . $request->input('code'))
            ->groupBy('cty')
            ->get()
            ->toJSON();

        return $flight;
    }

    private function division($a, $b)
    {
        $c = @($a / $b);
        if ($b === 0) {
            $c = null;
        }
        return $c;
    }

    public function storeTable(Request $request)
    {
        $stores = Store::whereStoreTerritoryId($request->id)
            ->with('consortium','agency', 'state', 'bdm')
            ->select([
                'stores.id',
                'stores.name',
                'stores.pcc',
                'stores.address1',
                'stores.address2',
                'stores.address3',
                'stores.suburb',
                'stores.postcode',
                'stores.store_consortium_id',
                'stores.store_agency_id',
                'stores.store_state_id',
                'stores.store_bdm_id'
            ]);
        return Datatables::eloquent($stores)
            ->addColumn('consortium', function (Store $store) {
                return $store->consortium ? str_limit($store->consortium->name, 30, '...') : '';
            })
            ->addColumn('agency', function (Store $store) {
                return $store->agency ? str_limit($store->agency->name, 30, '...') : '';
            })
            ->addColumn('state', function (Store $store) {
                return $store->state ? str_limit($store->state->name, 30, '...') : '';
            })
            ->addColumn('bdm', function (Store$store) {
                return $store->bdm ? str_limit($store->bdm->name, 30, '...') : '';
            })
            ->escapeColumns(['consortium'])
            ->escapeColumns(['agency'])
            ->escapeColumns(['state'])
            ->escapeColumns(['bdm'])
            ->make(true);
    }
}
