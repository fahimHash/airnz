<?php

namespace App\Http\Controllers\Backend;

use App\Destination;
use App\Http\Controllers\Controller;
use App\Permission;
use App\Role;
use App\User;
use function array_push;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
use App\Sale;
use App\Territory;

/**
 * Class DashboardController
 * @package App\Http\Controllers\Backend
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $activeUser = auth()->user();
        if($activeUser->hasRole(Role::ROLE_ADMIN) == 1 || $activeUser->hasRole(Role::ROLE_SUPER_USER) == 1){
          $sales = Sale::count();
          $agents = User::whereHas('roles', function($q){
            $q->where('name', '=', Role::ROLE_MINOR);
          })->orWhereHas('roles', function($query){
            $query->where('name', '=',Role::ROLE_MOVER);
          })->count();
        }else if($activeUser->hasRole(Role::ROLE_BDM) == 1){

          $id = $activeUser->manageTerritory()->value('id');
          if($id){
              $sales = Sale::whereTerritoryId($id)->count();
              $agents = User::where('territory_id','=', $id)->whereHas('roles', function($q){
                  $q->where('name', '=', Role::ROLE_MINOR);
              })->orWhereHas('roles', function($query){
                  $query->where('name', '=',Role::ROLE_MOVER);
              })->count();
          }else{
              $sales = 0;
              $agents = 0;
          }
        }
        $destinations = Destination::all();
        $territories = Territory::all();
        $sales_target = [];
        foreach($territories as $territory){
            $target = User::whereTerritoryId($territory->id)->sum('sale_target');
            $s = Sale::whereTerritoryId($territory->id)->whereStatus('Valid')->count('id');
            if($s > 0 && $target > 0){
                $percentage = ((int)$s/$target)*100;
            }else{
                $percentage = 0;
            }
            array_push($sales_target, ['territory'=>$territory->name,'target' => $target, 'sales' => $s,'percentage'=>$percentage]);
        }

        $currentWeekStart = Carbon::now()->startOfWeek()->toDateString();
        $currentWeekEnd = Carbon::now()->endOfWeek()->toDateString();
        $pastWeekStart = Carbon::now()->subWeek()->startOfWeek()->toDateString();
        $pastWeekEnd = Carbon::now()->subWeek()->endOfWeek()->toDateString();

        $userCreatedThisWeek = User::select(DB::raw("(COUNT(id)) as usersCount"))
            ->whereBetween( DB::raw('date(created_at)'), [$currentWeekStart, $currentWeekEnd])
            ->first()
            ->toArray();
        $userCreatedLastWeek = User::select(DB::raw("(COUNT(id)) as usersCount"))
            ->whereBetween( DB::raw('date(created_at)'), [$pastWeekStart, $pastWeekEnd])
            ->first()
            ->toArray();
        $percentageNewAgent = $this->calculatePercentage($userCreatedLastWeek['usersCount'], $userCreatedThisWeek['usersCount']);
        $newAgents = $userCreatedThisWeek['usersCount'];

        $userTrashCreatedThisWeek = User::onlyTrashed()
            ->select(DB::raw("(COUNT(id)) as usersCount"))
            ->whereBetween( DB::raw('date(deleted_at)'), [$currentWeekStart, $currentWeekEnd])
            ->first()
            ->toArray();
        $userTrashCreatedLastWeek = User::onlyTrashed()
            ->select(DB::raw("(COUNT(id)) as usersCount"))
            ->whereBetween( DB::raw('date(deleted_at)'), [$pastWeekStart, $pastWeekEnd])
            ->first()
            ->toArray();
        $percentageTrashAgent = $this->calculatePercentage($userTrashCreatedLastWeek['usersCount'], $userTrashCreatedThisWeek['usersCount']);
        $archivedAgent = User::onlyTrashed()->count();

        $userCreatedTillLastWeek = User::select(DB::raw("(COUNT(id)) as usersCount"))
            ->where( DB::raw('date(created_at)'), '<=', $pastWeekEnd)
            ->first()
            ->toArray();
        $percentageTotalAgent = $this->calculatePercentage($userCreatedTillLastWeek['usersCount'], $userCreatedThisWeek['usersCount']);

        $saleCreatedThisWeek = Sale::select(DB::raw("(COUNT(id)) as salesCount"))
            ->whereBetween( DB::raw('date(created_at)'), [$currentWeekStart, $currentWeekEnd])
            ->first()
            ->toArray();
        $saleCreatedLastWeek = Sale::select(DB::raw("(COUNT(id)) as salesCount"))
            ->whereBetween( DB::raw('date(created_at)'), [$pastWeekStart, $pastWeekEnd])
            ->first()
            ->toArray();
        $percentageSale = $this->calculatePercentage($saleCreatedLastWeek['salesCount'], $saleCreatedThisWeek['salesCount']);

        return view('backend.dashboard', compact(
            'sales',
            'agents',
            'destinations',
            'sales_target',
            'percentageNewAgent',
            'percentageTrashAgent',
            'percentageTotalAgent',
            'percentageSale',
            'newAgents',
            'archivedAgent'
        ));
    }

    private function calculatePercentage($valueOne, $valueTwo){
        try{
            $value = (($valueTwo - $valueOne)/$valueOne)*100;
        }catch (Exception $e){
            $value = (($valueTwo - 1)/1)*100;
        }
        if($value > 0){
            return ['value' => round($value), 'sign' => 'up', 'bgColor' => 'green'];
        }else if($value < 0){
            return ['value' => round($value), 'sign' => 'down', 'bgColor' => 'red'];
        }else{
            return ['value' => round($value), 'sign' => 'left', 'bgColor' => 'yellow'];
        }
    }

    public function dateRange(Request $request)
    {
        $this->validate($request,[
            'startDate' => 'required|date_format:"Y-m-d"',
            'endDate' => 'required|date_format:"Y-m-d"'
        ]);
//        DB::enableQueryLog();
        $startDate = Carbon::parse($request->input('startDate'));
        $endDate = Carbon::parse($request->input('endDate'));
        $difference = $startDate->diffInDays($endDate);
//        dd(DB::getQueryLog());

        if($difference == 0 ){
            //today or yesterday or any single date
            $validSales = Sale::whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween( DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')] )
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween( DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')] )
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            return response()->json([
                'labels' => [$startDate->format('d-m-Y')],
                'valid' => $validSales,
                'pending' => $pendingSales
            ]);
        }elseif ($difference == 6){
            //a week
            $validSales = Sale::whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%W') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween( DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')] )
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%W') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween( DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')] )
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $validFormatted = array(
                array('time' => 'Monday', 'sales' => 0 ),
                array('time' => 'Tuesday', 'sales' => 0),
                array('time' => 'Wednesday', 'sales' => 0 ),
                array('time' => 'Thursday', 'sales' => 0 ),
                array('time' => 'Friday', 'sales' => 0 ),
                array('time' => 'Saturday', 'sales' => 0 ),
                array('time' => 'Sunday', 'sales' => 0 )
            );

            $pendingFormatted = $validFormatted;

            for($i = 0; $i < count($validSales); $i++ ){
                for($j = 0; $j < count($validFormatted); $j++){
                    if($validFormatted[$j]['time'] == $validSales[$i]['time']){
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for($i = 0; $i < count($pendingSales); $i++ ){
                for($j = 0; $j < count($pendingFormatted); $j++){
                    if($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']){
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels' => ['Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sunday'],
                'valid' => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        }elseif ($difference == 29 || $difference == 30 || $difference >= 7 && $difference < 31){
            //day by day view

            $dates = [];
            $validFormatted = [];
            $pendingFormatted = [];

            for($date = Carbon::parse($request->input('startDate')); $date->lte(Carbon::parse($request->input('endDate'))); $date->addDay()) {
                $dates[] = $date->format('d-m-Y');
                $validFormatted [] = ['time' => $date->format('d-m-Y'), 'sales' => 0];
                $pendingFormatted [] = ['time' => $date->format('d-m-Y'), 'sales' => 0];
            }

            $validSales = Sale::whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween( DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')] )
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%d-%m-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween( DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')] )
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            for($i = 0; $i < count($validSales); $i++ ){
                for($j = 0; $j < count($validFormatted); $j++){
                    if($validFormatted[$j]['time'] == $validSales[$i]['time']){
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for($i = 0; $i < count($pendingSales); $i++ ){
                for($j = 0; $j < count($pendingFormatted); $j++){
                    if($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']){
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels' => $dates,
                'valid' => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        }else{
            //custom range greater than 30 days show month wise
            $dates = [];
            $validFormatted = [];
            $pendingFormatted = [];

            for($date = Carbon::parse($request->input('startDate')); $date->lte(Carbon::parse($request->input('endDate'))); $date->addMonth()) {
                $dates[] = $date->format('F-Y');
                $validFormatted [] = ['time' => $date->format('F-Y'), 'sales' => 0];
                $pendingFormatted [] = ['time' => $date->format('F-Y'), 'sales' => 0];
            }

            $validSales = Sale::whereStatus('Valid')
                ->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween( DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')] )
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            $pendingSales = Sale::whereStatus('Pending')
                ->select(DB::raw("DATE_FORMAT(created_at, '%M-%Y') as time"), DB::raw("(COUNT(id)) as sales"))
                ->whereBetween( DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')] )
                ->groupBy('time')
                ->orderBy('time', 'asc')
                ->get();

            for($i = 0; $i < count($validSales); $i++ ){
                for($j = 0; $j < count($validFormatted); $j++){
                    if($validFormatted[$j]['time'] == $validSales[$i]['time']){
                        $validFormatted[$j]['sales'] = $validSales[$i]['sales'];
                    }
                }
            }

            for($i = 0; $i < count($pendingSales); $i++ ){
                for($j = 0; $j < count($pendingFormatted); $j++){
                    if($pendingFormatted[$j]['time'] == $pendingSales[$i]['time']){
                        $pendingFormatted[$j]['sales'] = $pendingSales[$i]['sales'];
                    }
                }
            }

            return response()->json([
                'labels' => $dates,
                'valid' => $validFormatted,
                'pending' => $pendingFormatted
            ]);
        }
    }

    public function flights(Request $request)
    {
        $flight = Sale::select('cty', DB::raw("(COUNT(id)) as sales"))
            ->whereStatus('Valid')
            ->where('cty','like', $request->input('code').'-%')
            ->whereBetween( DB::raw('date(created_at)'), [$request->input('startDate'), $request->input('endDate')] )
            ->orWhere('cty','like','%-'.$request->input('code'))
            ->groupBy('cty')
            ->get()
            ->toJSON();

        return $flight;
    }
}
