<?php

namespace App\Http\Controllers\Frontend;

use App\Page;
use function compact;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TermConditionController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page = Page::where('type','=','terms-and-conditions')->first();
        return view('frontend.tc', compact('page'));
    }
}
