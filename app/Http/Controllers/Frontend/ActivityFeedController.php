<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActivityFeed;

class ActivityFeedController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index(Request $request)
  {
      $user = auth()->user();
      $activities = ActivityFeed::whereUser_id($user->id)->orderBy('id','DESC')->paginate(5);

    	if ($request->ajax()) {
    		$view = view('frontend.profile.activity.single',compact('activities'))->render();
            return response()->json(['html'=>$view]);
        }

      return view('frontend.profile.activity.index',compact('activities'));
  }
}
