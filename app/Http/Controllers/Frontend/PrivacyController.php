<?php

namespace App\Http\Controllers\Frontend;

use App\Page;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PrivacyController extends Controller
{
    public function index()
    {
        $page = Page::where('type','=','privacy-policy')->first();
        return view('frontend.privacy', compact('page'));
    }
}
