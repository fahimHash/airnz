<?php

namespace App\Http\Controllers\Frontend;

use App\Destination;
use App\Notifications\NewSale;
use function array_push;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\File;
use App\ActivityFeed;
use Carbon\Carbon;
use App\Sale;
use Maatwebsite\Excel\Facades\Excel;
use Config;
use function strtoupper;
use Validator;

class SalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    //Auckland Airport Lat/Long
    const AKL_LAT = -37.008172;
    const AKL_LON = 174.785383;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        $all_count = Sale::whereUser_id($id)->count();
        $valid_count = Sale::whereUser_id($id)->whereStatus('Valid')->count();
        $pending_count = Sale::whereUser_id($id)->whereStatus('Pending')->count();
        $cancelled_count = Sale::whereUser_id($id)->whereStatus('Cancelled')->count();
        $problem_count = Sale::whereUser_id($id)->whereStatus('Problem')->count();
        $month_year = Carbon::now()->format('F Y');

        $fromDate = Carbon::now()->startOfWeek()->toDateString();
        $tillDate = Carbon::now()->endOfWeek()->toDateString();

        $validatedCurrentWeek = Sale::whereUser_id($id)
            ->select(DB::raw("DAYNAME(pnr_creation) as day, (COUNT(id)) as saleCurrent"))
            ->where('status', '=', 'Valid')
            ->orWhere('status','=','Cancelled')
            ->whereBetween(DB::raw('date(pnr_creation)'), [$fromDate, $tillDate])
            ->count();

        $validatedSales = $valid_count + $cancelled_count;
        $emptySeat = (360 - $validatedSales);
        if($emptySeat < 0){
            $emptySeat = 0;
        }

        $class = '';
        for ($i = 1; $i <= $validatedSales; $i++) {
            $class .= '#seatid' . $i;
            if ($i != $validatedSales) {
                $class .= ', ';
            }
        }
        $destinations = Cache::remember('destinations', 1440, function () {
            return Destination::all();
        });
        $pathways = Cache::remember('pathways', 1440, function () {
            $all = Destination::all();
            $dest = [];
            foreach ($all as $item){
                $temp = [
                    'latS' => $this::AKL_LAT,
                    'lonS' => $this::AKL_LON,
                    'lat' => $item->latitude,
                    'lon' => $item->longitude
                ];
                array_push($dest, $temp);
            }
            return $dest;
        });

        $validatedWeeklySales = $this->validatedWeeklySales($id, $fromDate, $tillDate);
        $validatedYearlySales = $this->validatedYearlySales($id);
        $validatedMonthlySales = $this->validatedMonthlySales($id);

        return view('frontend.sales.my-sales', compact(
            'all_count',
            'valid_count',
            'pending_count',
            'cancelled_count',
            'problem_count',
            'month_year',
            'validatedCurrentWeek',
            'validatedSales',
            'emptySeat',
            'class',
            'destinations',
            'validatedWeeklySales',
            'validatedMonthlySales',
            'validatedYearlySales',
            'pathways'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.sales.add-sales');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, ['pnr' => 'required', 'creation' => 'required', 'groupid' => 'sometimes']);
        $saleCount = 0;

        $user = auth()->user();

        $pnr = Input::get('pnr');
        $creation = Input::get('creation');
        $groupShellId = Input::get('groupid');

        foreach ($pnr as $key => $value) {
            $shellId = '';
            if(!empty ($groupShellId[$key])){
                $shellId = $groupShellId[$key];
            }
            if (!empty ($pnr[$key]) && !empty ($creation[$key]) ) {
                $sale = new Sale;
                $sale->pnr = strtoupper($pnr[$key]);
                $sale->pnr_creation = Carbon::createFromFormat('d-m-Y', $creation[$key])->toDateString();
                $sale->group_shell_id = $shellId;
                $sale->state_id = $user->state_id;
                $sale->consortium_id = $user->consortium_id;
                $sale->store_id = $user->store_id;
                $sale->user_id = $user->id;
                $sale->territory_id = $user->territory_id;
                $sale->save();
                $saleCount++;
            }
        }
        $activity = new ActivityFeed;
        $activity->module = 'Sales';
        $activity->activity_type = 'new-sale';

        if ($saleCount > 1) {
            $activity->activity_message = $saleCount . ' Sales has been successfully added!';
            Auth::user()->notify(new NewSale());
        } else {
            $activity->activity_message = 'Sale has been successfully added!';
            Auth::user()->notify(new NewSale());
        }

        $activity->user_id = $user->id;
        $activity->save();

        return Redirect::to('/add-sales')->with('modal','success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Download sales template
     *
     * @return \Illuminate\Http\Response
     */
    public function downloadSampleCSV()
    {
        $pathToFile = public_path('storage/SampleTemplate.csv');
        $name = 'SampleTemplate.csv';
        $headers = ['Content-Type: text/csv'];
        return response()->download($pathToFile, $name, $headers);
    }

    /**
     * Upload Sales csv
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function uploadSales(Request $request)
    {
        ini_set('auto_detect_line_endings', true);

        $this->validate($request, [
            'file' => 'required|file|mimes:application/vnd.ms-excel,csv,txt|max:10000'
        ]);


        if (Input::hasFile('file') && Input::file('file')->isValid()) {
            $fileName = Input::file('file')->getClientOriginalName();
            $filepath = Storage::putFileAs('uploadSales', new File(Input::file('file')), $fileName);

            $path = Config::get('filesystems.disks.local.root') . '/' . $filepath;
            $noOfSalesAdded = 0;

            try {
                $import = Excel::load($path, function ($reader) use (&$noOfSalesAdded) {
                    $saleCount = 0;
                    $user = auth()->user();

                    $reader->each(function ($row) use ($user, &$saleCount, &$noOfSalesAdded) {

                        $sale = new Sale;
                        $sale->pnr = $row->pnr;
                        $sale->stock = $row->stock;
                        $sale->ticket_number = $row->ticket_no;
                        $sale->state_id = $user->state_id;
                        $sale->consortium_id = $user->consortium_id;
                        $sale->store_id = $user->store_id;
                        $sale->user_id = $user->id;
                        $sale->territory_id = $user->territory_id;
                        $sale->save();
                        $saleCount++;
                        $noOfSalesAdded++;
                    });

                    $activity = new ActivityFeed;
                    $activity->module = 'Sales';
                    $activity->activity_type = 'new-sale';

                    if ($saleCount > 1) {
                        $activity->activity_message = $saleCount . ' Sales has been successfully added!';
                    } else {
                        $activity->activity_message = 'Sale has been successfully added!';
                    }

                    $activity->user_id = $user->id;
                    $activity->save();

                    Auth::user()->notify(new NewSale());
                }
                );
                if ($noOfSalesAdded > 0) {
                    return response()->json(['flag' => true, 'msg' => $noOfSalesAdded . ' Sales processed successfully']);
                } else {
                    return response()->json(['flag' => false, 'msg' => 'Sales processing failed']);
                }

            } catch (Exception $e) {
                return response()->json(['flag' => false, 'msg' => 'Sales processing failed']);
            }
        }
    }

    public function validatedWeeklySales($id, $fromDate, $tillDate)
    {
        return Cache::remember('VWS_' . $id . '_' . $fromDate . '_' . $tillDate, 15, function () use ($id, $fromDate, $tillDate) {
            $validatedWeeklySales = Sale::whereUser_id($id)
                ->select(DB::raw("cty, (COUNT(id)) as saleCurrent"))
                ->whereStatus('Valid')
//                ->orWhere('status','=','Cancelled')
                ->whereBetween(DB::raw('date(pnr_creation)'), [$fromDate, $tillDate])
                ->groupBy('cty')
                ->orderBy('cty')
                ->get();
            return $this->formatDestination($validatedWeeklySales);
        });
    }

    public function validatedYearlySales($id)
    {
        $lastYear = Carbon::now()->subYear()->startOfYear()->format('Y-m-d');
        $now = Carbon::now()->format('Y-m-d');
        return Cache::remember('VYS_' . $id . '_' . $lastYear . '_' . $now, 15, function () use ($id, $lastYear, $now) {
            $validatedYearlySales = Sale::whereUser_id($id)
                ->select(DB::raw("cty"), DB::raw("(COUNT(id)) as saleCurrent"))
                ->whereStatus('Valid')
//                ->orWhere('status','=','Cancelled')
                ->whereBetween(DB::raw('date(pnr_creation)'), [$lastYear, $now])
                ->groupBy('cty')
                ->orderBy('cty', 'ASC')
                ->get();
            return $this->formatDestination($validatedYearlySales);
        });
    }

    public function validatedMonthlySales($id)
    {
        $sixMonthBack = Carbon::now()->subMonths(6)->startOfMonth();
        $monthEnd = Carbon::now()->endOfMonth();
        return Cache::remember('VMS_' . $id . '_' . $sixMonthBack . '_' . $monthEnd, 15, function () use ($id, $sixMonthBack, $monthEnd) {
            $validatedMonthlySales = Sale::whereUser_id($id)
                ->select(DB::raw("cty"), DB::raw("(COUNT(id)) as saleCurrent"))
                ->whereStatus('Valid')
//                ->orWhere('status','=','Cancelled')
                ->whereBetween(DB::raw('date(pnr_creation)'), [$sixMonthBack, $monthEnd])
                ->groupBy('cty')
                ->orderBy('cty', 'ASC')
                ->get();
            return $this->formatDestination($validatedMonthlySales);
        });
    }

    function formatDestination($data)
    {
        if(count($data) > 0 ){
            $d = [];
            for ($i=0; $i < count($data); $i++) {
                $d[$data[$i]['cty']] = $data[$i]['saleCurrent'];
            }
            return $d;
        }else{
            $data = [];
            $destinations = Destination::all(['code']);
            foreach ($destinations as $destination){
                $data[$destination->code] = 0;
            }
            return $data;
        }
    }

    /**
     * Response 200 sent as pnr does not match with existing pnr
     * Response 404 sent as pnr match with existing pnr
     */
    public function checkPNR(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'pnr' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json('Validation Error');
        }
        $pnr = Input::get('pnr');
        $pnr = strtoupper($pnr[0]);
        $hasPnr = Sale::wherePnr($pnr)->count();
        return $hasPnr == 0 ? response()->json(true) : response()->json(false,404);
    }
}
