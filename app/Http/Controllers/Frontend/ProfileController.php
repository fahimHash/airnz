<?php

namespace App\Http\Controllers\Frontend;

use App\ActivityFeed;
use App\Http\Controllers\Controller;
use App\Mail\CloseAccount;
use App\State;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation\Rule;
use Mail;
use function compact;
use function is_numeric;
use function view;


class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $job = $user->job()->first();
        if (is_null($job)) {
            $job = null;
        }
        $territory = $user->territory()->first();
        if (!is_null($territory)) {
            $manager = $territory->manager()->first();
        } else {
            $manager = '';
        }
        $activities = ActivityFeed::whereUser_id($user->id)->orderBy('id', 'DESC')->take(2)->get();
        return view('frontend.profile.index', compact('user', 'job', 'manager', 'activities'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $user = auth()->user();
        return view('frontend.profile.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user = auth()->user();
        $splitName = explode(' ', $user->name, 2);
        $first_name = $splitName[0];
        $last_name = !empty($splitName[1]) ? $splitName[1] : '';
        return view('frontend.profile.edit', compact('user', 'first_name', 'last_name'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = auth()->user();
        $this->validate($request, [
            'startDate'   => 'required|date',
            'title'       => 'required|alpha',
            'first_name'  => 'required|alpha|max:32',
            'last_name'   => 'required|alpha|max:32',
            'phoneNumber' => 'required|numeric|unique:users,phoneNumber,' . $user->id,
            'address1'    => 'required|max:30',
            'address2'    => 'sometimes|max:30',
            'address3'    => 'required|max:30',
            'suburb'      => 'required|max:30',
            'postcode'    => 'required|max:4',
            'state'       => 'required|regex:/^[a-zA-Z0-9\s]+$/'
        ]);

        $user->update([
            'name'        => $request->input('first_name') . ' ' . $request->input('last_name'),
            'title'       => $request->input('title'),
            'phoneNumber' => $request->input('phoneNumber'),
            'startDate'   => Carbon::createFromFormat('d-m-Y', $request->input('startDate'))->toDateString(),
            'address1'    => $request->input('address1'),
            'address2'    => $request->input('address2'),
            'address3'    => $request->input('address3'),
            'suburb'      => $request->input('suburb'),
            'postcode'    => $request->input('postcode'),
        ]);
        if (is_numeric($request->input('state'))) {
            $state = State::whereId($request->input('state'))->first();
        } else {
            $state = State::whereName($request->input('state'))->first();
        }

        if ($state != null && Rule::exists('states', ['name' => $state])) {
            $user->state()->associate($state)->save();
        }

        return redirect('/profile/edit')->withSuccess('Profile updated!');
    }


    public function uploadPhoto(Request $request)
    {
        $user = auth()->user();
        $this->validate($request, [
            'upload_photo' => 'mimes:png,jpeg,jpg',
        ]);

        if ($request->hasFile('upload_photo')) {
            $file = $request->file('upload_photo');
            $fileName = str_random(10);
            $name = $fileName . '-' . $file->getClientOriginalName();
            $user->update(['image' => $name]);
            $file->move(public_path() . '/assets/img/', $name);
        }

        return redirect('/profile/edit')->withSuccess('Photo updated!');
    }

    /**
     * Remove the user image from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function remove()
    {
        $user = auth()->user();
        if ($user->image != 'default-image.png') {
            File::delete(public_path() . '/assets/img/' . $user->image);
            $user->image = 'default-image.png';
            $user->save();
            return redirect('/profile/detail')->withSuccess('Profile updated!');
        }
        return redirect('/profile/detail');
    }

    public function bdm()
    {
        $user = auth()->user();
        $territory = $user->territory()->first();
        if (!is_null($territory)) {
            $manager = $territory->manager()->first();
        } else {
            $manager = '';
        }
        return view('frontend.profile.bdm', compact('manager'));
    }

    public function showMyAccount()
    {
        return view('frontend.profile.account');
    }

    /**
     * Update User password
     */
    public function updatePassword(Request $request)
    {
        $user = auth()->user();

        $this->validate($request, [
            'old-password' => 'required|min:8',
            'password'     => 'required|min:8|confirmed',
        ]);

        if (Hash::check($request->input('old-password'), $user->password)) {
            $user->fill([
                'password' => bcrypt($request->input('password'))
            ])->save();

            return Redirect::to('/my-account')->withSuccess('Password Changed Successfully!');
        } else {
            return Redirect::to('/my-account')->withDanger('Your Current Password does not match!');
        }
    }

    /**
     * Close User Account
     */
    public function closeAccount(Request $request)
    {
        $user = auth()->user();
        $user->active = 0;
        $user->save();

        Mail::to($user->email)
            ->send(new CloseAccount($user));

        return Redirect::to('/my-account')->withDanger('Your account will be deactivated soon!');
    }
}
