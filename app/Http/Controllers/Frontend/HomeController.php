<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Page;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $territory = $user->territory()->first();
        if (!is_null($territory)) {
            $news = $territory->news()->orderBy('id', 'desc')->take(2)->get();
        } else {
            $news = '';
        }
        $pinned = Page::AgentPages($user)->Pinned()->Published()->get();
        $pages = Page::AgentPages($user)->NotPinned()->Published()->get();
        return view('frontend.home', compact('user', 'news', 'pages', 'pinned'));
    }


    /**
     * User inactive
     */
    public function inactiveAccount()
    {
        $user = auth()->user();
        $territory = $user->territory()->first();
        //$manager = $territory->manager()->first(); //TODO
        return view('frontend.profile.inactive', compact('user'));
    }

    /**
     * Mark Notification As Read
     */
    public function markNotificationAsRead()
    {
        Auth::user()->unreadNotifications()->update(['read_at' => Carbon::now()]);
        return response()->json(['message' => 'success']);
    }

    public function newsAjax(Request $request)
    {
        $user = auth()->user();
        $news = $user->territory()->first()->news()->orderBy('id', 'desc')->paginate(2);

        if ($request->ajax()) {
            $view = view('frontend.layouts.news', compact('news'))->render();
            return response()->json(['html' => $view]);
        }
        return null;
    }

    public function showTour()
    {
        return view('frontend.tour.index');
    }

    public function showTraining()
    {
        return view('frontend.training.index');
    }

    public function showTools()
    {
        return view('frontend.tools.index');
    }

    public function showSixMonthSales()
    {
        return view('frontend.sales.6months-sales');
    }

    public function showTwelveMonthSales()
    {
        return view('frontend.sales.12months-sales');
    }

    public function showAucklandSales()
    {
        return view('frontend.sales.auckland-sales');
    }

    public function showVancouverSales()
    {
        return view('frontend.sales.vancouver-sales');
    }

    public function showEmptySales()
    {
        return view('frontend.sales.empty-sales');
    }

    public function showContentPage()
    {
        return view('frontend.newsfeed.index');
    }
}
