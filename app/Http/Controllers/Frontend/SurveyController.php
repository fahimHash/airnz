<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Question;
use App\Survey;
use Illuminate\Http\Request;
use function compact;
use function intval;
use function redirect;
use function view;

class SurveyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $surveys = Survey::ForAgent($user)->get();
        return view('frontend.survey.index', compact('surveys'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $user = auth()->user();
        $survey = Survey::whereSlug($slug)->first();
        $questions = Question::whereSurveyId($survey->id)->with('answers')->paginate(1);
        $surveys = Survey::ForAgent($user)->get();
        $user->surveys()->syncWithoutDetaching($survey->id);
        return view('frontend.survey.show', compact('surveys', 'survey', 'questions'));
    }

    public function next(Request $request, $slug)
    {
        $this->validate($request, [
            'answers' => 'required|min:1'
        ],
        [
            'answers.required' => 'Please choose an answer or answers.'
        ]);
        $currentPage = intval($request->input('currentPage'));
        $lastPage = intval($request->input('lastPage'));
        $next = $currentPage + 1;
        $user = auth()->user();
        $user->questions()->syncWithoutDetaching($request->input('question'));
        $user->answers()->syncWithoutDetaching($request->input('answers'));
        if ($currentPage == $lastPage) {
            return redirect('surveys')->withSuccess('Survey Completed!');
        } else {
            return redirect('surveys/' . $slug . '?page=' . $next);
        }
    }
}
