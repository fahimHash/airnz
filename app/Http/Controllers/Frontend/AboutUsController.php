<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Page;
use App\Http\Controllers\Controller;

class AboutUsController extends Controller
{
    public function index()
    {
        $page = Page::where('type','=','about-us')->first();
        return view('frontend.aboutus', compact('page'));
    }
}
