<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\News;
use Illuminate\Http\Request;
use function redirect;

class NewsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return redirect('home');
    }
    public function show($slug)

    {
        $post = News::whereSlug($slug)->firstOrFail();
        return view('frontend.newsfeed.index', compact('post'));
    }
}
