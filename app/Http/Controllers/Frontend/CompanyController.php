<?php

namespace App\Http\Controllers\Frontend;

use App\Agency;
use App\Consortium;
use App\Exceptions\GeneralException;
use App\Http\Controllers\Controller;
use App\Job;
use App\State;
use App\Store;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Validator;

/**
 * Class CompanyController
 * @package App\Http\Controllers\Frontend
 */
class CompanyController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $consortiums = Consortium::orderBy('name')->pluck('name', 'id');
        $jobs = Job::whereUserId($user->id)->with('workplace.consortium', 'workplace.agency', 'workplace.state')->orderByDesc('id')->get();
        return view('frontend.profile.company', compact('jobs', 'consortiums', 'agency', 'firstAgency'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws GeneralException
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'consortium' => 'required|numeric',
            'agency'     => 'required|numeric',
            'store'      => 'required|numeric',
            'state'      => 'required|numeric',
            'position'   => 'required|alpha_spaces',
            'startDate'  => 'required|date',
            'endDate'    => 'nullable',
            'active'     => 'sometimes|numeric'
        ]);
        if ($validator->fails()) {
            return redirect('/company')
                ->withErrors($validator)
                ->withInput();
        }

        $user = auth()->user();
        $store = Store::whereName($request->input('store'))->first();
        if ($user->workplace()->first() == null) {
            /**
             * Create new Job
             * Associate workplace
             */
            $user->workplace()->dissociate();
            $user->workplace()->associate($store);
            $user->consortium()->associate($store->consortium()->first());
            $user->territory()->associate($store->territory()->first());
            $user->state()->associate($store->state()->first());
            $user->save();

            Job::create([
                'startDate' => Carbon::today()->toDateString(),
                'active'    => 1,
                'position'  => $request->input('position'),
                'user_id'   => $user->id,
                'store_id'  => $store->id
            ]);
        } elseif ($user->workplace()->first()->id != $store->id) {
            /**
             * End Previous Job
             * Create new Job
             * Associate workplace
             */
            $user->workplace()->dissociate();
            $user->workplace()->associate($store);
            $user->consortium()->associate($store->consortium()->first());
            $user->territory()->associate($store->territory()->first());
            $user->state()->associate($store->state()->first());
            $user->save();

            $user->job()->first()->update([
                'endDate' => Carbon::today()->toDateString(),
                'active'  => 0
            ]);

            Job::create([
                'startDate' => Carbon::today()->toDateString(),
                'active'    => 1,
                'position'  => $request->input('position'),
                'user_id'   => $user->id,
                'store_id'  => $store->id
            ]);
        }
        return Redirect::to('/company')->withSuccess('Company Added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     * @throws GeneralException
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'position'  => 'required|alpha_spaces',
            'startDate' => 'required|date',
            'endDate'   => 'nullable'
        ]);
        $validator->after(function ($validator) use ($request) {
            if ($request->input('travel_agency') == 'No Travel Agency Record') {
                $validator->errors()->add('travel_agency', 'Sorry! Your choosen Consortium does not have any agency, please contact BDM.');
            }
        });

        if ($validator->fails()) {
            return redirect('/company')
                ->withErrors($validator)
                ->withInput();
        }

        $user = auth()->user();
        $job = Job::findOrFail($id);

        if ($request->has('endDate')) {
            $job->endDate = $request->input('endDate');
        }

        $job->position = $request->input('position');
        $job->startDate = $request->input('startDate');
        $job->save();

        return Redirect::to('/company')->withSuccess('Company updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function ajaxConsortium()
    {
        $consortium = Consortium::orderByRaw('ISNULL(sort_order),sort_order ASC')->select('id', 'label')->get();
        return response()->json($consortium);
    }

    /**
     * Return list of agency based on consortium
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function agencyAjax(Request $request)
    {

        $this->validate($request, [
            'consortium' => 'required|exists:consortiums,id',
        ]);

        $agency = Agency::select('id', 'name')
            ->where('fk_id_consortiums', '=', $request->consortium)
            ->orderBy('name', 'asc')
            ->get();

        return response()->json($agency);
    }

    public function storeAjax(Request $request)
    {
        $this->validate($request, [
            'consortium' => 'required|exists:consortiums,id',
            'agency'     => 'required|exists:agencies,id'
        ]);
        $store = Store::select('id', 'name')
            ->where('store_consortium_id', '=', $request->consortium)
            ->where('store_agency_id', '=', $request->agency)
            ->orderBy('name', 'asc')
            ->get();

        return response()->json($store);
    }

    public function stateAjax(Request $request)
    {
            $this->validate($request, [
                'store' => 'required|exists:stores,id'
            ]);
            $store = Store::find($request->store);
            $state = State::select('id', 'name')->whereId($store->store_state_id)->get();
            return response()->json($state);
    }
}
