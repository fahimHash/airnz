<?php

namespace App\Http\Controllers\Frontend\API;

use App\ActivityFeed;
use App\Http\Controllers\Controller;
use App\Sale;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use function array_push;
use function json_decode;

class MySalesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        // $sales = Sale::whereUser_id($id)->get();
        // return response()->json([
        //   'sales' => $sales
        // ]);
        $sales = Sale::whereUser_id($id)->orderBy('id','desc')->paginate(5);
        return response()->json($sales);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'pnr' => 'required'
        ]);

        $sale = Sale::find($id);

        $sale->update($request->all());

        return response()->json([
            'message' => 'Sale updated successfully'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Sale::find($id)->delete();
        $user = auth()->user();

        $activity = new ActivityFeed;
        $activity->module = 'Sales';
        $activity->activity_type = 'delete-sale';
        $activity->activity_message = 'Sale has been deleted successfully!';
        $activity->user_id = $user->id;
        $activity->save();

        return response()->json([
            'message' => 'Sale deleted successfully'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function valid()
    {
        $id = Auth::id();
        $sales = Sale::whereUser_id($id)->whereStatus('Valid')->get();
        return response()->json([
            'sales' => $sales
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function pending()
    {
        $id = Auth::id();
        $sales = Sale::whereUser_id($id)->whereStatus('Pending')->get();
        return response()->json([
            'sales' => $sales
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function cancelled()
    {
        $id = Auth::id();
        $sales = Sale::whereUser_id($id)->whereStatus('Cancelled')->get();
        return response()->json([
            'sales' => $sales
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function problem()
    {
        $id = Auth::id();
        $sales = Sale::whereUser_id($id)->whereStatus('Problem')->get();
        return response()->json([
            'sales' => $sales
        ]);
    }

    /**
     * Return Yearly Sales JSON data
     *
     * @return \Illuminate\Http\Response
     */
    public function salesChartYearly()
    {
        $id = Auth::id();
        $now = Carbon::now();

        $lastYear = Carbon::now()->subYear()->startOfYear();
        $saleschartyearly = Sale::whereUser_id($id)
            ->select(DB::raw("DATE_FORMAT(pnr_creation, '%Y') as year"), DB::raw("(SUM(pax)) as saleCurrent"))
            ->whereBetween(DB::raw('YEAR(pnr_creation)'), [$lastYear, $now->year])
            ->groupBy('year')
            ->orderBy('year', 'ASC')
            ->get()
            ->toJSON();

        if (count(json_decode($saleschartyearly)) < 1) {
            $saleschartyearly = [];
            for ($i = 2; $i >= 1; $i--) {
                array_push($saleschartyearly, [
                    'year'       => Carbon::now()->subYears($i)->format('Y'),
                    'saleCurrent' => 0.
                ]);
            }
            array_push($saleschartyearly, [
                'year'       => Carbon::now()->format('Y'),
                'saleCurrent' => 0.
            ]);
        }
        return $saleschartyearly;
    }

    /**
     * Return Monthly Sales JSON data
     *
     * @return \Illuminate\Http\Response
     */
    public function salesChartMonthly()
    {
        $id = Auth::id();
        $now = Carbon::now();
        $sixMonthBack = $now->subMonths(6)->startOfMonth();
        $monthEnd = Carbon::now()->endOfMonth();

        $saleschartmonthly = Sale::whereUser_id($id)
            ->select(DB::raw("DATE_FORMAT(pnr_creation, '%Y-%m') as month"), DB::raw("(SUM(pax)) as saleCurrent"))
            ->whereBetween(DB::raw('date(pnr_creation)'), [$sixMonthBack, $monthEnd])
            ->groupBy('month')
            ->orderBy('month', 'ASC')
            ->get()
            ->toJSON();

        if (count(json_decode($saleschartmonthly)) < 1) {
            $saleschartmonthly = [];
            for ($i = 6; $i >= 1; $i--) {
                array_push($saleschartmonthly, [
                    'month'       => Carbon::now()->subMonths($i)->format('Y-m'),
                    'saleCurrent' => 0.
                ]);
            }
            array_push($saleschartmonthly, [
                'month'       => Carbon::now()->format('Y-m'),
                'saleCurrent' => 0.
            ]);
        }
        return $saleschartmonthly;
    }

    /**
     * Return Weekly Sales JSON data
     *
     * @return \Illuminate\Http\Response
     */
    public function salesChartWeekly()
    {
        $id = Auth::id();
        $fromDate = Carbon::now()->startOfWeek()->toDateString();
        $tillDate = Carbon::now()->endOfWeek()->toDateString();

        $salesCurrentWeek = Sale::whereUser_id($id)
            ->select(DB::raw("DAYNAME(pnr_creation) as day, (SUM(pax)) as saleCurrent"))
            ->whereBetween(DB::raw('date(pnr_creation)'), [$fromDate, $tillDate])
            ->groupBy('day')
            ->orderBy('day')
            ->get();

        $fromDate = Carbon::now()->subWeek()->startOfWeek()->toDateString();
        $tillDate = Carbon::now()->subWeek()->endOfWeek()->toDateString();

        $salesLastWeek = Sale::whereUser_id($id)
            ->select(DB::raw("DAYNAME(pnr_creation) as day, (SUM(pax)) as saleLast"))
            ->whereBetween(DB::raw('date(pnr_creation)'), [$fromDate, $tillDate])
            ->groupBy('day')
            ->orderBy('day')
            ->get();

        $salesFormatted = [
            ['day' => 'Monday', 'saleCurrent' => 0, 'saleLast' => 0],
            ['day' => 'Tuesday', 'saleCurrent' => 0, 'saleLast' => 0],
            ['day' => 'Wednesday', 'saleCurrent' => 0, 'saleLast' => 0],
            ['day' => 'Thursday', 'saleCurrent' => 0, 'saleLast' => 0],
            ['day' => 'Friday', 'saleCurrent' => 0, 'saleLast' => 0],
            ['day' => 'Saturday', 'saleCurrent' => 0, 'saleLast' => 0],
            ['day' => 'Sunday', 'saleCurrent' => 0, 'saleLast' => 0]
        ];

        for ($i = 0; $i < count($salesCurrentWeek); $i++) {
            for ($j = 0; $j < count($salesFormatted); $j++) {
                if ($salesFormatted[$j]['day'] == $salesCurrentWeek[$i]['day']) {
                    $salesFormatted[$j]['saleCurrent'] = $salesCurrentWeek[$i]['saleCurrent'];
                }
            }
        }
        for ($i = 0; $i < count($salesLastWeek); $i++) {
            for ($j = 0; $j < count($salesFormatted); $j++) {
                if ($salesFormatted[$j]['day'] == $salesLastWeek[$i]['day']) {
                    $salesFormatted[$j]['saleLast'] = $salesLastWeek[$i]['saleLast'];
                }
            }
        }
        return $salesFormatted;
    }

    /**
     * Return Sales Count by last & this year, last & this month, last & this week  JSON data
     *
     * @return \Illuminate\Http\Response
     */
    public function salesCount()
    {
        $id = Auth::id();
        $now = Carbon::now();

        $lastYear = Carbon::now()->subYear()->startOfYear();
        $salesLastYear = Sale::whereUser_id($id)->select(DB::raw("(SUM(pax)) as salesLastYear"))->where(DB::raw('YEAR(pnr_creation)'), [$lastYear])->pluck('salesLastYear');

        $salesThisYear = Sale::whereUser_id($id)->select(DB::raw("(SUM(pax)) as salesThisYear"))->where(DB::raw('YEAR(pnr_creation)'), [$now->year])->pluck('salesThisYear');

        $monthStart = Carbon::now()->subMonth()->startOfMonth();
        $monthEnd = Carbon::now()->subMonth()->endOfMonth();

        $salesLastMonth = Sale::whereUser_id($id)->select(DB::raw("(SUM(pax)) as salesLastMonth"))->whereBetween(DB::raw('date(pnr_creation)'), [$monthStart, $monthEnd])->pluck('salesLastMonth');

        $monthStart = Carbon::now()->startOfMonth();
        $monthEnd = Carbon::now()->endOfMonth();

        $salesThisMonth = Sale::whereUser_id($id)->select(DB::raw("(SUM(pax)) as salesThisMonth"))->whereBetween(DB::raw('date(pnr_creation)'), [$monthStart, $monthEnd])->pluck('salesThisMonth');

        $fromDate = Carbon::now()->startOfWeek()->toDateString();
        $tillDate = Carbon::now()->endOfWeek()->toDateString();

        $salesThisWeek = Sale::whereUser_id($id)->select(DB::raw("(SUM(pax)) as salesThisWeek"))->whereBetween(DB::raw('date(pnr_creation)'), [$fromDate, $tillDate])->pluck('salesThisWeek');

        $fromDate = Carbon::now()->subWeek()->startOfWeek()->toDateString();
        $tillDate = Carbon::now()->subWeek()->endOfWeek()->toDateString();

        $salesLastWeek = Sale::whereUser_id($id)->select(DB::raw("(SUM(pax)) as salesLastWeek"))->whereBetween(DB::raw('date(pnr_creation)'), [$fromDate, $tillDate])->pluck('salesLastWeek');

        return response()->json([
            'salesLastYear'  => $salesLastYear,
            'salesThisYear'  => $salesThisYear,
            'salesLastMonth' => $salesLastMonth,
            'salesThisMonth' => $salesThisMonth,
            'salesThisWeek'  => $salesThisWeek,
            'salesLastWeek'  => $salesLastWeek
        ]);
    }
}
