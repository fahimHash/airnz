<?php

namespace App\Http\Controllers\Frontend;

use App\ActivityFeed;
use App\Notifications\CampaignJoined;
use App\Sale;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use App\Http\Controllers\Controller;
use App\User;
use App\Campaign;

class CampaignController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        /*
         * Throw 404 error with message when user does not belong to any territory
         * */
        if(is_null($user->territory_id)){
            abort(404, 'The resource you are looking for could not be found');
        }

        $user = User::with(['saleSegments.campaigns','systemSegments.campaigns','trainingSegments.campaigns','territory.campaigns'])->find($user->id);

        $allCampaigns = new Collection;
        $allCampaigns = $allCampaigns->merge($user->saleSegments->pluck('campaigns')->collapse());
        $allCampaigns = $allCampaigns->merge($user->systemSegments->pluck('campaigns')->collapse());
        $allCampaigns = $allCampaigns->merge($user->trainingSegments->pluck('campaigns')->collapse());
        if (isset($user->territory)) {
            $allCampaigns = $allCampaigns->merge($user->territory->activeCampaigns);
        }
        $agentCampaignsInProgress = User::find($user->id)->campaignsInProgress()->get();
        $agentCompletedCampaigns = User::find($user->id)->completedCampaigns()->get();
        $allCampaigns = $allCampaigns->merge($agentCampaignsInProgress)->sortBy('joined');
        $allCampaigns->unique();
        $salesCampaigns = Campaign::isActive()->filterBySales()->findByTerritories($user->territory_id)->get();
        $tierCampaigns = Campaign::isActive()->filterByTier()->findByTerritories($user->territory_id)->get();
        // $engagementCampaigns = Campaign::isActive()->filterByEngagement()->findByTerritories($user->territory_id)->get();
        return view('frontend.campaigns.index', compact('agentCampaignsInProgress','agentCompletedCampaigns','salesCampaigns','tierCampaigns','allCampaigns'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth()->user();
        $agentCampaign = $user->campaignsInProgress()->get();
        $countInProgress = count($agentCampaign);

        if($countInProgress >= 5 )
        {
            return Redirect::to('/campaigns')->withDanger('You have reached maximum active campaigns limit!');
        }
        $campaign = Campaign::find($request->input('campaign_id'));

        $countSales = count($user->salesCampaigns()->get());
        $countTier = count($user->tierCampaigns()->get());
        $countEngagement = count($user->engagementCampaigns()->get());

        if($countSales == 2 && $campaign->type == 'Sales') {
            return Redirect::to('/campaigns')->withDanger('Sorry! You have reached maximum active Sales campaigns limit!');
        }

        if($countTier == 1 && $campaign->type == 'Tier Promotion') {
            return Redirect::to('/campaigns')->withDanger('Sorry! You have reached maximum active Tier Promotion campaigns limit!');
        }

        if($countEngagement == 2 && $campaign->type == 'Engagement Competitions') {
            return Redirect::to('/campaigns')->withDanger('Sorry! You have reached maximum active Engagement Competitions campaigns limit!');
        }

        $activity = new ActivityFeed;
        $activity->module = 'Campaigns';
        $activity->activity_type = 'campaign-joined';
        $activity->activity_message = ' You have joined '.$campaign->name.' campaign.';
        $activity->user_id = $user->id;
        $activity->save();

        Auth::user()->notify(new CampaignJoined($campaign));

        $user->campaigns()->attach($campaign->id, ['type' => $campaign->type, 'joined' => true, 'inprogress' => true]);
        return Redirect::to('/campaigns')->withSuccess('Success! You have joined a new campaign!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $campaign = Campaign::find($id);
        $end = Carbon::parse($campaign->endDate);
        $now = Carbon::now();
        if($now > $end){
            $daysLeft = 0;
        }else{
            $daysLeft = $end->diffInDays($now);
        }
        $saleCount = Sale::whereCampaignId($id)->count();
        $sale = ( $saleCount /  $campaign->sales )*100;
        return view('frontend.campaigns.detail', compact('campaign', 'daysLeft', 'sale'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function progressAjax(Request $request)
    {
        $campaign = Campaign::find($request->id);
        $end = Carbon::parse($campaign->endDate);
        $now = Carbon::now();
        if($now > $end){
            $daysLeft = 0;
        }else{
            $daysLeft = $end->diffInDays($now);
        }
        $saleCount = Sale::whereCampaignId($request->id)->count();
        $sale = ( $saleCount /  $campaign->sales )*100;
        return response()->json([
            'daysLeft' => $daysLeft,
            'sales' => $sale
        ]);
    }
}
