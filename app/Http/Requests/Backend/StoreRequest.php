<?php

namespace App\Http\Requests\Backend;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $store_id = $this->store ? ','.$this->store->id : '';
        return [
            'agency'   => ['required', 'exists:agencies,id'],
            'name'     => 'required|max:255',
            'pcc'      => 'required|unique:stores,pcc'.$store_id,
            'address1' => 'sometimes|nullable|string|max:255',
            'address2' => 'sometimes|nullable|string|max:255',
            'address3' => 'sometimes|nullable|string|max:255',
            'suburb'   => ['required', 'alpha_spaces'],
            'postcode' => ['required', 'numeric'],
            'state'    => ['required', 'exists:states,id'],
            'bdm'      => ['required', 'exists:users,id']
        ];
    }
}
