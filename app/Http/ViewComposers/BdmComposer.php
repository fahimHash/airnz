<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use App\User;


class BdmComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $territory = auth()->user()->territory;
        if(!is_null($territory)){
            $view->with('manager', $territory->manager);
        }else{
            $view->with('manager', '');
        }
    }
}
