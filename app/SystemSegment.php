<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemSegment extends Model
{
    protected $table = 'system_segments';

    protected $fillable = ['name', 'description','filters'];

    public function agents()
    {
        return $this->morphToMany(User::class,'segment');
    }

    public function campaigns()
    {
        return $this->morphToMany(Campaign::class, 'targetables');
    }

    public function surveys()
    {
        return $this->morphToMany(Survey::class, 'surveyables');;
    }
}
