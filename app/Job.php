<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Job extends Model
{
    use SoftDeletes;
    // LINK THIS MODEL TO OUR DATABASE TABLE
    protected $table = 'employments';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'position', 'startDate', 'endDate', 'active', 'user_id','store_id'
    ];

    /**
     * Agent who has the job
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
      return $this->belongsTo('App\User', 'user_id');
    }

    /**
     * The store where agent is working
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function workplace(){
        return $this->belongsTo('App\Store', 'store_id');
    }

    /**
     * Query scope return the active job for the agent
     * @param $query
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where('active', '=', 1);
    }
}
