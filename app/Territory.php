<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;

class Territory extends Model
{
    use SoftDeletes;
    protected $table = 'territories';
    protected $dates = ['deleted_at'];

    //The attributes that are mass assignable
    protected $fillable = ['name','sale_target'];

    /**
     * The agents who belongs to the territory.
     */
    public function agents(){
      return $this->hasMany('App\User','territory_id');
    }

    /**
    * Territory managed by biz dev manager
    */
    public function manager(){
      return $this->belongsTo('App\User','user_id');
    }

    /**
     * The stores which are in the territory
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stores()
    {
        return $this->hasMany(Store::class, 'store_territory_id');
    }

    /**
     * A territory has many consortiums
     */
    public function consortiums()
    {
      return $this->belongsToMany(Consortium::class,'consortium_territory','territory_id','consortium_id');
    }

    /**
    * A Territory has many sales
    */
    public function sales(){
      return $this->hasMany('App\Sale','territory_id');
    }

    /**
     * A territory has many agents
     */
    /*
    public function agents()
    {
      return $this->users()->whereHas('roles', function($q){
        $q->where('name', '=', 'minor');
      })->orWhereHas('roles', function($query){
        $query->where('name', '=','mover');
      });
    }
    */
    /**
     * The territory has many campaign
     */
    public function campaigns()
    {
        return $this->belongsToMany('App\Campaign', 'campaign_territory');
    }

    public function activeCampaigns()
    {
        return $this->campaigns()->where('status', '=', true);
    }
    public function salesCampaigns()
    {
      return $this->campaigns()->where('type', '=', 'Sales');
    }
    public function tierCampaigns()
    {
      return $this->campaigns()->where('type', '=', 'Tier Promotion');
    }
    public function engagementCampaigns()
    {
      return $this->campaigns()->where('type', '=', 'Engagement Competitions');
    }

    /**
     * A territory has many agencies
     */
    public function agencies()
    {
        return $this->belongsToMany(Agency::class,'agency_territory','territory_id','agency_id');
    }

    /**
     * A territory has many states
     */
    public function states()
    {
        return $this->belongsToMany(State::class,'state_territory','territory_id','state_id');
    }

    public function scopeByState($query, State $state){
        return $query->whereHas('states', function($query) use ($state) {
                $query->where('name', $state->name);
        });
    }

    public function scopeByConsortium($query, Consortium $consortium){
        return $query->whereHas('consortiums', function($query) use ($consortium) {
            $query->where('name', $consortium->name);
        });
    }

    public function scopeByAgency($query, Agency $agency){
        return $query->whereHas('agencies', function ($query) use ($agency){
            $query->where('name', $agency->name);
        });
    }

    public function news()
    {
        return $this->belongsToMany('App\News', 'blog_territory', 'territory_id', 'blog_id')->where('publish', true);
    }

    public function surveys()
    {
        return $this->morphToMany(Survey::class, 'surveyables');
    }

    public function pages()
    {
        return $this->belongsToMany(Page::class, 'page_territory', 'page_id', 'territory_id');
    }
}
