<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Campaign extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'type', 'message','startDate','endDate','image','sales','status','tag_type_id','tag_obj_id','tag_channel_id','reward_id'
    ];

    public function territories()
    {
        return $this->belongsToMany('App\Territory', 'campaign_territory');
    }

    public function agent($user_id)
    {
        return $this->agents()->wherePivot('user_id', '=', $user_id)->withPivot('type', 'joined', 'completed');
    }

    public function agents()
    {
        return $this->belongsToMany('App\User', 'campaign_user', 'campaign_id', 'user_id')->withPivot('type', 'joined', 'inprogress', 'completed', 'created_at', 'updated_at');
    }

    public function destinations()
    {
        return $this->belongsToMany('App\Destination', 'campaign_destination');
    }

    public function reward()
    {
        return $this->belongsTo('App\Reward', 'reward_id');
    }

    public function completed()
    {
        return $this->agents()->wherePivot('completed', '=', true);
    }

    public function joined()
    {
        return $this->agents()->wherePivot('joined', '=', true);
    }

    /*
        public function newPivot(Model $parent, array $attributes, $table, $exists)
        {
            if ($parent instanceof User) {
                return new CampaignUserPivot($parent, $attributes, $table, $exists);
            }

            return parent::newPivot($parent, $attributes, $table, $exists);
        }
    */
    public function campaignUser()
    {
        return $this->hasMany('App\CampaignUser', 'campaign_id');
    }

    public function sales()
    {
        return $this->hasMany('App\Sale', 'campaign_id');
    }

    /**
     * Get all of the tags for the campaign.
     */
    public function tagType()
    {
        return $this->belongsTo(TagType::class, 'tag_type_id');
    }

    public function tagObjective()
    {
        return $this->belongsTo(TagObjective::class, 'tag_obj_id');
    }

    public function tagChannel()
    {
        return $this->belongsTo(TagChannel::class, 'tag_channel_id');
    }

    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

    public function saleSegments()
    {
        return $this->morphedByMany(SaleSegment::class, 'targetables');
    }

    public function systemSegments()
    {
        return $this->morphedByMany(SystemSegment::class, 'targetables');
    }

    public function trainingSegments()
    {
        return $this->morphedByMany(TrainingSegment::class, 'targetables');
    }

    public function scopeFindByTerritories($query, $id)
    {
        return $query->whereHas('territories', function ($query) use ($id) {
            $query->where('territories.id', '=',$id);
        });
    }
    public function scopeFilterBySales($query)
    {
        return $query->where('type', '=', 'Sales');
    }
    public function scopeFilterByTier($query)
    {
        return $query->where('type', '=', 'Tier Promotion');
    }
    public function scopeFilterByEngagement($query)
    {
        return $query->where('type', '=', 'Engagement Competitions');
    }
    public function scopeIsActive($query)
    {
        return $query->where('status', '=', true);
    }
    public function scopeFindByDestination($query, $id)
    {
        return $query->whereHas('destinations', function ($query) use ($id) {
            $query->where('destinations.id', '=',$id);
        });
    }
}
