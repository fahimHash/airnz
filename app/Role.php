<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    const ROLE_ADMIN = 'admin';
    const ROLE_SUPER_USER = 'airnz-super-user';
    const ROLE_STATE_MANAGER = 'state-manager';
    const ROLE_NATIONAL_MANAGER = 'national-account-manager';
    const ROLE_BDM = 'territory';
    const ROLE_MINOR = 'minor';
    const ROLE_MOVER = 'mover';

    //The attributes that are mass assignable
    protected $fillable = ['name', 'label'];

    // A role may be given several permission
    public function permissions()
    {
      return $this->belongsToMany(Permission::class,'permission_role','role_id','permission_id')->orderBy('label','asc');
    }

    // Grant permission to a role
    public function givePermissionTo(Permission $permission)
    {
      return $this->permissions()->save($permission);
    }

    public function users()
    {
      return $this->belongsToMany(User::class,'role_user','role_id','user_id');
    }

    //return the active job/agency for the user
    public function scopeMinor($query)
    {
        return $query->where('name', '=', ROLE::ROLE_MINOR);
    }
}
