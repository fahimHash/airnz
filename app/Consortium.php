<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Consortium extends Model
{
    use SoftDeletes;
    // LINK THIS MODEL TO OUR DATABASE TABLE
    public $table = "consortiums";
    
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'address','email','phone','logo','active','suburb','state','postcode',
    ];

    /**
     * The agents that belong to the consortium.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users(){
      return $this->hasMany('App\User','consortium_id');
    }

    public function states()
    {
        return $this->belongsToMany(State::class, 'consortium_state', 'consortium_id', 'state_id');
    }

    /**
     * each consortium has many agency
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function agencies(){
      return $this->hasMany('App\Agency','fk_id_consortiums');
    }

    /**
     * each consortium  has many store
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function stores(){
      return $this->hasMany('App\Store','store_consortium_id');
    }

    /**
     * a consortium belongs to many territories
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function territories()
    {
        return $this->belongsToMany(Territory::class,'consortium_territory','consortium_id','territory_id');
    }

    /**
     * A consortium has many sales
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sales(){
      return $this->hasMany('App\Sale','consortium_id');
    }

    /**
     * Query Scope
     * @param $query
     * @param $territory
     * @return mixed
     */
    public function scopeByTerritory($query, $territory){
        return $query->whereHas('territories', function($query) use ($territory) {
            $query->where('territories.id', $territory);
        });
    }
}
