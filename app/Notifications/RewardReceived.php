<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class RewardReceived extends Notification
{
    use Queueable;
    protected $reward;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($reward)
    {
        $this->reward = $reward;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('You have received a reward in Duo')
            ->greeting('Congratulation!')
            ->line('You have received "'.$this->reward->name.'" reward in Duo.')
            ->action('View Reward', url('rewards'))
            ->line("Let's get it!");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'module' => 'rewards',
            'title' => 'Reward Received',
            'message' => 'You have received "'.$this->reward->name.'" reward in Duo.',
            'link' => 'rewards'
        ];
    }
}
