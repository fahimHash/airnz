<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CampaignCompleted extends Notification implements ShouldQueue
{
    use Queueable;
    protected $campaign;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('You have completed a campaign in Duo')
            ->greeting('Congratulation!')
            ->line('You have completed "'.$this->campaign->name.'" campaign in Duo.')
            ->action('View Reward', url('rewards'))
            ->line("Let's get it!");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'module' => 'campaigns',
            'title' => 'Campaign Completed',
            'message' => 'You have completed "'.$this->campaign->name.'" campaign in Duo.',
            'link' => 'campaigns'
        ];
    }
}
