<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class CampaignJoined extends Notification
{
    use Queueable;
    protected $campaign;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject('You have joined a campaign in Duo')
                ->greeting('Hello!')
                ->line('You have joined "'.$this->campaign->name.'" campaign in Duo.')
                ->action('View campaign', url('campaigns'))
                ->line("Let's get going!");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'module' => 'campaigns',
            'title' => 'Campaign Joined',
            'message' => 'You have joined "'.$this->campaign->name.'" campaign in Duo.',
            'link' => 'campaigns'
        ];
    }
}
