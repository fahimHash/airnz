<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class UpcomingCampaign extends Notification implements ShouldQueue
{
    use Queueable;

    private $campaign;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Upcoming Campaign in Duo')
                    ->greeting('Hello!')
                    ->line('A glimpse of uppcoming campaign "'.$this->campaign->name.'" in Duo.')
                    ->action('View campaign', url('campaigns'))
                    ->line("Let's get going!");
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'campaign_id' => $this->campaign->id,
            'campaign_name' => $this->campaign->name,
            'module' => 'campaigns',
            'title' => 'Upcoming Campaign',
            'message' => 'A glimpse of uppcoming campaign "'.$this->campaign->name.'" in Duo.',
            'link' => 'campaigns'
        ];
    }
}
