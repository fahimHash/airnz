<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Agency;
use App\HasRoles;
use App\Consortium;

class User extends Authenticatable
{
    use Notifiable, HasRoles, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'email', 'password','active', 'title', 'gender', 'phoneNumber', 'startDate', 'image','confirmation_code','isConfirmed','sale_target','agent_id','state_id',
        'address1','address2','address3','suburb','postcode'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
      * A user may have many job, both present and past
      */
    public function jobs()
    {
      return $this->hasMany('App\Job','user_id');
    }

    public function job()
    {
      return $this->hasOne('App\Job','user_id')->where('active', '=', 1);
    }
    /**
     * A user is active in one consortium
     */
    public function consortium()
    {
        return $this->belongsTo('App\Consortium','consortium_id');
    }

    public function workplace()
    {
      return $this->belongsTo('App\Store','store_id');
    }
    /**
    * A user is in one territory
    */
    public function territory()
    {
      return $this->belongsTo('App\Territory','territory_id');
    }

    public function state()
    {
        return $this->belongsTo(State::class,'state_id');
    }
    /**
    * Territory managed by manager
    */
    public function manageTerritory()
    {
      return $this->hasOne('App\Territory', 'user_id');
    }

    /**
     * State managed by manager
     */
    public function manageState()
    {
        return $this->hasOne('App\State', 'state_manager');
    }

    /**
    * A agent(user) has many sales
    */
    public function sales(){
      return $this->hasMany('App\Sale','user_id');
    }

    //Assign agency group to the user.
    public function assignConsortia($consortia)
    {
      return $this->consortiums()->save(
        Consortium::whereName($consortia)->firstOrFail()
      );
    }

    public function tenure()
    {
      return date('Y') - date("Y", strtotime($this->startDate));
    }

    public function joinedIn()
    {
      return $this->created_at->format('M-Y');
    }

    public function activity()
    {
      return $this->hasMany('App\ActivityFeed','user_id');
    }

    public function isActive()
    {
      return $this->active;
    }

    public function campaigns()
    {
      return $this->belongsToMany('App\Campaign', 'campaign_user', 'user_id', 'campaign_id')->withPivot('type', 'joined','completed')->withTimestamps();
    }
    public function joinedCampaigns()
    {
      return $this->campaigns()->wherePivot('joined', '=', true)->withPivot('type', 'joined','completed');
    }
    public function completedCampaigns()
    {
      return $this->campaigns()->wherePivot('completed', '=', true)->withPivot('type', 'joined','completed');
    }
    public function campaignsInProgress()
    {
      return $this->campaigns()->wherePivot('inprogress', '=', true)->withPivot('type', 'joined','completed');
    }
    public function salesCampaigns()
    {
      return $this->campaigns()->whereStatus(true)->wherePivot('type','=','Sales')->wherePivot('inprogress', '=', true)->withPivot('type', 'joined','completed');
    }
    public function tierCampaigns()
    {
      return $this->campaigns()->whereStatus(true)->wherePivot('type','=','Tier Promotion')->wherePivot('inprogress', '=', true)->withPivot('type', 'joined','completed');
    }
    public function engagementCampaigns()
    {
      return $this->campaigns()->whereStatus(true)->wherePivot('type','=','Engagement Competitions')->wherePivot('inprogress', '=', true)->withPivot('type', 'joined','completed');
    }

    public function scopeByRole($query, $role){
        return $query->whereHas('roles', function($query) use ($role) {
            $query->where('name', $role);
        });
    }
/*
    public function newPivot(Model $parent, array $attributes, $table, $exists)
    {
        if ($parent instanceof Campaign) {
            return new CampaignUserPivot($parent, $attributes, $table, $exists);
        }

        return parent::newPivot($parent, $attributes, $table, $exists);
    }
*/
    public function campaignUser()
    {
        return $this->hasMany('App\CampaignUser','user_id');
    }

    public function lastTenNotifications()
    {
        return $this->notifications()
            ->whereNotNull('read_at')->take(10);
    }

    public function scopeByAgents($query){
        return $query->with('roles')->whereHas('roles', function ($query) {
            $query->whereIn('name', ['minor', 'mover']);
        });
    }

    public function industryStartDate()
    {
        if(!$this->startDate == null){
            return \Carbon\Carbon::createFromFormat('Y-m-d', $this->startDate)->format('d-m-Y');
        }else {
            return null;
        }
    }

    public function segments(){
        return $this->hasMany(Segment::class,'user_id');
    }

    public function saleSegments()
    {
        return $this->morphedByMany(SaleSegment::class,'segment');
    }

    public function systemSegments()
    {
        return $this->morphedByMany(SystemSegment::class,'segment');
    }

    public function trainingSegments()
    {
        return $this->morphedByMany(TrainingSegment::class,'segment');
    }

    public function surveys()
    {
        return $this->morphedByMany(Survey::class, 'participatable');
    }

    public function questions()
    {
        return $this->morphedByMany(Question::class, 'participatable');
    }

    public function answers()
    {
        return $this->morphedByMany(Answer::class, 'participatable');
    }
}
