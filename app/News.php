<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class News extends Model
{
    //use SoftDeletes;

    // LINK THIS MODEL TO OUR DATABASE TABLE
    protected $table = 'blog';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title', 'summary', 'message', 'image','slug'
    ];

    public function territories()
    {
        return $this->belongsToMany('App\Territory', 'blog_territory', 'blog_id', 'territory_id');
    }
}
