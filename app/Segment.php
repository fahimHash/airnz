<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Segment extends Model
{
    protected $table = 'segments';

    protected $fillable = [];

    public function segment()
    {
        return $this->morphTo();
    }

    public function agent()
    {
        return $this->belongsTo(User::class,'user_id');
    }
}
