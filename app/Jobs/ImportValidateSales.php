<?php

namespace App\Jobs;

use App\ActivityFeed;
use App\Campaign;
use App\Destination;
use App\Notifications\SalesValidated;
use App\Sale;
use App\User;
use App\ValidateSaleProgress;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Maatwebsite\Excel\Facades\Excel;
use function explode;
use function strpos;

class ImportValidateSales implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;
    public $timeout = 120;
    protected $progressTrack;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ValidateSaleProgress $progressTrack)
    {
        $this->progressTrack = $progressTrack;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $path = $this->progressTrack->filepath;
        $progressTrackId = $this->progressTrack->id;

        Excel::filter('chunk')->selectSheetsByIndex(0)->load($path)->chunk(10, function ($reader) use ($progressTrackId) {
            $saleCount = 0;
            $counter = 0;
            $skipped = 0;

            $userid = [];
            $reader->each(function ($row) use (&$saleCount, &$userid, &$counter, &$skipped) {
                $sales = Sale::wherePnr($row->pnr_identifier)->get();
                if (strpos($row->passenger_itinerary, '***') != false) {
                    //include break of flight due to other mode of transportation
                    $itinerary_array = explode('***', $row->passenger_itinerary);
                    $dest_array = explode('/', $itinerary_array[0]);

                    if ($dest_array[0] != "AKL") {
                        $origin = $dest_array[0];
                    } else {
                        $origin = $dest_array[1];
                    }
                    $count = count($dest_array);
                    $destination = $dest_array[$count - 1];

                } else {
                    //either return flight or one way flight
                    $itinerary_array = explode('/', $row->passenger_itinerary);
                    if (count(array_unique($itinerary_array)) < count($itinerary_array)) {
                        // Return flight; duplicate airport code
                        $itinerary_counted = array_count_values($itinerary_array);
                        foreach ($itinerary_counted as $code => $value) {
                            if ($value == 1) {
                                $destination = $code;
                                break;
                            }
                        }
                    } else {
                        // One way flight; last airport code is destination
                        $count = count($itinerary_array);
                        $destination = $itinerary_array[$count - 1];
                    }
                    if ($itinerary_array[0] != "AKL") {
                        $origin = $itinerary_array[0];
                    } else {
                        $origin = $itinerary_array[1];
                    }
                }

                if($row->ticketed_flag == 'Y'){
                    $status = 'Valid';
                }else if($row->ticketed_flag == 'N'){
                    $status = 'Pending';
                }else{
                    $status = 'Cancelled';
                }

                $destination_id = Destination::whereCode($destination)->first();
                if($destination_id){
                    $campaign = Campaign::whereBetween('startDate',  [$row->pnr_creation_date,$row->pnr_creation_date] )
                        ->orWhereBetween('endDate',[$row->pnr_creation_date,$row->pnr_creation_date])
                    ->findByTerritories($destination_id)->first();
                    $campaign_id = $campaign->id;
                }else{
                    $campaign_id = 0;
                }

                foreach ($sales as $sale) {
                    if ($sale) {
                        $sale->status = $status;
                        $sale->pnr_creation = $row->pnr_creation_date;
                        $sale->origin = $origin;
                        $sale->cty = $destination;
                        $sale->campaign_id = $campaign_id;
                        $sale->save();
                        $userid[$sale->user_id] = $userid[$sale->user_id] + 1;
                        $saleCount++;
                    } else {
                        $skipped++;
                    }
                }
                $counter++;
            });

            $progressTrack = ValidateSaleProgress::whereId($progressTrackId)->first();
            $progressTrack->rows_validated = $progressTrack->rows_validated + $saleCount;
            $progressTrack->rows_imported = $progressTrack->rows_imported + $counter;
            $progressTrack->rows_skipped = $progressTrack->rows_skipped + $skipped;
            $progressTrack->save();

            if ($progressTrack->total_rows == $progressTrack->rows_imported) {
                $progressTrack->imported = 1;
                $progressTrack->save();
            }

            foreach ($userid as $key => $value) {
                $activity = new ActivityFeed;
                $activity->module = 'Sales';
                $activity->activity_type = 'valid-sale';

                if ($value > 1) {
                    $activity->activity_message = $value . ' Sales has been successfully validated!';
                } else {
                    $activity->activity_message = 'Sale has been successfully validated!';
                }
                $activity->user_id = $key;
                $activity->save();
                User::find($key)->notify(new SalesValidated());
            }
        });
        $this->progressTrack->message = 'completed';
        $this->progressTrack->save();
    }

    public function failed(Exception $exception)
    {
        // Send user notification of failure, etc...
        print_r($exception);
        $this->progressTrack->message = $exception;
        $this->progressTrack->save();
    }
}
