<?php

namespace App\Jobs;

use App\Campaign;
use App\Notifications\UpcomingCampaign;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Log;
use Notification;
use Exception;

class EmailCampaignAgents implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $campaign;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Campaign $campaign)
    {
        $this->campaign = $campaign;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->campaign->load(['territories.agents', 'saleSegments.agents', 'systemSegments.agents', 'trainingSegments.agents']);

        $allAgents = new Collection();
        if (isset($campaign->territories)) {
            $allAgents = $allAgents->merge($this->campaign->territories->each->agents->pluck('agents')->collapse());
        }
        $allAgents = $allAgents->merge($this->campaign->saleSegments->each->agents->pluck('agents')->collapse());
        $allAgents = $allAgents->merge($this->campaign->systemSegments->each->agents->pluck('agents')->collapse());
        $allAgents = $allAgents->merge($this->campaign->trainingSegments->each->agents->pluck('agents')->collapse());
        $allAgents->unique();
        Notification::send($allAgents, new UpcomingCampaign($this->campaign));
    }
    public function failed(Exception $exception)
    {
        Log::error('EmailCampaignAgents has failed.',$exception->getMessage());
    }
}
