<?php

namespace App\Console\Commands;

use App\Campaign;
use App\Notifications\UpcomingCampaign;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Collection;
use Notification;

class SendUpcomingCampaignEmails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'email:upcomingCampaigns';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sends upcoming campaign message to users via Email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $upcomingCampaign = Campaign::where('startDate', '>', Carbon::today()->toDateString())->get();

        foreach ($upcomingCampaign as $campaign){
            $campaign->load(['territories.agents', 'saleSegments.agents', 'systemSegments.agents', 'trainingSegments.agents']);

            $allAgents = new Collection();
            if (isset($campaign->territories)) {
                $allAgents = $allAgents->merge($campaign->territories->each->agents->pluck('agents')->collapse());
            }
            $allAgents = $allAgents->merge($campaign->saleSegments->each->agents->pluck('agents')->collapse());
            $allAgents = $allAgents->merge($campaign->systemSegments->each->agents->pluck('agents')->collapse());
            $allAgents = $allAgents->merge($campaign->trainingSegments->each->agents->pluck('agents')->collapse());
            $allAgents->unique();

            Notification::send($allAgents, new UpcomingCampaign($campaign));
        }

    }
}
