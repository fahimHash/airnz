<?php

namespace App\Console\Commands;

use App\Mail\NotifyLapsedProfile;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Mail;

class LapsedProfile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lapsed-profile:agent';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email agent whose profile are lapsed and need to action within 7 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $agents = User::whereLoggedInAt(Carbon::today()->subDays(30)->toDateString())->get();

        foreach ($agents as $agent) {
            $agent->update([
                'active'       => 0,
                'lapsed_token' => str_random(10)
            ]);
            Mail::to($agent)->send(new NotifyLapsedProfile($agent));
        }
    }
}
