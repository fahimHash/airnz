<?php

namespace App\Console\Commands;

use App\ActivityFeed;
use App\Destination;
use App\Role;
use App\Sale;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class GenerateSales extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:sales';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate random sales in Duo platform';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $users = User::byRole(Role::ROLE_MINOR)->get();

        foreach ($users as $user) {

            $sale = Sale::create([
                'status'        => 'Valid',
                'issued_date'   => Carbon::now(),
                'pnr'           => 'JU6' . $user->id . 'LH',
                'stock'         => $user->id . '234759',
                'ticket_number' => '0869321450325/' . $user->id,
                'origin'        => Destination::inRandomOrder()->first()->code,
                'cty'           => Destination::inRandomOrder()->first()->code,
                'departure_date'=> Carbon::now()->addDays(5),
                'pnr_creation'  => Carbon::now(),
                'created_at'    => Carbon::now(),
                'store_id'      => $user->store_id,
                'user_id'       => $user->id,
                'territory_id'  => $user->territory_id,
                'state_id'      => $user->state_id,
                'consortium_id' => $user->consortium_id
            ]);
            //$this->line($sale->toJson());
            $activity = ActivityFeed::create([
                'module'           => 'Sales',
                'activity_type'    => 'new-sale',
                'activity_message' => 'Sale has been successfully added!',
                'user_id'          => $user->id
            ]);

        }

    }
}
