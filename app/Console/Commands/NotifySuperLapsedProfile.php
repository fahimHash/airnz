<?php

namespace App\Console\Commands;

use App\Notifications\NotifySuperUserLapsedProfile;
use App\Role;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class NotifySuperLapsedProfile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lapsed-profile:superuser';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify super user about agent who has not action lapsed email';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $agents = User::whereLoggedInAt(Carbon::today()->subDays(37)->toDateString())->get();
        $super = User::byRole(Role::ROLE_SUPER_USER)->first();

        foreach ($agents as $agent) {
            $super->notify(new NotifySuperUserLapsedProfile($agent));
        }
    }
}
