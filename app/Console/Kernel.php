<?php

namespace App\Console;

use App\Console\Commands\GenerateSales;
use App\Console\Commands\LapsedProfile;
use App\Console\Commands\NotifySuperLapsedProfile;
use App\Notifications\UpcomingCampaign;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\SendUpcomingCampaignEmails::class,
        Commands\GenerateSales::class,
        Commands\LapsedProfile::class,
        Commands\NotifySuperLapsedProfile::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('email:upcomingCampaigns')->weekly();
        $schedule->command(GenerateSales::class)->daily();
        $schedule->command(LapsedProfile::class)->daily();
        $schedule->command(NotifySuperLapsedProfile::class)->daily();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
