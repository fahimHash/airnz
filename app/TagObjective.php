<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagObjective extends Model
{
    protected $table = 'tag_objectives';
    /**
     * Get all of the campaigns that are assigned this tag.
     */
    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }
}
