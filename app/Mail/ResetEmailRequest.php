<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetEmailRequest extends Mailable
{
    use Queueable, SerializesModels;

    protected $phone;
    protected $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($phone, $email)
    {
        $this->phone = $phone;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Agent Request Change Email Account')
            ->markdown('emails.change.email')
            ->with([
                'email'   => $this->email, //new email
                'phone'   => $this->phone //new email
            ]);
    }
}
