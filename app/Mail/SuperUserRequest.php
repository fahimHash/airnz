<?php

namespace App\Mail;

use App\User;
use function config;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class SuperUserRequest extends Mailable
{
    use Queueable, SerializesModels;
    protected $user;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from(config('mail.from.address'), config('mail.from.name'))
            ->subject('Activate Account – Super User Reset')
            ->markdown('emails.account.reactivate')
            ->with([
                'name' => $this->user->name,
                'route' => route('agents.show', $this->user->id),
            ]);
    }
}
