<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sale extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    //The attributes that are mass assignable
    protected $fillable = [
        'status',
        'issued_date',
        'pnr',
        'stock',
        'origin',
        'cty',
        'ticket_number',
        'departure_date',
        'class',
        'notes',
        'created_at',
        'user_id',
        'store_id',
        'territory_id',
        'state_id',
        'consortium_id',
        'pnr_creation',
        'group_shell_id'
    ];

    /**
    * A Sale is related to a agent(user)
    */
    public function user(){
      return $this->belongsTo('App\User','user_id');
    }

    /**
    * A Sale is related to a consortium
    */
    public function consortium(){
      return $this->belongsTo('App\Consortium','consortium_id');
    }

    /**
    * A Sale is related to a territory
    */
    public function territory(){
      return $this->belongsTo('App\Territory','territory_id');
    }
    /**
    * A Sale is related to a campaign
    */
    public function campaign(){
      return $this->belongsTo('App\Campaign','campaign_id');
    }

    public function state(){
        return $this->belongsTo(State::class,'state_id');
    }

    public function store(){
        return $this->belongsTo(Store::class,'store_id');
    }

    public static function findByPnr($pnr)
    {
        return self::where('pnr', $pnr)->first();
    }
}
