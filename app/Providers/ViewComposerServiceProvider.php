<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(
            [
                'frontend.account.*',
                'frontend.activity.index',
                'frontend.campaigns.index',
                'frontend.campaigns.detail',
                'frontend.coaching.*',
                'frontend.company.*',
                //'frontend.layouts.*',
                'frontend.profile.*',
                'frontend.rewards.*',
                'frontend.sales.my-sales',
                'frontend.sales.add-sales',
                'frontend.tools.*',
                'frontend.tour.*',
                'frontend.home',
            ],
            'App\Http\ViewComposers\BdmComposer'
        );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\App\Http\ViewComposers\BdmComposer::class);
    }
}
