<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
        'name', 'type', 'sort_order', 'question_id'
    ];

    public function question()
    {
        return $this->belongsTo(Question::class,'question_id');
    }

    public function participants()
    {
        return $this->morphToMany(User::class,'participatable');
    }
}
