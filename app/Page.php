<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $dates = ['deleted_at'];

    protected $fillable = [
        'title', 'summary', 'type', 'message', 'image', 'slug', 'publish', 'pinned', 'layout'
    ];

    public function territories()
    {
        return $this->belongsToMany(Territory::class, 'page_territory', 'page_id', 'territory_id');
    }

    public function states()
    {
        return $this->belongsToMany(State::class, 'page_state', 'page_id', 'state_id');
    }

    public function scopeAgentPages($query, User $user)
    {
        return $query->whereHas('territories.agents', function ($query) use ($user) {
                $query->where('id', $user->id);
            })->orWhereHas('states.agents', function ($query) use ($user) {
                $query->where('id', $user->id);
            });
    }
    public function scopePinned($query)
    {
        return $query->where('pinned', '=', 1);
    }
    public function scopeNotPinned($query)
    {
        return $query->where('pinned', '=', 0);
    }
    public function scopePublished($query)
    {
        return $query->where('publish', '=', 1);
    }
}
