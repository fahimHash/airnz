<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class State extends Model
{
    use SoftDeletes;
    protected $table = 'states';
    protected $dates = ['deleted_at'];

    public function consortiums()
    {
        return $this->belongsToMany(Consortium::class,'consortium_state','state_id','consortium_id');
    }

    public function territory()
    {
        return $this->belongsToMany(Territory::class,'state_territory','state_id','territory_id');
    }

    public function manager(){
        return $this->belongsTo('App\User','state_manager');
    }

    public function agency()
    {
        return $this->belongsToMany(Agency::class,'agency_state','state_id','agency_id');
    }

    public function stores()
    {
        return $this->hasMany(Store::class,'store_state_id');
    }

    public function agents()
    {
        return $this->hasMany(User::class,'state_id');
    }
    public function sales(){
        return $this->hasMany('App\Sale','state_id');
    }

    public function surveys()
    {
        return $this->morphToMany(Survey::class, 'surveyables');;
    }

    public function pages()
    {
        return $this->belongsToMany(Page::class, 'page_state', 'page_id', 'state_id');
    }
}
