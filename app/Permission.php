<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Permission extends Model
{
    //The attributes that are mass assignable
    protected $fillable = ['name', 'label'];

    //A Permission can be applied to roles
    public function roles()
    {
      return $this->belongsToMany(Role::class,'permission_role','permission_id','role_id');
    }
}
