<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = [
        'title', 'image', 'sort_order', 'survey_id'
    ];

    public function answers()
    {
        return $this->hasMany(Answer::class,'question_id');
    }

    public function survey()
    {
        return $this->belongsTo(Survey::class,'survey_id');
    }

    public function participants()
    {
        return $this->morphToMany(User::class,'participatable');
    }
}
