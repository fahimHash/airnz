<?php

namespace App\Listeners;

use App\User;
use Carbon\Carbon;

class UserEventSubscriber
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function onUserLogin($event){
        $user = User::find($event->user->id);
        $user->logged_in_at = Carbon::today()->toDateString();
        $user->save();

    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'Illuminate\Auth\Events\Login',
            'App\Listeners\UserEventSubscriber@onUserLogin'
        );
    }
}
