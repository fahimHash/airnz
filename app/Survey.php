<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Survey extends Model
{
    protected $fillable = [
        'name', 'type', 'description','force_complete','image','slug'
    ];

    public function questions()
    {
        return $this->hasMany(Question::class,'survey_id');
    }

    public function territories()
    {
        return $this->morphedByMany(Territory::class, 'surveyables');
    }

    public function states()
    {
        return $this->morphedByMany(State::class, 'surveyables');
    }

    public function saleSegments()
    {
        return $this->morphedByMany(SaleSegment::class, 'surveyables');
    }

    public function systemSegments()
    {
        return $this->morphedByMany(SystemSegment::class, 'surveyables');
    }

    public function trainingSegments()
    {
        return $this->morphedByMany(TrainingSegment::class, 'surveyables');
    }

    public function participants()
    {
        return $this->morphToMany(User::class,'participatable');
    }

    public function scopeForAgent($query, User $user){
        return $query->whereHas('territories.agents', function ($query) use ($user){
            $query->where('id', $user->id);
        })->orWhereHas('states.agents', function ($query) use ($user){
            $query->where('id', $user->id);
        })->orWhereHas('saleSegments.agents', function ($query) use ($user){
            $query->where('user_id', $user->id);
        })->orWhereHas('systemSegments.agents', function ($query) use ($user){
            $query->where('user_id', $user->id);
        })->orWhereHas('trainingSegments.agents', function ($query) use ($user){
            $query->where('user_id', $user->id);
        });
    }
}
