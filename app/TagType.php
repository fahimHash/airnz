<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagType extends Model
{
    protected $table = 'tag_types';
    /**
     * Get all of the campaigns that are assigned this tag.
     */
    public function campaigns()
    {
        return $this->hasMany(Campaign::class);
    }
}
