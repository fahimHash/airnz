<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/*
 * CampaignUser Pivot Table for Campaign - User M-M Relationship
 *
 * User belongsToMany Campaign
 *
 * User hasMany CampaignUser
 * CampaignUser belongsTo User
 *
 * Campaign hasMany CampaignUser
 * CampaignUser belongsTo Campaign
*/

class CampaignUser extends Model
{
    protected $table = 'campaign_user';

    public function agents()
    {
        return $this->belongsTo('App\User','user_id');
    }

    public function campaigns()
    {
        return $this->belongsTo('App\Campaign','campaign_id');
    }
}
