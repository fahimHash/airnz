<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Destination extends Model
{
    public $table = "destinations";

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name', 'code'
    ];

    public function campaign()
    {
        return $this->belongsToMany('App\Campaign', 'campaign_destination');
    }
}
