<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Participate extends Model
{
    protected $table = 'participatable';

    public function participatable()
    {
        return $this->morphTo();
    }
}
